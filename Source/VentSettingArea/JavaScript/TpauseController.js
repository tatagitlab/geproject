/* fetch the range list and the step size for Tpause */

var tpauseRangeList;
var tpauseMin;
var tpauseMax;
var tpauseStepSize;
tpauseRangeList = physicalparam.getRangeData("TPause", "%");
console.log("list...Tpause"+tpauseRangeList)
tpauseMin = tpauseRangeList[0]
tpauseMax = tpauseRangeList[1]
tpauseStepSize = tpauseRangeList[2]

/*setting constraint checker Min and Max*/
function onTpauseSelection() {
    console.log("onTpauseSelection!!! ")
    tpauseMin = ObjCCValue.getFinalTpauseMIN()
    tpauseMax = ObjCCValue.getFinalTpauseMAX()
}

/*Increment step logic for Tpause*/
function clkHandleTPause(iTPause) {

    if((iTPause < tpauseMax)) {
        iTPause =parseInt(iTPause)+parseInt(tpauseStepSize);
        iTPause = iTPause.toString()
    } else if(iTPause.localeCompare("Off") === 0){
        iTPause = parseInt(tpauseStepSize);
    }
    return iTPause
}

/*Decrement step logic for Tpause*/
function antClkHandleTPause(iTPause) {

    if(iTPause === 0) return;

    if((iTPause > parseInt(tpauseMin)) && (iTPause <= parseInt(tpauseMax))) {
        iTPause = parseInt(iTPause)-parseInt(tpauseStepSize);
        if(iTPause === 0){
            iTPause = "Off"
        }
        iTPause = iTPause.toString()
    } else if (parseInt(iTPause) === parseInt(tpauseMin)) {
        iTPause = "Off"
    }
    return iTPause
}

//*******************************************
// Purpose: handler for state change during value increment of Tpause
// Input Parameters: object name of the quickey - Tpause
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onTpauseIncrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("TPause: clicked on widget for increment:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam2" && selectedMode === "SIMVVCV") || (objectName === "secParam1" &&selectedMode === "VCV")){
        console.log("inside increment TPause")
        var tpause = currentSecParam.strValue
        if(tpause === "Off"){
            tpause = 0;
        }

        if(parseInt(tpause) < parseInt(tpauseMax)){
            currentSecParam.strValue = clkHandleTPause(tpause)
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of Tpause
// Input Parameters: object name of the quickey - Tpause
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onTpauseDecrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("TPause: clicked on widget for decrement:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam2" && selectedMode === "SIMVVCV") || (objectName === "secParam1" &&selectedMode === "VCV")){
        console.log("inside decrement TPause")
        var tpause = currentSecParam.strValue
        if(tpause === "Off"){
            tpause = 0;
        }
        if(parseInt(tpause) > parseInt(tpauseMin)){
            console.log("tpausedecrement!!!!!")
            currentSecParam.strValue = antClkHandleTPause(tpause)
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Tpause
//***********************************************/
function onTpauseEndOfScale(objectName) {

    console.log("checking end of scale IE")
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam2" && selectedMode === "SIMVVCV") || (objectName === "secParam1" &&selectedMode === "VCV")){
        var tpause = currentVentComp.strValue
        if(tpause === "Off"){
            tpause = 0;
        }
        if(parseInt(tpause) === parseInt(tpauseMin) && parseInt(tpause) === parseInt(tpauseMax)) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(parseInt(tpause) === parseInt(tpauseMin)) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(parseInt(tpause) === parseInt(tpauseMax)) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(parseInt(tpause) !== parseInt(tpauseMin) && parseInt(tpause) !== parseInt(tpauseMax)) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Tpause
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetTpauseUpArrowBgColor(objectName) {

    console.log("Tpause bg color"+objectName)
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting up arrow bg Tpause"+currentVentComp.strValue)
    if((objectName === "secParam2" && selectedMode === "SIMVVCV") || (objectName === "secParam1" &&selectedMode === "VCV")){
        var tpause = currentVentComp.strValue
        if(tpause === "Off"){
            tpause = 0;
        }
        if(parseInt(tpause) === parseInt(tpauseMax)) {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Tpause
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/
function onSetTpauseDownArrowBgColor(objectName) {

    console.log("Tpause bg color"+objectName)
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting down arrow bg Tpause"+currentVentComp.strValue)
    if((objectName === "secParam2" && selectedMode === "SIMVVCV") || (objectName === "secParam1" &&selectedMode === "VCV")){
        console.log("Tpause...........onSetTpauseDownArrowBgColor")
        var tpause = currentVentComp.strValue
        if(tpause === "Off"){
            tpause = 0;
        }
        if(parseInt(tpause) === parseInt(tpauseMin)) {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
