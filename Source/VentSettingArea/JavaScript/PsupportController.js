/* fetch the range list and the step size for Psupport */

var psupportRangeList;
var psupportMin;
var psupportMax;
var psupportStepSize;
psupportRangeList = physicalparam.getRangeData("Psupport", "cmH2O");
console.log("list...Psupport"+psupportRangeList)
psupportMin = psupportRangeList[0]
psupportMax = psupportRangeList[1]
psupportStepSize = psupportRangeList[2]

/*Increment step Logic for Psupport*/

function clkHandlePsupport(m_psupport) {

    if(m_psupport.localeCompare("Off") !== 0 && (m_psupport < parseInt(psupportMax))) {
        m_psupport = parseInt(m_psupport)+parseInt(psupportStepSize);
        m_psupport = m_psupport.toString()
    } else if(m_psupport.localeCompare("Off") === 0){
        m_psupport = parseInt(psupportMin);
    }
    return m_psupport
}

/*Decrement step Logic for Psupport*/

function antClkHandlePsupport(m_psupport) {

    if(m_psupport.localeCompare("Off") === 0) return;

    if((m_psupport <= parseInt(psupportMax)) && (m_psupport > parseInt(psupportMin))) {
        m_psupport -= parseInt(psupportStepSize);
        m_psupport = m_psupport.toString()
    } else if (m_psupport === psupportMin) {
        m_psupport = "Off"
    }
    return m_psupport
}

//*******************************************
// Purpose: handler for state change during value increment of Psupport
// Input Parameters: object name of the quickey - Psupport
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onPsupportIncrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("Psupport: clicked on widget for increment:"+currentVentComp+strName+selectedMode)
    if((objectName === "quickKey3" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey1" && selectedMode === "PSVPro")){
        console.log("inside increment Psupport")
        if(currentVentComp.strValue < parseInt(psupportMax) || currentVentComp.strValue === "Off"){
            currentVentComp.strValue = clkHandlePsupport(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
    var ventmodeObj =  getCompRef(ventParentId, "VentModeBtn")
}

//*******************************************
// Purpose: handler for state change during value decrement of Psupport
// Input Parameters: object name of the quickey - Psupport
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onPsupportDecrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("Psupport: clicked on widget for decrement:"+currentVentComp+strName+selectedMode)
    if((objectName === "quickKey3" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey1" && selectedMode === "PSVPro")){
        console.log("inside decrement Psupport")
        if(currentVentComp.strValue >= parseInt(psupportMin)){
            currentVentComp.strValue = antClkHandlePsupport(currentVentComp.strValue)
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
	    currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
        var ventmodeObj =  getCompRef(ventParentId, "VentModeBtn")
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Psupport
//***********************************************/

function onPsupportEndOfScale(objectName) {

    console.log("checking end of scale Psupport")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if((objectName === "quickKey3" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey1" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === "Off") {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(currentVentComp.strValue === psupportMax) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        } else if(currentVentComp.strValue !== "Off" && currentVentComp.strValue !== psupportMax) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Psupport
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPsupportUpArrowBgColor(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting up arrow bg Psupport"+currentVentComp.strValue)
    if((objectName === "quickKey3" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey1" && selectedMode === "PSVPro")){
        console.log("Psupport...........")
        if(currentVentComp.strValue === psupportMax) {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Psupport
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetPsupportDownArrowBgColor(objectName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting down arrow bg Psupport"+currentVentComp.strValue)
    if((objectName === "quickKey3" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV")) || (objectName === "quickKey1" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === "Off") {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler gets called when the mode changes to connect Psupport controller to the corresponding quick key widget based on mode
// Input Parameters: current mode
// Description: takes the current mode as input and based on the input connects the Psupport controller to the corresponding widget
//***********************************************/

function onModeChangedPsupport(currentMode) {

    var objref;
    console.log("current mode in Psupport ctrl",currentMode)
    if(currentMode === "SIMVVCV"  || currentMode === "SIMVPCV") {
        objref=getCompRef(ventParentId,"quickKey3");
        objref.setLabelofWidget("Psupport_qKey");
        objref.setUnitofWidget("cmH2O_unit")
        objref.setQKValue(settingReader.read("Psupport_qKey"))
        objref.setUniqueID("qkPsupport_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    }
    if(currentMode === "PSVPro") {
        objref=getCompRef(ventParentId,"quickKey1");
        objref.setLabelofWidget("Psupport_qKey");
        objref.setUnitofWidget("cmH2O_unit")
        objref.setQKValue(settingReader.read("Psupport_qKey"))
        objref.setUniqueID("qkPsupport_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        objref.bIsValueChanged = false
    }    
}
