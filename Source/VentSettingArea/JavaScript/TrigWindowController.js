/* fetch the range list and the step size for Trig Window */

var trigWindowRangeList;
var trigWindowMin;
var trigWindowMax;
var trigWindowStepSize;
trigWindowRangeList = physicalparam.getRangeData("TrigWindow", "%");
console.log("list...TrigWindow"+trigWindowRangeList)
trigWindowMin = trigWindowRangeList[0]
trigWindowMax = trigWindowRangeList[1]
trigWindowStepSize = trigWindowRangeList[2]

/*Increment step logic for Trig window*/

function clkHandleTrigWindow(iTrigWindow) {

    if((iTrigWindow >= parseInt(trigWindowMin)) && (iTrigWindow < parseInt(trigWindowMax))) {
        iTrigWindow += parseInt(trigWindowStepSize);
    }
    return iTrigWindow;
}


/*Decrement step logic for Trig window*/
function antClkHandleTrigWindow(iTrigWindow) {

    if((iTrigWindow > parseInt(trigWindowMin)) && (iTrigWindow <= parseInt(trigWindowMax))) {
        iTrigWindow -= parseInt(trigWindowStepSize);
    }
    return iTrigWindow;
}

//*******************************************
// Purpose: handler for state change during value increment of Trig window
// Input Parameters: object name of the quickey - Trig window
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onTrigWinIncrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Trig Window: clicked on widget for increment:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam4" && selectedMode === "SIMVVCV") || (objectName === "secParam3" && selectedMode === "SIMVPCV" ) || (objectName === "secParam1" && selectedMode === "PSVPro")){
        console.log("inside increment TrigWindow")
        if(currentSecParam.strValue < parseInt(trigWindowMax)){
            currentSecParam.strValue= parseInt(clkHandleTrigWindow(parseInt(currentSecParam.strValue)))
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of Trig window
// Input Parameters: object name of the quickey - Trig window
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onTrigWinDecrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Trig Window: clicked on widget for decrement:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam4" && selectedMode === "SIMVVCV") || (objectName === "secParam3" && selectedMode === "SIMVPCV" ) || (objectName === "secParam1" && selectedMode === "PSVPro")){
        console.log("inside decrement TrigWindow")
        if(currentSecParam.strValue > parseInt(trigWindowMin)){
            currentSecParam.strValue= parseInt(antClkHandleTrigWindow(parseInt(currentSecParam.strValue)))
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Trig window
//***********************************************/
function onTrigWinEndOfScale(objectName) {

    console.log("checking end of scale Trig window")
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam4" && selectedMode === "SIMVVCV") || (objectName === "secParam3" && selectedMode === "SIMVPCV" ) || (objectName === "secParam1" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === trigWindowMin) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownDisabled

        } else if(currentVentComp.strValue === trigWindowMax) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(currentVentComp.strValue !== trigWindowMin && currentVentComp.strValue !== trigWindowMax) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Trig window
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetTrigWinUpArrowBgColor(objectName) {

    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting up arrow bg TrigWin"+currentVentComp.strValue)
    if((objectName === "secParam4" && selectedMode === "SIMVVCV") || (objectName === "secParam3" && selectedMode === "SIMVPCV" ) || (objectName === "secParam1" && selectedMode === "PSVPro")){
        console.log("TrigWin...........")
        if(currentVentComp.strValue === trigWindowMax) {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Trig window
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetTrigWinDownArrowBgColor(objectName) {

    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting down arrow bg TrigWin"+currentVentComp.strValue)
    if((objectName === "secParam4" && selectedMode === "SIMVVCV") || (objectName === "secParam3" && selectedMode === "SIMVPCV" ) || (objectName === "secParam1" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === trigWindowMin) {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
