/* fetch the range list and the step size for I:E */

var endOfBreathRangeList;
var eobMin;
var eobMax;
var eobStepSize;
endOfBreathRangeList = physicalparam.getRangeData("EndOfBreath", "%");
eobMin = endOfBreathRangeList[0]
eobMax = endOfBreathRangeList[1]
eobStepSize = endOfBreathRangeList[2]

function clkHandleEndOfBreath(iEndOfBreath) {

    if((iEndOfBreath >= eobMin) && (iEndOfBreath < eobMax)) {
        iEndOfBreath += parseInt(eobStepSize);
    }
    return iEndOfBreath;
}

/*Decrement step logic for End of breath*/
function antClkHandleEndOfBreath(iEndOfBreath) {

    if((iEndOfBreath > eobMin) && (iEndOfBreath <= eobMax)) {
        iEndOfBreath -= parseInt(eobStepSize);
    }
    return iEndOfBreath;
}

//*******************************************
// Purpose: handler for state change during value increment of End of Breadth
// Input Parameters: object name of the quickey - End of Breadth
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/
function onEndOfBthIncrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("End of Breath: clicked on widget for increment:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam5" && selectedMode === "SIMVVCV") || (objectName === "secParam4" && selectedMode === "SIMVPCV" )||(objectName === "secParam2" && selectedMode === "PSVPro")){
        console.log("inside increment End of Breath")
        if(currentSecParam.strValue < parseInt(eobMax)){
            currentSecParam.strValue= parseInt(clkHandleEndOfBreath(parseInt(currentSecParam.strValue)))
           currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of End of Breadth
// Input Parameters: object name of the quickey - End of Breadth
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onEndOfBthDecrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("End of Breath: clicked on widget for decrement:"+objectName+currentSecParam+strName+selectedMode)
     if((objectName === "secParam5" && selectedMode === "SIMVVCV") || (objectName === "secParam4" && selectedMode === "SIMVPCV" )||(objectName === "secParam2" && selectedMode === "PSVPro")){
        console.log("inside decrement End of Breath")
        if(currentSecParam.strValue > parseInt(eobMin)){
            currentSecParam.strValue= parseInt(antClkHandleEndOfBreath(parseInt(currentSecParam.strValue)))
             currentSecParam.state = "Selected"
        } else {
             currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - End of Breadth
//***********************************************/

function onEndOfBthEndOfScale(objectName) {
    console.log("checking end of scale End of Breadth")
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam5" && selectedMode === "SIMVVCV") || (objectName === "secParam4" && selectedMode === "SIMVPCV" )||(objectName === "secParam2" && selectedMode === "PSVPro")){
        if(currentVentComp.strValue === eobMin) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(currentVentComp.strValue === eobMax) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(currentVentComp.strValue !== eobMin && currentVentComp.strValue !== eobMax) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - End of Breadth
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetEndOfBthUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting up arrow bg EndOfBth"+currentVentComp.strValue)
    if((objectName === "secParam5" && selectedMode === "SIMVVCV") || (objectName === "secParam4" && selectedMode === "SIMVPCV" )||(objectName === "secParam2" && selectedMode === "PSVPro")){
        console.log("EndOfBth...........")
        if(currentVentComp.strValue === eobMax) {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - End of Breadth
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/
function onSetEndOfBthDownArrowBgColor(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting down arrow bg EndOfBth"+currentVentComp.strValue)
    if((objectName === "secParam5" && selectedMode === "SIMVVCV") || (objectName === "secParam4" && selectedMode === "SIMVPCV" )||(objectName === "secParam2" && selectedMode === "PSVPro")){
       if(currentVentComp.strValue === eobMin) {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
