var backupTimeRangeList;
var bkTimeMin;
var bkTimeMax;
var bkTimeStepSize;
backupTimeRangeList = physicalparam.getRangeData("BackupTime", "s");
bkTimeMin = backupTimeRangeList[0]
bkTimeMax = backupTimeRangeList[1]
bkTimeStepSize = backupTimeRangeList[2]

/*Increment step logic for backupTime*/
function clkHandleBackupTime(iBackupTime) {

    if((iBackupTime>=parseInt(bkTimeMin))  && (iBackupTime < bkTimeMax)) {
        iBackupTime =parseInt(iBackupTime)+parseInt(bkTimeStepSize);
        iBackupTime = iBackupTime.toString()
    }
    return iBackupTime ;
}

/*Decrement step logic for Tpause*/
function antClkHandleBackupTime(iBackupTime) {

    if((iBackupTime > parseInt(bkTimeMin)) && (iBackupTime <= parseInt(bkTimeMax))) {
        iBackupTime = parseInt(iBackupTime)-parseInt(bkTimeStepSize);
        iBackupTime = iBackupTime.toString()
    }
    return iBackupTime ;
}

//*******************************************
// Purpose: handler for state change during value increment of Backup Time
// Input Parameters: object name of the quickey - Backup Time
//***********************************************/
function onBackupTimeIncrementValue(objectName, strName) {
    var currentSecParam = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam4" && selectedMode === "PSVPro")){
        if(currentSecParam.strValue < parseInt(bkTimeMax) ){
            currentSecParam.strValue = clkHandleBackupTime(currentSecParam.strValue)
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of Backup Time
// Input Parameters: object name of the quickey - Backup Time
//***********************************************/
function onBackupTimeDecrementValue(objectName, strName) {
    var currentSecParam = getCompRef(secParamParentId, objectName)
    if(((objectName === "secParam4" &&  selectedMode === "PSVPro"))){
        if(currentSecParam.strValue > parseInt(bkTimeMin)){
            currentSecParam.strValue = antClkHandleBackupTime(currentSecParam.strValue)
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Backup Time
//***********************************************/
function onBackupTimeEndOfScale(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if(((objectName === "secParam4" &&  selectedMode === "PSVPro"))){
        if(currentVentComp.strValue === bkTimeMin) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(currentVentComp.strValue === bkTimeMax) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(currentVentComp.strValue !== bkTimeMin && currentVentComp.strValue !== bkTimeMax) {
            currentVentComp.strSecParamUpArrowSource=currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Backup Time
//***********************************************/
function onSetBackupTimeUpArrowBgColor(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if(((objectName === "secParam4" &&  selectedMode === "PSVPro"))){
        if(currentVentComp.strValue === bkTimeMax) {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Backup Time
//***********************************************/
function onSetBackupTimeDownArrowBgColor(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if(((objectName === "secParam4" &&  selectedMode === "PSVPro"))){
        if(currentVentComp.strValue === bkTimeMin) {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
