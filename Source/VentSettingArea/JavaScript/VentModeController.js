
var ventMenuBtnComponent;
var modeBtnVCV;
var modeBtnPCV;
var modeBtnSIMVVCV;
var modeBtnSIMVPCV;
var modeBtnPSVPro;
var btnStartCardic;

/*vent mode menu button*/
var vcvMode = component.getComponentResourceString("Component","ModeMenuButton","Mode","VCV")
var pcvMode =  component.getComponentResourceString("Component","ModeMenuButton","Mode","PCV")
var simvvcvMode =  component.getComponentResourceString("Component","ModeMenuButton","Mode","SIMVVCV")
var simvpcvMode =  component.getComponentResourceString("Component","ModeMenuButton","Mode","SIMVPCV")
var psvproMode = component.getComponentResourceString("Component","ModeMenuButton","Mode","PSVPro")
var bypassMode = component.getComponentResourceString("Component","ModeMenuButton","Mode","Bypass")
var vcvExp = component.getComponentResourceString("Component","ModeMenuButton","ModeExpansion","VCV")
var pcvExp = component.getComponentResourceString("Component","ModeMenuButton","ModeExpansion","PCV")
var simvvcvExp = component.getComponentResourceString("Component","ModeMenuButton","ModeExpansion","SIMVVCV")
var simvpcvExp = component.getComponentResourceString("Component","ModeMenuButton","ModeExpansion","SIMVPCV")
var psvproExp = component.getComponentResourceString("Component","ModeMenuButton","ModeExpansion","PSVPro")
var bypassExp = component.getComponentResourceString("Component","ModeMenuButton","ModeExpansion","Bypass")

var modeMenuBtnWidth = parseInt(component.getComponentDimension("ModeMenuButton", "width"))
var modeMenuBtnHeight = parseInt(component.getComponentDimension("ModeMenuButton", "height"))
var modeMenuBtnColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "Color")
var modeMenuBtnLabelHeight = parseInt(component.getComponentXmlDataByTag("Component","ModeMenuButton", "LabelValueHeight"))
var modeMenuBtnLabelBorderColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "BorderColor")
var modeMenuBtnLabelBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","ModeMenuButton", "BorderWidth"))
var modeMenuBtnLabelRadius = parseInt(component.getComponentXmlDataByTag("Component","ModeMenuButton", "BorderRadius"))
var modeMenuBtnLabelColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "LabelValueColor")
var modeMenuBtnLabelFontFamily = geFont.geFontStyle() //component.getComponentXmlDataByTag("ModeMenuButton", "LabelFontFamily")
var modeMenuBtnLabelPixelSize = parseInt(component.getComponentXmlDataByTag("Component","ModeMenuButton", "LabelPixelSize"))
var modeExpansionLabelHeight = parseInt(component.getComponentXmlDataByTag("Component","ModeMenuButton", "ModeExpansionLabelHeight"))
var modeExpansionLabelColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "ModeExpansionLabelColor")
var modeExpansionFontFamily = geFont.geFontStyle() //component.getComponentXmlDataByTag("ModeMenuButton", "ModeExpansionFontFamily")
var modeExpansionPixelSize = parseInt(component.getComponentXmlDataByTag("Component","ModeMenuButton", "ModeExpansionPixelSize"))
var ventMenuBtnActiveStateColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "Color");
var ventMenuBtnDisabledStateColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "DisabledStateColor");
var ventMenuBtnEndOfScaleStateColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "EndOfScaleStateColor");
var ventMenuBtnEndOfScaleStateBorderColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "EndOfScaleStateBorderColor");
var ventMenuBtnScrollStateBorderColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "ScrollStateBorderColor");
var ventMenuBtnScrollStateBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","ModeMenuButton", "ScrollStateBorderWidth"))
var ventMenuBtnSelectedStateColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "SelectedStateColor");
var ventMenuBtnSelectedStateBorderColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "SelectedStateBorderColor");
var ventMenuBtnTouchedStateColor = component.getComponentXmlDataByTag("Component","ModeMenuButton", "TouchedStateColor")

function createVentMenuComponents() {

    console.log("creating vent mode menu components....")
    ventMenuBtnComponent = Qt.createComponent("qrc:/Source/VentSettingArea/Qml/VentMode.qml")
    if (ventMenuBtnComponent.status === Component.Ready){
        finishVentMenuComponentCreation();
    } else{
        ventMenuBtnComponent.statusChanged.connect(finishVentMenuComponentCreation);
    }
}

function finishVentMenuComponentCreation() {

    console.log("finishVentMenuComponentCreation...")
    //VCV Button
    modeBtnVCV = ventMenuBtnComponent.createObject(ventMenu.menuModesID,{"iCompID":0,"objectName":"BtnMode_VCV","strModeName":vcvMode,"strExpModeName":vcvExp,"strModeCompState":"Active",
                                                       "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                       "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                       "strModeMenuBtnColor":modeMenuBtnColor,
                                                       "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                       "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                       "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                       "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                       "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                       "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                       "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                       "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                       "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                       "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                       "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                       "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                       "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                       "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                       "iventMenuBtnScrollStateBorderWidth":ventMenuBtnScrollStateBorderWidth,
                                                       "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                       "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                       "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                   });

    //PCV Button
    modeBtnPCV = ventMenuBtnComponent.createObject(ventMenu.menuModesID,{"iCompID":1,"objectName":"BtnMode_PCV","strModeName":pcvMode,"strExpModeName":pcvExp,"strModeCompState":"Active",
                                                       "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                       "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                       "strModeMenuBtnColor":modeMenuBtnColor,
                                                       "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                       "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                       "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                       "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                       "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                       "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                       "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                       "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                       "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                       "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                       "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                       "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                       "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                       "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                       "iventMenuBtnScrollStateBorderWidth":ventMenuBtnScrollStateBorderWidth,
                                                       "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                       "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                       "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                   });

    //SIMVVCV Button
    modeBtnSIMVVCV = ventMenuBtnComponent.createObject(ventMenu.menuModesID,{"iCompID":2,"objectName":"BtnMode_SIMVVCV","strModeName":simvvcvMode,"strExpModeName":simvvcvExp,"strModeCompState":"Active",
                                                           "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                           "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                           "strModeMenuBtnColor":modeMenuBtnColor,
                                                           "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                           "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                           "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                           "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                           "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                           "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                           "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                           "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                           "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                           "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                           "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                           "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                           "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                           "iventMenuBtnScrollStateBorderWidth":ventMenuBtnScrollStateBorderWidth,
                                                           "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                           "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                           "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                           "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                       });

    //SIMVPCV Button
    modeBtnSIMVPCV = ventMenuBtnComponent.createObject(ventMenu.menuModesID,{"iCompID":3,"objectName":"BtnMode_SIMVPCV","strModeName":simvpcvMode,"strExpModeName":simvpcvExp,"strModeCompState":"Active",
                                                           "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                           "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                           "strModeMenuBtnColor":modeMenuBtnColor,
                                                           "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                           "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                           "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                           "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                           "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                           "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                           "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                           "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                           "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                           "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                           "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                           "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                           "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                           "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                           "iventMenuBtnScrollStateBorderWidth":ventMenuBtnScrollStateBorderWidth,
                                                           "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                           "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                           "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                       });

    //PSVPro Button
    modeBtnPSVPro = ventMenuBtnComponent.createObject(ventMenu.menuModesID,{"iCompID":4,"objectName":"BtnMode_PSVPro","strModeName":psvproMode,"strExpModeName":psvproExp,"strModeCompState":"Active",
                                                          "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                          "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                          "strModeMenuBtnColor":modeMenuBtnColor,
                                                          "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                          "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                          "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                          "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                          "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                          "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                          "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                          "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                          "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                          "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                          "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                          "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                          "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                          "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                          "iventMenuBtnScrollStateBorderWidth":ventMenuBtnScrollStateBorderWidth,
                                                          "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                          "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                          "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                      });

    //Start Cardic Bypass Button
    btnStartCardic = ventMenuBtnComponent.createObject(ventMenu.menuBypassID,{"iCompID":5,"objectName":"BtnMode_Bypass","strModeName":bypassMode,"strExpModeName":bypassExp,"strModeCompState":"Disabled",
                                                           "iModeMenuBtnWidth":modeMenuBtnWidth,
                                                           "iModeMenuBtnHeight":modeMenuBtnHeight,
                                                           "strModeMenuBtnColor":modeMenuBtnColor,
                                                           "iModeMenuBtnLabelHeight":modeMenuBtnLabelHeight,
                                                           "strModeMenuBtnLabelBorderColor":modeMenuBtnLabelBorderColor,
                                                           "iModeMenuBtnLabelBorderWidth":modeMenuBtnLabelBorderWidth,
                                                           "iModeMenuBtnLabelRadius":modeMenuBtnLabelRadius,
                                                           "strModeMenuBtnLabelColor":modeMenuBtnLabelColor,
                                                           "strModeMenuBtnLabelFontFamily":modeMenuBtnLabelFontFamily,
                                                           "iModeMenuBtnLabelPixelSize":modeMenuBtnLabelPixelSize,
                                                           "iModeExpansionLabelHeight":modeExpansionLabelHeight,
                                                           "strModeExpansionLabelColor":modeExpansionLabelColor,
                                                           "strModeExpansionFontFamily":modeExpansionFontFamily,
                                                           "iModeExpansionPixelSize":modeExpansionPixelSize,
                                                           "strVentMenuBtnActiveStateColor":ventMenuBtnActiveStateColor,
                                                           "strVentMenuBtnDisabledStateColor":ventMenuBtnDisabledStateColor,
                                                           "strVentMenuBtnScrollStateBorderColor":ventMenuBtnScrollStateBorderColor,
                                                           "iventMenuBtnScrollStateBorderWidth":ventMenuBtnScrollStateBorderWidth,
                                                           "strVentMenuBtnSelectedStateColor":ventMenuBtnSelectedStateColor,
                                                           "strVentMenuBtnSelectedStateBorderColor":ventMenuBtnSelectedStateBorderColor,
                                                           "strVentMenuBtnTouchedStateColor":ventMenuBtnTouchedStateColor
                                                       });
    modeBtnVCV.setUniqueID(vcvMode)
    modeBtnPCV.setUniqueID(pcvMode)
    modeBtnSIMVVCV.setUniqueID(simvvcvMode)
    modeBtnSIMVPCV.setUniqueID(simvpcvMode)
    modeBtnPSVPro.setUniqueID(psvproMode)
    btnStartCardic.setUniqueID(bypassMode)
}


//*******************************************
// Purpose: Handles key navigation (right) for vent mode menu components(close btn, maxmin btn)
// Description: Sets the focus to maxmin button when the mode button reach the last active button on
// clockwise rotation
//***********************************************/
function onNavigateRightInMenu() {

    var menuObj = getCompRef(mainitem, "VentMenuObj")
    var maxMin = getCompRef(menuObj,"MaximizeMinimize")
    maxMin.focus = true
    maxMin.forceActiveFocus()
}

//*******************************************
// Purpose: Handles key navigation (left) for vent mode menu components(close btn, maxmin btn)
// Description: Sets the focus to close button when the mode button reach the first active button on
// anticlockwise rotation
//***********************************************/
function onNavigateLeftInMenu() {

    var menuObj = getCompRef(mainitem, "VentMenuObj")
    var closeBtnRef = getCompRef(menuObj,"ModeMenuClose")
    closeBtnRef.focus = true
    closeBtnRef.forceActiveFocus()
}

//*******************************************
// Purpose: Handles key navigation (right) for vent mode menu button components(all mode buttons inside the menu)
// Input Parameters: objectName of the current mode button component
// Description:Gets the current component objectName and set the focus to next enabled component on clockwise rotation(comwheel)
//***********************************************/
function onNavigateRightMode(currentObjectName) {

    console.log("onNavigateRight...")
    var menuObj = getCompRef(mainitem, "VentMenuObj")
    var modeLength = menuObj.menuModesID.children.length ;
    var currentIndex = 0;
    var currentModeComp = getCompRef(menuObj.menuModesID,currentObjectName)
    if (currentObjectName !== "ModeMenuClose" && currentObjectName !== "MaximizeMinimize" && currentObjectName !== "VentMenuObj") {
        for(var compIndex = 0; compIndex < modeLength; compIndex++){
            if(menuObj.menuModesID.children[compIndex].objectName === currentObjectName){
                break;
            }else{
                currentIndex++;
            }
        }
        if (currentIndex === modeLength -1 ){
            onNavigateRightInMenu()
        } else if (currentIndex === modeLength -2 && menuObj.menuModesID.children[currentIndex +1].state === "Selected") {
            onNavigateRightInMenu()
        } else{
            if(menuObj.menuModesID.children[currentIndex+1].state !== "Selected"){
                menuObj.menuModesID.children[currentIndex + 1].focus = true
                menuObj.menuModesID.children[currentIndex + 1].forceActiveFocus()
            } else{
                menuObj.menuModesID.children[(currentIndex + 2)%modeLength].focus = true
            }
        }
    } else if (currentObjectName === "MaximizeMinimize"){
        var closeBtnRef = getCompRef(menuObj,"ModeMenuClose")
        closeBtnRef.focus = true
    } else if (currentObjectName === "ModeMenuClose"){
        var VCVBtnRef = getCompRef(menuObj.menuModesID,"BtnMode_VCV")
        var PCVBtnRef = getCompRef(menuObj.menuModesID,"BtnMode_PCV")
        if(VCVBtnRef.state === "Selected"){
            PCVBtnRef.focus = true
            PCVBtnRef.forceActiveFocus()
        } else{
            VCVBtnRef.focus = true
        }
    } else if (currentObjectName === "VentMenuObj"){
        if(menuObj.menuModesID.children[0].state === "Selected"){
            menuObj.menuModesID.children[1].focus = true
        } else {
            menuObj.menuModesID.children[0].focus = true
        }
    }
    menuObj.restartTimer()
}

//*******************************************
// Purpose: Handles key navigation (left) for vent mode menu button components(all mode buttons inside the menu)
// Input Parameters: objectName of the current mode button component
// Description:Gets the current component objectName and set the focus to previous enabled component on anticlockwise rotation(comwheel)
//***********************************************/
function onNavigateLeftMode(currentObjectName) {

    console.log("onNavigateLeft....")
    var menuObj = getCompRef(mainitem, "VentMenuObj")
    var modeLength = menuObj.menuModesID.children.length;//SIMVPCV and PSVPro modes are disabled currently
    var currentIndex = 0;
    var currentModeComp = getCompRef(menuObj.menuModesID,currentObjectName)
    if (currentObjectName !== "ModeMenuClose" && currentObjectName !== "MaximizeMinimize" && currentObjectName !== "VentMenuObj") {
        for(var compIndex = 0; compIndex < modeLength; compIndex++){
            if(menuObj.menuModesID.children[compIndex].objectName === currentObjectName){
                break;
            }else{
                currentIndex++;
            }
        }
        if(currentIndex === 1){
            if(menuObj.menuModesID.children[(currentIndex - 1)].state === "Selected"){
                onNavigateLeftInMenu()
            }else if(menuObj.menuModesID.children[(currentIndex - 1)].state !== "Selected") {
                menuObj.menuModesID.children[(currentIndex - 1)].focus = true
            }
        } else if(currentIndex === 0) {
            onNavigateLeftInMenu()
        } else{
            if(menuObj.menuModesID.children[(currentIndex - 1)].state !== "Selected"){
                menuObj.menuModesID.children[currentIndex - 1].focus = true
            } else{
                menuObj.menuModesID.children[currentIndex - 2].focus = true
            }
        }
    }  else if (currentObjectName === "MaximizeMinimize"){
        for(var comp =0; comp<modeLength;comp++){
            if(menuObj.menuModesID.children[modeLength-1].state === "Selected"){
                menuObj.menuModesID.children[modeLength - 2].focus = true
            } else {
                menuObj.menuModesID.children[modeLength-1].focus = true
            }
        }
    } else if (currentObjectName === "ModeMenuClose"){
        var maxMinRef = getCompRef(menuObj,"MaximizeMinimize")
        maxMinRef.focus = true
    } else if (currentObjectName === "VentMenuObj"){
        if(menuObj.menuModesID.children[0].state === "Selected"){
            menuObj.menuModesID.children[1].focus = true
        } else {
            menuObj.menuModesID.children[0].focus = true
        }
    }
    menuObj.restartTimer()
}

//*******************************************
// Purpose: Handles selection of mode in vent mode menu
// Input Parameters: Selected mode objectName and mode name
// Description: Once a particular mode is selected the menu, sends the signal to the quickkey controller
// to handle mode change, a signal to handle vent group state change and writes the selected mode to setting file
//***********************************************/
function onSelectMode(curObjName,selectedMode) {

    console.log("onselect mode...." + selectedMode)
    var ventGrpObj = getGroupRef("VentGroup")
    var menuObj = getCompRef(mainitem, "VentMenuObj")
    console.log(menuObj.state,menuObj.width)
    menuObj.strSelectedMode = selectedMode
    var currentComp = getCompRef(menuObj.menuModesID, curObjName)
    currentComp.focus = true
    currentComp.forceActiveFocus()
    modeBtnChangeState(currentComp,"Selected")
    menuObj.visible = false  //only setting the visibility of the menu to false
    var modeBtnRef = getCompRef(ventGrpObj.ventParentId,"ventModeBtn")
    modeBtnRef.focus = true
    var getMode = selectedMode.split("_").pop()
    menuObj.modeChange(getMode)
    menuObj.differentModeSelected(selectedMode)
    settingReader.writeDataInFile("MODE",selectedMode)
    menuObj.stopTimer()
}

//*******************************************
// Purpose: Handles state change of the vent mode button component
// Input Parameters: current component objectname and state to be set
//***********************************************/
function modeBtnChangeState(currentComp,stateName) {

    var menuObj = getCompRef(mainitem, "VentMenuObj")
    currentComp.state = stateName
    for(var i=0;i<menuObj.menuModesID.children.length;i++){
        var siblingObj = getCompRef(menuObj.menuModesID, menuObj.menuModesID.children[i].objectName)
        if(siblingObj.state !== "Disabled"){
            if(siblingObj.state !== "Active" && i !== currentComp.iCompID){
                siblingObj.state = "Active"
            }
        }
    }
}

//*******************************************
// Purpose: Destruction of vent mode menu
// Input Parameters: boolean which gives where alarm menu is opened
// Description: Destroys the vent mode menu when close button is clicked/ menu timeout /
// other menu opened / other area touch
//***********************************************/
function destroyVentMenu(isAPressed) {

    var ventGrpObj = getGroupRef("VentGroup")
    ventGrpObj.bIsModeMenuCreated = false
    var menuObj = getCompRef(mainitem, "VentMenuObj")
    menuObj.stopTimer()
    menuObj.destroy()
    if(isAPressed){
        console.log("Focus on Alarm Setup")}
    else{
        var modeBtnRef = getCompRef(ventGrpObj.ventParentId,"ventModeBtn")
        //modeBtnRef.focus = true
        modeBtnRef.state = modeBtnRef.strVentPreviousState
        if(modeBtnRef.strVentPreviousState === "Active" || modeBtnRef.strVentPreviousState === "Preset" ){
            ventGrpObj.iPreviousIndex = modeBtnRef.iCompID
            ventGrpObj.forceActiveFocus()
            ventGrpObj.focus = true
        } else {
           modeBtnRef.focus = true
        }
    }
}

//*******************************************
// Purpose: Mode button touch event
// Input Parameters: current selected mode and objectName
// Description: Set the state of the button to "Touched" based on selection
//***********************************************/
function onModeCompTouched(selectedMode, objectName) {

    console.log("onModeCompTouched   " ,objectName)
    var currentModeComp = getCompRef(menuModesID, objectName)
    console.log("state is :" + currentModeComp.state)
    var menuObj = getCompRef(mainitem, "VentMenuObj")
    menuObj.stopTimer()
    console.log("onModeCompTouched  stopTimer ")
    if (currentModeComp.state !== "Selected" && currentModeComp.state !== "Disabled"){
        currentModeComp.focus =true
        currentModeComp.forceActiveFocus()
        modeBtnChangeState(currentModeComp,"Touched")
    }
}

//*******************************************
// Purpose: Mode button selection (triggering mode change)
// Input Parameters: current selected mode and objectName
// Description: Once the component is selected , triggers the mode change signal (to QK controllers) and
// state change signal(to vent group contoller) and write the selected mode to settings file
//***********************************************/
function onModeCompSelected(selectedMode, objectName) {

    var ventGrpObj = getGroupRef("VentGroup")
    var menuObj = getCompRef(mainitem, "VentMenuObj")
    menuObj.restartTimer();
    console.log("onModeCompSelected....")
    var currentModeComp = getCompRef(menuModesID, objectName)
    if (currentModeComp.state === "Touched") {
        currentModeComp.focus =true
        currentModeComp.forceActiveFocus()
        modeBtnChangeState(currentModeComp,"Selected")
        menuObj.strSelectedMode = selectedMode
        menuObj.visible = false   //only setting the visibility of the menu to false
        var modeBtnRef = getCompRef(ventGrpObj.ventParentId,"ventModeBtn")
        modeBtnRef.focus = true
        // ventModeId.focus = true
        var getMode = selectedMode.split("_").pop()
        modeChange(getMode)
        differentModeSelected(selectedMode)
        settingReader.writeDataInFile("MODE",selectedMode)
        menuObj.stopTimer()
    }
}

//*******************************************
// Purpose: Handles reverting the selected mode inside the mode menu
// Input Parameters: previous selected mode
// Description: Once mode change is cancelled, the same will be updated in the vent mode menu
// by the vent group controller
//***********************************************/
function onRevertSelectionToPreviousMode(previousSelectedMode) {

    console.log("onRevertSelectionToPreviousMode....")
    var getMode = previousSelectedMode.split("_").pop()
    var menuObjRef = getCompRef(mainitem,"VentMenuObj")
    menuObjRef.modeChange(getMode)
    var modeLength = menuObjRef.menuModesID.children.length ;
    for(var comp=0;comp<modeLength;comp++){
        if(menuObjRef.menuModesID.children[comp].strModeName === previousSelectedMode){
            menuObjRef.menuModesID.children[comp].state = "Selected"
        } else {
            menuObjRef. menuModesID.children[comp].state = "Active"
        }
    }
}

//*******************************************
// Purpose: Handles comwheel press inside the menu(maxmin button/close button)
// Input Parameters: current selected component onjectName
// Description: When the focus is in the maxmin button , and on comwheel press will
// change the menu type. Focus on close button will close the menu
//***********************************************/
function inModeMenuEnterPressed(curObjName) {

    var menuObjRef = getCompRef(mainitem,"VentMenuObj")
    var ventGrpRef = getGroupRef("VentGroup")
    var ventModeRef = getCompRef(ventGrpRef.ventParentId,"ventModeBtn")
    if (curObjName === "ModeMenuClose"){
        destroyVentMenu()
    } else {
        if(menuObjRef.state === "Normal" || menuObjRef.state === ""){
            menuObjRef.state = "Narrow"
        } else {
            menuObjRef.state = "Normal"
        }
        ventModeRef.strVentMenuType = menuObjRef.state
    }
    ventMenuTypeChanged(menuObjRef.state)
}

//*******************************************
// Purpose: Handles mode menu tpe change(Narrow/Normal)
// Input Parameters: menutype needsto be set
// Description: Changes the state(properties) of the vent mode menu based on the menu type selected
//***********************************************/
function ventMenuTypeChanged(menuTypes) {

    var menuObjRef = getCompRef(mainitem,"VentMenuObj")
    var modeLength = menuObjRef.menuModesID.children.length;
    if(menuTypes === "Narrow"){
        for (var modeCount =0 ;modeCount<modeLength;modeCount++){
            menuObjRef.menuModesID.children[modeCount].width = 160
            menuObjRef.menuModesID.children[modeCount].iModeTypeWidth = 160
            menuObjRef.menuModesID.children[modeCount].iModeExpansionWidth = 0
            menuObjRef.menuModesID.children[modeCount].bIsModeExpansionVisible = false
            menuObjRef.menuModesID.anchors.left = menuObjRef.left
            menuObjRef.menuModesID.anchors.leftMargin=0
            menuObjRef.menuModesID.children[modeCount].anchors.horizontalCenter = menuObjRef.menuModesID.horizontalCenter

            if (modeCount === 0){
                menuObjRef.menuBypassID.children[modeCount].width = 160
                menuObjRef.menuBypassID.children[modeCount].iModeTypeWidth = 160
                menuObjRef.menuBypassID.children[modeCount].iModeExpansionWidth = 0
                menuObjRef.menuBypassID.children[modeCount].bIsModeExpansionVisible = false
                menuObjRef.menuBypassID.anchors.left = menuObjRef.left
                menuObjRef.menuBypassID.anchors.leftMargin=0
                menuObjRef.menuBypassID.children[modeCount].anchors.horizontalCenter = menuObjRef.menuBypassID.horizontalCenter
            }
        }
    } else {
        for (var modeCountOne =0 ;modeCountOne<modeLength;modeCountOne++){
            menuObjRef.menuModesID.children[modeCountOne].width = 400
            menuObjRef.menuModesID.children[modeCountOne].iModeTypeWidth = 160
            menuObjRef.menuModesID.children[modeCountOne].iModeExpansionWidth = 400
            menuObjRef.menuModesID.children[modeCountOne].bIsModeExpansionVisible = true
            if (modeCountOne === 0){
                menuObjRef.menuBypassID.children[modeCountOne].width = 400
                menuObjRef.menuBypassID.children[modeCountOne].iModeTypeWidth = 160
                menuObjRef.menuBypassID.children[modeCountOne].iModeExpansionWidth = 160
                menuObjRef.menuBypassID.children[modeCountOne].bIsModeExpansionVisible = true
            }
        }
    }
}

//*******************************************
// Purpose: Handles vent mode button states based on the current mode when vent menu is opened
// Description: When vent is opened, checks for the selected(current) mode and sets the focus
// (Scroll highlight) to the next enabled button
//***********************************************/
function onVentModeEnterPressed() {

    console.log("onVentModeEnterPressed....")
    var menuObjRef = getCompRef(mainitem,"VentMenuObj")
    menuObjRef.focus = true
    var modeLength = menuObjRef.menuModesID.children.length ;
    for(var compIndex =0; compIndex<modeLength;compIndex++){
        if(menuObjRef.menuModesID.children[compIndex].state === "Selected" && compIndex<modeLength){
            if(compIndex===0){
                menuObjRef.menuModesID.children[compIndex+1].focus = true
                menuObjRef.menuModesID.children[compIndex+1].forceActiveFocus()
            } else if(compIndex !==0){
                menuObjRef.menuModesID.children[0].focus = true
                menuObjRef.menuModesID.children[0].forceActiveFocus()
            }
        }
    }

    menuObjRef.restartTimer()
}
