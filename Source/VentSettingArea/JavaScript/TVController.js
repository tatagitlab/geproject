/* fetch the range list and the step size for TV */

var tvRangeList;
var tv_min;
var tv_max;
var tv_min_step5;
var tv_max_step5;
var tv_step5;
var tv_min_step10;
var tv_max_step10;
var tv_step10;
var tv_min_step25;
var tv_max_step25;
var tv_step25;
var tv_min_step50;
var tv_max_step50;
var tv_step50;
tvRangeList = physicalparam.getRangeData("TV", "ml");

tv_min_step5 = tvRangeList[0]
tv_max_step5 = tvRangeList[1]
tv_step5 = tvRangeList[2]
tv_min_step10 = tvRangeList[3]
tv_max_step10 = tvRangeList[4]
tv_step10 = tvRangeList[5]
tv_min_step25 = tvRangeList[6]
tv_max_step25 = tvRangeList[7]
tv_step25 = tvRangeList[8]
tv_min_step50 = tvRangeList[9]
tv_max_step50 = tvRangeList[10]
tv_step50 = tvRangeList[11]
tv_min = tvRangeList[0]
tv_max = tvRangeList[10]

/* function to fetch the updated min and max values after the contraint check when the IE quick key is selected*/

function onTVSelection() {
    console.log("onTVSelection!!! ")
    tv_min = ObjCCValue.getFinalTvMIN()
    tv_max = ObjCCValue.getFinalTvMAX()
}

/*Increment step logic TV*/

function clkHandleTVolume(m_tVolume) {

    console.log("range inside function"+tvRangeList)
    if(m_tVolume < parseInt(tv_max_step5)) {
        m_tVolume = m_tVolume+parseInt(tv_step5);
    }
    else if(m_tVolume < parseInt(tv_max_step10)) {
        m_tVolume = m_tVolume+parseInt(tv_step10);
    }
    else if(m_tVolume < parseInt(tv_max_step25)) {
        m_tVolume = m_tVolume+parseInt(tv_step25);
    }
    else if(m_tVolume < parseInt(tv_max_step50)) {
        m_tVolume = m_tVolume+parseInt(tv_step50);
    }
    return m_tVolume;
}

/*Decrement step logic TV*/

function antClkHandleTVolume(m_tVolume) {

    if((parseInt(tv_min_step50) < m_tVolume) && (m_tVolume <= parseInt(tv_max_step50))) {
        m_tVolume = m_tVolume - parseInt(tv_step50) ;
    }
    else if((parseInt(tv_min_step25) < m_tVolume) && (m_tVolume <= parseInt(tv_max_step25))) {
        m_tVolume = m_tVolume - parseInt(tv_step25) ;
    }
    else if((parseInt(tv_min_step10) < m_tVolume) && (m_tVolume <= parseInt(tv_max_step10))) {
        m_tVolume = m_tVolume - parseInt(tv_step10);
    }
    else if((parseInt(tv_min_step5) < m_tVolume) && (m_tVolume <= parseInt(tv_max_step5))) {
        m_tVolume = m_tVolume - parseInt(tv_step5);
    }
    return m_tVolume;
}

//*******************************************
// Purpose: handler for state change during value increment of TV
// Input Parameters: object name of the quickey - TV
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onTVIncrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("TV: clicked on widget for increment:"+objectName+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey1" && (selectedMode === "VCV" || selectedMode === "SIMVVCV")){
        console.log("inside increment TV")
        if(currentVentComp.strValue < parseInt(tv_max)){

            currentVentComp.strValue= parseInt(clkHandleTVolume(parseInt(currentVentComp.strValue)))
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
            currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of TV
// Input Parameters: object name of the quickey - TV
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onTVDecrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("TV: clicked on widget for decrement:"+objectName+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey1" && (selectedMode === "VCV" || selectedMode === "SIMVVCV")){
        console.log("inside decrement TV")

        if(currentVentComp.strValue > parseInt(tv_min)){

            currentVentComp.strValue= parseInt(antClkHandleTVolume(parseInt(currentVentComp.strValue)))
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
            currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - TV
//***********************************************/

function onTVEndOfScale(objectName) {

    console.log("checking end of scale TV")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if(objectName === "quickKey1" && (selectedMode === "VCV" || selectedMode === "SIMVVCV")){
        if(parseInt(currentVentComp.strValue) === parseInt(tv_min) && parseInt(currentVentComp.strValue) === parseInt(tv_max)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(parseInt(currentVentComp.strValue) === parseInt(tv_min)) {
            currentVentComp.strQuickKeyUpArrowSource=currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource=currentVentComp.strQuickkeySpinnerDownDisabled
        } else if(parseInt(currentVentComp.strValue) === parseInt(tv_max)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        } else if(currentVentComp.strValue !== tv_min && currentVentComp.strValue !== tv_max) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - TV
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetTVUpArrowBgColor(objectName) {

    console.log("setting up arrow bg TV")
    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting up arrow bg TV"+currentVentComp.strValue)
    if(objectName === "quickKey1" && (selectedMode === "VCV" || selectedMode === "SIMVVCV")){
        if(parseInt(currentVentComp.strValue) === parseInt(tv_max)) {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - TV
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetTVDownArrowBgColor(objectName) {

    console.log("setting up arrow bg TV")
    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("setting down arrow bg TV"+currentVentComp.strValue)
    if(objectName === "quickKey1" && (selectedMode === "VCV" || selectedMode === "SIMVVCV")){
        if(parseInt(currentVentComp.strValue) === parseInt(tv_min)) {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler gets called when the mode changes to connect TV controller to the corresponding quick key widget based on mode
// Input Parameters: current mode
// Description: takes the current mode as input and based on the input connects the TV controller to the corresponding widget
//***********************************************/

function onModeChangedTV(currentMode) {

    var objref;
    console.log("current mode in TV ctrl",currentMode)
    if(currentMode === "VCV" || currentMode === "SIMVVCV") {
        var tvValue = settingReader.read("TV_qKey")
        objref=getCompRef(ventParentId,"quickKey1");
        objref.setLabelofWidget("TV_qKey");
        objref.setUnitofWidget("ml_unit")
        backupData.validateParams("TV_qKey",tvValue)
        objref.setQKValue(tvValue)
        objref.setUniqueID("qkTV_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        ObjCCValue.setCurrentMode(currentMode)
        objref.bIsValueChanged = false
    }
}
