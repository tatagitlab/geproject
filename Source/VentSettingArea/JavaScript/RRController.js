/* fetch the range list and the step size for RR */

var rrRangeList;
var rrMin;
var rrMax;
var rrStepSize;
rrRangeList = physicalparam.getRangeData("RR", "/min");
rrMin = rrRangeList[0]
rrMax = rrRangeList[1]
rrStepSize = rrRangeList[2]

/* function to fetch the updated min and max values after the contraint check when the IE quick key is selected*/

function onRRSelection() {
    console.log("onRRSelection!!! ")
    if ( selectedMode === "VCV" || selectedMode === "PCV") {
        rrMin = ObjCCValue.getFinalRRMIN()
        rrMax = ObjCCValue.getFinalRRMAX()
    }
}

/*Increment step Logic for RR*/

function clkHandleRasp(m_rasp) {

    console.log("rangelist inisde rr increment"+rrRangeList)
    if((m_rasp>=parseInt(rrMin)) && (m_rasp < parseInt(rrMax))) {
        m_rasp += parseInt(rrStepSize) ;
    }
    return m_rasp;
}

/*Decrement step Logic for RR*/

function antClkHandleRasp(m_rasp) {

    if((m_rasp>parseInt(rrMin)) && (m_rasp <= parseInt(rrMax))) {
        m_rasp -= parseInt(rrStepSize) ;
    }
    return m_rasp;
}

//*******************************************
// Purpose: handler for state change during value increment of RR
// Input Parameters: object name of the quickey - RR
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onRRIncrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("RR: clicked on widget for increment:"+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey2" && (selectedMode === "VCV" || selectedMode === "PCV" )){
        console.log("inside increment RR")
        console.log("rrMax !!!!!!!!!!", rrMax)
        if(parseInt(currentVentComp.strValue) < parseInt(rrMax)){
            console.log("rrMax !!!!!!!!!!", rrMax)
            currentVentComp.strValue = parseInt(clkHandleRasp(parseInt(currentVentComp.strValue)))
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
            currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
        var ventmodeObj =  getCompRef(ventParentId, "VentModeBtn")
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of RR
// Input Parameters: object name of the quickey - RR
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onRRDecrementValue(objectName, strName) {

    var currentVentComp = getCompRef(ventParentId, objectName)
    console.log("RR: clicked on widget for decrement:"+currentVentComp+strName+selectedMode)
    if(objectName === "quickKey2" && (selectedMode === "VCV" || selectedMode === "PCV" )){
        console.log("inside decrement RR")
        if(parseInt(currentVentComp.strValue) > parseInt(rrMin)){
            currentVentComp.strValue= parseInt(antClkHandleRasp(parseInt(currentVentComp.strValue)))
            currentVentComp.state = "Selected"
        } else {
            currentVentComp.restartTimers()
            currentVentComp.bIsValueChanged = true
            currentVentComp.state = "End of Scale"
        }
        var ventmodeObj =  getCompRef(ventParentId, "VentModeBtn")
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - RR
//***********************************************/

function onRREndOfScale(objectName) {

    console.log("checking end of scale RR")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if(objectName === "quickKey2" && (selectedMode === "VCV" || selectedMode === "PCV")){
        if(parseInt(currentVentComp.strValue) === parseInt(rrMin) && parseInt(currentVentComp.strValue) === parseInt(rrMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownDisabled
        }else if(parseInt(currentVentComp.strValue) === parseInt(rrMin)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource=currentVentComp.strQuickkeySpinnerDownDisabled
        } else if( parseInt(currentVentComp.strValue) ===  parseInt(rrMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpDisabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        } else if( parseInt(currentVentComp.strValue) !==  parseInt(rrMin) &&  parseInt(currentVentComp.strValue) !==  parseInt(rrMax)) {
            currentVentComp.strQuickKeyUpArrowSource = currentVentComp.strQuickkeySpinnerUpEnabled
            currentVentComp.strQuickKeyDownArrowSource = currentVentComp.strQuickkeySpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - RR
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetRRUpArrowBgColor(objectName) {

    console.log("setting up arrow bg RR")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if(objectName === "quickKey2" && (selectedMode === "VCV" || selectedMode === "PCV")){
        if( parseInt(currentVentComp.strValue) ===  parseInt(rrMax)) {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerUpArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - RR
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetRRDownArrowBgColor(objectName) {

    console.log("setting up arrow bg RR")
    var currentVentComp = getCompRef(ventParentId, objectName)
    if(objectName === "quickKey2" && (selectedMode === "VCV" || selectedMode === "PCV")){
        if( parseInt(currentVentComp.strValue) ===  parseInt(rrMin)) {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strQuickkeySpinnerDownArrColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler gets called when the mode changes to connect RR controller to the corresponding quick key widget based on mode
// Input Parameters: current mode
// Description: takes the current mode as input and based on the input connects the RR controller to the corresponding widget
//***********************************************/


function onModeChangedRR(currentMode) {

    console.log("current mode in RR ctrl",currentMode)
    var objref;
    if((currentMode === "VCV") || currentMode === "PCV") {
        var rrValue = settingReader.read("RR_qKey")
        objref=getCompRef(ventParentId,"quickKey2");
        objref.setLabelofWidget("RR_qKey");
        objref.setUnitofWidget("min_unit")
        backupData.validateParams("RR_qKey",rrValue)
        objref.setQKValue(rrValue)
        objref.setUniqueID("qkRR_"+currentMode) //Setting unique Id for the quickkey based on the current mode
        ObjCCValue.setCurrentMode(currentMode)
        if(selectedMode === "VCV" || selectedMode === "PCV" ){
            ObjCCValue.updateConstraintParam("RR_qKey",rrValue)
        }
        objref.bIsValueChanged = false
    }
}
