/* fetch the range list and the step size for Tinsp */

var tinspRangeList;
var tinspMin;
var tinspMax;
var tinspStepSize;
tinspRangeList = physicalparam.getRangeData("Tinsp", "S");
console.log("list...Tinsp"+tinspRangeList)
tinspMin = tinspRangeList[0]
tinspMax = tinspRangeList[1]
tinspStepSize = tinspRangeList[2]
var tinspMin_temp
var tinspMax_temp

// should call this function on select or touch of Tinsp secparam in moresetting
function onTinspSelection() {
    console.log("onTinspSelection!!! ")
    tinspMin = (ObjCCValue.getFinalTinspMIN()).toFixed(1)
    tinspMax = (ObjCCValue.getFinalTinspMAX()).toFixed(1)
}

/*Increment step logic for Tinsp*/
function clkHandleTinsp(iTinsp) {

    if((parseFloat(iTinsp) >= parseFloat(tinspMin)) && (parseFloat(iTinsp) < parseFloat(tinspMax))) {
        iTinsp += parseFloat(tinspStepSize);
        iTinsp = iTinsp.toFixed(1)
    }
    return iTinsp;
}

/*Decrement step logic for Tinsp*/
function antClkHandleTinsp(iTinsp) {

    if((parseFloat(iTinsp) > parseFloat(tinspMin)) && (parseFloat(iTinsp) <= parseFloat(tinspMax))) {
        iTinsp -= parseFloat(tinspStepSize);
        iTinsp = iTinsp.toFixed(1)
    }
    return iTinsp;
}

//*******************************************
// Purpose: handler for state change during value increment of Tinsp
// Input Parameters: object name of the quickey - Tinsp
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onTinspIncrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Tinsp: clicked on widget for increment:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam1" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" ))|| (objectName === "secParam5" &&  selectedMode === "PSVPro")){
        console.log("inside increment Tinsp")
        if(parseFloat(currentSecParam.strValue) < parseFloat(tinspMax)){
            currentSecParam.strValue= clkHandleTinsp(parseFloat(currentSecParam.strValue))
            settingsBtnChangeState(currentSecParam,"Selected")
        } else {
            settingsBtnChangeState(currentSecParam,"End of Scale")
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of Tinsp
// Input Parameters: object name of the quickey - Tinsp
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onTinspDecrementValue(objectName, strName) {

    var currentSecParam = getCompRef(secParamParentId, objectName)
    console.log("Tinsp: clicked on widget for decrement:"+objectName+currentSecParam+strName+selectedMode)
    if((objectName === "secParam1" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" ))|| (objectName === "secParam5" &&  selectedMode === "PSVPro")){
        console.log("inside decrement Tinsp")
        if(parseFloat(currentSecParam.strValue) > parseFloat(tinspMin)){
            currentSecParam.strValue= antClkHandleTinsp(parseFloat(currentSecParam.strValue))
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }

    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Tinsp
//***********************************************/
function onTinspEndOfScale(objectName) {

    console.log("checking end of scale Tinsp")
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam1" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" ))|| (objectName === "secParam5" &&  selectedMode === "PSVPro")){
        if(parseFloat(currentVentComp.strValue) === parseFloat(tinspMin) && parseFloat(currentVentComp.strValue) === parseFloat(tinspMax)) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(parseFloat(currentVentComp.strValue) === parseFloat(tinspMin)) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownDisabled
        } else if(parseFloat(currentVentComp.strValue) === parseFloat(tinspMax)) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(parseFloat(currentVentComp.strValue) !== parseFloat(tinspMin) && parseFloat(currentVentComp.strValue) !== parseFloat(tinspMax)) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Tinsp
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/
function onSetTinspUpArrowBgColor(objectName) {

    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting up arrow bg Tinsp"+currentVentComp.strValue)
    if((objectName === "secParam1" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" ))|| (objectName === "secParam5" &&  selectedMode === "PSVPro")){
        console.log("Tinsp...........")
        if(parseFloat(currentVentComp.strValue) === parseFloat(tinspMax)) {
            currentVentComp.strSecParamUpArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Tinsp
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetTinspDownArrowBgColor(objectName) {

    var currentVentComp = getCompRef(secParamParentId, objectName)
    console.log("setting down arrow bg Tinsp"+currentVentComp.strValue)
    if((objectName === "secParam1" && (selectedMode === "SIMVVCV" || selectedMode === "SIMVPCV" ))|| (objectName === "secParam5" &&  selectedMode === "PSVPro")){
        if(parseFloat(currentVentComp.strValue) === parseFloat(tinspMin)) {
            currentVentComp.strSecParamDownArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
