/* fetch the range list and the step size for Exit Backup */

var exitBackupRangeList;
var exitBackupMin;
var exitBackupMax;
var exitBackupStepSize;
exitBackupRangeList = physicalparam.getRangeData("ExitBackup", "Breaths");
exitBackupMin = exitBackupRangeList[0]
exitBackupMax = exitBackupRangeList[1]
exitBackupStepSize = exitBackupRangeList[2]

/*Increment step logic for exit backup*/
function clkHandleExitBackup(iExitBackup) {

    if(iExitBackup.localeCompare("Off") !== 0 && (iExitBackup < exitBackupMax)) {
        iExitBackup =parseInt(iExitBackup)+parseInt(exitBackupStepSize);
        iExitBackup = iExitBackup.toString()
    } else if(iExitBackup.localeCompare("Off") === 0){
        iExitBackup = (exitBackupStepSize);
    }
    return iExitBackup
}

/*Decrement step logic for ExitBackup*/
function antClkHandleExitBackup(iExitBackup) {

    if(iExitBackup.localeCompare("Off") === 0) return;

    if((iExitBackup > parseInt(exitBackupMin)) && (iExitBackup <= parseInt(exitBackupMax))) {
        iExitBackup =parseInt(iExitBackup)-parseInt(exitBackupStepSize);
        iExitBackup = iExitBackup.toString()
    } else if (iExitBackup === exitBackupMin) {
        iExitBackup = "Off"
    }
    return iExitBackup
}

//*******************************************
// Purpose: handler for state change during value increment of Exit Backup
// Input Parameters: object name of the quickey - Exit Backup
// Decription: changes the state to end of scale when the value reaches the maximum value
//***********************************************/

function onExitBackupIncrementValue(objectName, strName) {
    var currentSecParam = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam6" && selectedMode === "PSVPro") || (objectName === "secParam5" && selectedMode === "SIMVPCV")){
        if(currentSecParam.strValue < parseInt(exitBackupMax) || currentSecParam.strValue === "Off"){
            currentSecParam.strValue = clkHandleExitBackup(currentSecParam.strValue)
            currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler for state change during value decrement of Exit Backup
// Input Parameters: object name of the quickey - Exit Backup
// Decription: changes the state to end of scale when the value reaches the minimum value
//***********************************************/

function onExitBackupDecrementValue(objectName, strName) {
    var currentSecParam = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam6" && selectedMode === "PSVPro") || (objectName === "secParam5" && selectedMode === "SIMVPCV")){
        if(currentSecParam.strValue >= parseInt(exitBackupMin)){
            currentSecParam.strValue = antClkHandleExitBackup(currentSecParam.strValue)
           currentSecParam.state = "Selected"
        } else {
            currentSecParam.state = "End of Scale"
        }
    }
}

//*******************************************
// Purpose: handler to enable/disable the arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Exit Backup
//***********************************************/
function onExitBackupEndOfScale(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam6" && selectedMode === "PSVPro") || (objectName === "secParam5" && selectedMode === "SIMVPCV")){
        if(currentVentComp.strValue === "Off") {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource=currentVentComp.strMoreSettingParamSpinnerDownDisabled
            //changeState(currentVentComp,"End of Scale")
        } else if(currentVentComp.strValue === exitBackupMax) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpDisabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        } else if(currentVentComp.strValue !== "Off" && currentVentComp.strValue !== exitBackupMax) {
            currentVentComp.strSecParamUpArrowSource = currentVentComp.strMoreSettingParamSpinnerUpEnabled
            currentVentComp.strSecParamDownArrowSource = currentVentComp.strMoreSettingParamSpinnerDownEnabled
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the up arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Exit Backup
// Description: shows the touch effect on the up arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/

function onSetExitBackupUpArrowBgColor(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam6" && selectedMode === "PSVPro") || (objectName === "secParam5" && selectedMode === "SIMVPCV")){
        if(currentVentComp.strValue === exitBackupMax) {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamUpArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

//*******************************************
// Purpose: handler to set the background color of the down arrow on the spinner widget based on state
// Input Parameters: object name of the quickey - Exit Backup
// Description: shows the touch effect on the down arrow when the arrow is touched and the quick key value is
//within the range, touch effect is not seen when value of quick key is out of scale.
//***********************************************/
function onSetExitBackupDownArrowBgColor(objectName) {
    var currentVentComp = getCompRef(secParamParentId, objectName)
    if((objectName === "secParam6" && selectedMode === "PSVPro") || (objectName === "secParam5" && selectedMode === "SIMVPCV")){
        if(currentVentComp.strValue === "Off") {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSecParamDownArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
