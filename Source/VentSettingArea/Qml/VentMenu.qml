﻿import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtMultimedia 5.8
import "../JavaScript/VentGroupController.js" as GrpCtrl
import "../JavaScript/VentModeController.js" as VentmodeCtrl
import "../JavaScript/TVController.js" as TVCtrl
import "../JavaScript/RRController.js" as RRCtrl
import "../JavaScript/IEController.js" as IECtrl
import "../JavaScript/PEEPController.js" as PEEPCtrl
import "../JavaScript/PsupportController.js" as PsupportCtrl
import "../JavaScript/PinspController.js" as PinspCtrl
import "../JavaScript/RRMechController.js" as RRMechCtrl

Rectangle {
    id: ventMenu
    objectName: "VentMenuObj"

    property string strMenuTitle
    property string strMenuTitlecolor: "#787878"
    property string strSelectedMode
    property var menuBypassID: bypassColumn
    property var menuModesID: modeColumn

    //parent property
    property int iVentModeMenuBorderWidth: 2
    property int iVentModeMenuHeight: 602
    property int iVentModeMenuWidth: 538
    property int iVentModeMenuRadius: 3
    property int iMenuTimeoutInterval: 25000
    property string strVentModeMenuBorderColor: "#3c3d3d"
    property string strVentModeMenuColor: "#242424"
    property string strcloseBtnVentMenu
    property string strcloseBtnTchVentMenu
    property string strventMenuMinimizeBtn
    property string strventMenuMaximizeBtn
    property string strVentMenuBtnScrollStateBorderColor: "#edbf00"
    property string strVentMenuClosebtnBorderColor: "#444444"
    property string strVentMenuTchColor: "#4565A6"
    property string strCloseBtnTchSource: "qrc:/Config/Assets/icon_close_touched.png"


    property string menuState

    //child property
    property int  iVentModeMenuLabelPixelsize
    property string ventModeMenuLabelFontFamily: geFont.geFontStyle()

    signal differentModeSelected(string mode)
    signal modeChange(string mode)
    anchors.left:parent.left
    state: menuState

    function restartTimer() {
        modeTimeout.running = true
        modeTimeout.restart()
    }

    function stopTimer() {
        modeTimeout.running = false
        modeTimeout.stop()
    }

//    SoundEffect {
//        id: mediaPlayer
//        source: "qrc:/Config/Assets/reject.wav"
//    }

    width: iVentModeMenuWidth
    height: iVentModeMenuHeight
    color: strVentModeMenuColor
    border.width: iVentModeMenuBorderWidth
    border.color: strVentModeMenuBorderColor
    radius: iVentModeMenuRadius
    visible: true

    Label {
        id: menuLabel
        text: qsTrId(strMenuTitle)
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.topMargin: 13
        color: strMenuTitlecolor
        font.family: ventModeMenuLabelFontFamily
        font.pixelSize: iVentModeMenuLabelPixelsize
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            console.log("menu clicked")
        }
    }

    Rectangle {
        id: closeBtn
        objectName: "ModeMenuClose"
        height: 42
        width: 42
        color: closeMouseArea.pressed? strVentMenuTchColor:"#2e2e2e"
        border.color: activeFocus? strVentMenuBtnScrollStateBorderColor :strVentMenuClosebtnBorderColor
        border.width: activeFocus? 2 : 1
        radius: 3
        anchors.right: parent.right
        anchors.rightMargin: 9
        anchors.top: parent.top
        anchors.topMargin: 9
        property string uniqueObjectID: "ModeMenuCloseBtn"
        Image {
            source: closeMouseArea.pressed? strCloseBtnTchSource : strcloseBtnVentMenu
            anchors.centerIn: parent
        }
        MouseArea {
            id: closeMouseArea
            anchors.fill: parent
            onPressed: {
                modeTimeout.running = false;
                modeTimeout.stop()
                closeBtn.border.color = strVentMenuClosebtnBorderColor
                closeBtn.border.width = 0
            }
            onReleased: {
                VentmodeCtrl.destroyVentMenu()
            }
        }
    }

    Rectangle {
        id: maxMinBtn
        objectName: "MaximizeMinimize"
        anchors.right: parent.right
        anchors.rightMargin: 9
        anchors.top: parent.top
        anchors.topMargin: parent.height/2
        height: 42
        width: 42
        border.width: (activeFocus && ! minMaxArea.pressed)? 2 : 0
        color: minMaxArea.pressed? strVentMenuTchColor:strVentModeMenuColor
        border.color: (activeFocus && ! minMaxArea.pressed) ?strVentMenuBtnScrollStateBorderColor : strVentMenuClosebtnBorderColor
        property string uniqueObjectID: "MaxMinArrow"
        Image {
            id: minImage
            source: strventMenuMinimizeBtn
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            smooth: true
            opacity: 1

        }
        Image {
            id: maxImage
            source: strventMenuMaximizeBtn
            smooth: true
            opacity: 0
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter

        }
        MouseArea {
            id: minMaxArea
            anchors.fill: parent
            onClicked: {
                VentmodeCtrl.inModeMenuEnterPressed(maxMinBtn.objectName)
                ventMenu.forceActiveFocus()
                modeTimeout.running = true
                modeTimeout.restart()
            }
        }
    }

    Column {
        id: modeColumn
        width: 340
        height: ventMenu.height*0.75
        spacing: 10
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.top: parent.top
        anchors.topMargin: 60
    }

    Column {
        id:bypassColumn
        width:340
        height: parent.height * 0.25
        spacing: 1
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.top: modeColumn.bottom
        anchors.topMargin: 15
    }

    Keys.onRightPressed: {
        if(maxMinBtn.focus === true){
            VentmodeCtrl.onNavigateRightMode(maxMinBtn.objectName)
        } else if (closeBtn.focus === true) {
            VentmodeCtrl.onNavigateRightMode(closeBtn.objectName)
        } else if (ventMenu.focus === true){
            VentmodeCtrl.onNavigateRightMode(ventMenu.objectName)
        }
        modeTimeout.running = true
        modeTimeout.restart()
    }

    Keys.onLeftPressed: {
        if(maxMinBtn.focus === true){
            VentmodeCtrl.onNavigateLeftMode(maxMinBtn.objectName)
        } else if (closeBtn.focus === true) {
            VentmodeCtrl.onNavigateLeftMode(closeBtn.objectName)
        } else if (ventMenu.focus === true){
            VentmodeCtrl.onNavigateLeftMode(ventMenu.objectName)
        }
        modeTimeout.running = true
        modeTimeout.restart()
    }

    Keys.onReturnPressed: {
        if(closeBtn.focus === true){
            VentmodeCtrl.inModeMenuEnterPressed(closeBtn.objectName)
        } else if (maxMinBtn.focus === true) {
            VentmodeCtrl.inModeMenuEnterPressed(maxMinBtn.objectName)
            modeTimeout.running = true
            modeTimeout.restart()
        }
    }


    states: [
        State{
            name: "Narrow"
            PropertyChanges {
                target: minImage; opacity: 0
            }
            PropertyChanges {
                target: maxImage; opacity: 1
            }
            PropertyChanges {
                target: ventMenu; width: 339
            }
            PropertyChanges {
                target: modeColumn; width: 339
            }
            PropertyChanges {
                target: bypassColumn; width: 339
            }

           },
        State{
            name: "Normal"
            PropertyChanges {
                target: ventMenu; width: 538
            }
            PropertyChanges {
                target: minImage; opacity: 1
            }
            PropertyChanges {
                target: maxImage; opacity: 0
            }
            PropertyChanges {
                target: modeColumn; width: 430
            }
            PropertyChanges {
                target: bypassColumn; width: 430
            }

        }
    ]

    Timer {
        id: modeTimeout
        interval: iMenuTimeoutInterval
        running: false
        repeat: false
        onTriggered: {
            //mediaPlayer.play()
            modeTimeout.running = false
            modeTimeout.stop()
            VentmodeCtrl.destroyVentMenu()
        }
    }

    Component.onCompleted: {
        VentmodeCtrl.createVentMenuComponents()
        var currentMode = settingReader.read("MODE")
        var modeRef= getCompRef(menuModesID,currentMode)
        modeRef.state = "Selected"
        ventMenu.differentModeSelected.connect(GrpCtrl.onModeSelectionInMenu)
        ventMenu.modeChange.connect(TVCtrl.onModeChangedTV)
        ventMenu.modeChange.connect(RRCtrl.onModeChangedRR)
        ventMenu.modeChange.connect(IECtrl.onModeChangedIE)
        ventMenu.modeChange.connect(PEEPCtrl.onModeChangedPEEP)
        ventMenu.modeChange.connect(PsupportCtrl.onModeChangedPsupport)
        ventMenu.modeChange.connect(PinspCtrl.onModeChangedPinsp)
        ventMenu.modeChange.connect(RRMechCtrl.onModeChangedRRMech);
    }
}
