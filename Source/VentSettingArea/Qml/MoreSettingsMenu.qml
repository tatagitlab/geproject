import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtMultimedia 5.8
import "../JavaScript/MoreSettingsController.js" as MenuCompCreation
import "qrc:/Source/VentSettingArea/JavaScript/MoreSettingsController.js" as MenuCntrl

Rectangle {
    id: moreMenuId
    objectName: "MoreSettingsMenuObj"

    property bool bTimerRunning: false
    property var secParamParentId: paramColumn
    property var prevValue
    //Parent property
    property int iMoreMenuWidth: 580
    property int iMoreMenuHeight: 580
    property int iMoreMenuBorderWidth: 2
    property int iMoreMenuRadius: 3
    property int iMenuTimeoutInterval: 25000
    //child property
    property int iPixelSizeMoreSet: 18
    property string strMoreSLabel
    property string strMoreSMode
    property string strmoreSettingsLabelFontColor: "#787878"
    //Parent property
    property string strMoreMenuColor: "#242424"
    property string strMoreMenuBorderColor: "#3C3D3D"
    property string strMoreMenuTchColor: "#4565A6"
    property string strCloseBtnTchSource
    property string strCloseBtnSource
    //child property
    property string strFontFamilyMoreSet: geFont.geFontStyle()

    anchors.left: parent.left

    //Parent Property settings
    width: iMoreMenuWidth
    height: iMoreMenuHeight
    color: strMoreMenuColor
    border.color: strMoreMenuBorderColor
    border.width: iMoreMenuBorderWidth
    radius: iMoreMenuRadius
    visible: true

    /* Negative audio tone for exit cases*/
//    SoundEffect {
//        id: mediaPlayer
//        source: "qrc:/Config/Assets/reject.wav"
//    }

    function restartTimer() {
        timeOut.running = true ;
        timeOut.restart();
        console.log("timer restart")
    }

    function stopTimer() {
        timeOut.running = false ;
        timeOut.stop() ;
    }

    Label {
        id: moreSetLabel
        text: qsTrId(strMoreSLabel)
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.margins: 20
        color: strmoreSettingsLabelFontColor
        font.family: strFontFamilyMoreSet
        font.pixelSize: iPixelSizeMoreSet //18
    }

    Label {
        id: moreSetMode
        text: qsTrId(strMoreSMode)
        anchors.top: parent.top
        anchors.left: moreSetLabel.right
        anchors.leftMargin: 5
        anchors.margins: 20
        color: strmoreSettingsLabelFontColor
        font.family: strFontFamilyMoreSet
        font.pixelSize: iPixelSizeMoreSet
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("more settings menu clicked")
        }
    }

    Rectangle {
        id: closeBtn
        objectName: "MoreSettingMenuClose"
        anchors.right: parent.right
        anchors.rightMargin: 9
        anchors.top: parent.top
        anchors.topMargin: 9
        border.width: activeFocus? 2 : 1
        height: 42
        width: 42
        radius: 3
        color: closeMouseArea.pressed? strMoreMenuTchColor:"#2e2e2e"
        border.color: activeFocus ? "#edbf00" :"#444444"
        property string uniqueObjectID: "MoreMenuCloseBtn"
        Image {
            source: closeMouseArea.pressed? "qrc:/Config/Assets/icon_close_touched.png" : "qrc:/Config/Assets/icon_close_transparent.png"
            anchors.centerIn: parent
        }
        MouseArea {
            id: closeMouseArea
            anchors.fill: closeBtn
            onPressed: {
                timeOut.running = false;
                timeOut.stop()
                closeBtn.border.color = "#444444"
                closeBtn.border.width = 0
            }
            onReleased: {
                MenuCntrl.destroyMoreSettingMenu()
              //  moreSettingId.focus = true
            }
        }
    }
    Keys.onRightPressed: {
        secParamParentId.children[0].focus = true
        timeOut.running = true;
        timeOut.restart()
    }

    Keys.onLeftPressed: {
        secParamParentId.children[secParamParentId.children.length-1].focus = true
        timeOut.running = true;
        timeOut.restart()
    }

    Keys.onReturnPressed: {
        MenuCntrl.destroyMoreSettingMenu()
        timeOut.running = true;
        timeOut.restart()
    }

    Column {
        id: paramColumn
        width: parent.width
        height: parent.height
        spacing: 15
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 60
    }

    Timer {
        id : timeOut
        objectName: "timeOutObj"
        interval: iMenuTimeoutInterval
        running: bTimerRunning
        repeat: false
        onTriggered: {
            //mediaPlayer.play()
            console.log("time out initiated")
            timeOut.running = false
            timeOut.stop()
            console.log("timer stopped !!!!")
            MenuCntrl.destroyMoreSettingMenu()
          //  moreSettingId.focus = true
        }
    }

    Component.onCompleted: {
        MenuCompCreation.createMenuComponents()
        secParamParentId.children[0].focus = true
        secParamParentId.children[0].forceActiveFocus();
        timeOut.start()
    }
}
