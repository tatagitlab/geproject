import QtQuick 2.0
import QtQuick.Controls 2.2
import "qrc:/Source/VentSettingArea/JavaScript/VentGroupController.js" as GrpCtrl

Rectangle {
    id: modeConfirm
    objectName: "ConfirmPopup"

    property string strConfirmationText
    property string strModeText

    //parent property
    property int iPopupHeight: 60
    property int iPopupRadius: 3
    property int iPopupWidth: 540
    property string strPopupColor: "#616161"
    property string strPopupFontColor: "#151515" //should this in xml and xsd files
    property string strPopupCancel
    //child property
    property int iPopupLabelPixelSize: 16
    property string strCancelBtnColor: "#737373"
    property string strPopupLabelFontFamily: geFont.geFontStyle()
    property string strToolTipTriangleSource: "qrc:/Config/Assets/icon_popuptriangle.png"
    property string uniqueObjectID: "ModeConfirmPopup"

    width: iPopupWidth
    height: iPopupHeight
    color: strPopupColor
    radius: iPopupRadius
    z: 1

    Text {
        id: confirmMsg
        width: parent.width * 0.75
        height: parent.height
        text: qsTrId(strConfirmationText)
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        font.family: strPopupLabelFontFamily
        color: strPopupFontColor
        font.pixelSize: iPopupLabelPixelSize
        wrapMode: Text.WordWrap
    }

    Rectangle {
        id: cancelBtn
        width: parent.width * 0.20
        height: parent.height *0.75
        anchors.left: confirmMsg.right
        anchors.leftMargin: 5
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        radius: iPopupRadius
        color: strCancelBtnColor

        Label {
            id: cancelText
            anchors.centerIn: parent
            text: qsTrId(strPopupCancel)
            color: strPopupFontColor
            font.family: strPopupLabelFontFamily
            font.pixelSize: iPopupLabelPixelSize
        }

        MouseArea {
            anchors.fill: parent
            onPressed: {
                console.log("on pressed")
                //Making sure that interrupted (30s) and initial timer(10s) are stopped
                var ventGrpRef = getGroupRef ("VentGroup")
                var qkRef = getCompRef(ventGrpRef.ventParentId,"quickKey1")
                qkRef.bIsTimerRunning = false
                qkRef.bIsTotalTimerRunning = false
                qkRef.stopTimers()  //to make sure that interrupted (30s) and initial timer(10s) are stopped

            }
            onReleased: {
                console.log("on released")
                GrpCtrl.onCancellingModeConfirmation()
            }
        }
    }
    Image {
        id : toolTipImage
        anchors.top: parent.bottom
        anchors.left: parent.left
        anchors.leftMargin: 30
        source: strToolTipTriangleSource
    }
}
