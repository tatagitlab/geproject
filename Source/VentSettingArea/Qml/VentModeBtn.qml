import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import "qrc:/Source/VentSettingArea/JavaScript/VentGroupController.js" as GrpCtrl
import "../JavaScript/VentModeController.js" as VentModeCtrl

Rectangle {
    id: ventModeId
    objectName: "ventModeBtn"

    property bool bKeyOnePressed: false
    property bool bKeyFifthPressed: false
    property bool bKeySecondPressed: false
    property bool bKeyThreePressed: false
    property bool bKeySixthPressed: false
    property bool bIsModeChangeConfirmed: true
    property bool bIsModeCompTouched: false
    property bool bIsMousePressed: modeBtnMouseArea.containsPress

    property int iCompID
    property int iOneMinInactiveInterval: 60000
    property string previousMode
    //Parent property
    property int iCustomizedBorder: 1
    property int iVentModeWidth: 338
    property int iVentModeHeight: 127
    //Child Property
    property int iStatusTextFontSize: 28
    property int iStatusiconWidth: 50
    property int iStatusiconHeight: 62
    property int iVentModeBtnWidth: 228
    property int iVentModeBtnHeight: 60
    property int iVentModeBtnBorderWidth: 1
    property int iVentModeBtnRadius: 3
    property int ilabelFontPixelSize: 28
    property int iText1FontPixelSize: 20

    //Parent Property
    property string strCustomizedBorderColor: "#424242"
    property string strStatusTextFontColor: "#5d5d5d"
    property string strVentModeColor: "#2a2a2a"
    property string strVentModeBorderColor: "#555555"

    //Child Property
    property string strStatusTextFontFamily: geFont.geFontStyle()

    property string strType: "VentMode"
    property string strModeLabel
    property string strVentswitch: "On"
    property string strModeBtnLabel: "Btn_Label"
    property string strVentilationStatus: "Vent_ON"
    property string strVentArrowIcon
    property string strVentArrowDisableIcon
    property string strVentArrowPresetIcon
    property string strVentStatusOn: component.getComponentResourceString("Component","VentStatus","Status","ON")
    property string strVentStatusOff: component.getComponentResourceString("Component","VentStatus","Status","OFF")
    //Child Property
    property string strVentModeBtnBorderColor: "#555555"
    property string strVentModeBtnColor: "#464646"
    property string strModeValueFontColor: "#f2f2f2"
    property string strlabelFontFamily: geFont.geFontStyle()
    property string strText1FontFamily: geFont.geFontStyle()
    
    // state styling properties
    property string strVentModeActiveStateColor: "#464646"
    property string strVentModeDisabledStateColor: "#2a2a2a"
    property string strVentModeDisabledFontColor: "#424242"
    property string strVentModeEndOfScaleStateColor: "#f6f925"
    property string strVentModeEndOfScaleStateBorderColor: "#a89c8b"
    property string strVentModeScrollStateBorderColor: "#edbf00"
    property string strVentModeSelectedStateColor: "#f6f925"
    property string strVentModeSelectedStateBorderColor: "#edbf00"
    property string strVentModeTouchedStateColor: "#4565a6"
    property string strVentPresetTextColor: "#909090"
    property string strVentIconOn
    property string strVentIconOff
    property string strVentMenuType: "Normal"
    property int iVentModeStateHeightChange: 140
    property string strVentPreviousState
    
    property string uniqueObjectID: "VentModeButton"

    //For switching Bag/Vent
    property string strBtnState
    property string strVentState

    signal shortCutKeyPressed(int iCompID, string currentKey, string strType)
    signal destroyOpenMenu()

    width: iVentModeWidth
    height: iVentModeHeight
    color: strVentModeColor
    radius: 3
    state:strBtnState
    Layout.preferredHeight: iVentModeHeight
    Layout.preferredWidth: iVentModeWidth
    anchors.bottom: parent.bottom
    anchors.left: parent.left

    //*******************************************
    // Purpose: To start the scroll active timer (1min)
    // Description: Handles timer restart whenever the quickkey is in scroll state.(after
    // 1 min of inactivity goes back to active state)
    //***********************************************/
    function resetScrollActiveTimer() {
        scrollActiveTimer.running = true ;
        scrollActiveTimer.restart() ;
    }

    //*******************************************
    // Purpose: To stop the scroll active timer (1min)
    // Description: Handles stopping the timer whenever the quickkey goes to any state other
    // than scroll state / when the focus is lost
    //***********************************************/
    function stopScrollActiveTimer() {
        scrollActiveTimer.running = false ;
        scrollActiveTimer.stop() ;
    }

    Rectangle {
        id: borderRight
        width: 10
        height: parent.height
        visible: true
        anchors.right: parent.right
        color: strVentModeColor
    }

    Rectangle {
        id: borderBottom
        width: parent.width
        height: 10
        visible: true
        anchors.left: parent.left
        color: strVentModeColor
    }

    Rectangle {
        id: borderLeft
        width: iCustomizedBorder
        height: parent.height
        visible: true
        anchors.left: parent.left
        color: strCustomizedBorderColor
    }

    Label {
        id: statustext
        text: qsTrId(strVentilationStatus)
        font.family:  strStatusTextFontFamily
        font.pixelSize: iStatusTextFontSize
        color: strStatusTextFontColor
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
    }

    Image {
        id: statusicon
        source: strVentswitch == "On"?strVentIconOn:strVentIconOff
        width: iStatusiconWidth
        height: iStatusiconHeight
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
    }

    Rectangle {
        id: ventmodebtnId
        width: iVentModeBtnWidth
        height: iVentModeBtnHeight
        border.color: strVentModeBtnBorderColor
        border.width: iVentModeBtnBorderWidth
        radius: iVentModeBtnRadius
        color: strVentModeBtnColor
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 30

        Label {
            id: text1
            anchors.centerIn: ventmodebtnId
            text: qsTrId(strModeLabel)
            font.family: strText1FontFamily
            font.pixelSize: iText1FontPixelSize
            color : strModeValueFontColor
        }

        Image {
            id : ventArrowIcon
            source: strVentArrowIcon
            anchors.verticalCenter:  parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
        }

        MouseArea {
            id: modeBtnMouseArea
            anchors.fill: parent
            onReleased: {
                var ventGrpObj = getGroupRef("VentGroup")
                if (!ventGrpObj.bIsModeMenuCreated && modeBtnMouseArea.containsMouse) {
                    GrpCtrl.onVentModeSelected(ventModeId.objectName)
                }
                console.log("sending destroyOpenMenu onVentModeTouched")
                destroyOpenMenu();
            }
        }
    }

    Keys.onRightPressed: {
        GrpCtrl.onNavigateRightInVentGroup(objectName)
    }

    Keys.onLeftPressed: {
        GrpCtrl.onNavigateLeftInVentGroup(objectName)
    }

    //key board Shortcuts
    Keys.onPressed: {
        event.accepted = true;
        GrpCtrl.shortcutKeysPress(event.key, objectName)
    }

    Keys.onReleased: {
        event.accepted = true;
        GrpCtrl.shortcutKeysRelease(event.key, objectName)
    }

    Keys.onReturnPressed: {
        bIsModeCompTouched = true
        ventModeId.strVentPreviousState = ventModeId.state
        GrpCtrl.createVentModeMenu(ventModeId.strVentMenuType)
        VentModeCtrl.onVentModeEnterPressed()
        //to make sure that mode btn remains in preset mode /active mode when mode menu is opened
        if (ventModeId.strVentState === "Vent") {
            ventModeId.state = "Active"
        } else if (ventModeId.strVentState === "Bag"){
            ventModeId.state = "Preset"
        }
    }

    onStateChanged: {
        if(ventModeId.state === "Scroll" || ventModeId.state === "PresetScroll") {
            ventModeId.resetScrollActiveTimer()
        }
        else if(ventModeId.state === "Selected" || ventModeId.state === "End of Scale" || ventModeId.state === "Active" || ventModeId.state === "Disabled"
                || ventModeId.state === "Preset" || ventModeId.state === "PresetDisabled") {
            ventModeId.stopScrollActiveTimer() ;
        }
    }
    onFocusChanged:{
        if(ventModeId.focus === false){
            ventModeId.stopScrollActiveTimer() ;
            console.log("stop scroll active timer of vent mode button since focus is false on"+iCompID)

            if (ventModeId.strVentState === "Bag"){
                ventModeId.state = "Preset"}
        }
    }

    onBIsMousePressedChanged:{
        console.log("mouse press changed..."+modeBtnMouseArea.containsPress+ventModeId.uniqueObjectID)
        if(modeBtnMouseArea.containsPress) {
            console.log("state of button on pressed"+ventModeId.state)
            var ventGrpObj = getGroupRef("VentGroup")
            if (ventModeId.state !== "Disabled" && !ventGrpObj.bIsModeMenuCreated && ventModeId.state !== "PresetDisabled"){
                ventModeId.strVentPreviousState = ventModeId.state
                GrpCtrl.onSettingsCompTouched(ventModeId.objectName)
            }
        } else if(!modeBtnMouseArea.containsPress) {
            if(ventModeId.state === "Touched") {
                ventModeId.state = ventModeId.strVentPreviousState
            }
        }
    }

    states: [State{
            name: "Scroll"
            when: ventModeId.activeFocus && ventModeId.strVentState == "Vent"
            PropertyChanges {
                target: ventmodebtnId;
                border.color: strVentModeScrollStateBorderColor;
                border.width: 2
            }},
        State{
            name: "PresetScroll"
            when: ventModeId.activeFocus && ventModeId.strVentState == "Bag"
            PropertyChanges {
                target: ventmodebtnId;
                border.color: strVentModeScrollStateBorderColor;
                border.width: 2

            }

            PropertyChanges {
                target: ventModeId;
                strVentswitch: "Off"
                strVentilationStatus: strVentStatusOff
            }
            PropertyChanges {
                target: text1
                font.pixelSize : iText1FontPixelSize
                color: strVentPresetTextColor
                text:"["+qsTrId(strModeLabel)+"]"
            }
            PropertyChanges {
                target: ventArrowIcon
                source : strVentArrowPresetIcon
            }},
        State{
            name: "Selected"
            PropertyChanges {
                target: ventmodebtnId;
                color: strVentModeSelectedStateColor ;
                border.color: strVentModeSelectedStateBorderColor ;
                height: iVentModeStateHeightChange
            }},
        State{
            name: "Disabled"
            PropertyChanges {
                target: ventmodebtnId;
                color: strVentModeDisabledStateColor
            }
            PropertyChanges {
                target: statustext
                color : strVentModeDisabledFontColor
            }
            PropertyChanges {
                target: text1
                color : strVentModeDisabledFontColor

            }
            PropertyChanges {
                target: ventModeId;
                color: strVentModeDisabledStateColor
            }
            PropertyChanges {
                target: ventArrowIcon
                source :strVentArrowDisableIcon

            }
            PropertyChanges {
                target: borderBottom
                color: strVentModeDisabledStateColor
            }
            PropertyChanges {
                target: borderRight
                color: strVentModeDisabledStateColor
            }
        },
        State{
            name: "Active"
            PropertyChanges {
                target: ventmodebtnId;
                color: strVentModeActiveStateColor
            }},
        State{
            name: "Touched"
            PropertyChanges {
                target: ventmodebtnId;
                color: strVentModeTouchedStateColor
            }},
        State{
            name: "PresetTouched"
            PropertyChanges {
                target: ventmodebtnId;
                color: strVentModeTouchedStateColor
            }
            PropertyChanges {
                target: ventModeId;
                strVentswitch:"Off"
                strVentilationStatus:strVentStatusOff
            }
            PropertyChanges {
                target: text1
                font.pixelSize : iText1FontPixelSize
                color: strVentPresetTextColor
                text:"["+qsTrId(strModeLabel)+"]"
            }},
        State{
            name: "Preset"
            PropertyChanges {
                target: ventModeId;
                strVentswitch:"Off"
                strVentilationStatus:strVentStatusOff
            }
            PropertyChanges {
                target: ventArrowIcon
                source : strVentArrowPresetIcon
            }
            PropertyChanges {
                target: text1
                font.pixelSize : iText1FontPixelSize
                color: strVentPresetTextColor
                text:"["+qsTrId(strModeLabel)+"]"
            }},
        State{
            name: "PresetDisabled"
            extend: "Disabled"
            PropertyChanges {
                target: ventModeId;
                strVentswitch:"Off"
                strVentilationStatus:strVentStatusOff
            }
            PropertyChanges {
                target: text1
                text:"["+qsTrId(strModeLabel)+"]"
            }
        }
    ]

    Timer {
        id: scrollActiveTimer
        running: false
        repeat: false
        interval: iOneMinInactiveInterval ; // 1 min of inactivity
        onTriggered: {
            GrpCtrl.onScrollActiveTimeOut(ventModeId.objectName)
        }
    }
    Connections{
        target: objCSBValue

        //Signal for CSBCommunication to change mode from PSVPro to SIMVPCV
        //Updating the Label
        onCsbBkpModeSIMVPCV: {
            GrpCtrl.updatedModeLabelSIMVPCV();
        }
        //Signal for CSBCommunication to change mode from SIMVPCV to PSVPro
        //Updating the Label
        onCsbBkpModePSVPro: {
            GrpCtrl.updatedModeLabelPSVPro();
        }
    }
}


