import QtQuick 2.9
import QtQuick.Layouts 1.3
import "../JavaScript/VentGroupController.js" as Ventcontrol
import "../JavaScript/RRMechController.js" as RRMechCtrl
import "../JavaScript/IEController.js" as IECtrl
import "../JavaScript/PEEPController.js" as PEEPCtrl
import "../JavaScript/PsupportController.js" as PsupportCtrl
import "../JavaScript/PinspController.js" as PinspCtrl
import "qrc:/Source/VentSettingArea/JavaScript/MoreSettingsController.js" as MenuCntrl
import "../../Checkout/JavaScript/CheckoutController.js" as CheckoutComp
Rectangle {
    id: ventGrpId
    objectName: "VentGroup"

    property bool bIsModeMenuCreated: false
    property bool bIsSettingsMenuCreated: false
    property bool bSelectModeBtn: false
    property bool bCSBTrigger: false
    property bool bIsBackModeTriggered: false
    property int iPreviousIndex
    property int iVentGrpHeight: 128
    property int iVentGrpWidth: 1280
    property string parentID: "mainrow"
    property string selectedMode
    property string strVentGrpColor: "Gray"
    property string strForRevertingPQKValue
    property var firstfocus
    property var keySequenceList: []
    property var ventParentId: mainrow

    width: iVentGrpWidth
    height: iVentGrpHeight
    color: strVentGrpColor
    anchors.bottom: parent.bottom
    z: 1

    //ShortCut
    property bool bKeyFivePressed
    property bool bKeyOnePressed
    property bool bKeySixPressed
    property bool bKeyTwoPressed
    property bool bKeyOnePressedInsideMenu
    property bool bKeyThreePressedInsideMenu
    property bool bKeyTwoPressedInsideMenu
    property bool bKeyFivePressedInsideMenu
    property bool bKeySixPressedInsideMenu

    signal backmode(string mode) //signal to support backup mode(switching from PSVPro to SIMVPCV and vice versa)
    signal finalDestroy()
    signal shortCutKeyPressed(string currentKey, string objectName)
    signal shortCutKeyReleased(string currentKey, string objectName)

    //*******************************************
    // Purpose: Handles navigation change across groups
    // Input Parameters: gets whether Right/Left navigation
    // Description: Gets the navigation type(right/left) and based on that calls the
    // corresponding handler
    //***********************************************/
    function groupNavigationChange(key) {
        if(key === "Left") {
            Ventcontrol.groupChangeOnLeft();
        } else if(key === "Right") {
            Ventcontrol.groupChangeOnRight();
        }
    }

    RowLayout {
        id: mainrow
        anchors.fill: parent
        spacing: 1
    }

    Keys.onRightPressed: {
        Ventcontrol.setFocusToChild(iPreviousIndex)
    }

    Keys.onLeftPressed: {
        Ventcontrol.setFocusToChild(iPreviousIndex)
    }

    onFirstfocusChanged: {
        if(ventGrpId.objectName === "VentGroup"){
            if(ventGrpId.firstfocus === "right"){
                ventParentId.children[0].forceActiveFocus()
            } else if(ventGrpId.firstfocus === "left"){
                ventParentId.children[ventParentId.children.length - 1].forceActiveFocus()
            }
        }
    }

    Keys.onPressed: {
        event.accepted = true;
        shortCutKeyPressed(event.key, "")
    }

    Keys.onReleased: {
        shortCutKeyReleased(event.key, "")
    }

    Connections{
        target: objCSBValue

        //Signal for CSBCommunication to change mode from PSVPro to SIMVPCV
        onCsbBkpModeSIMVPCV: {
            Ventcontrol.changeModeToSIMVPCV()
        }

        //Signal for CSBCommunication to change mode from SIMVPCV to PSVPro
        onCsbBkpModePSVPro: {
            Ventcontrol.changeModeToPSVPro()
        }
    }

    Component.onCompleted: {
        console.log("ventgrp created");
        Ventcontrol.createComponent();
        ventGrpId.shortCutKeyPressed.connect(Ventcontrol.shortcutKeysPress)
        ventGrpId.shortCutKeyReleased.connect(Ventcontrol.shortcutKeysRelease)
        //ventGrpId.backmode.connect(TVCtrl.onModeChangedTV)
        //ventGrpId.backmode.connect(RRCtrl.onModeChangedRR)
        ventGrpId.backmode.connect(IECtrl.onModeChangedIE)
        ventGrpId.backmode.connect(PEEPCtrl.onModeChangedPEEP)
        ventGrpId.backmode.connect(PsupportCtrl.onModeChangedPsupport)
        ventGrpId.backmode.connect(PinspCtrl.onModeChangedPinsp)
        ventGrpId.backmode.connect(RRMechCtrl.onModeChangedRRMech)

        //signal to signal
        Ventcontrol.ventModeBtn.destroyOpenMenu.connect(ventGrpId.finalDestroy)
        Ventcontrol.qKeyWidget1.destroyOpenMenu.connect(ventGrpId.finalDestroy)
        Ventcontrol.qKeyWidget2.destroyOpenMenu.connect(ventGrpId.finalDestroy)
        Ventcontrol.qKeyWidget3.destroyOpenMenu.connect(ventGrpId.finalDestroy)
        Ventcontrol.qKeyWidget4.destroyOpenMenu.connect(ventGrpId.finalDestroy)
        Ventcontrol.moreSettingbtn.destroyOpenMenu.connect(ventGrpId.finalDestroy)
    }
}
