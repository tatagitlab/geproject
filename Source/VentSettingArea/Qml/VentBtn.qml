import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtMultimedia 5.8
import "qrc:/Source/VentSettingArea/JavaScript/VentGroupController.js" as GrpCtrl
import "qrc:/Source/VentSettingArea/JavaScript/TVController.js" as TVCtrl
import "qrc:/Source/VentSettingArea/JavaScript/RRController.js" as RRCtrl
import "qrc:/Source/VentSettingArea/JavaScript/IEController.js" as IECtrl
import "qrc:/Source/VentSettingArea/JavaScript/PEEPController.js" as PEEPCtrl
import "qrc:/Source/VentSettingArea/JavaScript/PsupportController.js" as PsupportCtrl
import "qrc:/Source/VentSettingArea/JavaScript/PinspController.js" as PinspCtrl
import "qrc:/Source/VentSettingArea/JavaScript/RRMechController.js" as RRMechCtrl

Rectangle {
    id: ventId
    objectName: "quickKey" + iCompID

    property bool bBlink: false
    property bool bInitialInterruptedStateStarted : false
    property bool bIsCustomBorderVisible: false  //To hide or show spinner border.Highlight (On value changed using key press)
    property bool bIsSpinnerTouchedUp
    property bool bIsSpinnerTouchedDown
    property bool bIsTimerRunning: false
    property bool bIsTotalTimerRunning: false
    property bool bIsValueChanged: false
    property bool bKeyOnePressed: false
    property bool bKeyFifthPressed: false
    property bool bKeySecondPressed: false
    property bool bKeySixthPressed: false
    property bool bKeyThreePressed: false
    property bool bModeChanged
    property bool bIsMousePressed: ventBtnMouseArea.containsPress

    property int iCompID
    property int iAccelerationInterval: 200
    property int iblinkInterval: 10000
    property int iduration: 1000
    property int itotalInterval: 30000
    property int iOneMinInactiveInterval: 60000
    property int iPixelSizelabel: 16
    property int iPixelSizeText: 48
    property int iPixelSizeUnitlabel: 16
    property int iPopUpBtnHeight
    property int iPopUpBtnWidth
    property int iPopUpBtnRadius
    property int iPopUpBtnMargin
    property int iPopUpTriHeight
    property int iPopUpTriWidth
    property int iSettingBtnValuePixelSize
    property int iSettingPopUpWidth
    property int iSettingPopUpHeight
    property int iVentStateHeightChange: 140
    property int iVentWidth: 199
    property int iVentHeight: 127
    property int iVentValuePresetFontSize: 38
    property string strTouchedStateColor: "#4565A6"
    property string strPresetStateTextColor: "#7f7f7f"
    //Spinner properties.Value are read from component creation js file.
    property int iQuickeySpinnerArrowBtnHeight: 87
    property int iQuickeySpinnerArrowBtnWidth: 100
    property int iQuickkeySpinnerAreaHeight
    property int iQuickkeySpinnerAreaWidth
    property int iQuickkeySpinnerIconHeight: 175
    property int iQuickkeySpinnerIconWidth: 100
    //End of Spinner properties

    property string strFontFamilylabel: geFont.geFontStyle()
    property string strFontColor: "#d4d4d4"
    property string strUnitFontColor: "#7f7f7f"
    property string strName
    property string strPopUpBtnColor
    property string strPopUpTriIconSource

    //For switching Bag/Vent
    property string strBtnState
    property string strVentState
    
    property string uniqueObjectID

    property string strSpinnerIconTchStateColor: "#4565a6"
    property string strSpinnerIconSelStateColor: "#616161"
    property string strSettingPopUpColor
    property string strSettingConfirmLabel
    property string strSettingBtnFontColor
    property string strSettingCancelLabel
    property string strType: "quickKeys"
    property string strUnit
    property string strValue
    property string strVentPreviousVal
    property string strVentcolor: "#2a2a2a"
    property string strQuickkeySpinnerUpEnabled
    property string strQuickkeySpinnerUpDisabled
    property string strQuickkeySpinnerDownEnabled
    property string strQuickkeySpinnerDownDisabled
    property string strQuickKeyUpArrowSource: strQuickkeySpinnerUpEnabled
    property string strQuickKeyDownArrowSource: strQuickkeySpinnerDownEnabled
    property string strVentActiveStateColor: "#2a2a2a"
    property string strVentDisabledStateColor: "#2a2a2a"
    property string strDisabledStateFontColor: "#424242"
    property string strVentEndOfScaleStateColor: "#f6f925"
    property string strVentEndOfScaleStateBorderColor: "#a89c8b"
    property string strVentScrollStateBorderColor: "#edbf00"
    property string strVentSelectedStateColor: "#616161"
    property string strVentSelectedStateBorderColor: "orange"
    property string strVentTouchedStateColor: "#b2b2a2"
    property string strVentPreviousState
    //Spinner properties
    property string strQuickkeySpinnerSelStateColor
    property string strQuickkeySpinnerTchStateColor
    property string strQuickkeySpinnerScrlStateBorderColor
    property string strQuickkeySpinnerDisStateColor
    property string strQuickkeySpinnerSelStateBorderColor
    property string strQuickkeySpinnerEoSStateColor
    property string strQuickkeySpinnerUpArrColor: strVentSelectedStateColor
    property string strQuickkeySpinnerDownArrColor: strVentSelectedStateColor
    //End of spinner properties

    signal checkEndOfScaleForSpinnerWidget(string objectName)
    signal decrementValue(string objectName, string strName)
    signal incrementValue(string objectName, string strName)
    signal selection()
    signal setDownArrowBgColor(string objectName)
    signal setUpArrowBgColor(string objectName)
    signal shortCutKeyPressed(int iCompID, string currentKey, string strType)
    signal shortCutKeyReleased(int iCompID, string currentKey)
    signal tenSecondsTimerIsTriggerred(int iCompID);
    signal destroyOpenMenu();
    signal setQKData(string mode)

    width: iVentWidth
    height: iVentHeight
    color: strVentcolor
    state:strBtnState
    Layout.preferredWidth: iVentWidth
    Layout.preferredHeight: iVentHeight
    anchors.bottom: parent.bottom

    function reset() {
        initialTimer.restart()
    }

    function stopTimers(){
        initialTimer.stop()
        finalTimer.stop()
        ventId.resetScrollSpinnerProperties()
        if (colorAnim.running === true){
            colorAnim.running = false
        }
    }
    
    function setUniqueID(uniqueName) {
        ventId.uniqueObjectID = uniqueName
        console.log("********uniqueObjectID QUICKKEY************",ventId.uniqueObjectID)
    }

    function stopFinalTimer() {
        colorAnim.running = false
        finalTimer.stop()
        //settingconfirmpopup.visible = false
        // ventId.color = strVentSelectedStateColor
        bInitialInterruptedStateStarted = false
    }
    function startFinalTimer() {
        finalTimer.start()
        //mediaPlayer.play()
        colorAnim.running = true
        bInitialInterruptedStateStarted = true
    }
    function stopInitialTimer() {
        initialTimer.stop()
        settingconfirmpopup.visible = false
    }

    function startBlinking() {
        console.log("start blinking: Interrupted state")
        ventId.resetScrollSpinnerProperties()
        settingconfirmpopup.visible = true
        initialTimer.stop()
        tenSecondsTimerIsTriggerred(iCompID)
        finalTimer.restart()
        colorAnim.running =  true
        //mediaPlayer.play()
    }

    //    function rejectTone(){
    //        mediaPlayer.play()
    //    }

    function onCancelClicked() {
        console.log("popup cancel clicked")
        settingconfirmpopup.visible = false
        ventId.resetScrollSpinnerProperties()
        colorAnim.running = false
        console.log("value after cancel"+settingconfirmpopup.visible)
        GrpCtrl.onInterruptedTimeout(objectName, strVentPreviousVal)
        initialTimer.stop()
        finalTimer.stop()
        //mediaPlayer.play();
    }

    function onConfirmClicked() {
        console.log("popup confirm clicked")
        settingconfirmpopup.visible = false
        ventId.resetScrollSpinnerProperties()
        console.log("value after confirm"+settingconfirmpopup.visible)
        GrpCtrl.onEnterPressed(objectName)
    }

    function resetScrollActiveTimer() {
        scrollActiveTimer.running = true ;
        scrollActiveTimer.restart() ;
    }

    function resetScrollSpinnerProperties() {
        if(ventId.state === "Selected" || ventId.state === "End of Scale") {
            borderLeft.visible = true
        }
        qk_up_down_arrow.color = strVentSelectedStateColor
        qk_up_down_arrow.radius = 3
        qk_up_arrow.width = iQuickeySpinnerArrowBtnWidth
        qk_up_arrow.height = iQuickeySpinnerArrowBtnHeight
        qk_up_arrow.anchors.topMargin = 0
        qk_up_arrow.anchors.leftMargin = 0
        borderBottom.width = iQuickeySpinnerArrowBtnWidth
        borderBottom.anchors.leftMargin = 0
        qk_down_arrow.width = iQuickeySpinnerArrowBtnWidth
        qk_down_arrow.height = iQuickeySpinnerArrowBtnHeight
        qk_down_arrow.anchors.bottomMargin = 0
        qk_down_arrow.anchors.leftMargin = 0
        qk_down_arrow.radius = 3
        qk_up_arrow.radius = 3
    }

    function scrollSpinnerProperties() {
        borderLeft.visible = false
        qk_up_down_arrow.color = strVentScrollStateBorderColor
        qk_up_down_arrow.radius = 3
        qk_up_arrow.width = iQuickeySpinnerArrowBtnWidth - 4
        qk_up_arrow.height = iQuickeySpinnerArrowBtnHeight - 2
        qk_up_arrow.border.width = 0
        qk_up_arrow.anchors.topMargin = 2
        qk_up_arrow.anchors.leftMargin = 2
        borderBottom.width = iQuickeySpinnerArrowBtnWidth - 4
        borderBottom.anchors.leftMargin = 2
        qk_down_arrow.width = iQuickeySpinnerArrowBtnWidth - 4
        qk_down_arrow.height = iQuickeySpinnerArrowBtnHeight - 2
        qk_down_arrow.border.width = 0
        qk_down_arrow.anchors.bottomMargin = 2
        qk_down_arrow.anchors.leftMargin = 2
        unitLabel.color = strFontColor
        qk_down_arrow.radius = 0
        qk_up_arrow.radius = 0
    }

    function restartTimers() {
        console.log("restarting initial timers")
        colorAnim.running = false
        ventId.color = strVentSelectedStateColor
        initialTimer.restart()
        finalTimer.restart()
        settingconfirmpopup.visible = false
    }

    function startInitialTimer() {
        initialTimer.start()
        settingconfirmpopup.visible = false
        ventId.resetScrollSpinnerProperties()
    }

    function stopAccelarationTimers() {
        bIsSpinnerTouchedUp = false ;
        bIsSpinnerTouchedDown = false ;
        strQuickkeySpinnerUpArrColor= strSpinnerIconSelStateColor ;
        strQuickkeySpinnerDownArrColor = strSpinnerIconSelStateColor ;
    }

    function stopBlinking() {
        settingconfirmpopup.visible = false
        colorAnim.running =  false
        ventId.resetScrollSpinnerProperties()
        initialTimer.stop()
        finalTimer.stop()
        if(ventId.state == "Selected"){
            ventId.color = strVentSelectedStateColor
        }
    }

    function stopScrollActiveTimer() {
        scrollActiveTimer.running = false ;
        scrollActiveTimer.stop() ;
    }

    //*******************************************
    // Purpose: To set the quickkey widget label based on mode change
    // Input Parameters: Updated label based on mode change
    //***********************************************/
    function setLabelofWidget(str) {
        strName=str;
    }

    //*******************************************
    // Purpose: To set the quickkey widget value based on mode change
    // Input Parameters: Updated value based on mode change
    //***********************************************/
    function setQKValue(value) {
        strValue = value
        strVentPreviousVal = value
    }

    //*******************************************
    // Purpose: To set the quickkey widget unit based on mode change
    // Input Parameters: Updated unit based on mode change
    //***********************************************/
    function setUnitofWidget(unit) {
        strUnit = unit
    }

    Rectangle{
        id: radiusmain
        width: 10
        height: parent.height
        anchors.right: parent.right
        color: strVentSelectedStateColor
        visible: false
        z: -1
    }

    Rectangle {
        id: borderLeft
        width: 1
        height: ventId.Layout.preferredHeight
        visible: false
        anchors.right:   parent.right
        anchors.bottom: parent.bottom
        color: strVentSelectedStateBorderColor
    }

    //    SoundEffect {
    //        id: mediaPlayer
    //        source: "qrc:/Config/Assets/reject.wav"
    //    }

    ColorAnimation on color {
        id: colorAnim
        from: strTouchedStateColor
        to: strVentSelectedStateColor
        duration: iduration
        running: bBlink
        loops: itotalInterval / iduration
    }

    Label {
        id: label
        text: qsTrId(strName)
        font.family: strFontFamilylabel
        font.pixelSize: iPixelSizelabel
        color: strFontColor
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
    }

    Text {
        id: text1
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        text: strValue
        font.family: strFontFamilylabel
        font.pixelSize: iPixelSizeText
        color: strFontColor
    }

    Label {
        id: unitLabel
        text: qsTrId(strUnit)
        color: strUnitFontColor
        font.family: strFontFamilylabel
        font.pixelSize: iPixelSizeUnitlabel
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
    }

    Label {
        id: currentLabel
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        text: "Current = " + strVentPreviousVal
        color : strFontColor
        font.family: strFontFamilylabel
        font.pixelSize: iPixelSizeUnitlabel
        anchors.margins: 3
        visible: false
    }
    Rectangle {
        id: qk_up_down_arrow;
        width: iQuickeySpinnerArrowBtnWidth;
        height: iQuickeySpinnerArrowBtnHeight*2 + 1 ;
        color: strVentSelectedStateColor;
        anchors.left: ventId.right;
        anchors.bottom: ventId.bottom;
        visible: false;
        Rectangle {
            id: radiusrect
            width: 10
            height: qk_up_arrow.height*2 + 1
            anchors.left: qk_up_arrow.left
            color: strQuickkeySpinnerUpArrColor
            z: -1
        }
        Rectangle {
            id:qk_up_arrow ;
            width:iQuickeySpinnerArrowBtnWidth;
            height:iQuickeySpinnerArrowBtnHeight;
            color:strQuickkeySpinnerUpArrColor;
            anchors.top:qk_up_down_arrow.top;
            anchors.left: qk_up_down_arrow.left;
            radius: 3

            Image {
                id: qk_up_arrow_image
                width: iQuickkeySpinnerIconWidth;
                height: iQuickkeySpinnerIconHeight;
                anchors.centerIn: parent
                source: strQuickKeyUpArrowSource
            }

            MouseArea {
                anchors.fill: parent
                pressAndHoldInterval: iAccelerationInterval
                hoverEnabled: true
                onClicked: {
                    console.log("incrment value for"+ventId.objectName)
                    finalTimer.restart()
                    initialTimer.restart()
                    incrementValue(ventId.objectName,ventId.strName)
                }
                onPressed: {
                    resetScrollSpinnerProperties()
                    console.log("on up arrow pressed")
                    setUpArrowBgColor(ventId.objectName)
                }
                onPressAndHold: {
                    incrementTimer.running = true ;
                    bIsSpinnerTouchedUp = true ;
                    incrementTimer.restart() ;
                }
                onReleased: {
                    ventId.strQuickkeySpinnerUpArrColor=strSpinnerIconSelStateColor ;
                    bIsSpinnerTouchedUp = false ;
                    incrementTimer.running = false ;
                    incrementTimer.stop() ;
                }

                onExited: {
                    ventId.strQuickkeySpinnerUpArrColor=strSpinnerIconSelStateColor ;
                    bIsSpinnerTouchedUp = false ;
                    incrementTimer.running = false ;
                    incrementTimer.stop() ;
                }
            }
        }

        Rectangle {
            id: borderBottom
            width: parent.width
            height: 1
            visible: true
            anchors.top:  qk_up_arrow.bottom
            anchors.left: qk_up_down_arrow.left;
            color: strVentSelectedStateBorderColor
        }

        Rectangle {
            id: qk_down_arrow;
            width: iQuickeySpinnerArrowBtnWidth;
            height: iQuickeySpinnerArrowBtnHeight;
            color: strQuickkeySpinnerDownArrColor;
            anchors.bottom: qk_up_down_arrow.bottom;
            anchors.left: qk_up_down_arrow.left
            radius: 3
            property string uniqueObjectID: "DownArrow_"+ventId.uniqueObjectID
            Image {
                id: qk_down_arrow_Image
                width: iQuickkeySpinnerIconWidth
                height: iQuickkeySpinnerIconHeight
                anchors.centerIn: parent
                source: strQuickKeyDownArrowSource
            }

            MouseArea {
                anchors.fill: parent
                pressAndHoldInterval: iAccelerationInterval
                hoverEnabled: true
                onClicked: {
                    console.log("decrement value for"+ventId.objectName)
                    finalTimer.restart()
                    initialTimer.restart()
                    decrementValue(ventId.objectName,ventId.strName)

                }
                onPressAndHold: {
                    bIsSpinnerTouchedDown = true ;
                    decrementTimer.running = true ;
                    decrementTimer.start() ;
                }
                onPressed: {
                    ventId.resetScrollSpinnerProperties()
                    console.log("on down arrow pressed")
                    setDownArrowBgColor(ventId.objectName)
                }

                onReleased: {
                    bIsSpinnerTouchedDown = false ;
                    ventId.strQuickkeySpinnerDownArrColor=strSpinnerIconSelStateColor ;
                    decrementTimer.running = false ;
                    decrementTimer.stop() ;
                }
                onExited: {
                    bIsSpinnerTouchedDown = false ;
                    ventId.strQuickkeySpinnerDownArrColor=strSpinnerIconSelStateColor ;
                    decrementTimer.running = false ;
                    decrementTimer.stop() ;
                }
            }
        }
    }

    SettingConfirmPopUp {
        id: settingconfirmpopup
        visible: false
        anchors.left: parent.left
        popUpWidth: iSettingPopUpWidth
        popUpHeight: iSettingPopUpHeight
        popUpColor: strSettingPopUpColor
        popUpBtnHeight: iPopUpBtnHeight
        popUpBtnWidth: iPopUpBtnWidth
        popUpBtnColor: strPopUpBtnColor
        popUpBtnRadius: iPopUpBtnRadius
        popUpBtnMargin: iPopUpBtnMargin
        settingConfirmLabel: strSettingConfirmLabel
        settingBtnFontColor: strSettingBtnFontColor
        settingBtnValuePixelSize: iSettingBtnValuePixelSize
        settingCancelLabel: strSettingCancelLabel
        popUpTriHeight: iPopUpTriHeight
        popUpTriWidth: iPopUpTriWidth
        popUpTriIconSource: strPopUpTriIconSource
    }

    MouseArea {
        id: ventBtnMouseArea
        anchors.fill: parent
        onReleased: {
            console.log("released....contains mouse quick key"+ventBtnMouseArea.containsMouse+ventId.uniqueObjectID)
            if(ventBtnMouseArea.containsMouse){
                console.log("state of button on clicked"+ventId.state)
                initialTimer.stop()
                if (ventId.state !== "Disabled" && ventId.state !== "PresetDisabled"){
                    GrpCtrl.onVentBtnClicked(ventId.objectName)
                    console.log("sending destroyOpenMenu onVentModeTouched")
                    destroyOpenMenu();
                }
            }
        }
    }

    Keys.onRightPressed: {
        stopAccelarationTimers() ;
        ventId.scrollSpinnerProperties()
        if(ventId.state === "Selected" || ventId.state === "End of Scale") {
            console.log("calling increment signal"+ventId.objectName+ventId.strName)
            finalTimer.restart()
            initialTimer.restart()
            incrementValue(ventId.objectName,ventId.strName)
        } else {
            GrpCtrl.onNavigateRightInVentGroup(objectName)
        }
    }

    Keys.onLeftPressed: {
        stopAccelarationTimers() ;
        ventId.scrollSpinnerProperties()
        if(ventId.state === "Selected" || ventId.state === "End of Scale") {
            console.log("calling decrement signal"+ventId.objectName+ventId.strName)
            finalTimer.restart()
            initialTimer.restart()
            decrementValue(ventId.objectName,ventId.strName);
        } else {
            GrpCtrl.onNavigateLeftInVentGroup(objectName)
        }
    }

    Keys.onReturnPressed: {
        if(ventId.state === "Active" || ventId.state === "Scroll" || ventId.state === "Preset" || ventId.state === "PresetScroll"){
            ventId.strVentPreviousState = ventId.state
        }
        ventId.resetScrollSpinnerProperties()
        GrpCtrl.onEnterPressed(objectName)
    }

    //key board Shortcuts
    Keys.onPressed: {
        event.accepted = true;
        GrpCtrl.shortcutKeysPress(event.key, objectName)
    }

    Keys.onReleased: {
        // resetScrollSpinnerProperties()
        event.accepted = true;
        GrpCtrl.shortcutKeysRelease(event.key, objectName)
        console.log("destroyOpenMenu")
        destroyOpenMenu();
    }

    states: [State{
            name: "Scroll"
            when: ventId.activeFocus && ventId.strVentState == "Vent"
            PropertyChanges {
                target: ventId; border.color: strVentScrollStateBorderColor;
                border.width: 2
                bIsCustomBorderVisible: false
            }
            PropertyChanges {
                target: unitLabel
                color : strUnitFontColor
            }
            PropertyChanges {
                target: borderLeft
                visible: false
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false ;
            }
            PropertyChanges {
                target:qk_up_down_arrow
                visible:false ;
            }},
        State{
            name: "PresetScroll"
            when: ventId.activeFocus && ventId.strVentState == "Bag"
            PropertyChanges {
                target: ventId; border.color: strVentScrollStateBorderColor;
                border.width: 2
                bIsCustomBorderVisible :false
            }
            PropertyChanges {
                target: label;
                color :strPresetStateTextColor
            }
            PropertyChanges {
                target: unitLabel
                color :strPresetStateTextColor
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }
            PropertyChanges {
                target: borderLeft
                visible : false
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false ;
            }
            PropertyChanges {
                target: text1
                text: "[ "+strValue+" ]"
                font.pixelSize: iVentValuePresetFontSize
                color: strPresetStateTextColor
            }},
        State{
            name: "Selected"
            PropertyChanges{
                target: ventId; Layout.preferredHeight: 175;
                color: strVentSelectedStateColor;
                border.color: strVentSelectedStateBorderColor ;
                z:99 ;border.width: 0;radius:3
            }

            PropertyChanges {
                target: text1
                anchors.centerIn: ventId
            }

            PropertyChanges {
                target: unitLabel
                color : strFontColor
            }
            PropertyChanges {
                target: borderLeft
                visible : true
            }
            PropertyChanges {
                target: radiusmain
                visible : true
            }
            PropertyChanges {
                target: qk_up_down_arrow
                radius: 3
                visible: true
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false
            }
            PropertyChanges {
                target: currentLabel ; visible: true
            }
            StateChangeScript {
                name: "example"
                script: {selection(); checkEndOfScaleForSpinnerWidget(ventId.objectName)}
            }
        },
        State{
            name: "Disabled"
            PropertyChanges {
                target: ventId;
                color: strVentDisabledStateColor
                border.width: 0
                bIsCustomBorderVisible: false
            }
            PropertyChanges {
                target: label;
                color: strDisabledStateFontColor
            }
            PropertyChanges {
                target: unitLabel
                color: strDisabledStateFontColor
            }
            PropertyChanges {
                target: text1
                color: strDisabledStateFontColor
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }},
        State{
            name: "Active"
            PropertyChanges {
                target: ventId;
                color: strVentActiveStateColor;
                border.width: 0
                bIsCustomBorderVisible: false

            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false
            }
            PropertyChanges {
                target:qk_up_down_arrow
                visible:false ;
            }
            PropertyChanges {
                target: borderLeft
                visible : false
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }},
        State{
            name: "Touched"
            PropertyChanges {
                target: ventId;
                color: strVentTouchedStateColor;
                border.width : 0
                bIsCustomBorderVisible: false
            }
            PropertyChanges {
                target: qk_up_arrow ; color: strVentTouchedStateColor
            }

            PropertyChanges {
                target: qk_down_arrow ; color: strVentTouchedStateColor
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false
            }
            PropertyChanges {
                target: borderLeft
                visible : false
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }},
        State{
            name: "PresetTouched"
            PropertyChanges {
                target: ventId;
                color: strVentTouchedStateColor;
                border.width : 0
                bIsCustomBorderVisible :false
            }
            PropertyChanges {
                target: qk_up_arrow ; color:strVentTouchedStateColor
            }
            PropertyChanges {
                target: qk_down_arrow ; color:strVentTouchedStateColor
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false
            }
            PropertyChanges {
                target: text1
                text: "[ "+strValue+" ]"
                font.pixelSize: iVentValuePresetFontSize
                color: strPresetStateTextColor
            }
            PropertyChanges {
                target: borderLeft
                visible : false
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }},
        State{
            name: "Preset"
            PropertyChanges {
                target: ventId;
                border.width : 0
                bIsCustomBorderVisible :false
            }
            PropertyChanges {
                target: text1
                text: "[ "+strValue+" ]"
                font.pixelSize: iVentValuePresetFontSize
                color:strPresetStateTextColor
            }
            PropertyChanges {
                target: label;
                color :strPresetStateTextColor
            }
            PropertyChanges {
                target: unitLabel
                color :strPresetStateTextColor
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false
            }
            PropertyChanges {
                target: borderLeft
                visible : false
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }},
        State{
            name: "PresetDisabled"
            extend: "Disabled"
            PropertyChanges {
                target: text1
                text: "[ "+strValue+" ]"
                font.pixelSize: iVentValuePresetFontSize
            }
            PropertyChanges {
                target: borderLeft
                visible : false
            }
            PropertyChanges {
                target: radiusmain
                visible : false
            }},
        State{
            name: "End of Scale"
            PropertyChanges {
                target: ventId; Layout.preferredHeight: 175;
                color: strVentSelectedStateColor;
                border.color: strVentSelectedStateBorderColor ;
                z:99 ;border.width: 0;radius:3
            }
            PropertyChanges {
                target: borderLeft
                visible : true
            }
            PropertyChanges {
                target: radiusmain
                visible : true
            }
            PropertyChanges {
                target: qk_up_down_arrow
                visible: true ;
            }
            PropertyChanges {
                target: settingconfirmpopup; visible: false
            }
            PropertyChanges {
                target: unitLabel
                color: strFontColor
            }
            PropertyChanges {
                target: currentLabel ; visible: true
            }
        }
    ]

    Timer {
        id: initialTimer
        running:bIsTimerRunning
        repeat: false
        interval: iblinkInterval     //blinkInterval
        onTriggered: {
            console.log("Initial Timer trigerred->>>>>>>")
            //On timer expiration , make custom border invisible
            console.log("inside initial timer")
            ventId.resetScrollSpinnerProperties()
            if (strVentPreviousVal !== strValue || bModeChanged === true) {
                //mediaPlayer.play()
                finalTimer.restart()
                colorAnim.running = true
                bInitialInterruptedStateStarted = true
                settingconfirmpopup.visible = true

            } else {
                console.log("no value chnaged in 10 seconds"+strVentPreviousVal+":"+strValue)
                GrpCtrl.onNoValueChangeInTenSeconds(ventId.objectName)
            }
            console.log("bmodeChange value.."+bModeChanged)
            if(ventId.state === "Selected" || ventId.state === "End of Scale") {
                if (bModeChanged === true) {
                    settingconfirmpopup.visible = false
                } else if(bModeChanged === false) {
                    settingconfirmpopup.visible = true
                    //mediaPlayer.play()
                }
            }
        }
    }

    Timer {
        id: finalTimer
        running:  bIsTotalTimerRunning
        repeat:false
        interval: itotalInterval
        onTriggered: {
            //On timer expiration , make custom border invisible
            console.log("inside final timer")
            ventId.resetScrollSpinnerProperties()
            GrpCtrl.onInterruptedTimeout(ventId.objectName,strVentPreviousVal)
            initialTimer.stop()
            finalTimer.stop()
            //mediaPlayer.play()
            settingconfirmpopup.visible = false
            console.log("Modechange bool is 4: " + bModeChanged)
            if( bModeChanged === true) {
                GrpCtrl.resetPrimaryValues()
                bModeChanged = false
                bInitialInterruptedStateStarted = false
            }
        }
    }

    Timer {
        id: scrollActiveTimer
        running: false
        repeat: false
        interval: iOneMinInactiveInterval ; // 1 min of inactivity
        onTriggered: {
            //On timer expiration , make custom border invisible
            ventId.resetScrollSpinnerProperties()
            GrpCtrl.onScrollActiveTimeOut(ventId.objectName)
        }
    }

    Timer {
        id: incrementTimer
        running: false
        repeat: true
        interval: iAccelerationInterval
        onTriggered: {
            setUpArrowBgColor(ventId.objectName)
            incrementValue(ventId.objectName,ventId.strName)
        }
    }

    Timer {
        id: decrementTimer
        running: false
        repeat: true
        interval: iAccelerationInterval
        onTriggered: {
            setDownArrowBgColor(ventId.objectName)
            decrementValue(ventId.objectName,ventId.strName)
        }
    }

    onFocusChanged: {
        if(ventId.focus === false) {
            if (ventId.strVentState === "Vent") {
                ventId.state = "Active"
            } else if (ventId.strVentState === "Bag"){
                ventId.state = "Preset"
            }
            ventId.stopScrollActiveTimer() ;
            console.log("stop scroll active timer of quick keys since focus is false on"+iCompID)
        }
    }

    onStrValueChanged: {
        settingconfirmpopup.visible = false
        if((state === "Selected"  || state === "End of Scale") ) {
            if(strVentPreviousVal != strValue && !bModeChanged){
                ventId.bIsValueChanged = true ;
                finalTimer.restart()
                initialTimer.restart()
            } else {
                ventId.bIsValueChanged = false ;
                initialTimer.restart()
                stopFinalTimer()
            }
            colorAnim.running = false
            ventId.color = strVentSelectedStateColor
            bInitialInterruptedStateStarted = false
        }
        checkEndOfScaleForSpinnerWidget(ventId.objectName)
    }

    onStateChanged: {
        if(ventId.state === "Scroll" || ventId.state === "PresetScroll") {
            ventId.resetScrollActiveTimer()
        } else if(ventId.state === "Selected" || ventId.state === "End of Scale" || ventId.state === "Active"  || ventId.state ==="Disabled"
                  || ventId.state === "Preset" || ventId.state === "PresetDisabled") {
            ventId.stopScrollActiveTimer() ;
        }
        if(state === "Selected" || state === "End of Scale") {
            checkEndOfScaleForSpinnerWidget(ventId.objectName)
        }
    }
    onBIsCustomBorderVisibleChanged: {
        if(ventId.bIsCustomBorderVisible) {
            scrollSpinnerProperties()
        } else {
            resetScrollSpinnerProperties()
        }
    }

    onBIsSpinnerTouchedUpChanged:  {
        if (bIsSpinnerTouchedUp === false) {
            incrementTimer.stop() ;
        }
    }

    onBIsSpinnerTouchedDownChanged: {
        if(bIsSpinnerTouchedDown === false) {
            decrementTimer.stop() ;
        }
    }

    onBIsMousePressedChanged:{
        console.log("mouse press changed..."+ventBtnMouseArea.containsPress+ventId.uniqueObjectID)
        if(ventBtnMouseArea.containsPress) {
            if(ventId.state === "Active" || ventId.state === "Scroll" || ventId.state === "Preset" || ventId.state === "PresetScroll"){
                ventId.strVentPreviousState = ventId.state
            }
            console.log("state of button on pressed"+ventId.state)
            ventId.resetScrollSpinnerProperties()
            if (ventId.state !== "Disabled" && ventId.state !== "PresetDisabled"){
                GrpCtrl.onVentBtnTouched(ventId.objectName)
            } else if (ventId.state === "Disabled" || ventId.state === "PresetDisabled" ) {
                //mediaPlayer.play()
            }
        } else if (!ventBtnMouseArea.containsPress) {
            if(ventId.state == "Touched"){
                ventId.state = ventId.strVentPreviousState
            }
        }
    }

    Component.onCompleted: {
        settingconfirmpopup.cancelClicked.connect(onCancelClicked)
        settingconfirmpopup.confirmClicked.connect(onConfirmClicked)
    }
}

