#include "../Source/VentSettingArea/Include/paramdualstorage.h"
#include <QDebug>

ParamDualStorage::ParamDualStorage(bool dualStorageMismatch):m_bDualStorageMismatch(dualStorageMismatch)
{
    initializeEnum();
}

/**
 * @brief ParamDualStorage::init - Stores the primary data from settings file as one's
 * complement (critical parameters) to the backup param
 * @param mode
 * @param rr
 * @param ie
 * @param tv
 * @param peep
 * @param pinsp
 * @param pmax
 * @param backupTime
 */
void ParamDualStorage::init(QString mode, int rr, QString ie, int tv, QString peep, int pinsp, int pmax, int backupTime)
{
    m_bk_MMode =~(setMode(mode));
    //Converting String values to integer
    //RR
    m_bk_RR = ~rr;
    //TV
    m_bk_TV = ~tv;
    //PEEP
    if (peep == "Off"){
        m_bk_Peep = ~(0);
    }
    else {
        m_bk_Peep = ~(peep.toInt());
    }
    //backuptime
    m_bk_BackupTime = ~(backupTime);

    //PINSP
    m_bk_pinsp = ~pinsp;
    //PMAX
    m_bk_pmax = ~pmax;
    //IE
    if(ie == "2:1"){
        m_bk_IE = ~(5);
    }
    else {
        QRegExp rx("(\\:)");
        QStringList inspExp = ie.split(rx);
        Q_ASSERT(inspExp.count()>1);
        float ieValue = inspExp[1].toFloat();
        m_bk_IE = ~(int (ieValue * 10));
    }
}

/**
 * @brief ParamDualStorage::storeCriticalParams - Stores the critical parameters as one's complement
 * whenever the value is confirmed from UI.
 * @param id - Quickkey name(eg.TV_qKey)
 * @param value -Corresponding value of the quickkey
 */
void ParamDualStorage::storeCriticalParams(QString id, QString value)
{
    CriticalParams param;
    if (criticalParamsList.count(id.toUtf8().constData())>0){
        param = criticalParamsList.at(id.toUtf8().constData());
    } else {
        return;
    }
    switch (param) {
    case CriticalParams::MODE:
    {
        m_bk_MMode =~(setMode(value));
    }
        break;
    case CriticalParams::TV_qkey:
    {
        m_bk_TV = ~(value.toInt());
    }
        break;
    case CriticalParams::RR_qkey:
    {
        m_bk_RR = ~(value.toInt());
    }
        break;
    case CriticalParams::IE_qkey:
    {
        if(value == "2:1"){
            m_bk_IE = ~(5);
        }
        else {
            QRegExp rx("(\\:)");
            QStringList inspExp = value.split(rx);
            Q_ASSERT(inspExp.count()>1);
            float ieValue = inspExp[1].toFloat();
            m_bk_IE = ~(int (ieValue * 10));
        }
    }
        break;
    case CriticalParams::PEEP_qkey:
    {
        if (value == "Off"){
            m_bk_Peep = ~(0);
        }
        else {
            m_bk_Peep = ~(value.toInt());
        }
    }
        break;
    case CriticalParams::Pinsp_qkey:
    {
        m_bk_pinsp = ~(value.toInt());
    }
        break;
    case CriticalParams::Pmax_sKey:
    {
        m_bk_pmax = ~(value.toInt());
    }
        break;
    case CriticalParams::BackupTime_sKey:
    {
        m_bk_BackupTime = ~(value.toInt());
    }
        break;
    }
}

/**
 * @brief ParamDualStorage::validateParams -Used to validate the primary copy against the backup/secondary copy.
 * @param id - Quickkey name
 * @param value - Corresponding value of the Quickkey
 */
void ParamDualStorage::validateParams(QString id, QString value)
{
    CriticalParams param;
    if (criticalParamsList.count(id.toUtf8().constData())>0){
        param = criticalParamsList.at(id.toUtf8().constData());
    } else {
        return;
    }
    switch (param) {
    case CriticalParams::MODE:
    {
        int mode_value = setMode(value);
        dualStorageMisMatch(mode_value,m_bk_MMode);
    }
        break;
    case CriticalParams::TV_qkey:
    {
        int tv_value = value.toInt();
        dualStorageMisMatch(tv_value,m_bk_TV);
    }
        break;
    case CriticalParams::RR_qkey:
    {
        int rr_value = value.toInt();
        dualStorageMisMatch(rr_value,m_bk_RR);
    }
        break;
    case CriticalParams::IE_qkey:
    {
        int ie_value;
        if(value == "2:1"){
            ie_value = 5;
        }
        else {
            QRegExp rx("(\\:)");
            QStringList inspExp = value.split(rx);
            Q_ASSERT(inspExp.count()>1);
            float ieValue = inspExp[1].toFloat();
            ie_value = int(ieValue * 10);
        }
        dualStorageMisMatch(ie_value,m_bk_IE);
    }
        break;
    case CriticalParams::PEEP_qkey:
    {
        int peep_value;
        if (value == "Off"){
            peep_value = 0;
        }
        else {
            peep_value = value.toInt();
        }
        dualStorageMisMatch(peep_value,m_bk_Peep);
    }
        break;
    case CriticalParams::Pinsp_qkey:
    {
        int pinsp_value = value.toInt();
        dualStorageMisMatch(pinsp_value,m_bk_pinsp);
    }
        break;
    case CriticalParams::Pmax_sKey:
    {
        int pmax_value = value.toInt();
        dualStorageMisMatch(pmax_value,m_bk_pmax);
    }
        break;
    case CriticalParams::BackupTime_sKey:
    {
        int backup_value = 0 ;
        if (value == "Off"){
            backup_value = 0;
        }
        else {
            backup_value = value.toInt();
        }
        dualStorageMisMatch(backup_value,m_bk_BackupTime);
    }
        break;
    }
}

/**
 * @brief ParamDualStorage::dualStorageMisMatch - To check whether primry and secondary copies are same and update
 * the m_bDualStorageMismatch flag if there is any mismatch
 * @param primary - primary copy's value
 * @param secondary - backup copy's one's complement value
 */
void ParamDualStorage::dualStorageMisMatch(int primary, int secondary)
{
    if((m_guardband_1 != 0x11223344) || (m_guardband_2 != 0x55667788) || (~secondary!= primary)){
        m_bDualStorageMismatch = true;
        qDebug()<<"Error"<<endl;
    } else {
        qDebug()<<"No Mismatch!!"<<endl;
    }
}

int ParamDualStorage::getBkMechMode()
{
    return m_bk_MMode;
}

int ParamDualStorage::getBkTvValue()
{
    return m_bk_TV;
}

int ParamDualStorage::getBkRrValue()
{
    return m_bk_RR;
}

int ParamDualStorage::getBkIeValue()
{
    return m_bk_IE;
}

int ParamDualStorage::getBkPeepValue()
{
    return m_bk_Peep;
}

int ParamDualStorage::getBkPinspValue()
{
    return m_bk_pinsp;
}

int ParamDualStorage::getBkPmaxValue()
{
    return m_bk_pmax;
}

bool ParamDualStorage::getIsDualStorageMismatch()
{
    return m_bDualStorageMismatch;
}

int ParamDualStorage::getBk_BackupTime() const
{
    return m_bk_BackupTime;
}


/**
 * @brief ParamDualStorage::setMode - Gets the mode name from UI and map it with the enum in this class
 * @param mode - Mode name returned by UI
 * @return
 */
int ParamDualStorage::setMode(QString mode)
{
    int mode_value = -1;
    //Mode data
    if (mode == VCV_MODE){
        mode_value = MachineMode::VCV;
    }
    else if(mode == PCV_MODE){
        mode_value = MachineMode::PCV;
    }
    else if(mode == SIMVVCV_MODE){
        mode_value = MachineMode::SIMVVC;
    }
    else if(mode == SIMVPCV_MODE){
        mode_value = MachineMode::SIMVPC;
    }
    else if(mode == PSVPRO_MODE){
        mode_value = MachineMode::PSVPRO;
    }
    return mode_value;
}

void ParamDualStorage::initializeEnum()
{
    criticalParamsList[TV_QKEY] = CriticalParams::TV_qkey;
    criticalParamsList[RR_QKEY] = CriticalParams::RR_qkey;
    criticalParamsList[IE_QKEY] = CriticalParams::IE_qkey;
    criticalParamsList[PEEP_QKEY] = CriticalParams::PEEP_qkey;
    criticalParamsList[PINSP_QKEY] = CriticalParams::Pinsp_qkey;
    criticalParamsList[PMAX_SKEY] = CriticalParams::Pmax_sKey;
    criticalParamsList[BACKUPTIME_SKEY] = CriticalParams::BackupTime_sKey;
    criticalParamsList[VENT_MODE] = CriticalParams::MODE;
}


