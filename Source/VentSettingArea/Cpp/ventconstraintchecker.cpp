#include "../Source/VentSettingArea/Include/ventconstraintchecker.h"
#include "../Source/VentSettingArea/Include/settingfileupdate.h"

VentConstraintChecker::VentConstraintChecker()
{
    INT_FIVE_CONSTANT = 5;
    INT_TEN_CONSTANT = 10;
    IE_Min = 0.5;
    IE_Max = 8;
    TV_MIN_PEEP = 100;
    TV_Max = 1500;
    INT_TWENTY_CONSTANT = 20;
    TV_300_RANGE = 300;
    TV_1000_RANGE = 1000;
    INT_TWENTYFIVE_CONSTANT = 25;
    INT_FIFTY_CONSTANT = 50;
    TexpMin = 0.4f;
    Tinsp_Min = 0.2f;
    Tinsp_Max = 5.0f;
    m_modeChangeAction = false;
    count = 0;
}

/**
 * @brief VentConstraintChecker::setSettingReader - initialize constraint parameters to previous set values
 * @param settings
 */
void VentConstraintChecker::setSettingReader(SettingFileUpdate *settings)
{
    objSettingFile = settings;
    QString c_mode = objSettingFile->getMode();
    int tv = objSettingFile->getTv();
    int rr = objSettingFile->getRr();
    int m_rr = objSettingFile->getRrMech();
    QString ie = objSettingFile->getIe();
    QString peep = objSettingFile->getPeep() ;
    float tinsp = objSettingFile->getTinsp();
    int tpause = objSettingFile->getTpause();
    int pmax = objSettingFile->getPmax();
    int pinsp = objSettingFile->getPinsp();
    int psupport = objSettingFile->getPsupport();
    int ppeeklow = objSettingFile->getPpeaklow();
    float ieValue;
    auto parts = c_mode.split('_');
    auto ie_Value = ie.split(':');
    m_mode = parts[1];
    m_uiTV = tv;
    m_uiRR = rr;
    m_uiSpontRR = m_rr;
    if(ie == "2:1")
    {
        ieValue = 5;
        m_uiIE = int(ieValue);
    }
    else
    {
        QString ie_temp = ie_Value[1];
        ieValue = ie_temp.toFloat() * 10;
        m_uiIE = int(ieValue);
    }
    if(peep == "Off")
    {
        m_uiPEEP = 0;
    }
    else
    {
        m_uiPEEP = peep.toInt();
    }
    m_uiTpause = tpause;
    m_uiPMax = pmax;
    m_uiTinsp = tinsp;
    m_uiPinsp = pinsp;
    m_uiPsupport = psupport;
    m_uiPpeakLow = ppeeklow;
}

/**
 * @brief VentConstraintChecker::init - set the constraint parameters on inital bootup
 */
void VentConstraintChecker::init()
{
    //On inital bootup
    constraintChecker(true);
}

/**
 * @brief VentConstraintChecker::setPhysicalParamReader - read the min and max value from physicalparam file
 * @param physical
 */
void VentConstraintChecker::setPhysicalParamReader(PhysicalParamDomReader* physical)
{
    physicalParam = physical;
    QStringList tvRange = physicalParam->getRangeData("TV","ml");
    TV_Min = (tvRange[0]).toInt();
    QStringList rrRange = physicalParam->getRangeData("RR", "/min");
    rr_Min = (rrRange[0]).toInt();
    rr_Max = (rrRange[1]).toInt();
    QStringList rrMechRange = physicalParam->getRangeData("RRMech", "/min");
    rrMech_Min = (rrMechRange[0]).toInt();
    rrMech_Max = (rrMechRange[1]).toInt();
    QStringList peepRange = physicalParam->getRangeData("PEEP", "cmH2O");
    peep_Max = (peepRange[1]).toInt();
    QStringList pMaxRange = physicalParam->getRangeData("Pmax", "CmH2o");
    pmax_Min = (pMaxRange[0]).toInt();
    pmax_Max = (pMaxRange[1]).toInt();
    QStringList tPauseRange = physicalParam->getRangeData("TPause", "%");
    tpause_Min = 0;
    tpause_Max = (tPauseRange[1]).toInt();
    Tinsp_Min = 0.2f;
    Tinsp_Max = 5.0f;
    TV_Max = 1500;
    IE_Min = 0.5;
    IE_Max = 8;
}

/**
 * @brief VentConstraintChecker::setCurrentMode - update current Mode
 * @param currentMode - Current mode name returned by UI
 */
void VentConstraintChecker::setCurrentMode(QString currentMode)
{
    m_mode = currentMode;
}

/**
 * @brief VentConstraintChecker::setPrevMode - update previous Mode
 * @param prevMode - prev mode name returned by UI
 */
void VentConstraintChecker::setPrevMode(QString prevMode)
{
    auto parts = prevMode.split('_');
    m_prevMode = parts[1];
}

/**
 * @brief VentConstraintChecker::getTvMIN - Get the Tv min value
 * @param tv_val - Tv Min value
 */
void VentConstraintChecker::getTvMIN(int tv_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    if (m_uiPEEP > INT_TEN_CONSTANT) {
        tv_val = TV_MIN_PEEP;
    }
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TV, tv_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_tvFinalMIN = tv_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
        tv_val = increaseVT(tv_val, false);
        getTvMIN(tv_val);
        break;
    case Constraint_Type::TINSP_CONSTRAINT:
        break;
    case Constraint_Type::TEXP_CONSTRAINT:
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getTvMAX - Get the Tv Max value
 * @param tv_val - Tv Max value
 */
void VentConstraintChecker::getTvMAX(int tv_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TV, tv_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_tvFinalMAX = tv_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
        tv_val = decreaseVT(tv_val, false);
        getTvMAX(tv_val);
        break;
    case Constraint_Type::TINSP_CONSTRAINT:
        break;
    case Constraint_Type::TEXP_CONSTRAINT:
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getFinalTvMIN - set the Tv Min value
 * @return
 */
int VentConstraintChecker::getFinalTvMIN()
{
    getTvMIN(TV_Min);
    return m_tvFinalMIN;
}

/**
 * @brief VentConstraintChecker::getFinalTvMAX - set the Tv Max value
 * @return
 */
int VentConstraintChecker::getFinalTvMAX()
{
    getTvMAX(TV_Max);
    return m_tvFinalMAX;
}

/**
 * @brief VentConstraintChecker::getRRMIN - Get the RR Min value
 * @param rr_val - RR Min value
 */
void VentConstraintChecker::getRRMIN(int rr_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::RR, rr_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_rrFinalMIN = rr_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        rr_val = rr_val + 1;
        getRRMIN(rr_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getRRMAX - Get the RR Max value
 * @param rr_val - RR Max value
 */
void VentConstraintChecker::getRRMAX(int rr_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::RR, rr_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_rrFinalMAX = rr_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        rr_val = rr_val - 1;
        getRRMAX(rr_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getFinalRRMIN - set the RR Min value
 * @return
 */
int VentConstraintChecker::getFinalRRMIN()
{
    getRRMIN(rr_Min);
    return m_rrFinalMIN;
}

/**
 * @brief VentConstraintChecker::getFinalRRMAX - set the RR Max value
 * @return
 */
int VentConstraintChecker::getFinalRRMAX()
{
    getRRMAX(rr_Max);
    return m_rrFinalMAX;
}

/**
 * @brief VentConstraintChecker::getSpontRRMIN - Get the RRMech Min value
 * @param rr_val
 */
void VentConstraintChecker::getSpontRRMIN(int rr_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::RR, rr_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_spontRRFinalMIN = rr_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        rr_val = rr_val + 1;
        getSpontRRMIN(rr_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getSpontRRMAX - Get the RRMech Max value
 * @param rr_val
 */
void VentConstraintChecker::getSpontRRMAX(int rr_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::RR, rr_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        if(rr_val > 60){
            m_spontRRFinalMAX = 60;
        }else {
            m_spontRRFinalMAX = rr_val;
        }
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        rr_val = rr_val - 1;
        getSpontRRMAX(rr_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getFinalSpontRRMIN - set the RRMech Min value
 * @return
 */
int VentConstraintChecker::getFinalSpontRRMIN()
{
    getSpontRRMIN(rrMech_Min);
    return m_spontRRFinalMIN;
}

/**
 * @brief VentConstraintChecker::getFinalSpontRRMAX - set the RRMech Max value
 * @return
 */
int VentConstraintChecker::getFinalSpontRRMAX()
{
    getSpontRRMAX(rrMech_Max);
    return m_spontRRFinalMAX;
}

/**
 * @brief VentConstraintChecker::getIEMIN - Get the IE Min value
 * @param ie_val
 */
void VentConstraintChecker::getIEMIN(float ie_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    int temp_ie = int(ie_val * 10);
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::IE, temp_ie);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_ieFinalMIN = temp_ie/10.0f;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        ie_val = float((temp_ie / 10.0) + 0.5);
        getIEMIN(ie_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getIEMAX - Get the IE Max value
 * @param ie_val
 */
void VentConstraintChecker::getIEMAX(float ie_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    int temp_ie = int(ie_val * 10);
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::IE, temp_ie);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_ieFinalMAX = temp_ie/10.0f;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        ie_val = float((temp_ie / 10.0) - 0.5);
        getIEMAX(ie_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getFinalIEMIN - set the IE Min value
 * @return
 */
QString VentConstraintChecker::getFinalIEMIN()
{
    getIEMIN(IE_Min);
    QString temp_ie;
    if(int(m_ieFinalMIN * 10 ) == int(IE_Min * 10)){
        temp_ie = "2:1";
    }else{
        temp_ie = QString::number(1) + ":" +  QString::number(m_ieFinalMIN);
    }
    return temp_ie;
}

/**
 * @brief VentConstraintChecker::getFinalIEMAX - set the IE Max value
 * @return
 */
QString VentConstraintChecker::getFinalIEMAX()
{
    getIEMAX(IE_Max);
    QString   temp_ie = QString::number(1) + ":" +  QString::number(m_ieFinalMAX);
    return temp_ie;
}

/**
 * @brief VentConstraintChecker::getPeepMAX - Get the Peep Max value
 * @param peep_val
 */
void VentConstraintChecker::getPeepMAX(int peep_val)
{
    m_peepFinalMAX = peep_val;
    if (m_mode == "VCV" || m_mode == "SIMVVCV")
    {
        if (m_uiTV < TV_MIN_PEEP)
        {
            m_peepFinalMAX = INT_TEN_CONSTANT;
        }
    }
    int tPmax = m_uiPMax - 5;
    if (m_peepFinalMAX > tPmax){
        m_peepFinalMAX = tPmax;
    }
}

/**
 * @brief VentConstraintChecker::getFinalPeepMAX - set the Peep Max value
 * @return
 */
int VentConstraintChecker::getFinalPeepMAX()
{
    getPeepMAX(peep_Max);
    return m_peepFinalMAX;
}

/**
 * @brief VentConstraintChecker::getTpauseMIN - Get the Tpause Min value
 * @param tp_val
 */
void VentConstraintChecker::getTpauseMIN(int tp_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TPAUSE, tp_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_tpFinalMIN = tp_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        tp_val = tp_val + 5;
        getTpauseMIN(tp_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getTpauseMAX - Get the Tpause Max value
 * @param tp_val
 */
void VentConstraintChecker::getTpauseMAX(int tp_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TPAUSE, tp_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_tpFinalMAX = tp_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        tp_val = tp_val - 5;
        getTpauseMAX(tp_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getFinalTpauseMIN - set the Tpause Min value
 * @return
 */
int VentConstraintChecker::getFinalTpauseMIN()
{
    getTpauseMIN(tpause_Min);
    return  m_tpFinalMIN;
}

/**
 * @brief VentConstraintChecker::getFinalTpauseMAX - set the Tpause Max value
 * @return
 */
int VentConstraintChecker::getFinalTpauseMAX()
{
    getTpauseMAX(tpause_Max);
    return  m_tpFinalMAX;
}

/**
 * @brief VentConstraintChecker::getPmaxMIN - Get the Pmax Min value
 * @param pmax_val
 */
void VentConstraintChecker::getPmaxMIN(int pmax_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::PMAX, pmax_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_pmaxFinalMIN = pmax_val;
        break;
    case Constraint_Type::PMAX_CONSTARINT:
        pmax_val = pmax_val + 1;
        getPmaxMIN(pmax_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getPmaxMAX - Get the Pmax Max value
 * @param pmax_val
 */
void VentConstraintChecker::getPmaxMAX(int pmax_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::PMAX, pmax_val);
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_pmaxFinalMAX = pmax_val;
        break;
    case Constraint_Type::PMAX_CONSTARINT:
        pmax_val = pmax_val - 1;
        getPmaxMAX(pmax_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getFinalPmaxMIN - Get the Pmax Min value
 * @return
 */
int VentConstraintChecker::getFinalPmaxMIN()
{
    int pPeakLow = (objSettingFile->read("Ppeak_Low")).toInt();
    if(pPeakLow < 10) {
        pPeakLow = 10;
    }
    getPmaxMIN(pPeakLow);
    return m_pmaxFinalMIN;
}

/**
 * @brief VentConstraintChecker::getFinalPmaxMAX - set the Pmax Min value
 * @return
 */
int VentConstraintChecker::getFinalPmaxMAX()
{
    getPmaxMAX(pmax_Max);
    return  m_pmaxFinalMAX;
}

/**
 * @brief VentConstraintChecker::getTinspMIN - Get the Tinsp Min value
 * @param tinsp_val
 */
void VentConstraintChecker::getTinspMIN(float tinsp_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TINSP, int(tinsp_val * 10));
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_TinspFinalMIN = tinsp_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        tinsp_val = float(tinsp_val) + float(0.1);
        getTinspMIN(tinsp_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getTinspMAX - Get the Tinsp Max value
 * @param tinsp_val
 */
void VentConstraintChecker::getTinspMAX(float tinsp_val)
{
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;

    constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TINSP, int(tinsp_val * 10));
    switch (constraint_sts)
    {
    case Constraint_Type::NO_CONSTRAINT:
        m_TinspFinalMAX = tinsp_val;
        break;
    case Constraint_Type::FLOW_MIN_CONSTRAINT:
    case Constraint_Type::FLOW_MAX_CONSTRAINT:
    case Constraint_Type::TINSP_CONSTRAINT:
    case Constraint_Type::TEXP_CONSTRAINT:
        tinsp_val = float(tinsp_val) - float(0.1);
        getTinspMAX(tinsp_val);
        break;
    default:
        break;
    }
}

/**
 * @brief VentConstraintChecker::getFinalTinspMIN - set the Tinsp Min value
 * @return
 */
float VentConstraintChecker::getFinalTinspMIN()
{
    getTinspMIN(Tinsp_Min);
    return m_TinspFinalMIN;
}

/**
 * @brief VentConstraintChecker::getFinalTinspMAX - set the Tinsp Max value
 * @return
 */
float VentConstraintChecker::getFinalTinspMAX()
{
    getTinspMAX(Tinsp_Max);
    return m_TinspFinalMAX;
}

/**
 * @brief VentConstraintChecker::utilitiesConstraintChecker - Checks contraints by taking the edited value as input and
 * returns whether that is causing any constraint.
 * @param constraintParamenter - Current constraint checker parameter that is being evaluated
 * @param value - constraint value
 * @return
 */
VentConstraintChecker::Constraint_Type VentConstraintChecker::utilitiesConstraintChecker(Constraint_Parameter constraintParamenter,int value)
{
    qDebug() << "Mode is" << m_mode <<endl;
    qDebug() << "constraintParamenter" << constraintParamenter << endl;
    if ((constraintParamenter != Constraint_Parameter::TV) && (constraintParamenter != Constraint_Parameter::TPAUSE) &&
            (constraintParamenter != Constraint_Parameter::IE) && (constraintParamenter != Constraint_Parameter::RR) &&
            (constraintParamenter != Constraint_Parameter::TINSP) && (constraintParamenter != Constraint_Parameter::PMAX))
    {
        return Constraint_Type::INVALID;
    }
    float inspFlow = 0.0f;
    float temp_Texp = 0.0f;
    float temp_Tinsp = float((constraintParamenter == Constraint_Parameter::TINSP) ? float(value / 10.0f) : float(m_uiTinsp));
    int temp_rr = m_uiRR;

    //copying rr to rrMech
    if (m_mode == "SIMVVCV" || m_mode == "SIMVPCV" || m_mode == "PSVPro")
    {
        temp_rr = m_uiSpontRR;
        qDebug() << "m_uiSpontRR :" <<m_uiSpontRR;
    }

    int tv = ((constraintParamenter == Constraint_Parameter::TV) ? int(value) : m_uiTV);
    int IE = ((constraintParamenter == Constraint_Parameter::IE) ? int(value) : m_uiIE);
    int rr = ((constraintParamenter == Constraint_Parameter::RR) ? int(value) : temp_rr);
    int tPause = ((constraintParamenter == Constraint_Parameter::TPAUSE) ? int(value) : m_uiTpause);
    int pmax = ((constraintParamenter == Constraint_Parameter::PMAX) ? int(value) : m_uiPMax);

    float cycletime = float(60.0f / rr);

    if (m_mode == "VCV" || m_mode == "PCV")
    {
        temp_Tinsp = float(cycletime * 10.0f / (IE + 10.0f));
    }

    temp_Texp = (float(int(cycletime * 1000.0f) - int(temp_Tinsp * 1000.0f))) / 1000.0f;

    if (m_mode == "VCV" || m_mode == "SIMVVCV")
    {
        temp_Tinsp = float(temp_Tinsp - ((temp_Tinsp * tPause) / 100.0f));
        if(int(temp_Tinsp * 10) < 0){
            return Constraint_Type::INVALID;
        }
        qDebug() << "temp_Tinsp in VCV and SIMVVCV mode" << temp_Tinsp;
    }

    //Time constraint - inspiration and expiration
    if (int(temp_Tinsp * 10) < int(qRound(Tinsp_Min * 10)))
    {
        return Constraint_Type::TINSP_CONSTRAINT;
    }

    if (int(temp_Texp * 10) < int(qRound(TexpMin * 10)))
    {
        return Constraint_Type::TEXP_CONSTRAINT;
    }

    //Flow  constraint
    if (m_mode == "VCV" || m_mode == "SIMVVCV")
    {
        inspFlow = float((tv * 60.0f) / (temp_Tinsp * 1000.0f));
        QString temp_inspFlow;
        temp_inspFlow.setNum(inspFlow, 'f' ,2);
        inspFlow = (temp_inspFlow).toFloat();

        if (inspFlow < 1.0f)
        {
            return Constraint_Type::FLOW_MIN_CONSTRAINT;
        }
        else if (inspFlow > 70.0f)
        {
            return Constraint_Type::FLOW_MAX_CONSTRAINT;
        }
    }

    // PMAX Constraint
    if (constraintParamenter == Constraint_Parameter::PMAX)
    {
        int min = 10;
        int tPmin = m_uiPpeakLow + 1; //update the LowPpeak value from ui
        int tPeep = m_uiPEEP + 5;

        if (tPmin > min)
        {
            min = tPmin;
        }
        if (min <= tPeep)
        {
            min = tPeep;
        }

        if (pmax < min)
        {
            return Constraint_Type::PMAX_CONSTARINT;
        }
    }
    return Constraint_Type::NO_CONSTRAINT;
}

/**
 * @brief VentConstraintChecker::updateConstraintParam - Checks contraints during Mode Change Operation and initial load
 * @param id
 * @param value
 */
void VentConstraintChecker::updateConstraintParam(QString id, QString value)
{
    qDebug() << "id inital" <<id;
    qDebug() << "value inital" <<value;
    if(id == "TV_qKey")
    {
        m_uiTV = value.toInt();
    }
    else if(id == "Pinsp_qKey")
    {
        m_uiPinsp = value.toInt();
    }
    else if(id == "RR_qKey")
    {
        if( m_mode == "VCV" || m_mode == "PCV" )
        {
            m_uiRR = value.toInt();
        }
    }
    else if(id == "RRMech_qKey")
    {
        if( m_mode == "SIMVVCV" || m_mode == "SIMVPCV" || m_mode == "PSVPro")
        {
            m_uiSpontRR = value.toInt();
        }
    }
    else if(id == "IE_qKey")
    {
        float ieValue;
        QString ie = value;
        auto ie_Value = ie.split(':');
        if(ie == "2:1")
        {
            int ieValue = 5;
            m_uiIE = ieValue;
        }
        else
        {
            QString ie_temp = ie_Value[1];
            ieValue = ie_temp.toFloat() * 10;
            m_uiIE = int(ieValue);
        }
    }
    else if(id == "PEEP_qKey")
    {
        m_uiPEEP = value.toInt();
    }
    else if(id == "Tinsp_sKey")
    {
        m_uiTinsp = value.toFloat();
    }
    else if(id == "Tpause_sKey")
    {
        m_uiTpause = value.toInt();
    }
    else if(id == "Ppeak_Low")
    {
        m_uiPpeakLow = value.toInt();
    }
    else if( id == "Psupport_qKey")
    {
        m_uiPsupport = value.toInt();
    }
    else if(id == "Pmax_sKey")
    {
        m_uiPMax = value.toInt();
    }
}

/**
 * @brief VentConstraintChecker::constraintChecker - Checks contraints during Mode Change Operation and initial load
 * @param isUserConfirm
 */
void VentConstraintChecker::constraintChecker(bool isUserConfirm)
{
    // during init update only local copy after confirmation send those
    qDebug()<< "initalbootup called " << isUserConfirm;
    if (isUserConfirm == false)
    {
        qDebug() << "mode change not conformed";
        m_ccTV = m_uiTV;
        m_ccRR = m_uiRR;
        m_ccIE = m_uiIE;
        m_ccPEEP = m_uiPEEP;
        m_ccTpause = m_uiTpause;
        m_ccSpontRR = m_uiSpontRR;
        m_ccTinsp = m_uiTinsp;
    }
    assureTPause();
    assureIE(isUserConfirm);
    assureTv();
    assureRR();
    updateTVControl();
    updateRRControl();
    updateIEControl();
    if (isUserConfirm == true)
    {
        QString tPauseVal = QString::number(m_uiTpause);
        csbComObj->writeToCSB("Tpause_sKey",tPauseVal,"Secondary");
        updateTpause();

        QString spontRRVal = QString::number(m_uiSpontRR);
        csbComObj->writeToCSB("RRMech_qKey",spontRRVal,"Primary");

        QString update_IE;
        if (m_uiIE == 5){
            update_IE = "2:1";
            csbComObj->writeToCSB("IE_qKey",update_IE,"Primary");
        }else{
            int temp = 1;
            float ie = float(m_uiIE / 10.0f);
            update_IE = QString::number(temp) + ":" + QString::number(ie);
            csbComObj->writeToCSB("IE_qKey",update_IE,"Primary");
        }
    }
}

/**
 * @brief VentConstraintChecker::assureTv - Ensures TV is a not making constraint
 */
void VentConstraintChecker::assureTv()
{
    int tv;
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;

    if (m_mode == "VCV" || m_mode == "SIMVVCV")
    {
        if ((m_uiTV <= TV_MIN_PEEP) && (m_uiPEEP > INT_TEN_CONSTANT))
        {
            m_uiTV = TV_MIN_PEEP;
        }
        tv = m_uiTV;
        while ((constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TV, tv)) != Constraint_Type::NO_CONSTRAINT)
        {
            if (constraint_sts == Constraint_Type::FLOW_MAX_CONSTRAINT)
            {
                tv = decreaseVT(tv, true);		//pass true since the DecreaseVT is invoked from the constraint checker
            }
            else if (constraint_sts == Constraint_Type::FLOW_MIN_CONSTRAINT)
            {
                tv = increaseVT(tv, true);		//pass true since the IncreaseVT is invoked from the constraint checker
            }
            else
            {
                break;
            }
        }
        if (m_uiTV != tv)
        {
            m_uiTV = tv;
        }
    }
}

/**
 * @brief VentConstraintChecker::assureRR - Copy RR value to MechRR in VCV and PCV mode
 */
void VentConstraintChecker::assureRR()
{
    if (m_mode == "VCV" || m_mode == "PCV")
    {
        if(m_uiRR  > 60){
            m_uiSpontRR = 60;
        }else{
            m_uiSpontRR = m_uiRR;
        }
    }
}

/**
 * @brief VentConstraintChecker::assureTPause - Ensure Tpause setting is valid
 */
void VentConstraintChecker::assureTPause()
{
    qDebug() << "/***** Assure TPause *******/" ;
    Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
    if (m_mode == "VCV" || m_mode == "SIMVVCV")
    {
        while ((constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::TPAUSE, m_uiTpause)) == Constraint_Type::TINSP_CONSTRAINT)
        {
            m_uiTpause -= INT_FIVE_CONSTANT;
            if (m_uiTpause == 0)
            {
                break;
            }
        }
    }
}

/**
 * @brief VentConstraintChecker::assureIE - Ensures IE is a not making constraint
 * @param isUserConfirm - indicates if the setting is temporary or user confirmed
 */
void VentConstraintChecker::assureIE(bool isUserConfirm)
{
    if (m_mode == "VCV" || m_mode == "PCV")
    {
        int ie = m_uiIE;
        Constraint_Type constraint_sts = Constraint_Type::NO_CONSTRAINT;
        while ((constraint_sts = utilitiesConstraintChecker(Constraint_Parameter::IE, ie)) != Constraint_Type::NO_CONSTRAINT)
        {
            if (constraint_sts == Constraint_Type::TEXP_CONSTRAINT)
            {
                ie = ie + 5;
            }
            else if (constraint_sts == Constraint_Type::TINSP_CONSTRAINT)
            {
                ie = ie - 5;
            }
            else
            {
                break;
            }
            if ((ie == int(IE_Min * 10)) || (ie == int(IE_Max * 10)))
            {
                break;
            }
        }
        if (ie != m_uiIE)
        {
            m_uiIE = ie;
        }
        updateTinsp(isUserConfirm);
    }
}

/**
 * @brief VentConstraintChecker::updateTinsp - Ensure Tpause setting is valid
 * @param isUserConfirm
 */
void VentConstraintChecker::updateTinsp(bool isUserConfirm)
{
    int rr = ((m_uiRR > 60) ? 60 : m_uiRR);
    float cycletime = float(60.0f / rr);
    m_uiTinsp = float(cycletime * 10.0f / (m_uiIE + 10));
    if (isUserConfirm)
    {
        qDebug()<<"Tinsp value is..."<<m_uiTinsp;
        QString tInspVal = QString::number(m_uiTinsp);
        csbComObj->writeToCSB("Tinsp_sKey",tInspVal,"Secondary");
        setTinspInAllMenus(); // update the Tinsp value in setting
    }
}

/**
 * @brief VentConstraintChecker::setTinspInAllMenus -  Update Tinsp value in all the menus
 */
void VentConstraintChecker::setTinspInAllMenus()
{
    QString tInsp;
    if(m_mode == "VCV" || m_mode == "PCV")
    {
        float f_uiTinsp = m_uiTinsp;
        int i_insp = int((float(f_uiTinsp) + float(0.05)) * 10);
        m_uiTinsp = float(i_insp)/10.0f;
        if(m_uiTinsp > 5)
        {
            m_uiTinsp = 5.0f;
        }
        tInsp = QString::number(m_uiTinsp);
        tInsp.setNum(m_uiTinsp, 'f' ,1);
        objSettingFile->writeDataInFile("Tinsp_sKey", tInsp);
        csbComObj->writeToCSB("Tinsp_sKey",tInsp,"Secondary");
    }
}

/**
 * @brief VentConstraintChecker::getTVValue - Get Tv value
 * @return
 */
int VentConstraintChecker::getTVValue()
{
    return m_uiTV;
}

/**
 * @brief VentConstraintChecker::getPinspValue - Get Pinsp value
 * @return
 */
int VentConstraintChecker::getPinspValue()
{
    return  m_uiPinsp;
}

/**
 * @brief VentConstraintChecker::getPsupportValue - Get Psupport value
 * @return
 */
int VentConstraintChecker::getPsupportValue()
{
    return  m_uiPsupport;
}

/**
 * @brief VentConstraintChecker::setCsbObjCom - set the CSB Object
 * @param csbObj
 */
void VentConstraintChecker::setCsbObjCom(CSBCommunication *csbObj)
{
    csbComObj = csbObj;
}

/**
 * @brief VentConstraintChecker::setDualStorageObj - set the Dualstorage Object
 * @param dualStorage
 */
void VentConstraintChecker::setDualStorageObj(ParamDualStorage *dualStorage)
{
    dualStorageObj = dualStorage;
}

/**
 * @brief VentConstraintChecker::decreaseVT - Decrease VT setting value
 * @param ttvv - tv setting value
 * @param isConstraintChecker - true indicates the method is invoked from the constraint checker
 * false indicates the method is invoked during TV quick key decreament
 * @return
 */
int VentConstraintChecker::decreaseVT(int ttvv, bool isConstraintChecker)
{
    if (m_uiPEEP >  INT_TEN_CONSTANT)
        m_tvMin = TV_MIN_PEEP; //TV_MIN_PEEP = 100
    else
        m_tvMin = INT_TWENTY_CONSTANT; // INT_TWENTY_CONSTANT = 25

    if (ttvv <= TV_MIN_PEEP)
    {
        ttvv -= INT_FIVE_CONSTANT;
    }
    else if (ttvv <= TV_300_RANGE)
    {
        ttvv -= INT_TEN_CONSTANT;
    }
    else if (ttvv <= TV_1000_RANGE)
    {
        ttvv -= INT_TWENTYFIVE_CONSTANT;
    }
    else if (ttvv <= TV_Max)
    {
        ttvv -= INT_FIFTY_CONSTANT;
    }
    if (ttvv < m_tvMin && isConstraintChecker)
        ttvv = m_tvMin;

    return ttvv;
}

/**
 * @brief VentConstraintChecker::increaseVT - Increase VT setting value
 * @param ttvv - tv setting value
 * @param isConstraintChecker - true indicates the method is invoked from the constraint checker
 * false indicates the method is invoked during TV quick key increament
 * @return
 */
int VentConstraintChecker::increaseVT(int ttvv, bool isConstraintChecker)
{
    m_tvMax = TV_Max;
    if (ttvv < TV_MIN_PEEP)
    {
        ttvv += INT_FIVE_CONSTANT;
    }
    else if (ttvv < TV_300_RANGE)
    {
        ttvv += INT_TEN_CONSTANT;
    }
    else if (ttvv < TV_1000_RANGE)
    {
        ttvv += INT_TWENTYFIVE_CONSTANT;
    }
    else if (ttvv <= TV_Max)
    {
        ttvv += INT_FIFTY_CONSTANT;
    }

    if (ttvv > m_tvMax && isConstraintChecker)
        ttvv = m_tvMax;

    return ttvv;
}

/**
 * @brief VentConstraintChecker::updateTVControl - Update the TV control values per the mode that we are in
 * In VCV and SIMVVCV - the TV Control has TV value
 * In PCV and SIMVPCV - the TV Control has Pinsp value
 * In PSVPro mode - the TV Control has PSupport value
 */
void VentConstraintChecker::updateTVControl()
{  
    if(m_mode == "VCV" || m_mode == "SIMVVCV")
    {
        QString tv = QString::number(m_uiTV);
        objSettingFile->writeDataInFile("TV_qKey", tv);
        dualStorageObj->storeCriticalParams("TV_qKey", tv);
        csbComObj->writeToCSB("TV_qKey",tv,"Primary");
    }
    else if(m_mode == "PCV" || m_mode == "SIMVPCV")
    {
        QString pinsp = QString::number(m_uiPinsp);
        objSettingFile->writeDataInFile("Pinsp_qKey", pinsp);
    }
    else if(m_mode == "PSVPro")
    {
        QString psupport;
        if(m_uiPsupport == 0)
        {
            psupport = "Off";
            objSettingFile->writeDataInFile("Psupport_qKey", psupport);
        }
        else
        {
            psupport = QString::number(m_uiPsupport);
            objSettingFile->writeDataInFile("Psupport_qKey", psupport);
        }
    }
}

/**
 * @brief VentConstraintChecker::updateRRControl - Update the RR value
 */
void VentConstraintChecker::updateRRControl()
{  
    if (m_mode == "VCV" || m_mode == "PCV")
    {
        QString rr = QString::number(m_uiRR);
        objSettingFile->writeDataInFile("RR_qKey", rr);
        dualStorageObj->storeCriticalParams("RR_qKey", rr);
        csbComObj->writeToCSB("RR_qKey",rr,"Primary");
        if(m_uiRR > 60)
        {
            m_uiSpontRR = 60;
        }
        else
        {
            m_uiSpontRR = m_uiRR;
        }
        QString s_rr = QString::number(m_uiSpontRR);
        objSettingFile->writeDataInFile("RRMech_qKey", s_rr);
        csbComObj->writeToCSB("RRMech_qKey",s_rr,"Primary");
    }
    else if (m_mode == "SIMVVCV" || m_mode == "SIMVPCV" || m_mode  == "PSVPro")
    {
        QString s_rr = QString::number(m_uiSpontRR);
        objSettingFile->writeDataInFile("RRMech_qKey", s_rr);
        csbComObj->writeToCSB("RRMech_qKey",s_rr,"Primary");
    }
}

/**
 * @brief VentConstraintChecker::updateIEControl - Update the IE value
 */
void VentConstraintChecker::updateIEControl()
{
    QString update_IE;
    if (m_uiIE == 5)
    {
        update_IE = "2:1";
        objSettingFile->writeDataInFile("IE_qKey", update_IE);
    }
    else
    {
        int temp = 1;
        float ie = float(m_uiIE / 10.0f);
        update_IE = QString::number(temp) + ":" + QString::number(ie);
        objSettingFile->writeDataInFile("IE_qKey", update_IE);
    }
    dualStorageObj->storeCriticalParams("IE_qKey", update_IE);
}

/**
 * @brief VentConstraintChecker::updatePEEPControl - Update the Peep control value
 */
void VentConstraintChecker::updatePEEPControl()
{
    QString update_Peep;
    if ( m_uiPEEP == 0)
    {
        update_Peep = "Off";
        objSettingFile->writeDataInFile("PEEP_qKey", update_Peep);
    }
    else
    {
        update_Peep   = QString::number(m_uiPEEP);
        objSettingFile->writeDataInFile("PEEP_qKey", update_Peep);
    }
    dualStorageObj->storeCriticalParams("PEEP_qKey", update_Peep);
}

/**
 * @brief VentConstraintChecker::updatePinspPSVProControl - Update the Pinsp value in PSVPro mode
 */
void VentConstraintChecker::updatePinspPSVProControl()
{
    QString update_Pinsp = QString::number(m_uiPinsp);
    objSettingFile->writeDataInFile("Pinsp_qKey", update_Pinsp);
    dualStorageObj->storeCriticalParams("Pinsp_qKey", update_Pinsp);
}

/**
 * @brief VentConstraintChecker::updatePSupportControl - Update the PSupport value
 */
void VentConstraintChecker::updatePSupportControl()
{
    QString update_Psupport;
    if ( m_uiPsupport == 0)
    {
        update_Psupport = "Off";
        objSettingFile->writeDataInFile("Psupport_qKey", update_Psupport);
    }
    else
    {
        update_Psupport   = QString::number(m_uiPsupport);
        objSettingFile->writeDataInFile("Psupport_qKey", update_Psupport);
    }
}

/**
 * @brief VentConstraintChecker::setmodeChangeAction - Set the Mode change boolen value
 * @param modeAction
 */
void VentConstraintChecker::setmodeChangeAction(bool modeAction) {
    m_modeChangeAction = modeAction;
}

/**
 * @brief VentConstraintChecker::cancelControlSettingAction - Cancel the control setting action if the control setting is in edit mode
 * Update the controls with last saved values
 */
void VentConstraintChecker::cancelControlSettingAction()
{
    if (m_modeChangeAction == false)
    {
        return;
    }
    if (m_modeChangeAction)
    {
        m_mode  = m_prevMode;
        m_uiTV = m_ccTV;
        m_uiRR = m_ccRR;
        m_uiIE = m_ccIE;
        m_uiPEEP = m_ccPEEP;
        m_uiTpause = m_ccTpause;
        m_uiSpontRR = m_ccSpontRR;
        m_uiTinsp = m_ccTinsp;
        m_modeChangeAction = false;
    }
    //Update the control setting values to last saved
    updateTVControl();
    updateRRControl();
    updateIEControl();
    updatePEEPControl();
    updatePinspPSVProControl();
    updatePSupportControl();
}

/**
 * @brief VentConstraintChecker::updateTpause - Update Tpause value in all the menus
 */
void VentConstraintChecker::updateTpause()
{
    qDebug() << "updateTpause() called" << count++;
    if (m_mode == "SIMVVCV")
    {
        QString update_Tpause;
        if ( m_uiTpause == 0)
        {
            update_Tpause = "Off";
            objSettingFile->writeDataInFile("Tpause_sKey", update_Tpause);
        }
        else
        {
            update_Tpause   = QString::number(m_uiTpause);
            objSettingFile->writeDataInFile("Tpause_sKey", update_Tpause);
        }
    }
}
