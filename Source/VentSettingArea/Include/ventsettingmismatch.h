#ifndef VENTSETTINGMISMATCH_H
#define VENTSETTINGMISMATCH_H

#include <QObject>
#include "../Source/VentSettingArea/Include/paramdualstorage.h"
#include "../Source/VentSettingArea/Include/settingfileupdate.h"

class VentSettingMismatch : public QObject
{
    Q_OBJECT
public:
    VentSettingMismatch();
    void defaultSettings(SettingFileUpdate* settings);
    bool isVentilationSettingsMismatch(int mode, int vmodeValue, ParamDualStorage *dualStorageData, int &csbCommand, int &csbData, bool &isBannerShown) ;
    void setVmismatchTVLow(const int &vMismatchTVLow);
    void setVmismatchTVHigh(const int &vMismatchTVHigh);
    void setCntTVLow(const int &cntTVLow);
    void setCntTVHigh(const int &cntTVHigh);

    void setVmismatchRR(const int &vMismatchRR);
    void setCntRR(const int &cntRR);

    void setVmismatchIE(const int &vMismatchIE);
    void setCntIE(const int &cntIE);

    void setVmismatchPeep(const int &vMismatchPeep);
    void setCntPeep(const int &cntPeep);

    void setVmismatchPinsp(const int &vMismatchPinsp);
    void setCntPinsp(const int &cntPinsp);

    void setVmismatchPmax(const int &vMismatchPmax);
    void setCntPmax(const int &cntPmax);

    void setVmismatchBackupTime(const int &vMismatchBackupTime);
    void setCntBackupTime(const int &cntBackupTime);

    void setVmismatchMode(const int &vMismatchMode);
    void setCntMode(const int &cntMode);

    int getVmismatchTVLow() const;
    int getVmismatchTVHigh() const;

signals:
    void showVentMismatchBanner();

private:
    int m_cntIE;
    int m_cntTVLow;
    int m_cntTVHigh;
    int m_cntRR;
    int m_cntPeep;
    int m_cntPinsp;
    int m_cntMode;
    int m_cntPmax;
    int m_cntBackupTime;

    int m_vMismatchIE;
    int m_vMismatchTV ;
    int m_vMismatchTVLow;
    int m_vMismatchTVHigh;
    int m_vMismatchRR;
    int m_vMismatchPeep;
    int m_vMismatchPinsp;
    int m_vMismatchMode;
    int m_vMismatchPmax;
    int m_vMismatchBackupTime;
};

#endif // VENTSETTINGMISMATCH_H
