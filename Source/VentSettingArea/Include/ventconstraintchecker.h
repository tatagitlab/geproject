#ifndef VENTCONSTRAINTCHECKER_H
#define VENTCONSTRAINTCHECKER_H

#include <QObject>
#include "../Source/VentSettingArea/Include/settingfileupdate.h"
#include "../Source/CSB/Include/csbcommunication.h"
#include "../Source/VentSettingArea/Include/paramdualstorage.h"
#include "../Source/Utilities/Include/physicalparamdomreader.h"

class VentConstraintChecker: public QObject
{
    Q_OBJECT
    Q_ENUMS(Constraint_Type)
    Q_ENUMS(Constraint_Parameter)

public:
    VentConstraintChecker();
    void setSettingReader(SettingFileUpdate* settings);
    void setPhysicalParamReader(PhysicalParamDomReader* physicalParam);
    Q_INVOKABLE void init();
    Q_INVOKABLE void setCurrentMode(QString currentMode);
    Q_INVOKABLE void setPrevMode(QString setPrevMode);
    Q_INVOKABLE void updateConstraintParam(QString id, QString value);
    Q_INVOKABLE void constraintChecker(bool isUserConfirm);
    Q_INVOKABLE int getTVValue();
    Q_INVOKABLE int getPinspValue();
    Q_INVOKABLE int getPsupportValue();

    void setCsbObjCom(CSBCommunication* csbObj);
    void setDualStorageObj(ParamDualStorage* dualStorage);
    void assureTPause();
    void assureIE(bool isUserConfirm);
    void assureTv();
    void assureRR();
    void setTinspInAllMenus();
    Q_INVOKABLE int decreaseVT(int ttvv, bool isConstraintChecker);
    Q_INVOKABLE int increaseVT(int ttvv, bool isConstraintChecker);
    void updateTVControl();
    Q_INVOKABLE void updateRRControl();
    void updateTpause();
    Q_INVOKABLE void updateTinsp(bool isUserConfirm);
    Q_INVOKABLE void updateIEControl();
    void updatePEEPControl();
    void updatePinspPSVProControl();
    void updatePSupportControl();
    Q_INVOKABLE void cancelControlSettingAction();
    Q_INVOKABLE void setmodeChangeAction(bool modeAction);
    void getTvMIN(int tv_val);
    void getTvMAX(int tv_val);
    Q_INVOKABLE int getFinalTvMIN();
    Q_INVOKABLE int getFinalTvMAX();
    void getRRMIN(int rr_val);
    void getRRMAX(int rr_val);
    Q_INVOKABLE int getFinalRRMIN();
    Q_INVOKABLE int getFinalRRMAX();
    void getSpontRRMIN(int rr_val);
    void getSpontRRMAX(int rr_val);
    Q_INVOKABLE int getFinalSpontRRMIN();
    Q_INVOKABLE int getFinalSpontRRMAX();
    void getIEMIN(float ie_val);
    void getIEMAX(float ie_val);
    Q_INVOKABLE QString getFinalIEMIN();
    Q_INVOKABLE QString getFinalIEMAX();
    void getPeepMAX(int peep_val);
    Q_INVOKABLE int getFinalPeepMAX();
    void getTpauseMIN(int tp_val);
    void getTpauseMAX(int tp_val);
    Q_INVOKABLE int getFinalTpauseMIN();
    Q_INVOKABLE int getFinalTpauseMAX();
    void getPmaxMIN(int pmax_val);
    void getPmaxMAX(int pmax_val);
    Q_INVOKABLE int getFinalPmaxMIN();
    Q_INVOKABLE int getFinalPmaxMAX();
    void getTinspMIN(float tinsp_val);
    void getTinspMAX(float tinsp_val);
    Q_INVOKABLE float getFinalTinspMIN();
    Q_INVOKABLE float getFinalTinspMAX();

    enum Constraint_Parameter
    {
        TV = 0,
        IE,
        RR,
        TINSP,
        PMAX,
        TPAUSE,

    };
    enum Constraint_Type
    {
        NO_CONSTRAINT = 0,
        FLOW_MIN_CONSTRAINT,
        FLOW_MAX_CONSTRAINT,
        TINSP_CONSTRAINT,
        TEXP_CONSTRAINT,
        PMAX_CONSTARINT,
        INVALID
    };
    Q_INVOKABLE Constraint_Type  utilitiesConstraintChecker(Constraint_Parameter cp,int value);
public:
    int INT_FIVE_CONSTANT;
    int INT_TEN_CONSTANT;
    int TV_Min;
    int TV_Max;
    int rr_Min;
    int rr_Max;
    int rrMech_Min;
    int rrMech_Max;
    int peep_Max;
    float IE_Min;
    float IE_Max;
    int pmax_Min;
    int pmax_Max;
    int tpause_Min;
    int tpause_Max;
    float Tinsp_Min;
    float Tinsp_Max;
    int TV_MIN_PEEP;
    int INT_TWENTY_CONSTANT;
    int TV_300_RANGE;
    int TV_1000_RANGE;
    int INT_TWENTYFIVE_CONSTANT;
    int INT_FIFTY_CONSTANT;
    float TexpMin;
    SettingFileUpdate *objSettingFile;
    CSBCommunication* csbComObj;
    ParamDualStorage* dualStorageObj;
    PhysicalParamDomReader* physicalParam;
    int count;

private: //CnstChkPara
    int m_uiTV;
    int m_uiPinsp;
    int m_uiRR;
    int m_uiIE;
    int m_uiPEEP;
    int m_uiTpause;
    int m_uiSpontRR;
    int m_uiPMax;
    float m_uiTinsp;
    int m_uiPsupport;
    int  m_uiPpeakLow;
    int m_ccTV;
    int m_ccRR;
    int m_ccIE;
    int m_ccPEEP;
    int m_ccTpause;
    int m_ccSpontRR;
    float m_ccTinsp;
    bool m_errorStat;
    bool m_modeChangeAction;
    QString m_mode ;// read from setting file
    QString m_prevMode;
    int m_tvMin;
    int m_tvFinalMIN;
    int m_tvMax;
    int m_tvFinalMAX;
    float m_ieFinalMIN;
    float m_ieFinalMAX;
    int m_rrFinalMIN;
    int m_rrFinalMAX;
    int m_spontRRFinalMAX;
    int m_spontRRFinalMIN;
    int m_peepFinalMAX;
    int m_peepFinalMIN;
    int m_tpFinalMIN;
    int m_tpFinalMAX;
    int m_pmaxFinalMIN;
    int m_pmaxFinalMAX;
    float m_TinspFinalMIN;
    float m_TinspFinalMAX;
};

#endif // VENTCONSTRAINTCHECKER_H
