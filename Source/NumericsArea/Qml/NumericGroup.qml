import QtQuick 2.0

Rectangle {
    id: numericsGrpId
    objectName: "numericGroup"

    property int iNumGrpHeight: 612
    property int iNumGrpWidth: 295
    property string strNumGrpColor: "Gray"

    width: iNumGrpWidth
    height: iNumGrpHeight
    color: strNumGrpColor
    border.color: "black"
    border.width: 0
    signal numericGrpClicked();
    signal setNumericAsObjectName(string objectName);

    Column {
        id:maincolumn
        anchors.fill:parent
    }
    Rectangle{
        id: borderGroupLeft
        height: parent.height
        width: 1
        color: "black"
        anchors.left: parent.left
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("numeric grp clicked!!!!")
            numericGrpClicked()
            setNumericAsObjectName("numeric")
        }
    }


    Component.onCompleted:{
    }
}
