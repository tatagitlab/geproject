#ifndef CSBCOMMUNICATION_H
#define CSBCOMMUNICATION_H
#include <QFile>
#include <QObject>

#include "../Source/VentSettingArea/Include/settingfileupdate.h"
#include "../Source/VentSettingArea/Include/paramdualstorage.h"
#include "../Source/AlarmArea/Include/alarmreader.h"
#include "../Source/VentSettingArea/Include/ventsettingmismatch.h"
#include <QDateTime>

#define MODE_VCV "BtnMode_VCV"
#define MODE_PCV "BtnMode_PCV"
#define MODE_SIMV_VCV "BtnMode_SIMVVCV"

#define MODE_SIMV_PCV "BtnMode_SIMVPCV"
#define MODE_PSV_PRO "BtnMode_PSVPro"

#define SETTINGS_MODE_VCV "VCV_mode"
#define SETTINGS_MODE_PCV "PCV_mode"
#define SETTINGS_MODE_SIMV_VCV "SIMV VCV_mode"

#define PRIMARY "Primary"
#define SECONDARY "Secondary"

#define ALARM "AlarmParam"

#define CSB_WAVEFORM_PKT 3
#define CSB_LOG_PKT 12
#define CSB_FIRST_BYTE 170
#define CSB_LAST_BYTE 85

#define CSB_ALARM_SYSERR 39
#define CSB_ALARM_BACKUPMODE 56

#define HALARM_START_BYTE 17
#define HALARM_END_BYTE 19
#define MALARM_START_BYTE 13
#define MALARM_END_BYTE 15
#define LALARM_START_BYTE 16
#define LALARM_END_BYTE 20

#define MODE 0

class CSBCommunication : public QObject
{
    Q_OBJECT
    Q_ENUMS(VentilationModes)

private:
    int m_ventilationMode;
    QString value;
    int m_d0;
    int m_d1;
    int m_d2;
    int m_d3;
    int m_d4;

public:
    CSBCommunication(QObject *parent= nullptr);
    ~CSBCommunication();
    SettingFileUpdate *m_settingObj;
    void openPort();
    void run();
    void initializeEnum();
    void setVentMismatch(VentSettingMismatch *ventMismatch);

    Q_INVOKABLE void writeToCSB(QString id,QString data, QString ventParmType);
    Q_INVOKABLE void readFromCSB(ParamDualStorage *dualStorageData, AlarmReader *alarmReader);
    Q_INVOKABLE void writeVentModeToCSB(QString mode);
    Q_INVOKABLE void writeOnCSB(int D1, int D2);


    enum  PrimaryQK{
        TV_qkey = 1,
        RR_qkey = 2,
        IE_qkey = 3,
        PEEP_qkey = 4,
        PSUPPORT_qkey = 5,
        PINSP_qkey = 6,
        RRMECH_qkey = 7};

    enum SecondaryParams {
        FLOW_TRIGGER_skey = 1,
        TINSP_skey = 2,
        PMAX_skey = 3,
        TRIG_WINDOW_skey = 4,
        END_OF_BREATH_skey = 5,
        TPAUSE_skey = 6,
        BACKUPTIME_skey=7,
        EXITBACKUP_skey=8 };



    enum  CsbPackets{
        WAVEFORM_packet = 3,
        ALARM_packet = 12
    };

    enum AlarmSettingParams{
        PpeakAlarmHigh = 9,
        PpeakAlarmLow = 10,
        MVAlarmLow = 14,
        MVAlarmHigh = 13,
        TVAlarmLow = 12,
        TVAlarmHigh = 11,
        O2AlarmLow = 16,
        O2AlarmHigh = 15,
        AlarmVolume = 5,
        ApneaDelay = 17

    };

    std::map<std::string, PrimaryQK> primaryQKList;
    std::map<std::string, SecondaryParams> secondaryParamList;
    std::map<std::string, AlarmSettingParams> alarmSettingsParamList;

signals :
    Q_INVOKABLE void tvValues(int tvs);
    void tvValuesUI(QString tvs);
    void rrValuesUI(int rrs);
    void ieValuesUI(QString ies);
    void ppValuesUI(int pps);
    void psValueUI(int psupport);
    void pinspValueUI(int pinsp);
    void flowTriggerValueUI(float flowTrig);
    void tInspValueUI(float tInsp);
    void pmaxValueUI(int pmax);
    void trigWindowValueUI(int trigWindow);
    void eobValueUI(int eob);
    void tPauseValueUI(int tPause);

    void rrMechValueUI(int rrMech);
    void backupTimeValueUI(int backupTime);
    void exitBackupValueUI(int exitBackup);

    void showBanner();
    void csbBkpModeSIMVPCV();
    void csbBkpModePSVPro();
    void setStandByMode();
    void ventilationOn();
    void ventilationOff();

private:
    int ventilationMode;
    QString i_e;
    int rrValue;
    int ppValue;
    int psValue;
    int pmaxValue;
    int pinspValue;
    int tvValuehigh;
    int tvValuelow;
    int tvValue;
    float ieValue;
    float flowTriggerValue;
    float tInspValue;
    int trigWindowValue;
    int eobValue;
    int tpauseValue;
    int ppeakAlarmLow;
    int ppeakAlarmHigh;
    int mvAlarmLow;
    int mvAlarmHigh;
    int tvAlarmLow;
    int tvAlarmHigh;
    int o2AlarmLow;
    int o2AlarmHigh;
    int alarmVolume;
    int apneaDelay;

    int rrMechValue;
    int backupTimeValue;
    int exitBackupValue;

    bool m_bkpModeActiveAlarm ;
    bool m_bkpModeSIMVPCV ;
    bool m_bkpModePSVPro ;
    bool m_standByMode;
    bool m_ventModeOn;
    bool m_bagModeOn;

    VentSettingMismatch *ventilationMismatch;
};

#endif // CSBCOMMUNICATION_H
