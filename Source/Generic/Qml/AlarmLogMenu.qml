import QtQuick 2.9
import QtQuick.Controls 2.2

import "qrc:/../Source/Generic/JavaScript/AlarmLogMenuController.js" as AlmLogCtrl
import "qrc:/../Source/Generic/JavaScript/AlarmSetupController.js" as AlmSetUpCtrl

Rectangle {
    id : idAlarmLogMenu
    property  bool bEnteredByTouch
    property int iAlarmLogMenuWidth : 538
    property int iAlarmLogMenuHeight : 602
    property int iAlarmLogMenuBorderWidth : 2
    property int iAlarmLogMenuRadius : 3
    property string strAlarmLogMenuBgColor : "#242424"
    property string strAlarmLogMenuBorderColor : "#3c3d3d"
    property string strAlarmLogMenuLabelFontColor : "#787878"
    property int ialarmLogMenuLabelFontSize :18
    property string strAlarmTableBorderColor :"#B5B5B6"
    property int iAlarmTableBorderWidth : 2
    property string strAlarmLogMenuLabel
    property int iAlarmLogMenuLabelFontSize :18
    property string strAlarmLogMenuLabelColor :"#787878"
    property string strAlarmLogTimeLabel :"Time"
    property int iAlarmLogTimeLabelFontSize :16
    property string strAlarmLogTimeLabelColor :"#787878"
    property string strAlarmLogAlarmLabel :"Alarm"
    property int iAlarmLogAlarmLabelFontSize :16
    property string strAlarmLogAlarmLabelColor :"#787878"
    property string strAlarmLogPriorityLabel :"Priority"
    property int iAlarmLogPriorityLabelFontSize :16
    property string strAlarmLogPriorityLabelColor :"#787878"
    property int iAlarmLogBackBtnWidth : 42
    property int iAlarmLogBackBtnHeight : 42
    property string strAlarmLogBackBtnBorderColor : "#444444"
    property string strAlarmLogBackBtnBgColor : "#2e2e2e"
    property string strAlarmLogTchBgColor : "#4565A6"
    property string strFontFamilyAlarmLog : geFont.geFontStyle()
    property string strBackBtnSource

    width: iAlarmLogMenuWidth
    height: iAlarmLogMenuHeight
    color: strAlarmLogMenuBgColor
    border.width: iAlarmLogMenuBorderWidth
    border.color: strAlarmLogMenuBorderColor
    radius: iAlarmLogMenuRadius
    objectName: "AlarmLogMenuObj"


    Label{
        id: idMenuTitle
        text: qsTrId(strAlarmLogMenuLabel)
        font.pixelSize:  iAlarmLogMenuLabelFontSize
        font.family: strFontFamilyAlarmLog
        color: strAlarmLogMenuLabelColor
        anchors.left: parent.left
        anchors.leftMargin:  20
        anchors.top: parent.top
        anchors.topMargin: 20
    }

    Label{
        id : idTimeLable
        text: qsTrId(strAlarmLogTimeLabel)
        font.pixelSize: iAlarmLogTimeLabelFontSize
        font.family: strFontFamilyAlarmLog
        color: strAlarmLogTimeLabelColor
        anchors.left: parent.left
        anchors.leftMargin:  21
        anchors.top: parent.top
        anchors.topMargin: 60
    }

    Label{
        id : idAlarmLable
        text: qsTrId(strAlarmLogAlarmLabel)
        font.pixelSize: iAlarmLogAlarmLabelFontSize
        font.family: strFontFamilyAlarmLog
        color: strAlarmLogAlarmLabelColor
        anchors.left: parent.left
        anchors.leftMargin:  106
        anchors.top: parent.top
        anchors.topMargin: 60
    }
    Label{
        id : idPriorityLable
        text: qsTrId(strAlarmLogPriorityLabel)
        font.pixelSize:  iAlarmLogPriorityLabelFontSize
        font.family: strFontFamilyAlarmLog
        color: strAlarmLogPriorityLabelColor
        anchors.left: parent.left
        anchors.leftMargin:  369
        anchors.top: parent.top
        anchors.topMargin: 60
    }
    Rectangle{
        id : idBackButton
        width: iAlarmLogBackBtnWidth
        height: iAlarmLogBackBtnHeight
        border.width: activeFocus ? 2 : component.getComponentXmlDataByTag("Group","AlarmScrollView", "BackBtnBorderWidth")
        border.color: activeFocus ? "#ecbd00": strAlarmLogBackBtnBorderColor
        radius: component.getComponentXmlDataByTag("Group","AlarmScrollView", "BackBtnRadius")
        color: bckMouseArea.pressed? strAlarmLogTchBgColor : strAlarmLogBackBtnBgColor
        anchors.left: parent.left
        anchors.leftMargin:  484
        anchors.top: parent.top
        anchors.topMargin: 11
        z: 10
        objectName: "BackBtnObj"

        Image {
            id: idBackBtnImg
            anchors.centerIn: parent
            source: strBackBtnSource

        }
        Keys.onRightPressed: {
            console.log("on right pressed")
        }
        Keys.onLeftPressed: {
            console.log("on left pressed")
        }
        Keys.onReturnPressed: {
            console.log("on return pressed")
            AlmLogCtrl.destroyAlarmLogMenu();
            AlmSetUpCtrl.navigateToAlarmLogBtn();
        }

        MouseArea{
            id: bckMouseArea
            anchors.fill: parent
            onPressed: {
                idBackButton.border.color = strAlarmLogBackBtnBorderColor;
                idBackButton.border.width = 0
            }
            onReleased: {
                console.log("clear list")
                AlmLogCtrl.destroyAlarmLogMenu();
                AlmSetUpCtrl.navigateToAlarmLogBtn();
            }
        }
    }

    Component.onCompleted: {
        console.log("component created ")
        idAlarmLogMenu.forceActiveFocus()
        AlmLogCtrl.createAlarmScrollView(bEnteredByTouch);
    }

    Keys.onReturnPressed: {
        console.log("AlarmLogMenu on return pressed")
    }
    Keys.onLeftPressed: {
        console.log("AlarmLogMenu on Left pressed")
    }
    Keys.onRightPressed: {
        console.log("AlarmLogMenu on Right pressed")
    }
    MouseArea{
        anchors.fill: parent
        onClicked: {
            console.log("do nothing")
        }
    }
}
