import QtQuick 2.0
import QtQuick.Controls 2.2
import QtMultimedia 5.8
import "qrc:/../Source/Generic/JavaScript/AlarmSetupController.js" as AlarmSetupController
import "qrc:/../Source/Generic/JavaScript/AlarmLogMenuController.js" as AlarmLogController
import "qrc:/../Source/ButtonBarArea/Qml"

Rectangle {
    id: alarmSetupMenuId
    property var alarmMenuId: alarmSetupMenuId
    //parent property
    property bool bTimerRunning: false
    property int iAlarmLogBorderWidth: 1
    property int iAlarmLogHeight: 50
    property int iAlarmLogRadius: 3
    property int iAlarmLogWidth: 152
    property int iAlarmMenuBorderWidth: 2
    property int iAlarmMenuHeight: 602
    property int iAlarmMenuRadius: 3
    property int iAlarmMenuWidth: 538
    property int iPixelSizeAlarmSetup: 18
    property int iPixelSizeLimitLabel: 16
    property int iPixelSizeLogLabel: 16
    property int iMenuTimeoutInterval: 25000
    property string strAlarmBorderColor: "#3c3d3d"
    property string strAlarmLogBgColor: "#2e2e2e"
    property string strAlarmLogBorderColor: "#444444"
    property string strAlarmMenuBgColor: "#242424"
    property string strAlarmMenuLabel: "Alarm Setup"
    property string strAlarmParamScrollStateBorderColor: "#edbf00"
    property int iAlarmParamScrollStateBorderWidth: 2
    property string strFontFamilyAlarmSet: geFont.geFontStyle()
    property string strFontColor: "#787878"
    property string strLimitLabel1: "Low Limit"
    property string strLimitLabel2: "High Limit"
    property string strLogLabel: "Alarm Log"
    property string strCloseBtnBorderColor: "#444444"
    property string strCloseBtnBgColor: "#444444"
    property string strAlarmTchColor: "#4565A6"
    property string strCloseBtnSource: "qrc:/Config/Assets/icon_close_transparent.png"
    property string strCloseBtnTchSource: "qrc:/Config/Assets/icon_close_touched.png"
    property int  iCloseBtnHeight: 42
    property int iCloseBtnWidth: 42
    property int iCloseBtnBorderWidth: 1
    property string strAlarmParamPpeak: "Ppeak"
    property string strAlarmParamPpeakUnit: "cmH2O"
    property string strAlarmParamMV: "MV"
    property string strAlarmParamMVUnit: "l/min"
    property string strAlarmParamTVexp: "TVexp"
    property string strAlarmParamTVexpUnit: "ml"
    property string strAlarmParamO2: "O2"
    property string strAlarmParamO2Unit: "%"
    property string strAlarmParamApneaDelay: "Apnea Delay"
    property string strAlarmParamApneaDelayUnit: "s"
    property string strAlarmParamAlarm: "Alarm"
    property string strAlarmParamAlarmUnit: "volume"
    property string strAlarmSetupParamColor: "#242424"
    property int iAlarmSetupParamFontSize: 16
    property string strAlarmSetupParamFontColor: "#787878"
    property string strAlarmLogIcon: "qrc:/Config/Assets/icon_linkarrow_enabled.png"
    property string strAlarmStepIcon: "qrc:/Config/Assets/icon_alarmstep_gray.png"
    property var prevValue

    signal alarmSetupMenuClosed();

    width: iAlarmMenuWidth
    height: iAlarmMenuHeight
    color: strAlarmMenuBgColor
    border.width: iAlarmMenuBorderWidth
    border.color: strAlarmBorderColor
    radius: iAlarmMenuRadius
    visible: true

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("Alarm setup menu clicked")
        }
    }

    Rectangle {
        id: closeBtn
        anchors.right: parent.right
        anchors.rightMargin: 9
        anchors.top: parent.top
        anchors.topMargin: 9
        border.width: iCloseBtnBorderWidth
        height: iCloseBtnHeight
        width: iCloseBtnWidth
        radius: 3
        color: closeMouseArea.pressed? strAlarmTchColor:strCloseBtnBgColor
        objectName: "CloseBtnObj"
        border.color: activeFocus ? strAlarmParamScrollStateBorderColor :strCloseBtnBorderColor
        Image {
            source: closeMouseArea.pressed? strCloseBtnTchSource : strCloseBtnSource
            anchors.centerIn: parent
        }

        MouseArea {
            id: closeMouseArea
            anchors.fill: closeBtn
            onPressed: {
                timeOut.running = false;
                timeOut.stop()
                closeBtn.border.color = strCloseBtnBorderColor
                closeBtn.border.width = 0
               // AlarmSetupController.resetStateInAlarmBtn();
            }
            onReleased: {
                AlarmSetupController.destroyAlarmSetupMenu()
                console.log("Alarm Setup Menu closed !!!!")
            }
        }
        Keys.onRightPressed: {
            console.log("right pressed in closed")
            if(closeBtn.focus === true){
                alarmSetupMenuId.children[19].focus = true
                closeBtn.border.color = strCloseBtnBorderColor
                timeOut.running = true;
                timeOut.restart()
            }
        }
        Keys.onLeftPressed: {
            console.log("left pressed in closed")
            if(closeBtn.focus === true){
                alarmLogId.focus = true
                closeBtn.border.color =strCloseBtnBorderColor
                timeOut.running = true;
                timeOut.restart()
            }
        }
        Keys.onReturnPressed: {
            AlarmSetupController.destroyAlarmSetupMenu()
            console.log("Alarm Setup Menu closed when Enter Pressed!!!!")
            timeOut.running = true;
            timeOut.restart()
        }
    }

    Label {
        id: alarmMenuLabel
        text: qsTrId(strAlarmMenuLabel)
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        font.family: strFontFamilyAlarmSet
        font.pixelSize: iPixelSizeAlarmSetup
        color: strFontColor
    }

    Label {
        id: lowLimitLabel
        objectName: "lowLimitLabelObj"
        text: qsTrId(strLimitLabel1)

        font.family: strFontFamilyAlarmSet
        font.pixelSize:  iPixelSizeLimitLabel
        color: strFontColor
    }

    Label {
        id: highLimitLabel
        text: qsTrId(strLimitLabel2)
        objectName: "highLimitLabelObj"

        font.family: strFontFamilyAlarmSet
        font.pixelSize: iPixelSizeLimitLabel
        color: strFontColor
    }

    Label {
        id: paramPpeakLabel
        text: qsTrId(strAlarmParamPpeak)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        font.family: strFontFamilyAlarmSet
        anchors.top: parent.top
        anchors.topMargin: 116
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramPpeakUnit
        text: qsTrId(strAlarmParamPpeakUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramPpeakLabel.bottom
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramMVLabel
        text: qsTrId(strAlarmParamMV)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: parent.top
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: 186
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramMVUnit
        text: qsTrId(strAlarmParamMVUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramMVLabel.bottom
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramTVexpLabel
        text: qsTrId(strAlarmParamTVexp)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: parent.top
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: 256
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramTVexpUnit
        text: qsTrId(strAlarmParamTVexpUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.topMargin: 5
        font.family: strFontFamilyAlarmSet
        anchors.top: paramTVexpLabel.bottom
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramO2Label
        text: qsTrId(strAlarmParamO2)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: parent.top
        font.family: strFontFamilyAlarmSet
        anchors.topMargin: 326
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramO2Unit
        text: qsTrId(strAlarmParamO2Unit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramO2Label.bottom
        anchors.topMargin: 5
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramAlarmLabel
        text: qsTrId(strAlarmParamAlarm)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 64
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramAlarmUnit
        text: qsTrId(strAlarmParamAlarmUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramAlarmLabel.bottom
        anchors.topMargin: 5
        font.family: strFontFamilyAlarmSet
        anchors.bottomMargin: 30
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramApneaDelayLabel
        text: qsTrId(strAlarmParamApneaDelay)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 134
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Label {
        id: paramApneaDelayUnit
        text: qsTrId(strAlarmParamApneaDelayUnit)
        font.pixelSize: iAlarmSetupParamFontSize
        color: strAlarmSetupParamFontColor
        anchors.top: paramApneaDelayLabel.bottom
        anchors.bottomMargin: 5
        font.family: strFontFamilyAlarmSet
        anchors.right: parent.right
        anchors.rightMargin: 385
    }

    Rectangle{
        height: 20
        width:19
        color:strAlarmMenuBgColor
        anchors.top : parent.top
        anchors.topMargin: 80
        anchors.right: highLimitLabel.left
        anchors.left : lowLimitLabel.right
        Image {
            id:stepIcon
            source: strAlarmStepIcon
            anchors.centerIn: parent
        }
    }

    Rectangle {
        objectName: "AlarmLogObj"
        id: alarmLogId
        width: iAlarmLogWidth
        height: iAlarmLogHeight
        radius: iAlarmLogRadius
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 30
        anchors.right: parent.right
        anchors.rightMargin: 10
        color: logMouseArea.pressed? strAlarmTchColor:strAlarmLogBgColor
        border.color: (activeFocus && !logMouseArea.pressed) ?  strAlarmParamScrollStateBorderColor : strAlarmLogBorderColor
        border.width: (activeFocus && !logMouseArea.pressed) ? iAlarmParamScrollStateBorderWidth : iAlarmLogBorderWidth

        Label {
            id: logLable
            text: qsTrId(strLogLabel)
            font.pixelSize: iPixelSizeLogLabel
            font.family: strFontFamilyAlarmSet
            color: strFontColor
            anchors.centerIn: parent
        }

        Image {
            id: logIconId
            source: strAlarmLogIcon
            anchors.left: logLable.right
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 15
        }
        MouseArea {
            id: logMouseArea
            anchors.fill: parent
            onPressed: {
                timeOut.running = false
                timeOut.stop();
                //alarmLogId.border.width = 0
                AlarmSetupController.resetStateInAlarmBtn();
            }

            onReleased: {
                AlarmLogController.createAlarmLogMenu(true);
            }
        }

        Keys.onRightPressed: {
            console.log("right pressed in closed")
            if(alarmLogId.focus === true){
                alarmSetupMenuId.children[1].focus = true
                timeOut.running = true;
                timeOut.restart()
            }
        }
        Keys.onLeftPressed: {
            console.log("left pressed in closed")
            if(alarmLogId.focus === true){
                alarmSetupMenuId.children[28].focus = true
                timeOut.running = true;
                timeOut.restart()
            }
        }
        Keys.onReturnPressed: {
            console.log("on key press")
            timeOut.running = false
            timeOut.stop()
            AlarmSetupController.resetStateInAlarmBtn();
            AlarmLogController.createAlarmLogMenu(false);
        }
    }

    Component.onCompleted: {
        AlarmSetupController.createAlarmSetupMenuComponents()
        alarmSetupMenuId.children[19].focus = true
        alarmSetupMenuId.children[19].forceActiveFocus();
        timeOut.start()
    }

    states: [
        State{
            name: "Scroll"
            when: closeBtn.activeFocus
            PropertyChanges {
                target: closeBtn;
                border.color: strAlarmParamScrollStateBorderColor ;
                border.width: iAlarmParamScrollStateBorderWidth;
            }
        }
    ]

    /* Negative audio tone for exit cases*/
//    SoundEffect{
//        id: mediaPlayer
//        source: "qrc:/Config/Assets/reject.wav"
//    }

    Timer {
        id : timeOut
        interval : iMenuTimeoutInterval
        repeat: false
        objectName: "timeOutObj"
        running: bTimerRunning

        onTriggered: {
            console.log("Alarm time out initiated")
           // mediaPlayer.play()
            timeOut.running = false
            timeOut.stop()
            AlarmSetupController.destroyAlarmSetupMenu();
        }
    }

    function restartTimer() {
        timeOut.running = true ;
        timeOut.restart();
        console.log("timer restart")
    }

    function stopTimer() {
        timeOut.running = false ;
        timeOut.stop() ;
    }

    Component.onDestruction: {
        console.log("Component.onDestruction")
        AlarmLogController.destroyAlarmLogMenu();
    }
}
