import QtQuick 2.0
import QtQuick.Controls 2.2
import QtMultimedia 5.8
import QtQuick.Layouts 1.3
import "qrc:/Source/Generic/JavaScript/AlarmSetupController.js" as AlarmSetupController

Rectangle {
    id: valueBox
    //Value Box Property
    property int iAccelerationInterval: 200
    property int iBtnValueFontSize: 16
    property int iValueBoxBorderwidth: 1
    property int ivalueBoxHeight: 50
    property int iValueBoxRadius: 3
    property int ivalueBoxWidth: 100
    property string strAlarmParamActiveStateColor: "#1d1d1d"
    property string strBgColor: "#1d1d1d"
    property string strBtnValueFontColor:"#dddddd"
    property string strParamName
    property string strValue
    property string strValueBoxBorderColor: "#31313131"
    property var prevValue
    //Spinner Property
    property bool bIsSpinnerTouchedDown
    property bool bIsSpinnerTouchedUp
    property int iAlarmParamScrollStateBorderWidth: 2
    property int iAlarmParamSelectedStateBorderWidth: 0
    property int iSelectedStateAlarmArrowHeight: 50
    property int iSelectedStateAlarmArrowRadius: 3
    property int iSelectedStateAlarmArrowWidth: 60
    property string strAlarmParamEndOfScaleStateColor:"#dddddd"
    property string strAlarmParamScrollStateBorderColor:"#edbf00"
    property string strAlarmParamSelectedStateColor: "ffe771"
    property string strAlarmParamSelectedStateFontColor: "#242424"
    property string strAlarmParamTouchedStateColor: "#4565a6"
    property string strAlarmSetupSpinnerDownDisabledIcon
    property string strAlarmSetupSpinnerDownEnabledIcon
    property string strAlarmSetupSpinnerUpEnabledIcon
    property string strAlarmSetupSpinnerUpDisabledIcon
    property string strSpinnerIconSelStateColor:"#616161"
    property string strSpinnerIconTchStateColor: "#4565a6"
    property string strSelectedStateAlarmArrowColor:"#616161"
    property string strSelectedStateUpAlarmArrowColor
    property string strSelectedStateDownAlarmArrowColor
    property string uniqueObjectID

    //Spinner Image Property
    property string strAlarmKeySpinnerUpArrowImage: strAlarmSetupSpinnerUpEnabledIcon
    property string strAlarmKeySpinnerDownArrowImage: strAlarmSetupSpinnerDownEnabledIcon

    //signals
    signal incrementValue(string objectName)
    signal decrementValue(string objectName)
    signal checkEndOfScaleForSpinnerWidget(string objectName)
    signal onSetArrowUpBgColor(string objectName)
    signal onSetArrowDownBgColor(string objectName)
    signal selection()

    //AlarmSetupBtn properties
    width: ivalueBoxWidth
    height: ivalueBoxHeight
    color: strBgColor
    radius: iValueBoxRadius
    border.width: iValueBoxBorderwidth
    border.color: strValueBoxBorderColor

    function setUniqueID(uniqueName) {
        valueBox.uniqueObjectID = uniqueName
        console.log("********uniqueObjectID ALARMSETUP************",valueBox.uniqueObjectID)
    }

    function stopAccelarationTimers(){
        bIsSpinnerTouchedUp = false ;
        bIsSpinnerTouchedDown = false ;
        valueBox.strSelectedStateUpAlarmArrowColor =strSelectedStateAlarmArrowColor
        valueBox.strSelectedStateDownAlarmArrowColor =strSelectedStateAlarmArrowColor
    }

    function stopTouch(){

        valueBox.color = strBgColor

    }
    Text {
        id: btnValue
        text: strValue
        anchors.centerIn: parent
        font.pointSize: iBtnValueFontSize
        color: strBtnValueFontColor
    }

    Rectangle {
        id: spinnerRectDown
        width: iSelectedStateAlarmArrowWidth
        height: iSelectedStateAlarmArrowHeight
        color: strSelectedStateDownAlarmArrowColor
        visible: false
        anchors.right: valueBox.left
        Rectangle{
            id: radiusDownArr
            width: 5
            height: spinnerRectDown.height
            anchors.right: spinnerRectDown.right
            color: spinnerRectDown.color
            visible: spinnerRectDown.visible
            z: -1
        }

        Image {
            id: spinnerRectDownIcon
            anchors.centerIn: parent
            source: strAlarmKeySpinnerDownArrowImage
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                decrementValue(valueBox.objectName)
            }
            onPressAndHold: {
                alarmParDecrementTimer.running = true ;
                alarmParDecrementTimer.restart() ;
                AlarmSetupController.restartAlarmMenuTimeoutTimer()
            }
            onPressed: {
                bIsSpinnerTouchedDown= true ;
                onSetArrowDownBgColor(valueBox.objectName) ;
                AlarmSetupController.restartAlarmMenuTimeoutTimer()
            }
            onReleased: {
                bIsSpinnerTouchedDown= false  ;
                valueBox.strSelectedStateDownAlarmArrowColor = strSpinnerIconSelStateColor;
                alarmParDecrementTimer.running = false ;
                alarmParDecrementTimer.stop() ;
                AlarmSetupController.restartAlarmMenuTimeoutTimer()
            }
            onExited: {
                bIsSpinnerTouchedDown= false  ;
                valueBox.strSelectedStateDownAlarmArrowColor = strSpinnerIconSelStateColor;
                alarmParDecrementTimer.running = false ;
                alarmParDecrementTimer.stop() ;
            }
        }
    }

    Rectangle {
        id: spinnerRectUp
        width: iSelectedStateAlarmArrowWidth
        height: iSelectedStateAlarmArrowHeight
        color: strSelectedStateUpAlarmArrowColor
        visible: false
        anchors.left: valueBox.right
        Rectangle{
            id: radiusUpArr
            width: 5
            height: spinnerRectUp.height
            anchors.left: spinnerRectUp.left
            color: spinnerRectUp.color
            visible: spinnerRectUp.visible
            z: -1
        }
        Image {
            id: spinnerRectUpIcon
            anchors.centerIn: parent
            source: strAlarmKeySpinnerUpArrowImage
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                console.log("incrment value for"+alarmSetupMenuId.objectName)
                incrementValue(valueBox.objectName)
            }
            onPressAndHold: {
                alarmParIncrementTimer.running = true ;
                alarmParIncrementTimer.restart() ;
                AlarmSetupController.restartAlarmMenuTimeoutTimer()
            }
            onPressed: {
                bIsSpinnerTouchedUp = true ;
                onSetArrowUpBgColor(valueBox.objectName);
                AlarmSetupController.restartAlarmMenuTimeoutTimer()
            }
            onReleased: {
                valueBox.strSelectedStateUpAlarmArrowColor = strSpinnerIconSelStateColor
                alarmParIncrementTimer.running = false ;
                bIsSpinnerTouchedUp = false ;
                alarmParIncrementTimer.stop() ;
                AlarmSetupController.restartAlarmMenuTimeoutTimer()
            }
            onExited: {
                valueBox.strSelectedStateUpAlarmArrowColor = strSpinnerIconSelStateColor
                alarmParIncrementTimer.running = false ;
                bIsSpinnerTouchedUp = false ;
                alarmParIncrementTimer.stop() ;
            }
        }
    }

    Keys.onRightPressed: {
        stopAccelarationTimers()
        if(valueBox.state === "Selected" || valueBox.state === "End of Scale"){
            incrementValue(valueBox.objectName);
            AlarmSetupController.restartAlarmMenuTimeoutTimer()
        }
        else if(valueBox.state === "Touched"){
            stopTouch()
            valueBox.state = "Scroll"
            AlarmSetupController.moveClockwiseAlarmSetup(valueBox.objectName)

        }
        else {
            AlarmSetupController.moveClockwiseAlarmSetup(valueBox.objectName)
        }
    }

    Keys.onLeftPressed: {
        stopAccelarationTimers()
        if(valueBox.state === "Selected" || valueBox.state === "End of Scale"){
            decrementValue(valueBox.objectName);
            AlarmSetupController.restartAlarmMenuTimeoutTimer()
        }
        else if(valueBox.state === "Touched"){
            valueBox.state = "Scroll"
            stopTouch()
            AlarmSetupController.moveAntiClockwiseAlarmSetup(valueBox.objectName)

        }
        else {
            AlarmSetupController.moveAntiClockwiseAlarmSetup(valueBox.objectName)
        }
    }

    Keys.onReturnPressed: {
        stopAccelarationTimers()
        if(valueBox.state === "Touched"){
            valueBox.state = "Selected"
            AlarmSetupController.onSecParamEnterPressed(valueBox.objectName)

        }

        else{
            AlarmSetupController.onSecParamEnterPressed(valueBox.objectName)
        }
    }

    MouseArea{
        anchors.fill: parent
        onPressed: {
            AlarmSetupController.onSecParamAlarmMenuTouched(valueBox.objectName)
        }
        onReleased: {
            AlarmSetupController.onSecParamAlarmMenuSelected(valueBox.objectName)
            AlarmSetupController.restartAlarmMenuTimeoutTimer()
        }
    }

    states: [State{
            name: "Scroll"
            when: valueBox.activeFocus
            PropertyChanges {
                target: valueBox;
                border.color: strAlarmParamScrollStateBorderColor ;
                border.width: iAlarmParamScrollStateBorderWidth ;
                radius:3
            }
        },
        State{
            name: "Selected"
            PropertyChanges {
                target: valueBox;
                color: strAlarmParamSelectedStateColor ;
                border.width: iAlarmParamSelectedStateBorderWidth;
                radius:0
            }
            PropertyChanges {
                target: btnValue; color: strAlarmParamSelectedStateFontColor

            }
            PropertyChanges {
                target: spinnerRectUp
                visible: true
                border.width: 0
                radius:3
            }
            PropertyChanges {
                target: spinnerRectDown
                visible: true
                border.width: 0
                radius:3

            }
            StateChangeScript {
                name: "example"
                script: {selection(); checkEndOfScaleForSpinnerWidget(valueBox.objectName)}
            }
        },
        State{
            name: "Active"
            PropertyChanges {
                target: valueBox; color: strAlarmParamActiveStateColor; radius:3
            }},
        State{
            name: "Touched"
            PropertyChanges {
                target: valueBox;  color: strAlarmParamTouchedStateColor; radius:3
            }},
        State{
            name: "End of Scale"
            PropertyChanges {
                target: valueBox; color: strAlarmParamEndOfScaleStateColor;border.width: 0; radius:0
            }
            PropertyChanges {
                target: btnValue; color: strAlarmParamSelectedStateFontColor

            }
            PropertyChanges {
                target: spinnerRectUp
                visible: true
                border.width: 0
                radius: 3
            }
            PropertyChanges {
                target: spinnerRectDown
                visible: true
                border.width: 0
                radius: 3

            }}
    ]

    onStateChanged: {
        if(state === "Selected"){
            checkEndOfScaleForSpinnerWidget(valueBox.objectName)
        }
    }

    onStrValueChanged: {
        checkEndOfScaleForSpinnerWidget(valueBox.objectName)
    }

    onBIsSpinnerTouchedUpChanged:{
        if (bIsSpinnerTouchedUp === false){
            alarmParIncrementTimer.stop() ;
        }
    }

    onBIsSpinnerTouchedDownChanged: {
        if(bIsSpinnerTouchedDown === false){
            alarmParDecrementTimer.stop() ;
        }
    }

    Timer {
        id: alarmParIncrementTimer
        running: false
        repeat: true
        interval: iAccelerationInterval
        onTriggered: {
            console.log("increment timer triggered menu spinner...")
            incrementValue(valueBox.objectName)
            onSetArrowUpBgColor(valueBox.objectName);
        }
    }

    Timer {
        id: alarmParDecrementTimer
        running: false
        repeat: true
        interval: iAccelerationInterval
        onTriggered: {
            console.log("decrement timer triggered menu spinner...")
            decrementValue(valueBox.objectName)
            onSetArrowDownBgColor(valueBox.objectName);
        }
    }
}





