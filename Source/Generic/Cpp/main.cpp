#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "../Source/Utilities/Include/layoutdomreader.h"
#include "../Source/Utilities/Include/componentdomreader.h"
#include "../Source/Utilities/Include/physicalparamdomreader.h"
#include "../Source/VentSettingArea/Include/settingfileupdate.h"
#include "../Source/VentSettingArea/Include/paramdualstorage.h"
#include "../Source/VentSettingArea/Include/ventconstraintchecker.h"
#include "../Source/CSB/Include/csbcommunication.h"
#include "../Source/Utilities/Include/consolemessageoutput.h"
#include "../Source/AlarmArea/Include/alarmreader.h"
#include "../Source/Utilities/Include/fontreader.h"
#include "../Source/Utilities/Include/checkoutreader.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QFileInfo>
#include <QTranslator>
#include <QDateTime>
#include "../Source/AlarmArea/Include/alarmdatamodel.h"

//Remove below commented line to execute the unit test case only.
//#define TEST_RUNNER

#ifdef TEST_RUNNER
#include <QtQuickTest/quicktest.h>
#include <QtQuickTest>
#include <QQmlEngine>
#include <QQmlContext>
#endif
#ifndef TEST_RUNNER

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qputenv("QT_IM_MODULE",QByteArray("qtvirtualkeyboard"));
    QGuiApplication app(argc, argv);
    QFileInfo exec_fileInfo(argv[0]);
    QString exFilePath = exec_fileInfo.absolutePath();

#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */


    QDateTime now = QDateTime::currentDateTime();
    QString timestamp = now.toString(QLatin1String("yyyyMMdd-hhmm"));
    consoleLogFilePath= "/var/log/messages"+timestamp ;
    qInstallMessageHandler(consoleMessageOutput);
#endif

    LayoutDomReader layoutReader(false,"no error");
    layoutReader.init(exFilePath+"/Layout.xml", ":/Config/Xml/LayoutSchema.xsd");

    PhysicalParamDomReader physicalParamReader(false,"no error");
    physicalParamReader.init(exFilePath+"/physicalParam.xml", ":/Config/Xml/physicalParamSchema.xsd");
    ComponentDomReader componentReader(false,"no error");
    componentReader.init(exFilePath+"/componentStyling.xml", ":/Config/Xml/componentStylingSchema.xsd");
    SettingFileUpdate settingreader(false);
    settingreader.init(exFilePath+"/setting.txt");

    Checkoutreader checkoutReader;
    checkoutReader.init(exFilePath+"/CheckoutCaliberation.xml",":/Config/Xml/CheckoutCaliberation.xsd");

    VentConstraintChecker ObjCC;
    CSBCommunication objCSBCom;
    VentSettingMismatch objVentMismatch;
    objCSBCom.setVentMismatch(&objVentMismatch);
    objVentMismatch.defaultSettings(&settingreader);
    ObjCC.setCsbObjCom(&objCSBCom);
    ObjCC.setSettingReader(&settingreader);
    ObjCC.setPhysicalParamReader(&physicalParamReader);
    ParamDualStorage dualStorageData(false);
    dualStorageData.init(settingreader.getMode(),settingreader.getRr(),settingreader.getIe(),settingreader.getTv(),settingreader.getPeep(),settingreader.getPinsp(),settingreader.getPmax(),settingreader.getBackupTime());
    ObjCC.setDualStorageObj(&dualStorageData);
    ObjCC.init();
    QString langName = settingreader.read("Language");
    QString langLoadPath = ":/Config/Localization/"+langName+".qm";
    AlarmReader alarmReader(false);   
    QTranslator translator;
    translator.load(langLoadPath);
    app.installTranslator(&translator);
    Fontreader objFont;
    QQmlApplicationEngine engine;
    if(componentReader.getErrorStat() || physicalParamReader.getErrorStat() || layoutReader.getErrorStat()){
     engine.load(QUrl(QStringLiteral("qrc:/Source/Generic/Qml/ErrorScreen.qml")));
     return app.exec();
    }else{

        engine.rootContext()->setContextProperty("geFont",&objFont);
        engine.rootContext()->setContextProperty("settingReader",&settingreader);
        engine.rootContext()->setContextProperty("backupData",&dualStorageData);
        engine.rootContext()->setContextProperty("layout",&layoutReader);
        engine.rootContext()->setContextProperty("physicalparam",&physicalParamReader);
        engine.rootContext()->setContextProperty("component",&componentReader);
        engine.rootContext()->setContextProperty("ObjCCValue",&ObjCC);
	    engine.rootContext()->setContextProperty("ventMismatch",&objVentMismatch);
        engine.rootContext()->setContextProperty("alarm",&alarmReader);
        engine.rootContext()->setContextProperty("objCSBValue",&objCSBCom);
        engine.rootContext()->setContextProperty("AlarmDataModel", alarmReader.getList());
        engine.rootContext()->setContextProperty("checkoutReader",&checkoutReader);

        engine.load(QUrl(QStringLiteral("qrc:/Source/Generic/Qml/main.qml")));
        if (engine.rootObjects().isEmpty())
            return -1;

        return app.exec();
    }
}

#if defined(__linux__) || defined(__FreeBSD__)   /* Linux & FreeBSD */
void consoleMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{

    QFile consoleLogFile(consoleLogFilePath);
    QTextStream outputStream(&consoleLogFile) ;
    if ( consoleLogFile.open(QIODevice::WriteOnly| QIODevice::Append | QIODevice::Text))
    {

        outputStream<<msg<<endl;
        consoleLogFile.close();
    }
}
#endif

#else
class Setup : public QObject
{
    Q_OBJECT
public:
    using QObject::QObject;

#ifdef QT_DEBUG
    Setup(): layoutReader(false,"no error"), componentReader(false,"no error"),settingreader(false),physicalParamReader(false,"no error"),objCSBCom(),dualStorageData(false), ObjCC() {qDebug()<<dir.currentPath()+"/debug"<<endl;}
#else
    Setup(): layoutReader(false,"no error"), componentReader(false,"no error"),settingreader(false),physicalParamReader(false,"no error"),objCSBCom(),dualStorageData(false), ObjCC() {qDebug()<<dir.currentPath()+"/release"<<endl;}
#endif

public slots:

#if QT_VERSION >= QT_VERSION_CHECK(5, 12,0)
    void applicationAvailable(){
    }
#endif
    void qmlEngineAvailable(QQmlEngine *engine)
    {

        layoutReader.init(":/Config/Xml/Layout.xml", ":/Config/Xml/LayoutSchema.xsd");
        componentReader.init(":/Config/Xml/componentStyling.xml", ":/Config/Xml/componentStylingSchema.xsd");
        physicalParamReader.init(":/Config/Xml/physicalParam.xml", ":/Config/Xml/physicalParamSchema.xsd");
#ifdef QT_DEBUG
        settingreader.init(dir.currentPath()+"/debug/settingTst.txt");
#else
        settingreader.init(dir.currentPath()+"/release/settingTst.txt");        
#endif
        objCSBCom.setVentMismatch(&objVentMismatch);
        objVentMismatch.defaultSettings(&settingreader);
        ObjCC.setCsbObjCom(&objCSBCom);
        ObjCC.setSettingReader(&settingreader);
        ObjCC.setPhysicalParamReader(&physicalParamReader);

        engine->rootContext()->setContextProperty("settingReader",&settingreader);
        engine->rootContext()->setContextProperty("layout",&layoutReader);
        engine->rootContext()->setContextProperty("component",&componentReader);
        engine->rootContext()->setContextProperty("physicalparam",&physicalParamReader);
        engine->rootContext()->setContextProperty("objCSBValue",&objCSBCom);
        engine->rootContext()->setContextProperty("backupData",&dualStorageData);
        engine->rootContext()->setContextProperty("ObjCCValue",&ObjCC);
        engine->rootContext()->setContextProperty("ventMismatch",&objVentMismatch);
    }
#if QT_VERSION >= QT_VERSION_CHECK(5, 12,0)
    void cleanupTestCase(){
    }
#endif
private:
    LayoutDomReader layoutReader;
    ComponentDomReader componentReader;
    SettingFileUpdate settingreader;
    PhysicalParamDomReader physicalParamReader;
    CSBCommunication objCSBCom;
    ParamDualStorage dualStorageData;
    VentConstraintChecker ObjCC;
    QDir dir;
    VentSettingMismatch objVentMismatch;
};
#define QUICK_TEST_SOURCE_DIR "../UnitTestCases"
QUICK_TEST_MAIN_WITH_SETUP(JARVISGUI, Setup)
#include "main.moc"
#endif
