#ifndef VENTSETTINGS_H
#define VENTSETTINGS_H

#define VENT_MISMATCH_CNT 5
#define CSB_BANNER_COMMAND 196
#define CSB_DUALSTRG_DATA 221
#define CSB_VENTMISMATCH_DATA 238

enum VentModeParamTypes{
    RR = 1,
    LOW_tv = 4,
    HIGH_tv = 6,
    PEEP = 8,
    PInsp = 24,
    IE = 2,
    PMax = 9,
    PSupport = 21,
    FlowTrigger = 3,
    TInsp = 27,
    TriggerWindow = 26,
    EndOfBreath = 7,
    BackUpTime = 18,
    ExitBackUp = 19,
    RRMech = 20,
    TPause = 22

};

enum  ventilationModes{
    BAG_mode=0,
    VCV_mode = 1,
    PCV_mode = 2,
    STANDBY_mode=3,
    SERVICE_mode=4,
    TEST_mode=5,
    ACGO_mode=6,
    IDLE_mode=7,
    SIMV_VCV_mode = 8,
    SIMV_PCV_mode = 9,
    PSV_pro_mode = 10,
    CHECKOUT_mode = 11,
    MECHANICAL_mode = 23
};

#endif // VENTSETTINGS_H
