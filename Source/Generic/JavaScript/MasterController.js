Qt.include("qrc:/Source/Generic/JavaScript/AlarmLogMenuController.js")

var ventGroupcomponent;
var alarmGroupComponent;
var numericGroupComponent;
var buttonBarGroupComponent;
var waveformGroupComponent;
var ventGroupContainer;
var buttonBarGroupContainer;
var alarmGroupContainer;
var numericGroupContainer;
var waveformGroupContainer;

var isKeyPressedInSequence = false
var sequenceList215 = [2,1,5]
var sequenceList216 = [2,1,6]
var sequenceList13 = [1,3]
var groupList = ["VentGroup","ButtonGroup"]


var ventGrpWidth = parseInt(layout.getGroupLayoutDataByTag("VentGroup","Dimension","width"))
var ventGrpHeight = parseInt(layout.getGroupLayoutDataByTag("VentGroup","Dimension","height"))
var ventGrpColor = component.getComponentXmlDataByTag("Group","VentGroup","Color")


var iconGrpWidth = parseInt(layout.getGroupLayoutDataByTag("IconGroup","Dimension","width"))
var iconGrpHeight = parseInt(layout.getGroupLayoutDataByTag("IconGroup","Dimension","height"))
var iconGrpColor = component.getComponentXmlDataByTag("Group","IconGroup","Color")

var waveGrpWidth = parseInt(layout.getGroupLayoutDataByTag("WaveformGroup","Dimension","width"))
var waveGrpHeight = parseInt(layout.getGroupLayoutDataByTag("WaveformGroup","Dimension","height"))
var waveGrpColor = component.getComponentXmlDataByTag("Group","WaveformGroup","Color")

var numGrpWidth = parseInt(layout.getGroupLayoutDataByTag("NumericGroup","Dimension","width"))
var numGrpHeight = parseInt(layout.getGroupLayoutDataByTag("NumericGroup","Dimension","height"))
var numGrpColor = component.getComponentXmlDataByTag("Group","NumericGroup","Color")

var alarmGrpWidth = parseInt(layout.getGroupLayoutDataByTag("AlarmGroup","Dimension","width"))
var alarmGrpHeight = parseInt(layout.getGroupLayoutDataByTag("AlarmGroup","Dimension","height"))
var alarmGrpColor = component.getComponentXmlDataByTag("Group","AlarmGroup","Color")


function createGroup() {
    console.log("inside Create Group")

    ventGroupcomponent = Qt.createComponent("qrc:Source/VentSettingArea/Qml/VentGroup.qml");
    alarmGroupComponent = Qt.createComponent("qrc:Source/AlarmArea/Qml/AlarmGroup.qml");
    numericGroupComponent = Qt.createComponent("qrc:Source/NumericsArea/Qml/NumericGroup.qml");
    buttonBarGroupComponent = Qt.createComponent("qrc:Source/ButtonBarArea/Qml/ButtonBarGroup.qml")
    waveformGroupComponent = Qt.createComponent("qrc:Source/WaveFormArea/Qml/WaveGroup.qml")

    if(ventGroupcomponent.status === Component.Ready && alarmGroupComponent.status === Component.Ready && numericGroupComponent.status === Component.Ready
            && buttonBarGroupComponent.status === Component.Ready && waveformGroupComponent.status === Component.Ready){
        finishGroupCreation();
    } else {
        ventGroupcomponent.statusChanged.connect(finishGroupCreation);
        alarmGroupComponent.statusChanged.connect(finishGroupCreation);
        numericGroupComponent.statusChanged.connect(finishGroupCreation);
        buttonBarGroupComponent.statusChanged.connect(finishGroupCreation);
        waveformGroupComponent.statusChanged.connect(finishGroupCreation);
    }
}

function finishGroupCreation() {
    console.log("inside finishgroupcreation")
    ventGroupContainer = ventGroupcomponent.createObject(mainitem,{"iVentGrpWidth":ventGrpWidth,"iVentGrpHeight":ventGrpHeight,"strVentGrpColor":ventGrpColor})
    buttonBarGroupContainer = buttonBarGroupComponent.createObject(mainitem,{"iIconGrpWidth":iconGrpWidth,"iIconGrpHeight":iconGrpHeight,"strIconGrpColor":iconGrpColor})
    alarmGroupContainer = alarmGroupComponent.createObject(mainitem,{"iAlarmGrpWidth":alarmGrpWidth,"iAlarmGrpHeight":alarmGrpHeight,"strAlarmGrpColor":alarmGrpColor})
    numericGroupContainer = numericGroupComponent.createObject(mainitem,{"iNumGrpWidth":numGrpWidth,"iNumGrpHeight":numGrpHeight,"strNumGrpColor":numGrpColor})
    waveformGroupContainer = waveformGroupComponent.createObject(mainitem,{"iWaveGrpWidth":waveGrpWidth,"iWaveGrpHeight":waveGrpHeight,"strWaveGrpColor":waveGrpColor})

    numericGroupContainer.anchors.top = alarmGroupContainer.bottom
    waveformGroupContainer.anchors.top = alarmGroupContainer.bottom
    numericGroupContainer.anchors.right = buttonBarGroupContainer.left

}

//*******************************************
// Purpose: To handle short cut key press event from the main layout
// Input Parameters: Currently pressed key
// Description: Handles short cut key press -  'I' - >for displaying version
// 'A' -> for opening alarm setup menu
// other key events - > pass it to the ventGroupController (such as 'C','D','E','F','G','V')
// '215','216','k','13' - > Handles in alram setup menu if it's opened else pass it to the
// vent group to handle for the quickkeys
//***********************************************/
function shortcutPressMain(currentKey) {
    console.log("shortcutPressMain!!...........")
    var key = String.fromCharCode(currentKey)
    alarmKeySequenceList.push(key);
    switch(key) {
    case 'I':
    case 'i':
        console.log("shortcutPressMain - case I!!")
        if( bIsVPopupVisible === false) {
            versionPopUp.visible = true
            infoID.start()
            bIsVPopupVisible = true
        } else if ( bIsVPopupVisible === true){
            versionPopUp.visible = false
            bIsVPopupVisible = false
        }
        break

    case 'A':
    case 'a':
        console.log("shortcutPressMain!! - case A")
        if(bIsAlarmSMOpen === false) {
            GrpCtrl.destroyMenus(currentKey, "")
            AlarmSetupController.createAlarmSetupMenu()
        }

        break
    case 'C':
    case 'D':
    case 'E':
    case 'F':
    case 'G':
    case 'V':
    case 'P':
    case 'Q':
        console.log("shortcutPressMain!! - others" + key)
        if(bIsAlarmSMOpen === true) {
            AlarmSetupController.destroyAlarmSetupMenu()
        }
        mainitem.shortcut_keyPressed(currentKey, "")
        break

    case 'K':
    case '2':
    case '1':
    case '5':
    case '6':
    case '3':
        var alarmLogMenuObj = getGroupRef("AlarmLogMenuObj")
        console.log("......>>>>>",bIsAlarmSMOpen,alarmLogMenuObj,typeof(alarmLogMenuObj))
        if(bIsAlarmSMOpen === true && typeof(alarmLogMenuObj) === "undefined") { // to Check whether only the alarm setup menu is open.
            console.log("ALARM MENU IS OPEN->>>>",key)
            if (key === 'K') {
                AlarmSetupController.shortcutInsideAlarmMenu()
            } else if (key === '1') {
                bIsKeyOnePressedInAlarm = true
                if (bIsKeyTwoPressedInAlarm) {
                    isKeyPressedInSequence = true
                }
            } else if (key === '3') {
                if (!arraysIdentical(alarmKeySequenceList,sequenceList13)) {
                    alarmKeySequenceList.length = 0
                    return
                } else {
                    alarmKeySequenceList.length = 0
                    bIsKeyThreePressedInAlarm = true
                    if (bIsKeyOnePressedInAlarm && bIsKeyThreePressedInAlarm) {
                        AlarmSetupController.shortcutInsideAlarmMenu()
                    }
                }
            } else if (key === '2') {
                bIsKeyTwoPressedInAlarm = true
            } else if (key === '5') {
                if (!arraysIdentical(alarmKeySequenceList,sequenceList215)) {
                    alarmKeySequenceList.length = 0
                    return
                } else {
                    alarmKeySequenceList.length = 0
                    bIsKeyFivePressedInAlarm = true
                    if (bIsKeyTwoPressedInAlarm && bIsKeyOnePressedInAlarm && bIsKeyFivePressedInAlarm && isKeyPressedInSequence){
                        AlarmSetupController.shortcut215InsideAlarmMenu()
                        isKeyPressedInSequence = false
                    }
                }
            } else if (key === '6') {
                if (!arraysIdentical(alarmKeySequenceList,sequenceList216)) {
                    alarmKeySequenceList.length = 0
                    return
                } else {
                    alarmKeySequenceList.length = 0
                    bIsKeySixPressedInAlarm = true
                    if (bIsKeyTwoPressedInAlarm && bIsKeyOnePressedInAlarm && bIsKeySixPressedInAlarm & isKeyPressedInSequence){
                        AlarmSetupController.shortcut216InsideAlarmMenu()
                        isKeyPressedInSequence = false
                    }
                }
            }
        } else if (alarmLogMenuObj !== null && typeof(alarmLogMenuObj) !== "undefined") {
            console.log("ALARM LOG MENU OPEN......")
            if (key === 'K') {
                shortcutInsideLogMenu()
            } else if (key === '1') {
                bIsKeyOnePressedInAlarm = true
            } else if (key === '3') {
                bIsKeyThreePressedInAlarm = true
                if (bIsKeyOnePressedInAlarm && bIsKeyThreePressedInAlarm) {
                    shortcutInsideLogMenu()
                }
            }
        } else {
            console.log("SEND SIGNAL TO GROUP!!!!")
            mainitem.shortcut_keyPressed(currentKey, "")
        }

        break ;
    }
}

//*******************************************
// Purpose: To handle short cut key release event from the main layout
// Input Parameters: Currently released key
// Description: Handles short cut key release - when a particular key is pressed
// and released
//***********************************************/
function shortcutReleaseMain(currentKey) {
    console.log("shortcutReleaseMain!!...........")
    var key = String.fromCharCode(currentKey)
    switch(key) {
    case '1':
        if(bIsAlarmSMOpen === true) {
            console.log("1 .... released in alarm ")
            bIsKeyOnePressedInAlarm = false
        } else {
            mainitem.shortcut_keyReleased(currentKey, "")
        }
        bIsKeyOnePressedInAlarm = false

        break
    case '3':
        if(bIsAlarmSMOpen === true) {
            bIsKeyThreePressedInAlarm = false
        } else {
            mainitem.shortcut_keyReleased(currentKey, "")
        }

        break
    case '2':
        if(bIsAlarmSMOpen === true) {
            bIsKeyTwoPressedInAlarm = false
        } else {
            mainitem.shortcut_keyReleased(currentKey, "")
        }

        break
    case '5':
        if(bIsAlarmSMOpen === true) {
            bIsKeyFivePressedInAlarm = false
        } else {
            mainitem.shortcut_keyReleased(currentKey, "")
        }

        break
    case '6':
        if(bIsAlarmSMOpen === true) {
            bIsKeySixPressedInAlarm = false
        } else {
            mainitem.shortcut_keyReleased(currentKey, "")
        }

        break
    default:
        mainitem.shortcut_keyReleased(currentKey, "")
        break

    }
}

//*******************************************
// Purpose: To check if the arrays are identical (for checking correct key sequence)
// to perform shortcut key handling
// Input Parameters: Actual key sequence and the key sequence pressed
//***********************************************/
function arraysIdentical(actualSequence, storedSequence) {
    var length = actualSequence.length;
    if (length !== storedSequence.length) {
        return false;
    }
    while (length--) {
        if (parseInt(actualSequence[length]) !== parseInt(storedSequence[length])) {
            return false;
        }
    }
    return true;
}

//*******************************************
// Purpose: To Handle focus shift when the someother application is opened and comes
// back to the Jarvis application in windows
// Input Parameters: Currently focused item in the application and the focus count(to ignore handling
// during launch of the application)
// Description: Handles the focus shift by storing the previously focused item before the application
// switch , and using the previousActiveItem to stop the timer(in case of quickkeys)/cancel the mode change/
// close the menu(if any) and returns to the normal state.
//***********************************************/
function handleWindowsFocusShift(activeFocusItem, iFocusCount) {
    if (activeFocusItem !== null) {
        previousActiveItem = activeFocusItem
    } else if (activeFocusItem === null && iFocusCount !== 1) {
        var ventGrpRef = getGroupRef("VentGroup")
        if (previousActiveItem.strType === "quickKeys" ) {
            var getQKRef = getCompRef(ventGrpRef.ventParentId,previousActiveItem.objectName)
            if (!previousActiveItem.bModeChanged) {
                getQKRef.strValue = getQKRef.strVentPreviousVal
                getQKRef.stopInitialTimer()
                getQKRef.stopFinalTimer()
            } else if (previousActiveItem.bModeChanged) {
                var qkRef = getCompRef(ventGrpRef.ventParentId,"quickKey1")
                getQKRef.bIsTimerRunning = false
                getQKRef.bIsTotalTimerRunning = false
                getQKRef.stopTimers()
                GrpCtrl.onCancellingModeConfirmation()
            }
        } else if (previousActiveItem.parent.parent.objectName !== null && previousActiveItem.parent.objectName !== null) {
            if (previousActiveItem.parent.parent.objectName === "VentMenuObj" || previousActiveItem.parent.objectName === "VentMenuObj") {
                VentModeController.destroyVentMenu(false)
                var ventGrpObj = getGroupRef("VentGroup")
                var modeBtnRef = getCompRef(ventGrpObj.ventParentId,"ventModeBtn")
                modeBtnRef.state = "Active"
            } else if (previousActiveItem.parent.parent.parent.objectName === "MoreSettingsMenuObj" || previousActiveItem.parent.objectName === "MoreSettingsMenuObj") {
                MoreSettingsCtrl.destroyMoreSettingMenu()
            } else if (previousActiveItem.parent.objectName === "AlarmSetupMenuObj" ||
                       previousActiveItem.parent.objectName === "AlarmLogMenuObj" ||
                       previousActiveItem.parent.objectName === "ScrollView") {
                AlarmSetupController.destroyAlarmSetupMenu()
            }
        }
        mainitem.forceActiveFocus()
        mainitem.focus = true
    }
}

//*******************************************
// Purpose: To get the next group where the focus should shift.
// Input Parameters: Current group name and naviagtion type(right)
// Description: Gets the current groupName once it reaches the last element in
// the group and loops through the groupList to get the next group and call the
// corresponding navigation change function in that group
//***********************************************/
function groupNavigatorOnRight(groupName, key) {
    console.log("***In Master Controller->>>groupNavigatorOnRight",groupName)
    var groupIndex = 0;
    //Loop through Array of groups to identify the index of next group
    for(var i = 0; i < groupList.length; i++) {
        if(groupName === groupList[i]) {
            groupIndex = groupIndex+1; //position of next group
            break;
        } else {
            groupIndex++;
        }
    }
    //Scenario when Right key is pressed on last elemnt of last group in referred array
    if(groupIndex === groupList.length) {
        groupIndex = 0
    }
    //To Navigate to next group, get the object of it.
    var groupReference = getGroupRef(groupList[groupIndex]);
    groupReference.groupNavigationChange(key);
}

//*******************************************
// Purpose: To get the previous group where the focus should shift.
// Input Parameters: Current group name and naviagtion type(left)
// Description: Gets the current groupName once it reaches the first element in
// the group and loops through the groupList to get the previous group and call the
// corresponding navigation change function in that group
//***********************************************/
function groupNavigatorOnLeft(groupName,key) {
    var groupIndex=0;
    //Loop through Array of groups to identify the index of next group
    for(var i=0; i < groupList.length; i++) {
        if(groupName === groupList[i]) {
            groupIndex = groupIndex - 1; //position of next group
            break;
        } else {
            groupIndex++;
        }
    }
    //Scenario when Left key is pressed on last elemnt of last group in referred array
    if(groupIndex < 0) {
        groupIndex = groupList.length - 1
    }
    //To Navigate to next group, get the object of it.
    var groupReference = getGroupRef(groupList[groupIndex]);
    groupReference.groupNavigationChange(key);
}
