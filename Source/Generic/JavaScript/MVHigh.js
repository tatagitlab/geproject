var mVHighRangeList;
var mVHighMin;
var mVHighMax;
var mVHighStepSize;
mVHighRangeList = physicalparam.getRangeData("MV_High", "l/min")
mVHighMin = mVHighRangeList[0]
mVHighMax = mVHighRangeList[1]
mVHighStepSize = mVHighRangeList[2]

/*Increment step Logic for MV High*/
function clkHandleMVHigh(m_mvHigh) {
    var mVLowValue = parseFloat(settingReader.read("MV_Low"))
    if(mVLowValue < parseFloat(mVHighMin))
    {
        mVLowValue = parseFloat(mVHighMin)
    }
    if((m_mvHigh >= mVLowValue) && (m_mvHigh < parseFloat(mVHighMax))) {
        m_mvHigh = (parseFloat(m_mvHigh + parseFloat(mVHighStepSize))).toFixed(1) ;
    }
    return m_mvHigh;
}

/*Decrement step Logic for MV High*/
function antClkHandleMVHigh(m_mvHigh){
    m_mvHigh = (parseFloat(m_mvHigh - parseFloat(mVHighStepSize))).toFixed(1) ;
    return m_mvHigh;
}

function onMVHighIncrementValue(objectName) {
    console.log("onMVHighIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(alarmSetupMenuId, objectName)
    console.log(currentAlarmSetupComp+"................")
    if(objectName === "MVHighObj"){
        if(parseFloat(currentAlarmSetupComp.strValue) < parseFloat(mVHighMax)){
            currentAlarmSetupComp.strValue = clkHandleMVHigh(parseFloat(currentAlarmSetupComp.strValue))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onMVHighDecrementValue(objectName) {
    console.log("onMVHighDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(alarmSetupMenuId, objectName)
    if(objectName === "MVHighObj"){
        var mVLowValue = parseFloat(settingReader.read("MV_Low"))
        if (mVLowValue < parseFloat(mVHighMin)) {
            mVLowValue = parseFloat(mVHighMin)
        } else {
            mVLowValue = parseFloat(mVLowValue +parseFloat(mVHighStepSize))
        }
        if(parseFloat(currentAlarmSetupComp.strValue) > parseFloat(mVLowValue)){
            currentAlarmSetupComp.strValue = antClkHandleMVHigh(parseFloat(currentAlarmSetupComp.strValue))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onMVHighEndOfScale(objectName){
    console.log("checking end of scale MVHigh")
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    if((objectName === "MVHighObj" ))
    {var mVLowValue = parseFloat(settingReader.read("MV_Low"))
        var mVHighLimit = parseFloat(mVHighMax)
        if (mVLowValue < parseFloat(mVHighMin)) {
            mVLowValue =parseFloat(mVHighMin)
        } else {
            mVLowValue = parseFloat(mVLowValue) + parseFloat(mVHighStepSize)
        }
        if(parseInt(currentVentComp.strValue) === parseInt(mVLowValue)) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(parseInt(currentVentComp.strValue) === parseInt(mVHighLimit)) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(parseInt(currentVentComp.strValue )!== parseInt(mVLowValue) &&  parseInt(currentVentComp.strValue) !== parseInt(mVHighLimit)) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetMVHighUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    console.log("setting up arrow bg MV"+currentVentComp)
    if((objectName === "MVHighObj" )){
        var mVHighlimit = parseFloat(mVHighMax)
        if(parseInt(currentVentComp.strValue) === parseInt(mVHighlimit) ){
            console.log("changing Color  of Up arrow for Select state...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing Color  of Up arrow for Touch state..........")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

function onSetMVHighDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    console.log("setting down arrow bg MV High"+currentVentComp.strValue)
    if((objectName === "MVHighObj" )){
        var mVLowValue = parseFloat(settingReader.read("MV_Low"))
        if (mVLowValue < parseFloat(mVHighMin)) {
            mVLowValue =parseFloat(mVHighMin)
        } else {
            mVLowValue = parseFloat(mVLowValue) +parseFloat(mVHighStepSize)
        }
        if(parseInt(currentVentComp.strValue) === parseInt(mVLowValue) ){
            console.log(" changing select color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log(" changing touch color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
