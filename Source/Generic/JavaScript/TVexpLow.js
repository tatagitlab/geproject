var tVexpLowRangeList;
var tVexpLowMin1;
var tVexpLowMax1;
var tVexpLowStepSize1;
var tVexpLowMin2;
var tVexpLowMax2;
var tVexpLowStepSize2;

tVexpLowRangeList = physicalparam.getRangeData("TVexp_Low", "ml")
tVexpLowMin1 = tVexpLowRangeList[0]
tVexpLowMax1 = tVexpLowRangeList[1]
tVexpLowStepSize1 = tVexpLowRangeList[2]
tVexpLowMin2 = tVexpLowRangeList[3]
tVexpLowMax2 = tVexpLowRangeList[4]
tVexpLowStepSize2 = tVexpLowRangeList[5]



/*Increment step Logic for TVexp Low*/
function clkHandleTVolumeExpLow(m_tVolumeLow)
{
    var tVexpHighValue = parseInt(settingReader.read("TVexp_High"))
    var highLimit1 = parseInt(tVexpLowMax1)
    var highLimit2 = parseInt(tVexpLowMax2)
    console.log("m_tVolumeLow " + m_tVolumeLow)
    if(tVexpHighValue <= highLimit1){
        highLimit1 = tVexpHighValue - parseInt(tVexpLowStepSize1)
    }
    if(tVexpHighValue <= highLimit2){
        highLimit2 = tVexpHighValue - parseInt(tVexpLowStepSize2)    }
    if(m_tVolumeLow < highLimit1) {
        m_tVolumeLow = m_tVolumeLow+ parseInt(tVexpLowStepSize1);
    }
    else if(m_tVolumeLow < highLimit2) {
        m_tVolumeLow = m_tVolumeLow+parseInt(tVexpLowStepSize2);
    }
    return m_tVolumeLow;
}

/*Decrement step Logic for TVexp Low*/
function antClkHandleTVolumeExpLow(m_tVolumeLow)
{
    if((parseInt(tVexpLowMax1) < m_tVolumeLow) && (m_tVolumeLow <= parseInt(tVexpLowMax2))) {
        m_tVolumeLow = m_tVolumeLow - parseInt(tVexpLowStepSize2) ;
    }
    else if((parseInt(tVexpLowMin1) < m_tVolumeLow) && (m_tVolumeLow <= parseInt(tVexpLowMin2))) {
        m_tVolumeLow = m_tVolumeLow - parseInt(tVexpLowStepSize1);
    }
    return m_tVolumeLow;
}

function onTVexpLowIncrementValue(objectName) {
    console.log("onTVexpLowIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(alarmSetupMenuId, objectName)
    if(objectName === "TVexpLowObj"){
        var tVexpHighValue = parseInt(settingReader.read("TVexp_High"))
        var highLimit = parseInt(tVexpLowMax2)
        if (tVexpHighValue <= highLimit) {
            highLimit = tVexpHighValue -  parseInt(tVexpLowStepSize2)
        }
        if(parseInt(currentAlarmSetupComp.strValue )< highLimit){
            currentAlarmSetupComp.strValue = parseInt(clkHandleTVolumeExpLow(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onTVexpLowDecrementValue(objectName) {
    console.log("onTVexpLowDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(alarmSetupMenuId, objectName)
    if(objectName === "TVexpLowObj"){
        if(parseInt(currentAlarmSetupComp.strValue) > parseInt(tVexpLowMin1)){
            currentAlarmSetupComp.strValue = parseInt(antClkHandleTVolumeExpLow(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onTVexpLowEndOfScale(objectName){
    console.log("checking end of scale TVexpLow")
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    if((objectName === "TVexpLowObj" ))
    { var tVexpHighValue = parseInt(settingReader.read("TVexp_High"))
        var highLimit = parseInt(tVexpLowMax2)
        if (tVexpHighValue <= highLimit) {
            highLimit = tVexpHighValue - parseInt(tVexpLowStepSize2)
        }
        if(currentVentComp.strValue === tVexpLowMin1) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(currentVentComp.strValue === highLimit.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !==tVexpLowMin1 && currentVentComp.strValue !== highLimit.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetTVexpLowUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    console.log("setting up arrow bg PpeakLow"+currentVentComp)
    if((objectName === "TVexpLowObj" )){
        var tVexpHighValue = parseInt(settingReader.read("TVexp_High"))
        var highLimitTVLow = parseInt(tVexpLowMax2)
        if (tVexpHighValue <= highLimitTVLow) {
            highLimitTVLow = tVexpHighValue - parseInt(tVexpLowStepSize2)
        }
        if(parseInt(currentVentComp.strValue) === parseInt(highLimitTVLow) ){
            console.log("changing Color  of Up arrow to select...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing Color  of Up arrow to touch....")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}



function onSetTVexpLowDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(alarmSetupMenuId, objectName)
    console.log("setting down arrow bg TVexpLow"+currentVentComp.strValue)
    if((objectName === "TVexpLowObj" )){
           if(parseInt(currentVentComp.strValue) === parseInt(tVexpLowMin1)){
            console.log(" changing select color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log(" changing touch color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}


