var checkoutComp;
var otherEquipComp;
var checkoutObj;
var otherEquipObj;
var byPassComp;
var byPassObj;

var chkoutAreaHeight = parseInt(layout.getGroupLayoutDataByTag("Checkout","Dimension","height"));
var chkoutAreaWidth = parseInt(layout.getGroupLayoutDataByTag("Checkout","Dimension","width"));
var chkoutAreaColor = component.getComponentXmlDataByTag("Component","Checkout","ChkAreaColor")
var titleAreaHeight = parseInt(component.getComponentXmlDataByTag("Group","Checkout","TitleAreaHeight"))
var titleAreaWidth = parseInt(component.getComponentXmlDataByTag("Group","Checkout","TitleAreaWidth"))
var chkoutInstAreaHeight = component.getComponentXmlDataByTag("Group","Checkout","ChkoutInstAreaHeight")
var chkoutInstAreaWidth = component.getComponentXmlDataByTag("Group","Checkout","ChkoutInstAreaWidth")
var chkoutPicAreaHeight = component.getComponentXmlDataByTag("Group","Checkout","chkoutPicAreaHeight")
var chkoutPicAreaWidth = component.getComponentXmlDataByTag("Group","Checkout","chkoutPicAreaWidth")
var actionBtnWidth = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ActionBtnWidth"))
var actionBtnHeight = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ActionBtnHeight"))
var actionBtnbgColor = component.getComponentXmlDataByTag("Group","Checkout","ActionBtnBgColor")
var actionBtnborderColor = component.getComponentXmlDataByTag("Group","Checkout","ActionBtnBorderColor")
var actionBtnborderWidth = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ActionBtnBorderWidth"))
var actionBtnradius = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ActionBtnRadius"))
var scrollStateBorderColor = component.getComponentXmlDataByTag("Group","Checkout","ScrollStateBorderColor")
var scrollStateBorderWidth = parseInt(component.getComponentXmlDataByTag("Group","Checkout","ScrollStateBorderWidth"))
var fontSize = parseInt(component.getComponentXmlDataByTag("Group","Checkout","FontSize"))
var instructionTextArea = component.getComponentXmlDataByTag("Group","Checkout","InstructionTextArea")
var fontColor = component.getComponentXmlDataByTag("Group","Checkout","FontColor")
var titleFontSize = parseInt(component.getComponentXmlDataByTag("Group","Checkout","TitleFontSize"))
var closeBtnSource = component.getComponentXmlDataByTag("Group","Checkout","CloseBtnCheckout")
var closeBtnTchSource = component.getComponentXmlDataByTag("Group","Checkout","CloseBtnTouchedCheckout")

var byPassPopupWidth = component.getComponentXmlDataByTag("Group","Checkout","ByPassPopupWidth")
var byPassPopupHeight = component.getComponentXmlDataByTag("Group","Checkout","ByPassPopupHeight")
var byPassPopupBgColor = component.getComponentXmlDataByTag("Group","Checkout","ByPassPopupBgColor")
var byPassBorderColor = component.getComponentXmlDataByTag("Group","Checkout","ByPassBorderColor")
var byPassBorderWidth = component.getComponentXmlDataByTag("Group","Checkout","ByPassBorderWidth")
var byPassPopupFontColor = component.getComponentXmlDataByTag("Group","Checkout","ByPassPopupFontColor")
var popupColumnTextPixelSize =  component.getComponentXmlDataByTag("Group","Checkout","PopupColumnTextPixelSize")
var popupColumnTextPixelHeight =  component.getComponentXmlDataByTag("Group","Checkout","PopupColumnTextPixelHeight")
var popupColumnTextPixelLeftMargin =  component.getComponentXmlDataByTag("Group","Checkout","PopupColumnTextPixelLeftMargin")
var byPassSubButtonTopMargin = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonTopMargin")
var byPassSubButtonLeftMargin = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonLeftMargin")
var popupTextLeftMargin = component.getComponentXmlDataByTag("Group","Checkout","PopupTextLeftMargin")
var popupTextTopMargin = component.getComponentXmlDataByTag("Group","Checkout","PopupTextTopMargin")
var popupThirdTextTopMargin = component.getComponentXmlDataByTag("Group","Checkout","PopupThirdTextTopMargin")
var byPassSubButtonWidth = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonWidth")
var byPassSubButtonHeight =component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonHeight")
var byPassSubButtonBottomMargin = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonBottomMargin")
var byPassSubButtonRightMargin = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonRightMargin")
var byPassSubButtonColor = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonColor")
var byPassSubButtonBorderColor = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonBorderColor")
var byPassSubButtonPixelSize = component.getComponentXmlDataByTag("Group","Checkout","ByPassSubButtonPixelSize")
var priorTestCheckInstructionWidth = component.getComponentXmlDataByTag("Group","Checkout","PriorTestCheckInstructionWidth")
var priorTestCheckInstructionHeight = component.getComponentXmlDataByTag("Group","Checkout","PriorTestCheckInstructionHeight")
var priorTestCheckInstructionLeftMargin = component.getComponentXmlDataByTag("Group","Checkout","PriorTestCheckInstructionLeftMargin")
var confirmButtonLeftMargin = component.getComponentXmlDataByTag("Group","Checkout","ConfirmButtonLeftMargin")
var confirmButtonBottomMargin = component.getComponentXmlDataByTag("Group","Checkout","ConfirmButtonBottomMargin")
var skipButtonLeftMargin = component.getComponentXmlDataByTag("Group","Checkout","SkipButtonLeftMargin")
var skipButtonBottomMargin = component.getComponentXmlDataByTag("Group","Checkout","SkipButtonBottomMargin")
var subTitleTextPixelSize = component.getComponentXmlDataByTag("Group","Checkout","SubTitleTextPixelSize")
var subTitleTextTopMargin = component.getComponentXmlDataByTag("Group","Checkout","SubTitleTextTopMargin")
var subTitleTextLeftMargin = component.getComponentXmlDataByTag("Group","Checkout","SubTitleTextLeftMargin")
var preCheckListRectColor = component.getComponentXmlDataByTag("Group","Checkout","PreCheckListRectColor")
var preCheckListTopMargin = component.getComponentXmlDataByTag("Group","Checkout","PreCheckListTopMargin")
var closeButtonWidth = component.getComponentXmlDataByTag("Group","Checkout","CloseButtonWidth")
var closeButtonHeight = component.getComponentXmlDataByTag("Group","Checkout","CloseButtonHeight")
var closeButtonRadius = component.getComponentXmlDataByTag("Group","Checkout","CloseButtonRadius")
var closeButtonTopMargin = component.getComponentXmlDataByTag("Group","Checkout","CloseButtonTopMargin")
var closeButtonRightMargin = component.getComponentXmlDataByTag("Group","Checkout","CloseButtonRightMargin")
var titleTextTopMargin = component.getComponentXmlDataByTag("Group","Checkout","TitleTextTopMargin")
var titleTextLeftMargin = component.getComponentXmlDataByTag("Group","Checkout","TitleTextLeftMargin")
var troubleshootTipWidth = component.getComponentXmlDataByTag("Group","Checkout","TroubleshootTipWidth")
var troubleshootTipHeight = component.getComponentXmlDataByTag("Group","Checkout","TroubleshootTipHeight")
var troubleshootTipBorderWidth = component.getComponentXmlDataByTag("Group","Checkout","TroubleshootTipBorderWidth")
var troubleshootTipRightMargin = component.getComponentXmlDataByTag("Group","Checkout","TroubleshootTipRightMargin")
var troubleshootTipBottomMargin = component.getComponentXmlDataByTag("Group","Checkout","TroubleshootTipBottomMargin")
var troubleshootTipBorderColor = component.getComponentXmlDataByTag("Group","Checkout","TroubleshootTipBorderColor")
var firstPriorCheckInstructionTopMargin = component.getComponentXmlDataByTag("Group","Checkout","FirstPriorCheckInstructionTopMargin")
var secondPriorCheckInstructionTopMargin = component.getComponentXmlDataByTag("Group","Checkout","SecondPriorCheckInstructionTopMargin")
var thirdPriorCheckInstructionTopMargin = component.getComponentXmlDataByTag("Group","Checkout","ThirdPriorCheckInstructionTopMargin")
var fourthPriorCheckInstructionTopMargin = component.getComponentXmlDataByTag("Group","Checkout","FourthPriorCheckInstructionTopMargin")
var fifthPriorCheckInstructionTopMargin = component.getComponentXmlDataByTag("Group","Checkout","FifthPriorCheckInstructionTopMargin")
var sixthPriorCheckInstructionTopMargin = component.getComponentXmlDataByTag("Group","Checkout","SixthPriorCheckInstructionTopMargin")

function createChkoutComponent() {
    console.log("inside Create component checkout")
    checkoutComp = Qt.createComponent("qrc:Source/Checkout/Qml/PriorToTest.qml");

    if(checkoutComp.status === Component.Ready) {
        finishChkoutComponentCreation();
    }else if(checkoutComp.status === Component.Error) {
        console.log("Error loading component:", checkoutComp.errorString());
    }
    else {
        checkoutComp.statusChanged.connect(finishChkoutComponentCreation);
    }
}


function finishChkoutComponentCreation() {
    console.log("Finish checkout creation....")
    checkoutObj = checkoutComp.createObject(mainitem,{
                                              //  "objectName": "CheckoutScreenObj",
                                                "iChkoutAreaHeight":chkoutAreaHeight,
                                                "iChkoutAreaWidth":chkoutAreaWidth,
                                                "strChkoutAreaColor":chkoutAreaColor,
                                                "iTitleAreaHeight":titleAreaHeight,
                                                "iTitleAreaWidth":titleAreaWidth,
                                                "iChkoutInstAreaHeight":chkoutInstAreaHeight,
                                                "iChkoutInstAreaWidth":chkoutInstAreaWidth,
                                                "iChkoutPicAreaHeight":chkoutPicAreaHeight,
                                                "iChkoutPicAreaWidth":chkoutPicAreaWidth,
                                                "iActionBtnWidth":actionBtnWidth,
                                                "iActionBtnHeight":actionBtnHeight,
                                                "strActionBtnBgColor": actionBtnbgColor,
                                                "strActionBtnBorderColor": actionBtnborderColor,
                                                "iActionBtnBorderWidth": actionBtnborderWidth,
                                                "iActionBtnRadius": actionBtnradius,
                                                "strScrollStateBorderColor":scrollStateBorderColor,
                                                "iScrollStateBorderWidth" :scrollStateBorderWidth,
                                                "iFontSize":fontSize,
                                                "iInstructionTextArea":instructionTextArea,
                                                "strFontColor":fontColor,
                                                "iTitleFontSize":titleFontSize,
                                                "strCloseBtnSource":closeBtnSource,
                                                "strCloseBtnTchSource":closeBtnTchSource,
                                                "iByPassPopupWidth":byPassPopupWidth,
                                                "iByPassPopupWidth":byPassPopupWidth,
                                                "iPopupColumnTextPixelSize": popupColumnTextPixelSize,
                                                "iPopupColumnTextPixelHeight":popupColumnTextPixelHeight,
                                                "iPopupColumnTextPixelLeftMargin":popupColumnTextPixelLeftMargin,
                                                "iByPassSubButtonWidth":byPassSubButtonWidth ,
                                                "iByPassSubButtonHeight":byPassSubButtonHeight,
                                                "strPopUpBgColor": byPassPopupBgColor,
                                                "strPopUpFontColor":byPassPopupFontColor,
                                                "iByPassSubButtonTopMargin":byPassSubButtonTopMargin,
                                                "iByPassSubButtonLeftMargin":byPassSubButtonLeftMargin,
                                                "strByPassSubButtonColor": byPassSubButtonColor,
                                                "strByPassSubButtonBorderColor":byPassSubButtonBorderColor,
                                                "iByPassSubButtonPixelSize":byPassSubButtonPixelSize,
                                                "iPriorTestCheckInstructionWidth":priorTestCheckInstructionWidth,
                                                "iPriorTestCheckInstructionHeight":priorTestCheckInstructionHeight,
                                                "iPriorTestCheckInstructionLeftMargin":priorTestCheckInstructionLeftMargin,
                                                "iConfirmButtonLeftMargin":confirmButtonLeftMargin,
                                                "iConfirmButtonBottomMargin":confirmButtonBottomMargin,
                                                "iSkipButtonLeftMargin":skipButtonLeftMargin,
                                                "iSkipButtonBottomMargin":skipButtonBottomMargin,
                                                "iSubTitleTextPixelSize":subTitleTextPixelSize,
                                                "iSubTitleTextTopMargin":subTitleTextTopMargin,
                                                "iSubTitleTextLeftMargin":subTitleTextLeftMargin,
                                                "strPreCheckListRectColor":preCheckListRectColor,
                                                "iPreCheckListTopMargin":preCheckListTopMargin,
                                                "iCloseButtonWidth":closeButtonWidth,
                                                "iCloseButtonHeight":closeButtonHeight,
                                                "iCloseButtonRadius":closeButtonRadius,
                                                "iCloseButtonTopMargin":closeButtonTopMargin,
                                                "iCloseButtonRightMargin":closeButtonRightMargin,
                                                "iTitleTextTopMargin":titleTextTopMargin,
                                                "iTroubleshootTipWidth":troubleshootTipWidth,
                                                "iTroubleshootTipHeight":troubleshootTipHeight,
                                                "iTroubleshootTipBorderWidth":troubleshootTipBorderWidth,
                                                "iTroubleshootTipRightMargin":troubleshootTipRightMargin,
                                                "iTroubleshootTipBottomMargin":troubleshootTipBottomMargin,
                                                "strTroubleshootTipBorderColor":troubleshootTipBorderColor,
                                                "iTitleTextLeftMargin":titleTextLeftMargin,
                                                "iFirstPriorCheckInstructionTopMargin":firstPriorCheckInstructionTopMargin,
                                                "iSecondPriorCheckInstructionTopMargin":secondPriorCheckInstructionTopMargin,
                                                "iThirdPriorCheckInstructionTopMargin":thirdPriorCheckInstructionTopMargin,
                                                "iFourthPriorCheckInstructionTopMargin":fourthPriorCheckInstructionTopMargin,
                                                "iFifthPriorCheckInstructionTopMargin":fifthPriorCheckInstructionTopMargin,
                                                "iSixthPriorCheckInstructionTopMargin":sixthPriorCheckInstructionTopMargin
                                            })

}

function createOtherEquipComponent() {
    console.log("inside equipment component checkout")
    otherEquipComp = Qt.createComponent("qrc:Source/Checkout/Qml/OtherEquipTest.qml");

    if(otherEquipComp.status === Component.Ready) {
        finishOtherEquipComponentCreation();
    }else if(otherEquipComp.status === Component.Error) {
        console.log("Error loading component:", otherEquipComp.errorString());
    }
    else {
        otherEquipComp.statusChanged.connect(finishOtherEquipComponentCreation);
    }
}


function finishOtherEquipComponentCreation() {
    console.log("Finish equipment creation....")
    otherEquipObj = otherEquipComp.createObject(mainitem,{
                                           //     "objectName": "OtherEquipListObj",
                                                "iChkoutAreaHeight":chkoutAreaHeight,
                                                "iChkoutAreaWidth":chkoutAreaWidth,
                                                "strChkoutAreaColor":chkoutAreaColor,
                                                "iTitleAreaHeight":titleAreaHeight,
                                                "iTitleAreaWidth":titleAreaWidth,
                                                "iChkoutInstAreaHeight":chkoutInstAreaHeight,
                                                "iChkoutInstAreaWidth":chkoutInstAreaWidth,
                                                "iChkoutPicAreaHeight":chkoutPicAreaHeight,
                                                "iChkoutPicAreaWidth":chkoutPicAreaWidth,
                                                "iActionBtnWidth":actionBtnWidth,
                                                "iActionBtnHeight":actionBtnHeight,
                                                "strActionBtnBgColor": actionBtnbgColor,
                                                "strActionBtnBorderColor": actionBtnborderColor,
                                                "iActionBtnBorderWidth": actionBtnborderWidth,
                                                "iActionBtnRadius": actionBtnradius,
                                                "strScrollStateBorderColor":scrollStateBorderColor,
                                                "iScrollStateBorderWidth" :scrollStateBorderWidth,
                                                "iFontSize":fontSize,
                                                "iInstructionTextArea":instructionTextArea,
                                                "strFontColor":fontColor,
                                                "iTitleFontSize":titleFontSize,
                                                "strCloseBtnSource":closeBtnSource,
                                                "strCloseBtnTchSource":closeBtnTchSource,
                                                "iByPassPopupWidth":byPassPopupWidth,
                                                "iByPassPopupWidth":byPassPopupWidth,
                                                "iPopupColumnTextPixelSize": popupColumnTextPixelSize,
                                                "iPopupColumnTextPixelHeight":popupColumnTextPixelHeight,
                                                "iPopupColumnTextPixelLeftMargin":popupColumnTextPixelLeftMargin,
                                                "iByPassSubButtonWidth":byPassSubButtonWidth ,
                                                "iByPassSubButtonHeight":byPassSubButtonHeight,
                                                "strPopUpBgColor": byPassPopupBgColor,
                                                "strPopUpFontColor":byPassPopupFontColor,
                                                "iByPassSubButtonTopMargin":byPassSubButtonTopMargin,
                                                "iByPassSubButtonLeftMargin":byPassSubButtonLeftMargin,
                                                "strByPassSubButtonColor": byPassSubButtonColor,
                                                "strByPassSubButtonBorderColor":byPassSubButtonBorderColor,
                                                "iByPassSubButtonPixelSize":byPassSubButtonPixelSize,
                                                "iPriorTestCheckInstructionWidth":priorTestCheckInstructionWidth,
                                                "iPriorTestCheckInstructionHeight":priorTestCheckInstructionHeight,
                                                "iPriorTestCheckInstructionLeftMargin":priorTestCheckInstructionLeftMargin,
                                                "iConfirmButtonLeftMargin":confirmButtonLeftMargin,
                                                "iConfirmButtonBottomMargin":confirmButtonBottomMargin,
                                                "iSkipButtonLeftMargin":skipButtonLeftMargin,
                                                "iSkipButtonBottomMargin":skipButtonBottomMargin,
                                                "iSubTitleTextPixelSize":subTitleTextPixelSize,
                                                "iSubTitleTextTopMargin":subTitleTextTopMargin,
                                                "iSubTitleTextLeftMargin":subTitleTextLeftMargin,
                                                "strPreCheckListRectColor":preCheckListRectColor,
                                                "iPreCheckListTopMargin":preCheckListTopMargin,
                                                "iCloseButtonWidth":closeButtonWidth,
                                                "iCloseButtonHeight":closeButtonHeight,
                                                "iCloseButtonRadius":closeButtonRadius,
                                                "iCloseButtonTopMargin":closeButtonTopMargin,
                                                "iCloseButtonRightMargin":closeButtonRightMargin,
                                                "iTitleTextTopMargin":titleTextTopMargin,
                                                    "iTitleTextLeftMargin":titleTextLeftMargin,
                                                    "iTroubleshootTipWidth":troubleshootTipWidth,
                                                    "iTroubleshootTipHeight":troubleshootTipHeight,
                                                    "iTroubleshootTipBorderWidth":troubleshootTipBorderWidth,
                                                    "iTroubleshootTipRightMargin":troubleshootTipRightMargin,
                                                    "iTroubleshootTipBottomMargin":troubleshootTipBottomMargin,
                                                    "strTroubleshootTipBorderColor":troubleshootTipBorderColor,
                                                    "iFirstPriorCheckInstructionTopMargin":firstPriorCheckInstructionTopMargin,
                                                    "iSecondPriorCheckInstructionTopMargin":secondPriorCheckInstructionTopMargin,
                                                    "iThirdPriorCheckInstructionTopMargin":thirdPriorCheckInstructionTopMargin,
                                                    "iFourthPriorCheckInstructionTopMargin":fourthPriorCheckInstructionTopMargin
                                            })

}


function createByPassComponent() {
    console.log("inside ByPass component checkout")
    byPassComp = Qt.createComponent("qrc:Source/Checkout/Qml/ByPassCheckout.qml");

    if(byPassComp.status === Component.Ready) {
        finishByPassComponentCreation();
    }else if(byPassComp.status === Component.Error) {
        console.log("Error loading component:", byPassComp.errorString());
    }
    else {
        byPassComp.statusChanged.connect(finishByPassComponentCreation);
    }
}


function finishByPassComponentCreation() {
    console.log("Finish ByPass creation....")
    byPassObj = byPassComp.createObject(mainitem,{
                                                "iChkoutAreaHeight":chkoutAreaHeight,
                                                "iChkoutAreaWidth":chkoutAreaWidth,
                                                "strChkoutAreaColor":chkoutAreaColor,
                                                "iTitleAreaHeight":titleAreaHeight,
                                                "iTitleAreaWidth":titleAreaWidth,
                                                "iChkoutInstAreaHeight":chkoutInstAreaHeight,
                                                "iChkoutInstAreaWidth":chkoutInstAreaWidth,
                                                "iChkoutPicAreaHeight":chkoutPicAreaHeight,
                                                "iChkoutPicAreaWidth":chkoutPicAreaWidth,
                                                "iActionBtnWidth":actionBtnWidth,
                                                "iActionBtnHeight":actionBtnHeight,
                                                "strActionBtnBgColor": actionBtnbgColor,
                                                "strActionBtnBorderColor": actionBtnborderColor,
                                                "iActionBtnBorderWidth": actionBtnborderWidth,
                                                "iActionBtnRadius": actionBtnradius,
                                                "strScrollStateBorderColor":scrollStateBorderColor,
                                                "iScrollStateBorderWidth" :scrollStateBorderWidth,
                                                "iFontSize":fontSize,
                                                "iInstructionTextArea":instructionTextArea,
                                                "strFontColor":fontColor,
                                                "iTitleFontSize":titleFontSize,
                                                "strCloseBtnSource":closeBtnSource,
                                                "strCloseBtnTchSource":closeBtnTchSource,
                                                "iByPassPopupWidth":byPassPopupWidth,
                                                "iByPassPopupWidth":byPassPopupWidth,
                                                "iPopupColumnTextPixelSize": popupColumnTextPixelSize,
                                                "iPopupColumnTextPixelHeight":popupColumnTextPixelHeight,
                                                "iPopupColumnTextPixelLeftMargin":popupColumnTextPixelLeftMargin,
                                                "iByPassSubButtonWidth":byPassSubButtonWidth ,
                                                "iByPassSubButtonHeight":byPassSubButtonHeight,
                                                "strPopUpBgColor": byPassPopupBgColor,
                                                "strByPassBorderColor": byPassBorderColor,
                                                "iByPassBorderWidth": byPassBorderWidth,
                                                "strPopUpFontColor":byPassPopupFontColor,
                                                "iPopupColumnTextPixelLeftMargin": popupColumnTextPixelLeftMargin,
                                                "iPopupTextLeftMargin": popupTextLeftMargin,
                                                "iPopupTextTopMargin": popupTextTopMargin,
                                                "iPopupThirdTextTopMargin": popupThirdTextTopMargin,
                                                "iByPassSubButtonBottomMargin":byPassSubButtonBottomMargin,
                                                "iByPassSubButtonRightMargin":byPassSubButtonRightMargin,
                                                "strByPassSubButtonColor": byPassSubButtonColor,
                                                "strByPassSubButtonBorderColor":byPassSubButtonBorderColor,
                                                "iByPassSubButtonPixelSize":byPassSubButtonPixelSize,
                                                "iPriorTestCheckInstructionWidth":priorTestCheckInstructionWidth,
                                                "iPriorTestCheckInstructionHeight":priorTestCheckInstructionHeight,
                                                "iPriorTestCheckInstructionLeftMargin":priorTestCheckInstructionLeftMargin,
                                                "iConfirmButtonLeftMargin":confirmButtonLeftMargin,
                                                "iConfirmButtonBottomMargin":confirmButtonBottomMargin,
                                                "iSkipButtonLeftMargin":skipButtonLeftMargin,
                                                "iSkipButtonBottomMargin":skipButtonBottomMargin,
                                                "iSubTitleTextPixelSize":subTitleTextPixelSize,
                                                "iSubTitleTextTopMargin":subTitleTextTopMargin,
                                                "iSubTitleTextLeftMargin":subTitleTextLeftMargin,
                                                "strPreCheckListRectColor":preCheckListRectColor,
                                                "iPreCheckListTopMargin":preCheckListTopMargin,
                                                "iCloseButtonWidth":closeButtonWidth,
                                                "iCloseButtonHeight":closeButtonHeight,
                                                "iCloseButtonRadius":closeButtonRadius,
                                                "iCloseButtonTopMargin":closeButtonTopMargin,
                                                "iCloseButtonRightMargin":closeButtonRightMargin,
                                                "iTitleTextTopMargin":titleTextTopMargin,
                                                "iTitleTextLeftMargin":titleTextLeftMargin
                                            })

}




/*******************************************
Purpose:move clock wise direction
Description:Used to provide navigation within a screen.When the user presses right navigation key
or uses comwheel to navigate clock wise
***********************************************/
function moveClockwise(currentObjectName, currentScreen) {
    console.log("In moveClockwise; currentObjectName, CurrentPage:",currentObjectName,currentScreen)
    if (currentScreen === "CheckoutScreenObj") {
        var checkoutScreenObj = getCompRef(mainitem, "CheckoutScreenObj")
        var instComp = getCompRef(checkoutScreenObj, "InstructRectObj")
        var titleComp = getCompRef(checkoutScreenObj, "titleRectObj")
        var closeComp = getCompRef(titleComp,"CheckoutScreenClose")
        var confirmComp = getCompRef(instComp,"confirmBtnObj")
        if (currentObjectName === "confirmBtnObj"){
            closeComp.focus = true
            closeComp.forceActiveFocus()
        }
        else if (currentObjectName === "CheckoutScreenClose") {
            confirmComp.focus = true
            confirmComp.forceActiveFocus()
        }
    }
    else if (currentScreen === "OtherEquipListObj") {
        var checkScreenObj = getCompRef(mainitem, "OtherEquipListObj")
        var titleRectObj = getCompRef(checkScreenObj,"OtherEquipTitleObj")
        var instructRectObj = getCompRef(checkScreenObj,"InstRecObj")
        var checkListObj = getCompRef(instructRectObj,"OtherEquipChecklist")
        var otherEquipConfirm = getCompRef(instructRectObj,"OtherEquipConfirmObj")
        var otherEquipClose = getCompRef(titleRectObj,"OtherEquipTestClose")
        var skipComp = getCompRef(instructRectObj,"OtherEquipSkipObj")
        if (currentObjectName === "OtherEquipConfirmObj") {

            skipComp.focus = true
        }
        else if (currentObjectName === "OtherEquipSkipObj" ) {
            otherEquipClose.focus = true
                  }
        else if (currentObjectName === "OtherEquipTestClose") {
            otherEquipConfirm.focus = true
            otherEquipConfirm.forceActiveFocus()
        }
        else {
            //Do nothing
        }
    }
 }


/*******************************************
Purpose:move clock wise direction
Description:Used to provide navigation within a screen.When the user presses left navigation key/uses
comwheel to turn anticlockwise
Input Parameters: objectName - currently active button reference
***********************************************/
function moveAntiClockwise(currentObjectName,currentScreen) {
    console.log("In moveClockwise; currentObjectName, iCurrentPage:",currentObjectName,currentScreen)

    if (currentScreen === "CheckoutScreenObj") {
        var checkoutScreenObj = getCompRef(mainitem, "CheckoutScreenObj")
        var instComp = getCompRef(checkoutScreenObj, "InstructRectObj")
        var titleComp = getCompRef(checkoutScreenObj, "titleRectObj")
        var closeComp = getCompRef(titleComp,"CheckoutScreenClose")
        var confirmComp = getCompRef(instComp,"confirmBtnObj")
        if (currentObjectName === "confirmBtnObj"){
            closeComp.focus = true
            closeComp.forceActiveFocus()
        }
        else if (currentObjectName === "CheckoutScreenClose") {
            confirmComp.focus = true
            confirmComp.forceActiveFocus()
        }
    }
    else if (currentScreen === "OtherEquipListObj") {
        var checkScreenObj = getCompRef(mainitem, "OtherEquipListObj")
        var titleRectObj = getCompRef(checkScreenObj,"OtherEquipTitleObj")
        var instructRectObj = getCompRef(checkScreenObj,"InstRecObj")
        var checkListObj = getCompRef(instructRectObj,"OtherEquipChecklist")
        var otherEquipConfirm = getCompRef(instructRectObj,"OtherEquipConfirmObj")
        var otherEquipClose = getCompRef(titleRectObj,"OtherEquipTestClose")
        var skipComp = getCompRef(instructRectObj,"OtherEquipSkipObj")
        if (currentObjectName === "OtherEquipConfirmObj") {

            otherEquipClose.focus = true
        }
        else if (currentObjectName === "OtherEquipSkipObj" ) {
            otherEquipConfirm.focus = true
                   }
        else if (currentObjectName === "OtherEquipTestClose") {
            skipComp.focus = true
            skipComp.forceActiveFocus()
        }
        else {
            //Do nothing
        }
    }

}

/*******************************************
Purpose:destroy the checkout screen
Description:destroy the screens on close or confirm bypass
***********************************************/
function destroyCheckoutScreen() {
    console.log("inside destroy function.......")
    var checkoutScreenObj = getCompRef(mainitem, "OtherEquipListObj")
   var priorCheckObj = getCompRef(mainitem, "CheckoutScreenObj")
    checkoutScreenObj.destroy()
    priorCheckObj.destroy()
}

/*******************************************
Purpose:display the title and instructions for other equipment checklist
Description: display the instructions using the string ids which is read from the xml
***********************************************/
function fullTestTitle() {
    console.log("fullTestTitle")
    var checkScreenObj = getCompRef(mainitem, "OtherEquipListObj")
    var titleRectObj = getCompRef(checkScreenObj,"OtherEquipTitleObj")
    titleRectObj.setFullTestTitle("EqpChkList_Title")
    var instructRectObj = getCompRef(checkScreenObj,"InstRecObj")
    instructRectObj.setSubTitle("EqpChkList_SubTitle")
    var checkListObj = getCompRef(instructRectObj,"OtherEquipChecklist")
    checkListObj.setFullTestFirstInst("EqpChkList_inst1")
    checkListObj.setFullTestSecInst("EqpChkList_inst2")
    checkListObj.setFullTestThirdInst("EqpChkList_inst3")
    checkListObj.setFullTestFourthInst("EqpChkList_inst4")

}

/*******************************************
Purpose:display the title and instructions for Prior to test checklist
Description: display the instructions using the string ids which is read from the xml
***********************************************/
function preCheckTitle() {
    var chkoutObj = getCompRef(mainitem, "CheckoutScreenObj")
    var titleObj = getCompRef(chkoutObj,"titleRectObj")
    titleObj.setTitle("Prior_test_title")
    var instObj = getCompRef(chkoutObj,"InstructRectObj")
    instObj.setSubTitle("Prior_test_subtitle")
    var instructionsText = getCompRef(instObj,"preChecklist")
    instructionsText.setPriorFirstInst("Prior_test_inst1")
    instructionsText.setPriorSecInst("Prior_test_inst2")
    instructionsText.setPriorThirdInst("Prior_test_inst3")
    instructionsText.setPriorFourthInst("Prior_test_inst4")
    instructionsText.setPriorFifthInst("Prior_test_inst5")
    instructionsText.setPriorSixthInst("Prior_test_inst6")
}

function byPassTitle() {
    var byPassChkoutObj = getCompRef(mainitem,"ByPassCheckoutObj")
    console.log("Check out object........",byPassChkoutObj)
//    var popupObj = getCompRef(byPassChkoutObj,"PopupObj")
//    console.log("popup object........",popupObj)
    byPassChkoutObj.setPopupFirstText("ByPass_Title")
    byPassChkoutObj.setPopupSecText("ByPass_WarningOne")
    byPassChkoutObj.setPopupThirdText("ByPass_WarningTwo")
}

function destroyPriorCheckScreen() {
    var priorCheckObj = getCompRef(mainitem, "CheckoutScreenObj")
    priorCheckObj.destroy()
}
