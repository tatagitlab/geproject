import QtQuick 2.9
import QtQuick.Controls 2.2
import "qrc:/Source/Checkout/JavaScript/CheckoutController.js" as CheckoutCntrl
import "qrc:/Source/Checkout/JavaScript/CheckoutResController.js" as CheckoutResCntrl

Rectangle{
    property var instructionRectId: instructionRect
    property var titleRectId: titleRect

    property int iChkoutAreaHeight
    property int iChkoutAreaWidth
    property int iByPassPopupWidth
    property int iByPassPopupHeight
    property int iPopupColumnTextPixelSize
    property int iPopupColumnTextPixelHeight
    property int iPopupColumnTextPixelLeftMargin
    property int iByPassSubButtonWidth
    property int iByPassSubButtonHeight
    property int iByPassSubButtonTopMargin
    property int iByPassSubButtonLeftMargin
    property int iByPassSubButtonPixelSize
    property int iActionBtnRadius
    property int iByPassBorderWidth
    property int iPopupTextTopMargin
    property int iPopupTextLeftMargin
    property int iPopupThirdTextTopMargin
    property string strByPassSubButtonColor
    property string strByPassSubButtonBorderColor
    property string strChkoutAreaColor
    property string strPopUpBgColor
    property string strByPassBorderColor
    property string strPopUpFontColor
    property string strActionBtnBorderColor

    property string strPopupFirstText
    property string strPopupSecText
    property string strPopupThirdText

    width: iByPassPopupWidth
    height: iByPassPopupHeight
    id: checkoutScreen
    objectName: "ByPassCheckoutObj"
    visible: true

    MouseArea{
        anchors.fill: parent
        onClicked: {
        }
    }

    function setPopupFirstText(firstText) {
        checkoutScreen.strPopupFirstText = firstText
    }

    function setPopupSecText(secText) {
        checkoutScreen.strPopupSecText = secText
    }

    function setPopupThirdText(thirdText) {
        checkoutScreen.strPopupThirdText = thirdText
    }

    Popup{
        id: popup
        objectName: "PopupObj"
        width: iByPassPopupWidth
        height: iByPassPopupHeight
        modal: true
        padding: 0
        x: 280 //Math.round((iChkoutAreaWidth - width) / 2)
        y: 220// Math.round((iChkoutAreaHeight - height) / 2)
        closePolicy: Popup.NoAutoClose


        Rectangle {
            id: popupRect
            width: iByPassPopupWidth
            height: iByPassPopupHeight
            anchors.fill: parent
            color: strPopUpBgColor
            border.width: iByPassBorderWidth
            border.color: strByPassBorderColor
            radius: iActionBtnRadius
        }

        Text {
            id: popupFirstText
            text: qsTrId(checkoutScreen.strPopupFirstText)
            color: strPopUpFontColor
            font.pixelSize: iPopupColumnTextPixelSize
            //  height:iPopupColumnTextPixelHeight
            anchors.top: popupRect.top
            anchors.topMargin: 30
            anchors.left: parent.left
            anchors.leftMargin: iPopupColumnTextPixelLeftMargin
        }
        Text {
            id: popupSecText
            text: qsTrId(checkoutScreen.strPopupSecText)
            color: strPopUpFontColor
            font.pixelSize: iPopupColumnTextPixelSize
            //  height:iPopupColumnTextPixelHeight
            anchors.top: popupRect.top
            anchors.topMargin: iPopupTextTopMargin
            anchors.left: parent.left
            anchors.leftMargin: iPopupTextLeftMargin
        }
        Text {
            id: popupThirdText
            text: qsTrId(checkoutScreen.strPopupThirdText)
            color: strPopUpFontColor
            font.pixelSize: iPopupColumnTextPixelSize
            // height: iPopupColumnTextPixelHeight
            anchors.top: popupSecText.bottom
            anchors.topMargin: iPopupThirdTextTopMargin
            anchors.left: parent.left
            anchors.leftMargin: iPopupColumnTextPixelLeftMargin
        }

        Rectangle {
            id: confirmBypassBtn
            width: iByPassSubButtonWidth
            height: iByPassSubButtonHeight
            anchors.bottom: popupRect.bottom
            anchors.bottomMargin: iByPassSubButtonTopMargin
            anchors.right: cancelBypassBtn.left
            anchors.rightMargin: iByPassSubButtonLeftMargin
            radius: iActionBtnRadius
            color: confirmMouseArea.pressed ? "#4565A6" : strByPassSubButtonColor
            focus: true
            border.color: activeFocus ? strByPassSubButtonBorderColor : strActionBtnBorderColor
            Text {
                text: "ByPass"
                font.pixelSize: iByPassSubButtonPixelSize
                color: strPopUpFontColor
                anchors.centerIn: parent
            }
            MouseArea {
                id: confirmMouseArea
                anchors.fill: parent
                onPressed: {
                    confirmBypassBtn.border.width = 0
                }

                onReleased:  {
                    confirmBypassBtn.border.width = 2
                    popup.close()
                    objCSBValue.writeOnCSB(80, 15) //User selected bypass button
                    checkoutScreen.destroy()
                    CheckoutCntrl.destroyPriorCheckScreen();
                    var ventGrpReference=getGroupRef("VentGroup")
                    var ventModeObject = getCompRef(ventGrpReference.ventParentId, "ventModeBtn")
                    ventModeObject.focus = true
                }
            }


            Keys.onLeftPressed: {
                cancelBypassBtn.focus = true
                cancelBypassBtn.forceActiveFocus()
            }
            Keys.onRightPressed: {
                cancelBypassBtn.focus = true
                cancelBypassBtn.forceActiveFocus()
            }
            Keys.onReturnPressed: {
                popup.close()
                checkoutScreen.destroy()
                CheckoutCntrl.destroyPriorCheckScreen();
                objCSBValue.writeOnCSB(80, 15) //User selected bypass button
                var ventGrpReference=getGroupRef("VentGroup")
                var ventModeObject = getCompRef(ventGrpReference.ventParentId, "ventModeBtn")
                ventModeObject.focus = true            }

        }
        Rectangle {
            id: cancelBypassBtn
            width: iByPassSubButtonWidth
            height: iByPassSubButtonHeight
            anchors.bottom: popupRect.bottom
            anchors.bottomMargin: iByPassSubButtonTopMargin
            anchors.right:  parent.right
            anchors.rightMargin: iByPassSubButtonLeftMargin
            color: closeMouseArea.pressed ? "#4565A6" : strByPassSubButtonColor
            radius: iActionBtnRadius
            border.color: activeFocus ? strByPassSubButtonBorderColor : strActionBtnBorderColor
            Text {

                text: "Cancel"
                font.pixelSize: iByPassSubButtonPixelSize
                color: strPopUpFontColor
                anchors.centerIn: parent
            }
            MouseArea {
                id: closeMouseArea
                anchors.fill: parent
                onPressed: {
                    cancelBypassBtn.border.width = 0
                    cancelBypassBtn.forceActiveFocus()
                }

                onReleased: {
                    cancelBypassBtn.border.width = 2
                    popup.close()
                    checkoutScreen.destroy()
                    var checkoutScreenObj = getCompRef(mainitem, "CheckoutScreenObj")
                    var instComp = getCompRef(checkoutScreenObj, "InstructRectObj")
                    var confirmComp = getCompRef(instComp,"confirmBtnObj")
                    console.log("confirmComp",confirmComp)
                    confirmComp.focus = true
                    confirmComp.forceActiveFocus()

                }
            }
            Keys.onLeftPressed: {
                confirmBypassBtn.focus = true
                confirmBypassBtn.forceActiveFocus()
            }
            Keys.onRightPressed: {
                confirmBypassBtn.focus = true
                confirmBypassBtn.forceActiveFocus()
            }
            Keys.onReturnPressed: {
                popup.close()
                checkoutScreen.destroy()
                var checkoutScreenObj = getCompRef(mainitem, "CheckoutScreenObj")
                var instComp = getCompRef(checkoutScreenObj, "InstructRectObj")
                var confirmComp = getCompRef(instComp,"confirmBtnObj")
                confirmComp.focus = true
                confirmComp.forceActiveFocus()

            }
        }
    }

    Component.onCompleted: {
        popup.open()
        CheckoutCntrl.byPassTitle()
        popup.forceActiveFocus()
    }

}
