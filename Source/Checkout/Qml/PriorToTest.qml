import QtQuick 2.9
import QtQuick.Controls 2.2
import "qrc:/Source/Checkout/JavaScript/CheckoutController.js" as CheckoutCntrl
import "qrc:/Source/Checkout/JavaScript/CheckoutResController.js" as CheckoutResCntrl

Rectangle{
    property var instructionRectId: instructionRect
    property var titleRectId: titleRect
    property var closeBtnId: closeBtn
    objectName: "CheckoutScreenObj"

    property int iActionBtnWidth
    property int iActionBtnHeight
    property int iActionBtnBorderWidth
    property int iActionBtnRadius
    property int iScrollStateBorderWidth
    property int iFontSize
    property int iInstructionTextArea
    property int iTitleFontSize
    property int iChkoutAreaHeight
    property int iChkoutAreaWidth
    property int iTitleAreaHeight
    property int iTitleAreaWidth
    property int iChkoutInstAreaHeight
    property int iChkoutInstAreaWidth
    property int iChkoutPicAreaHeight
    property int iChkoutPicAreaWidth
    property int iCurrentPag
    property int iByPassPopupWidth
    property int iByPassPopupHeight
    property int iPopupColumnTextPixelSize
    property int iPopupColumnTextPixelHeight
    property int iPopupColumnTextPixelLeftMargin
    property int iByPassSubButtonWidth
    property int iByPassSubButtonHeight
    property int iByPassSubButtonTopMargin
    property int iByPassSubButtonLeftMargin
    property int iByPassSubButtonPixelSize
    property int iPriorTestCheckInstructionWidth
    property int iPriorTestCheckInstructionHeight
    property int iPriorTestCheckInstructionLeftMargin
    property int iConfirmButtonLeftMargin
    property int iConfirmButtonBottomMargin
    property int iSkipButtonLeftMargin
    property int iSkipButtonBottomMargin
    property int iSubTitleTextPixelSize
    property int iSubTitleTextTopMargin
    property int iSubTitleTextLeftMargin
    property int iPreCheckListTopMargin
    property int iCloseButtonWidth
    property int iCloseButtonHeight
    property int iCloseButtonRadius
    property int iCloseButtonTopMargin
    property int iCloseButtonRightMargin
    property int iTitleTextTopMargin
    property int iTitleTextLeftMargin
    property int iCurrentPage
    property int iChkoutInstTopMargin
    property int iTroubleshootTipWidth/*: 520*/
    property int iTroubleshootTipHeight/*: 670*/
    property int iTroubleshootTipBorderWidth: 1
    property int iTroubleshootTipRightMargin: 20
    property int iTroubleshootTipBottomMargin: 20
    property int iFirstPriorCheckInstructionTopMargin
    property int iSecondPriorCheckInstructionTopMargin
    property int iThirdPriorCheckInstructionTopMargin
    property int iFourthPriorCheckInstructionTopMargin
    property int iFifthPriorCheckInstructionTopMargin
    property int iSixthPriorCheckInstructionTopMargin


    property string strPreCheckListRectColor
    property string strByPassSubButtonColor
    property string strByPassSubButtonBorderColor
    property string strChkoutAreaColor
    property string strFontColor
    property string strPopUpBgColor
    property string strPopUpFontColor
    property string strActionBtnBgColor
    property string strActionBtnBorderColor
    property string strCloseBtnSource
    property string strCloseBtnTchSource
    property string strScrollStateBorderColor
    property string strTroubleshootTipBorderColor: "white"

    visible: true
    width: iChkoutAreaWidth
    height: iChkoutAreaHeight
    id: checkoutScreen
    color: strChkoutAreaColor
    z: 3

    MouseArea{
        anchors.fill: parent
        onClicked: {
        }
    }

    Rectangle{
        id: titleRect
        objectName: "titleRectObj"
        property string strTitle

        height: iTitleAreaHeight
        width: iTitleAreaWidth
        color: strChkoutAreaColor

        function setTitle(title) {
            console.log("Title is set :" + title)
            strTitle = title
        }
        function setFullTestTitle(title) {
            strTitle = title
        }

        Text {
            id: titleText
            width:parent.width
            height:parent.height
            text: qsTrId(titleRect.strTitle)
            font.pixelSize: iTitleFontSize
            color:strFontColor
            anchors.top:parent.top
            anchors.topMargin: iTitleTextTopMargin
            anchors.left:parent.left
            anchors.leftMargin: iTitleTextLeftMargin
        }
        Rectangle {
            id: closeBtn
            objectName: "CheckoutScreenClose"
            height: iCloseButtonWidth
            width: iCloseButtonHeight
            color: closeMouseArea.pressed ? "#4565A6" : strActionBtnBgColor
            anchors.top: titleRect.top
            anchors.topMargin: iCloseButtonTopMargin
            anchors.right: titleRect.right
            anchors.rightMargin: iCloseButtonRightMargin
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth
            radius: iCloseButtonRadius
            Image {
                id: closeBtnIcon
                source: strCloseBtnSource
                anchors.centerIn: parent
            }
            MouseArea {
                id: closeMouseArea
                anchors.fill: closeBtn

                onPressed: {
                    closeBtn.forceActiveFocus()
                    closeBtn.border.width = 0

                }
                onReleased: {
                    CheckoutCntrl.createByPassComponent()
                    closeBtn.border.width = iActionBtnBorderWidth
                }
            }
            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(closeBtn.objectName,checkoutScreen.objectName)
            }

            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(closeBtn.objectName,checkoutScreen.objectName)
            }

            Keys.onReturnPressed: {
                CheckoutCntrl.createByPassComponent();
            }
        }
    }
    Rectangle{
        id: instructionRect
        objectName: "InstructRectObj"
        property string strSubTitle

        height: iChkoutInstAreaHeight
        width: iChkoutInstAreaWidth
        color: strChkoutAreaColor
        anchors.top: titleRect.bottom

        function setSubTitle(subTitle) {
            strSubTitle = subTitle
        }


        Text {
            id: subTitleText
            text: qsTrId(instructionRect.strSubTitle)
            font.pixelSize: iSubTitleTextPixelSize
            color: strFontColor
            anchors.top:checkoutScreen.top
            anchors.topMargin: iSubTitleTextTopMargin
            anchors.left: parent.left
            anchors.leftMargin: iSubTitleTextLeftMargin
        }
        Rectangle{
            id: preCheck
            objectName: "preChecklist"
            property string strPriorFirstInst
            property string strPriorSecInst
            property string strPriorThirdInst
            property string strPriorFourthInst
            property string strPriorFifthInst
            property string strPriorSixthInst

            color: strPreCheckListRectColor
            anchors.top: subTitleText.bottom
            anchors.topMargin: iPreCheckListTopMargin

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setPriorFirstInst(firstInst) {
                strPriorFirstInst = firstInst
            }

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setPriorSecInst(secInst) {
                strPriorSecInst = secInst
            }

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setPriorThirdInst(thirdInst) {
                strPriorThirdInst = thirdInst
            }

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setPriorFourthInst(fourthInst) {
                strPriorFourthInst = fourthInst
            }

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setPriorFifthInst(fifthInst) {
                strPriorFifthInst = fifthInst
            }

            /*******************************************
             Purpose:Set the title and instructions
             Description:Sets the title and instructions with the string ids from xml
             ***********************************************/
            function setPriorSixthInst(sixthInst) {
                strPriorSixthInst = sixthInst
            }



            /*******************************************
             Purpose:Set focus on confirm button
             Description:Sets the focus to confirm button of bypass checkout Popup
             ***********************************************/
            function setFocusOnConfirmButton() {
                confirmBtn.focus = true
                confirmBtn.forceActiveFocus();
            }

            Label {
                id: firstInst
                objectName: "firstInst"
                text: qsTrId(preCheck.strPriorFirstInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iFirstPriorCheckInstructionTopMargin
            }
            Label {
                id: secInst

                text: qsTrId(preCheck.strPriorSecInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: firstInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iSecondPriorCheckInstructionTopMargin
            }
            Label {
                id: thirdInst
                text:qsTrId(preCheck.strPriorThirdInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: secInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iThirdPriorCheckInstructionTopMargin
            }
            Label {
                id: fourthInst
                text:qsTrId(preCheck.strPriorFourthInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: thirdInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iFourthPriorCheckInstructionTopMargin
            }
            Label {
                id: fifthInst
                text:qsTrId(preCheck.strPriorFifthInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: fourthInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iFifthPriorCheckInstructionTopMargin
            }
            Label {
                id: sixthInst
                text:qsTrId(preCheck.strPriorSixthInst)
                color: strFontColor
                font.pixelSize: iFontSize
                anchors.top: fifthInst.bottom
                anchors.left: parent.left
                anchors.leftMargin: iPriorTestCheckInstructionLeftMargin
                anchors.topMargin: iSixthPriorCheckInstructionTopMargin
            }
        }
        Rectangle{
            id:confirmBtn
            objectName: "confirmBtnObj"
            height: iActionBtnHeight
            width:  iActionBtnWidth
            color: confirmBtnMouseArea.pressed ? "#4565A6" : "#2e2e2e"
            border.color: activeFocus ? strScrollStateBorderColor : strActionBtnBorderColor
            border.width: activeFocus ? iScrollStateBorderWidth : iActionBtnBorderWidth
            radius: iActionBtnRadius
            anchors.bottom: instructionRect.bottom
            anchors.bottomMargin: iConfirmButtonBottomMargin
            anchors.left: instructionRect.left
            anchors.leftMargin: iConfirmButtonLeftMargin

            Text {
                id: confirmText
                text: qsTr("Confirm")
                anchors.centerIn: parent
                font.pixelSize: iFontSize
                color: strFontColor
            }
            MouseArea {
                id: confirmBtnMouseArea
                anchors.fill: confirmBtn

                onPressed: {
                    confirmBtn.border.width = 0

                }
                onReleased: {
                    confirmBtn.border.width = 2
                    objCSBValue.writeOnCSB(80,0) //User selected checkout
                    CheckoutCntrl.createOtherEquipComponent();
                }
            }
            Keys.onRightPressed: {
                CheckoutCntrl.moveClockwise(confirmBtn.objectName,checkoutScreen.objectName)
            }
            Keys.onLeftPressed: {
                CheckoutCntrl.moveAntiClockwise(confirmBtn.objectName,checkoutScreen.objectName)
            }
            Keys.onReturnPressed: {
                objCSBValue.writeOnCSB(80,0) //User selected checkout
                CheckoutCntrl.createOtherEquipComponent();
            }
        }


        Component.onCompleted: {
            CheckoutCntrl.preCheckTitle();
            confirmBtn.focus = true
            confirmBtn.forceActiveFocus();
        }

        Component.onDestruction: {
            var buttonGrpRef = getGroupRef("ButtonGroup")
            console.log("***Destruction****",buttonGrpRef.strIconPreviousState)
            if (buttonGrpRef.strIconPreviousState === "Scroll") {
                buttonGrpRef.buttongrpnam.children[0].focus = true
            } else if (buttonGrpRef.strIconPreviousState === "Touched") {
                buttonGrpRef.focus = true
            } else {
                var ventGrpReference=getGroupRef("VentGroup")
                ventGrpReference.focus = true
            }
        }
    }
    Rectangle {
        id : troubleshootTip
        width: iTroubleshootTipWidth //520
        height: iTroubleshootTipHeight //670
        color: strChkoutAreaColor
        border.width: iTroubleshootTipBorderWidth // 1
        border.color: strTroubleshootTipBorderColor//"white"
        anchors.right: checkoutScreen.right
        anchors.rightMargin: iTroubleshootTipRightMargin
        anchors.bottom: checkoutScreen.bottom
        anchors.bottomMargin: iTroubleshootTipBottomMargin
    }
}
