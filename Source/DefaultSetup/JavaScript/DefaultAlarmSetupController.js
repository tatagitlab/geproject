Qt.include("qrc:/Source/Generic/JavaScript/MasterController.js")
//Qt.include("qrc:/Source/Generic/JavaScript/AlarmLogMenuController.js")
//Qt.include("qrc:/Source/Generic/JavaScript/AlarmSetupController.js")

var alarmSetupMenuComponent;
var alarmSetupMenuObj = null;
var alarmSetupSecondaryComponent;
var alarmParamCreationPpeakLow;
var alarmParamCreationPpeakHigh;
var alarmParamCreationMVLow;
var alarmParamCreationMVHigh;
var alarmParamCreationTVexpLow;
var alarmParamCreationTVexpHigh;
var alarmParamCreationO2Low;
var alarmParamCreationO2High;
var alarmParamCreationApneaDelay;
var alarmParamCreationAlarmVolume;
var PpeakLowObj
var PpeakHighObj
var MVLowObj
var MVHighObj
var TVexpLowObj
var TVexpHighObj
var O2LowObj
var O2HighObj
var ApneaDelayObj
var AlarmVolumeObj
var AlarmSetupMenuObj

//AlarmSetup Menu property
var alarmMenuLabel = component.getComponentResourceString("Group","AlarmSetUpMenu", "MenuTitle","default")
var alarmMenuWidth = parseInt(layout.getGroupLayoutDataByTag("AlarmSetupMenu","Dimension","width"));
var alarmMenuHeight = parseInt(layout.getGroupLayoutDataByTag("AlarmSetupMenu","Dimension","height"));
var alarmMenuRadius = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "BorderRadius"))
var alarmMenuBorderWidth = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "BorderWidth"))
var alarmLogWidth = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLogWidth"))
var alarmLogHeight = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLogHeight"))
var alarmLogRadius = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLogBorderRadius"))
var alarmLogBorderWidth = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLogBorderWidth"))
var alarmMenuBgColor = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "DefaultAlarmColor")
var alarmBorderColor = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "BorderColor")
//var alarmLogBgColor = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "DefaultAlarmColor")
var alarmLogBorderColor = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLogBorderColor")
var limitLabel1 = component.getComponentResourceString("Group","AlarmSetUpMenu", "LimitLabel","Low")
var limitLabel2 = component.getComponentResourceString("Group","AlarmSetUpMenu", "LimitLabel","High")
var logLabel = component.getComponentResourceString("Group","AlarmSetUpMenu", "LogLabel","default")
var fontFamilyAlarmSet = geFont.geFontStyle() //component.getComponentXmlDataByTag("AlarmSetUpMenu", "AlarmLabelFontFamily")
var fontColor = component.getComponentXmlDataByTag("Component","DefaultSetup", "AlarmLabelFontColor")
var pixelSizeAlarmSet = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmSetupLabelFontSize")
var pixelSizeLimitLabel = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "LimitLabelFontSize")
var pixelSizeLogLabel = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLogFontSize")
var alarmSetupParamFontColor = component.getComponentXmlDataByTag("Component","DefaultSetup", "AlarmLabelFontColor")
var alarmSetupParamFontSize = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmSetupParamFontSize")
var alarmPpeakLabel = component.getComponentResourceString("Group","AlarmSetUpMenu","Label","Ppeak")
var alarmPpeakUnit = component.getComponentResourceString("Group","AlarmSetUpMenu","Unit","Ppeak")
var alarmMVLabel = component.getComponentResourceString("Group","AlarmSetUpMenu","Label","MV")
var alarmMVUnit = component.getComponentResourceString("Group","AlarmSetUpMenu","Unit","MV")
var alarmTVexpLabel = component.getComponentResourceString("Group","AlarmSetUpMenu","Label","TVexp")
var alarmTVexpUnit = component.getComponentResourceString("Group","AlarmSetUpMenu","Unit","TVexp")
var alarmO2Label = component.getComponentResourceString("Group","AlarmSetUpMenu","Label","O2")
var alarmO2Unit = component.getComponentResourceString("Group","AlarmSetUpMenu","Unit","O2")
var alarmApneaLabel = component.getComponentResourceString("Group","AlarmSetUpMenu","Label","Apnea")
var alarmApneaUnit = component.getComponentResourceString("Group","AlarmSetUpMenu","Unit","Apnea")
var alarmVolumeLabel = component.getComponentResourceString("Group","AlarmSetUpMenu","Label","Volume")
var alarmVolumeUnit = component.getComponentResourceString("Group","AlarmSetUpMenu","Unit","Volume")
var alarmParamScrollStateBorderColor = component.getComponentXmlDataByTag("Group","AlarmSetupButton", "AlarmParamScrollStateBorderColor")
var alarmParamScrollStateBorderWidth = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetupButton", "AlarmParamScrollStateBorderWidth"))
var closeBtnSource = component.getComponentXmlDataByTag("Component","CloseBtn", "CloseIcon")
var closeBtnTchSource = component.getComponentXmlDataByTag("Component","CloseBtn", "CloseIconTouched")
var closeBtnHeight = parseInt(component.getComponentXmlDataByTag("Component","CloseBtn", "CloseBtnHeight"))
var closeBtnWidth = parseInt(component.getComponentXmlDataByTag("Component","CloseBtn", "CloseBtnWidth"))
var closeBtnBgColor = component.getComponentXmlDataByTag("Component","CloseBtn", "CloseBtnBgColor")
var closeBtnBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","CloseBtn", "CloseBtnBorderWidth"))
var closeBtnBorderColor = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "CloseBtnBorderColor")
//var alarmLogIcon = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLogIcon")
//var alarmStepIcon = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmStepIcon")
var alarmTchColor = component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmTchColor")

// AlarmSetup Button property
var btnValueFontSize = parseInt((component.getComponentXmlDataByTag("Component","AlarmSetupButton","AlarmValueBoxFontSize")))
var valueBoxBorderwidth = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmValueBoxBorderWidth"))
var valueBoxHeight = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmValueBoxHeight"))
var valueBoxRadius = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmValueBoxRadius"))
var valueBoxWidth = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmValueBoxWidth"))
var alarmParamActiveStateColor =(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupParamActiveStateColor"))
var bgColor = "white"
var btnValueFontColor = "black"
var valueBoxBorderColor = (component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmValueBoxBorderColor"))
var alarmParamScrollStateBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmParamScrollStateBorderWidth"))
var alarmParamSelectedStateBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmParamSelectedStateBorderWidth"))
var selectedStateAlarmArrowHeight = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "SelectedStateAlarmArrowHeight"))
var selectedStateAlarmArrowRadius = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "SelectedStateAlarmArrowRadius"))
var selectedStateAlarmArrowWidth =  parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "SelectedStateAlarmArrowWidth"))
var alarmParamEndOfScaleStateColor = (component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmParamEndOfScaleStateColor"))
var alarmParamScrollStateBorderColor =(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmParamScrollStateBorderColor"))
var alarmParamSelectedStateColor = (component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmParamSelectedStateColor"))
var alarmParamSelectedStateFontColor = (component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmParamSelectedStateFontColor"))
var alarmParamTouchedStateColor = (component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmParamTouchedStateColor"))
var alarmSetupSpinnerDownDisabledIcon =(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerDownDisabledIcon"))
var alarmSetupSpinnerDownEnabledIcon =(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerDownEnabledIcon"))
var alarmSetupSpinnerUpEnabledIcon =(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerUpEnabledIcon"))
var alarmSetupSpinnerUpDisabledIcon =(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerUpDisabledIcon"))
var spinnerIconSelStateColor=(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerSelectedColor"))
var spinnerIconTchStateColor = (component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerTouchColor"))
var selectedStateAlarmArrowColor = (component.getComponentXmlDataByTag("Component","AlarmSetupButton", "SelectedStateAlarmArrowColor"))
var lowLimitRightMargin = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "LowLimitRightMargin"))
var highLimitRightMargin = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "HighLimitRightMargin"))
var alarmVolumeBtnTopMargin = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmVolumeBtnTopMargin"))
var pPeakBtnTopMargin = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "PpeakBtnTopMargin"))
var btnTopMargin = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "BtnTopMargin"))
var labelBottomMargin = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "LabelBottomMargin"))
var spinnerRadius= parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "SpinnerRadius"))
var spinnerWidth = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "SpinnerWidth"))
var arrowWidth = parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "ArrowWidth"))
var arrowZvalue= parseInt(component.getComponentXmlDataByTag("Component","AlarmSetupButton", "ArrowZvalue"))

var alarmLabelTopMargin = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLabelTopMargin"))
var alarmLabelLeftMargin= parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmLabelLeftMargin"))
var defaultRightMargin=parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "DefaultRightMargin"))
var defaultUnitTopMargin =parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "DefaultUnitTopMargin"))
var alarmVolumeLabelTopMargin=parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "AlarmVolumeLabelTopMargin"))
var ppeakLabelTopMargin = parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "PpeakLabelTopMargin"))
var mvLabelTopMargin=parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "MVLabelTopMargin"))
var tvexpLabelTopMargin=parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "TVexpLabelTopMargin"))
var o2labelTopMargin=parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "O2labelTopMargin"))
var apneaDelayLabelBottomMargin=parseInt(component.getComponentXmlDataByTag("Group","AlarmSetUpMenu", "ApneaDelayLabelBottomMargin"))
var apeneaLabelTopMargin = parseInt(component.getComponentXmlDataByTag("Component","DefaultSetup","AlarmApenaLabelTopMargin"))
//Function for initiating AlarmSetupMenu
//return value is used for Checking constraint on component creation
function createAlarmSetupMenu(){

    console.log("Create Alarm SetUp Menu !!!!!!!!!")
    alarmSetupMenuComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/DefaultSetupAlarmsPage.qml")
    if (alarmSetupMenuComponent.status === Component.Ready){
        finishAlarmSetupMenuCreation();
    } else if (alarmSetupMenuComponent.status === Component.Error) {
        console.log("Error loading component:", alarmSetupMenuComponent.errorString());
    }else{
        alarmSetupMenuComponent.statusChanged.connect(finishAlarmSetupMenuCreation());
    }
    return 4;
}

// Function for creating AlarmSetupMenu
function finishAlarmSetupMenuCreation(){
    console.log("Alarm Setup Menu creation...........#####")
    var ppeakValue = settingReader.read("Ppeak_Low")
    ObjCCValue.updateConstraintParam("Ppeak_Low",ppeakValue)
    alarmSetupMenuObj = alarmSetupMenuComponent.createObject(rightrect,{"strAlarmMenuLabel":alarmMenuLabel,
                                                                 "objectName": "AlarmSetupMenuObj",
                                                                 "iAlarmLabelTopMargin":alarmLabelTopMargin,
                                                                 "iAlarmLabelLeftMargin":alarmLabelLeftMargin,
                                                                 "iDefaultRightMargin":defaultRightMargin,
                                                                 "iDefaultUnitTopMargin":defaultUnitTopMargin,
                                                                 "iAlarmVolumeLabelTopMargin":alarmVolumeLabelTopMargin,
                                                                 "iPpeakLabelTopMargin":ppeakLabelTopMargin,
                                                                 "iMVLabelTopMargin":mvLabelTopMargin,
                                                                 "iTVexpLabelTopMargin":tvexpLabelTopMargin,
                                                                 "iO2labelTopMargin":o2labelTopMargin,
                                                                 "iApneaDelayLabelBottomMargin":apneaDelayLabelBottomMargin,
                                                                 "iAlarmMenuRadius" : alarmMenuRadius,
                                                                 "iAlarmMenuBorderWidth" : alarmMenuBorderWidth,
                                                                 "iAlarmLogWidth" : alarmLogWidth,
                                                                 "iAlarmLogHeight" : alarmLogHeight,
                                                                 "iAlarmLogRadius" : alarmLogRadius,
                                                                 "iAlarmLogBorderWidth" : alarmLogBorderWidth,
                                                                 "strAlarmMenuBgColor" : alarmMenuBgColor,
                                                                 "strAlarmBorderColor" : alarmBorderColor,
                                                                 //                                                                 "strAlarmLogBgColor" : alarmLogBgColor,
                                                                 "strAlarmLogBorderColor" : alarmLogBorderColor,
                                                                 "strLimitLabel1" : limitLabel1,
                                                                 "strLimitLabel2" : limitLabel2,
                                                                 "strLogLabel" : logLabel,
                                                                 "strFontFamilyAlarmSet" : fontFamilyAlarmSet,
                                                                 "strFontColor" : fontColor,
                                                                 "iPixelSizeAlarmSetup" : pixelSizeAlarmSet,
                                                                 "iPixelSizeLimitLabel" : pixelSizeLimitLabel,
                                                                 //                                                                 "iPixelSizeLogLabel" : pixelSizeLogLabel,
                                                                 "iAlarmSetupParamFontSize":alarmSetupParamFontSize,
                                                                 "strAlarmSetupParamFontColor":alarmSetupParamFontColor,
                                                                 "strAlarmParamPpeak": alarmPpeakLabel,
                                                                 "strAlarmParamPpeakUnit": alarmPpeakUnit,
                                                                 "strAlarmParamMV": alarmMVLabel,
                                                                 "strAlarmParamMVUnit": alarmMVUnit,
                                                                 "strAlarmParamTVexp":alarmTVexpLabel,
                                                                 "strAlarmParamTVexpUnit": alarmTVexpUnit,
                                                                 "strAlarmParamO2":alarmO2Label,
                                                                 "strAlarmParamO2Unit": alarmO2Unit,
                                                                 "strAlarmParamApneaDelay":alarmApneaLabel,
                                                                 "strAlarmParamApneaDelayUnit":alarmApneaUnit,
                                                                 "strAlarmParamAlarm":alarmVolumeLabel,
                                                                 "strAlarmParamAlarmUnit":alarmVolumeUnit,
                                                                 "strAlarmParamScrollStateBorderColor":alarmParamScrollStateBorderColor,
                                                                 "strAlarmParamScrollStateBorderWidth":alarmParamScrollStateBorderWidth,
                                                                 "strCloseBtnSource" : closeBtnSource,
                                                                 "strCloseBtnTchSource" :closeBtnTchSource,
                                                                 "iCloseBtnHeight" : closeBtnHeight,
                                                                 "iCloseBtnWidth"  : closeBtnWidth ,
                                                                 "strCloseBtnBgColor" : closeBtnBgColor,
                                                                 "iCloseBtnBorderWidth" : closeBtnBorderWidth,
                                                                 "strCloseBtnBorderColor" : closeBtnBorderColor,
                                                                 "strAlarmTchColor" : alarmTchColor,
                                                                 "iApeneaLabelTopMargin":apeneaLabelTopMargin

                                                             });

    alarmSetupMenuObj.anchors.top = rightrect.top;

}

//AlarmSetup Btn component creation
function createAlarmSetupMenuComponents(){
    console.log("inside AlarmSetup btn creation !!!!!")
    alarmSetupSecondaryComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/DefaultAlarmSetupBtn.qml")
    console.log("statues of Alarm btn.........",alarmSetupSecondaryComponent.status)
    if (alarmSetupSecondaryComponent.status === Component.Ready){
        finishAlarmSetupMenuComponentCreation();
    } else{
        alarmSetupSecondaryComponent.statusChanged.connect(finishAlarmSetupMenuComponentCreation);
    }
}

// Function for creating AlarmSetup Button
function finishAlarmSetupMenuComponentCreation(){
    console.log("finishAlarmSetupMenuBtnCreation ------>")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/AlarmVolume.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/ApneaDelay.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/MVHigh.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/MVLow.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/O2High.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/O2Low.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/PpeakHigh.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/PpeakLow.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/TVexpHigh.js")
    Qt.include("qrc:/Source/DefaultSetup/JavaScript/TVexpLow.js")

    alarmParamCreationAlarmVolume = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "AlarmVolumeObj",
                                                                                  "strValue": settingReader.read("Alarm_Volume"),
                                                                                  "iBtnValueFontSize" :btnValueFontSize,
                                                                                  "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                                  //                                                                                  "iValueBoxHeight":valueBoxHeight,
                                                                                  "iValueBoxHeight"   :valueBoxHeight,
                                                                                  "iValueBoxRadius" : valueBoxRadius,
                                                                                  "iValueBoxWidth" : valueBoxWidth,
                                                                                  "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                                  "strBgColor" : bgColor,
                                                                                  "strBtnValueFontColor" : btnValueFontColor,
                                                                                  "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                                  "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                                  "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                                  "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                                  "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                                  "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                                  "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                                  "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                                  "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                                  "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                                  "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                                  "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                                  "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                                  "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                                  "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                                  "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                                  "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                                  "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                                  "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                                  "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                                  "iSpinnerRadius":spinnerRadius,
                                                                                  "iSpinnerWidth":spinnerWidth,
                                                                                  "iArrowWidth":arrowWidth,
                                                                                  "iArrowZvalue":arrowZvalue
                                                                              });

    alarmParamCreationPpeakLow = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "PpeakLowObj",
                                                                               "strValue": settingReader.read("Ppeak_Low"),
                                                                               "iBtnValueFontSize" :btnValueFontSize,
                                                                               "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                               "iValueBoxHeight"   :valueBoxHeight,
                                                                               "iValueBoxRadius" : valueBoxRadius,
                                                                               "iValueBoxWidth" : valueBoxWidth,
                                                                               "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                               "strBgColor" : bgColor,
                                                                               "strBtnValueFontColor" : btnValueFontColor,
                                                                               "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                               "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                               "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                               "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                               "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                               "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                               "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                               "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                               "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                               "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                               "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                               "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                               "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                               "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                               "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                               "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                               "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                               "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                               "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                               "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                               "iSpinnerRadius":spinnerRadius,
                                                                               "iSpinnerWidth":spinnerWidth,
                                                                               "iArrowWidth":arrowWidth,
                                                                               "iArrowZvalue":arrowZvalue
                                                                           });
    alarmParamCreationPpeakHigh = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "PpeakHighObj",
                                                                                "strValue": settingReader.read("Ppeak_High"),
                                                                                "iBtnValueFontSize" :btnValueFontSize,
                                                                                "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                                "iValueBoxHeight"   :valueBoxHeight,
                                                                                "iValueBoxRadius" : valueBoxRadius,
                                                                                "iValueBoxWidth" : valueBoxWidth,
                                                                                "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                                "strBgColor" : bgColor,
                                                                                "strBtnValueFontColor" : btnValueFontColor,
                                                                                "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                                "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                                "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                                "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                                "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                                "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                                "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                                "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                                "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                                "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                                "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                                "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                                "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                                "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                                "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                                "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                                "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                                "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                                "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                                "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                                "iSpinnerRadius":spinnerRadius,
                                                                                "iSpinnerWidth":spinnerWidth,
                                                                                "iArrowWidth":arrowWidth,
                                                                                "iArrowZvalue":arrowZvalue
                                                                            });


    alarmParamCreationMVLow = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "MVLowObj",
                                                                            "strValue": settingReader.read("MV_Low"),
                                                                            "iBtnValueFontSize" :btnValueFontSize,
                                                                            "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                            "iValueBoxHeight"   :valueBoxHeight,
                                                                            "iValueBoxRadius" : valueBoxRadius,
                                                                            "iValueBoxWidth" : valueBoxWidth,
                                                                            "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                            "strBgColor" : bgColor,
                                                                            "strBtnValueFontColor" : btnValueFontColor,
                                                                            "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                            "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                            "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                            "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                            "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                            "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                            "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                            "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                            "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                            "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                            "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                            "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                            "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                            "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                            "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                            "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                            "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                            "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                            "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                            "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                            "iSpinnerRadius":spinnerRadius,
                                                                            "iSpinnerWidth":spinnerWidth,
                                                                            "iArrowWidth":arrowWidth,
                                                                            "iArrowZvalue":arrowZvalue
                                                                        });
    alarmParamCreationMVHigh = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "MVHighObj",
                                                                             "strValue": settingReader.read("MV_High"),
                                                                             "iBtnValueFontSize" :btnValueFontSize,
                                                                             "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                             "iValueBoxHeight"   :valueBoxHeight,
                                                                             "iValueBoxRadius" : valueBoxRadius,
                                                                             "iValueBoxWidth" : valueBoxWidth,
                                                                             "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                             "strBgColor" : bgColor,
                                                                             "strBtnValueFontColor" : btnValueFontColor,
                                                                             "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                             "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                             "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                             "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                             "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                             "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                             "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                             "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                             "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                             "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                             "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                             "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                             "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                             "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                             "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                             "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                             "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                             "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                             "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                             "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                             "iSpinnerRadius":spinnerRadius,
                                                                             "iSpinnerWidth":spinnerWidth,
                                                                             "iArrowWidth":arrowWidth,
                                                                             "iArrowZvalue":arrowZvalue
                                                                         });
    alarmParamCreationTVexpLow = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "TVexpLowObj",
                                                                               "strValue": settingReader.read("TVexp_Low"),
                                                                               "iBtnValueFontSize" :btnValueFontSize,
                                                                               "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                               "iValueBoxHeight"   :valueBoxHeight,
                                                                               "iValueBoxRadius" : valueBoxRadius,
                                                                               "iValueBoxWidth" : valueBoxWidth,
                                                                               "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                               "strBgColor" : bgColor,
                                                                               "strBtnValueFontColor" : btnValueFontColor,
                                                                               "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                               "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                               "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                               "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                               "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                               "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                               "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                               "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                               "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                               "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                               "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                               "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                               "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                               "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                               "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                               "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                               "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                               "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                               "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                               "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                               "iSpinnerRadius":spinnerRadius,
                                                                               "iSpinnerWidth":spinnerWidth,
                                                                               "iArrowWidth":arrowWidth,
                                                                               "iArrowZvalue":arrowZvalue
                                                                           });
    alarmParamCreationTVexpHigh = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "TVexpHighObj",
                                                                                "strValue": settingReader.read("TVexp_High"),
                                                                                "iBtnValueFontSize" :btnValueFontSize,
                                                                                "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                                "iValueBoxHeight"   :valueBoxHeight,
                                                                                "iValueBoxRadius" : valueBoxRadius,
                                                                                "iValueBoxWidth" : valueBoxWidth,
                                                                                "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                                "strBgColor" : bgColor,
                                                                                "strBtnValueFontColor" : btnValueFontColor,
                                                                                "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                                "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                                "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                                "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                                "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                                "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                                "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                                "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                                "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                                "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                                "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                                "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                                "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                                "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                                "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                                "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                                "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                                "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                                "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                                "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                                "iSpinnerRadius":spinnerRadius,
                                                                                "iSpinnerWidth":spinnerWidth,
                                                                                "iArrowWidth":arrowWidth,
                                                                                "iArrowZvalue":arrowZvalue

                                                                            });
    alarmParamCreationO2Low = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "O2LowObj",
                                                                            "strValue": settingReader.read("O2_Low"),
                                                                            "iBtnValueFontSize" :btnValueFontSize,
                                                                            "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                            "iValueBoxHeight"   :valueBoxHeight,
                                                                            "iValueBoxRadius" : valueBoxRadius,
                                                                            "iValueBoxWidth" : valueBoxWidth,
                                                                            "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                            "strBgColor" : bgColor,
                                                                            "strBtnValueFontColor" : btnValueFontColor,
                                                                            "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                            "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                            "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                            "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                            "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                            "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                            "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                            "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                            "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                            "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                            "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                            "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                            "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                            "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                            "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                            "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                            "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                            "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                            "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                            "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                            "iSpinnerRadius":spinnerRadius,
                                                                            "iSpinnerWidth":spinnerWidth,
                                                                            "iArrowWidth":arrowWidth,
                                                                            "iArrowZvalue":arrowZvalue


                                                                        });
    alarmParamCreationO2High = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "O2HighObj",
                                                                             "strValue": settingReader.read("O2_High"),
                                                                             "iBtnValueFontSize" :btnValueFontSize,
                                                                             "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                             "iValueBoxHeight"   :valueBoxHeight,
                                                                             "iValueBoxRadius" : valueBoxRadius,
                                                                             "iValueBoxWidth" : valueBoxWidth,
                                                                             "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                             "strBgColor" : bgColor,
                                                                             "strBtnValueFontColor" : btnValueFontColor,
                                                                             "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                             "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                             "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                             "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                             "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                             "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                             "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                             "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                             "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                             "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                             "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                             "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                             "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                             "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                             "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                             "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                             "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                             "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                             "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                             "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                             "iSpinnerRadius":spinnerRadius,
                                                                             "iSpinnerWidth":spinnerWidth,
                                                                             "iArrowWidth":arrowWidth,
                                                                             "iArrowZvalue":arrowZvalue



                                                                         });
    alarmParamCreationApneaDelay = alarmSetupSecondaryComponent.createObject(defaultAlarmSetupMenu,{"objectName" : "ApneaDelayObj",
                                                                                 "strValue": settingReader.read("Apnea_Delay"),
                                                                                 "iBtnValueFontSize" :btnValueFontSize,
                                                                                 "iValueBoxBorderwidth":valueBoxBorderwidth,
                                                                                 "iValueBoxHeight"   :valueBoxHeight,
                                                                                 "iValueBoxRadius" : valueBoxRadius,
                                                                                 "iValueBoxWidth" : valueBoxWidth,
                                                                                 "strAlarmParamActiveStateColor": alarmParamActiveStateColor,
                                                                                 "strBgColor" : bgColor,
                                                                                 "strBtnValueFontColor" : btnValueFontColor,
                                                                                 "strValueBoxBorderColor" : valueBoxBorderColor,
                                                                                 "iAlarmParamScrollStateBorderWidth" : alarmParamScrollStateBorderWidth,
                                                                                 "iAlarmParamSelectedStateBorderWidth" :alarmParamSelectedStateBorderWidth,
                                                                                 "iSelectedStateAlarmArrowHeight" : selectedStateAlarmArrowHeight,
                                                                                 "iSelectedStateAlarmArrowRadius" : selectedStateAlarmArrowRadius,
                                                                                 "iSelectedStateAlarmArrowWidth" : selectedStateAlarmArrowWidth,
                                                                                 "strAlarmParamEndOfScaleStateColor": alarmParamEndOfScaleStateColor,
                                                                                 "strAlarmParamScrollStateBorderColor" : alarmParamScrollStateBorderColor,
                                                                                 "strAlarmParamSelectedStateColor" : alarmParamSelectedStateColor,
                                                                                 "strAlarmParamSelectedStateFontColor" : alarmParamSelectedStateFontColor,
                                                                                 "strAlarmParamTouchedStateColor" : alarmParamTouchedStateColor,
                                                                                 "strAlarmSetupSpinnerDownDisabledIcon" :alarmSetupSpinnerDownDisabledIcon,
                                                                                 "strAlarmSetupSpinnerDownEnabledIcon" : alarmSetupSpinnerDownEnabledIcon,
                                                                                 "strAlarmSetupSpinnerUpEnabledIcon" : alarmSetupSpinnerUpEnabledIcon,
                                                                                 "strAlarmSetupSpinnerUpDisabledIcon" : alarmSetupSpinnerUpDisabledIcon,
                                                                                 "strSpinnerIconSelStateColor" : spinnerIconSelStateColor,
                                                                                 "strSpinnerIconTchStateColor": spinnerIconTchStateColor,
                                                                                 "strSelectedStateAlarmArrowColor" : selectedStateAlarmArrowColor,
                                                                                 "strSelectedStateDownAlarmArrowColor":spinnerIconSelStateColor,
                                                                                 "strSelectedStateUpAlarmArrowColor":spinnerIconSelStateColor,
                                                                                 "iSpinnerRadius":spinnerRadius,
                                                                                 "iSpinnerWidth":spinnerWidth,
                                                                                 "iArrowWidth":arrowWidth,
                                                                                 "iArrowZvalue":arrowZvalue



                                                                             });

    //Setting unique Id for the alarm params
    alarmParamCreationPpeakHigh.setUniqueID("PpeakHigh")
    alarmParamCreationPpeakLow.setUniqueID("PpeakLow")
    alarmParamCreationMVLow.setUniqueID("MVLow")
    alarmParamCreationMVHigh.setUniqueID("MVHigh")
    alarmParamCreationTVexpLow.setUniqueID("TVexpLow")
    alarmParamCreationTVexpHigh.setUniqueID("TVexpHigh")
    alarmParamCreationO2Low.setUniqueID("O2Low")
    alarmParamCreationO2High.setUniqueID("O2High")
    alarmParamCreationAlarmVolume.setUniqueID("AlarmVolume")
    alarmParamCreationApneaDelay.setUniqueID("ApneaDelay")

    var alarmSetupReference =getCompRef(rightrect,"AlarmSetupMenuObj")

    alarmParamCreationAlarmVolume.anchors.top = alarmSetupReference.top;
    alarmParamCreationAlarmVolume.anchors.topMargin = alarmVolumeBtnTopMargin;
    alarmParamCreationAlarmVolume.anchors.right = alarmSetupReference.right;
    alarmParamCreationAlarmVolume.anchors.rightMargin = lowLimitRightMargin ;

    alarmParamCreationPpeakHigh.anchors.top = alarmSetupReference.top;
    alarmParamCreationPpeakHigh.anchors.topMargin = pPeakBtnTopMargin;
    alarmParamCreationPpeakHigh.anchors.right = alarmSetupReference.right;
    alarmParamCreationPpeakHigh.anchors.rightMargin = highLimitRightMargin;
    alarmParamCreationPpeakLow.anchors.top = alarmSetupReference.top;
    alarmParamCreationPpeakLow.anchors.topMargin = pPeakBtnTopMargin;
    alarmParamCreationPpeakLow.anchors.right = alarmSetupReference.right;
    alarmParamCreationPpeakLow.anchors.rightMargin = lowLimitRightMargin ;

    var alarmParamCreationPpeakLowRefrence = getCompRef(defaultAlarmSetupMenu,"PpeakLowObj")
    alarmParamCreationMVLow.anchors.top = alarmParamCreationPpeakLowRefrence.bottom
    alarmParamCreationMVLow.anchors.topMargin = btnTopMargin
    alarmParamCreationMVLow.anchors.right = alarmSetupReference.right;
    alarmParamCreationMVLow.anchors.rightMargin = lowLimitRightMargin ;

    var alarmParamCreationPpeakHighRefrence = getCompRef(defaultAlarmSetupMenu,"PpeakHighObj")
    alarmParamCreationMVHigh.anchors.top = alarmParamCreationPpeakHighRefrence.bottom
    alarmParamCreationMVHigh.anchors.topMargin = btnTopMargin
    alarmParamCreationMVHigh.anchors.right = alarmSetupReference.right;
    alarmParamCreationMVHigh.anchors.rightMargin = highLimitRightMargin ;

    var alarmParamCreationMVLowRefrence = getCompRef(defaultAlarmSetupMenu,"MVLowObj")
    alarmParamCreationTVexpLow.anchors.top = alarmParamCreationMVLowRefrence.bottom
    alarmParamCreationTVexpLow.anchors.topMargin = btnTopMargin
    alarmParamCreationTVexpLow.anchors.right = alarmSetupReference.right;
    alarmParamCreationTVexpLow.anchors.rightMargin = lowLimitRightMargin ;

    var alarmParamCreationMVHighRefrence = getCompRef(defaultAlarmSetupMenu,"MVHighObj")
    alarmParamCreationTVexpHigh.anchors.top = alarmParamCreationMVHighRefrence.bottom
    alarmParamCreationTVexpHigh.anchors.topMargin = btnTopMargin
    alarmParamCreationTVexpHigh.anchors.right = alarmSetupReference.right;
    alarmParamCreationTVexpHigh.anchors.rightMargin = highLimitRightMargin ;
    var alarmParamCreationTvexpLowRefrence = getCompRef(defaultAlarmSetupMenu,"TVexpLowObj")
    alarmParamCreationO2Low.anchors.top = alarmParamCreationTvexpLowRefrence.bottom
    alarmParamCreationO2Low.anchors.topMargin = btnTopMargin
    alarmParamCreationO2Low.anchors.right = alarmSetupReference.right;
    alarmParamCreationO2Low.anchors.rightMargin = lowLimitRightMargin ;
    var alarmParamCreationTVexpHighRefrence = getCompRef(defaultAlarmSetupMenu,"TVexpHighObj")
    alarmParamCreationO2High.anchors.top = alarmParamCreationTVexpHighRefrence.bottom
    alarmParamCreationO2High.anchors.topMargin = btnTopMargin
    alarmParamCreationO2High.anchors.right = alarmSetupReference.right;
    alarmParamCreationO2High.anchors.rightMargin = highLimitRightMargin ;


    var alarmParamCreationO2LowRefrence = getCompRef(defaultAlarmSetupMenu,"O2LowObj")
    alarmParamCreationApneaDelay.anchors.top = alarmParamCreationO2LowRefrence.bottom
    alarmParamCreationApneaDelay.anchors.topMargin = btnTopMargin
    alarmParamCreationApneaDelay.anchors.right = alarmSetupReference.right;
    alarmParamCreationApneaDelay.anchors.rightMargin = lowLimitRightMargin ;

    var refObjHighLimitLabel = getCompRef(defaultAlarmSetupMenu, "highLimitLabelObj")
    refObjHighLimitLabel.anchors.bottom = alarmParamCreationPpeakHighRefrence.top
    refObjHighLimitLabel.anchors.bottomMargin = labelBottomMargin
    refObjHighLimitLabel.anchors.horizontalCenter = alarmParamCreationPpeakHighRefrence.horizontalCenter

    var refObjLowLimitLabel = getCompRef(defaultAlarmSetupMenu, "lowLimitLabelObj")
    refObjLowLimitLabel.anchors.bottom = alarmParamCreationPpeakLowRefrence.top
    refObjLowLimitLabel.anchors.bottomMargin = labelBottomMargin
    refObjLowLimitLabel.anchors.horizontalCenter = alarmParamCreationPpeakLowRefrence.horizontalCenter

    //Connecting Increment Decrement for AlarmSetup Btns
    alarmParamCreationPpeakLow.incrementValue.connect(onPpeakLowIncrementValue)
    alarmParamCreationPpeakLow.decrementValue.connect(onPpeakLowDecrementValue)
    alarmParamCreationPpeakHigh.incrementValue.connect(onPpeakHighIncrementValue)
    alarmParamCreationPpeakHigh.decrementValue.connect(onPpeakHighDecrementValue)
    alarmParamCreationPpeakHigh.selection.connect(onPpeakHighSelection)
    alarmParamCreationMVLow.incrementValue.connect(onMVLowIncrementValue)
    alarmParamCreationMVLow.decrementValue.connect(onMVLowDecrementValue)
    alarmParamCreationMVHigh.incrementValue.connect(onMVHighIncrementValue)
    alarmParamCreationMVHigh.decrementValue.connect(onMVHighDecrementValue)
    alarmParamCreationTVexpLow.incrementValue.connect(onTVexpLowIncrementValue)
    alarmParamCreationTVexpLow.decrementValue.connect(onTVexpLowDecrementValue)
    alarmParamCreationTVexpHigh.incrementValue.connect(onTVexpHighIncrementValue)
    alarmParamCreationTVexpHigh.decrementValue.connect(onTVexpHighDecrementValue)
    alarmParamCreationO2Low.incrementValue.connect(onO2LowIncrementValue)
    alarmParamCreationO2Low.decrementValue.connect(onO2LowDecrementValue)
    alarmParamCreationO2High.incrementValue.connect(onO2HighIncrementValue)
    alarmParamCreationO2High.decrementValue.connect(onO2HighDecrementValue)
    alarmParamCreationApneaDelay.incrementValue.connect(onApneaDelayIncrementValue)
    alarmParamCreationApneaDelay.decrementValue.connect(onApneaDelayDecrementValue)
    alarmParamCreationAlarmVolume.incrementValue.connect(onAlarmVolumeIncrementValue)
    alarmParamCreationAlarmVolume.decrementValue.connect(onAlarmVolumeDecrementValue)

    // Setting of touch state color & End of Scale up down arrow for Alarm Setup Spinner
    alarmParamCreationPpeakLow.checkEndOfScaleForSpinnerWidget.connect(onPpeakLowEndOfScale)
    alarmParamCreationPpeakLow.onSetArrowUpBgColor.connect(onSetPpeakLowUpArrowBgColor)
    alarmParamCreationPpeakLow.onSetArrowDownBgColor.connect(onSetPpeakLowDownArrowBgColor)

    alarmParamCreationPpeakHigh.checkEndOfScaleForSpinnerWidget.connect(onPpeakHighEndOfScale)
    alarmParamCreationPpeakHigh.onSetArrowUpBgColor.connect(onSetPpeakHighUpArrowBgColor)
    alarmParamCreationPpeakHigh.onSetArrowDownBgColor.connect(onSetPpeakHighDownArrowBgColor)

    alarmParamCreationMVLow.checkEndOfScaleForSpinnerWidget.connect(onMVLowEndOfScale)
    alarmParamCreationMVLow.onSetArrowUpBgColor.connect(onSetMVLowUpArrowBgColor)
    alarmParamCreationMVLow.onSetArrowDownBgColor.connect(onSetMVLowDownArrowBgColor)

    alarmParamCreationMVHigh.checkEndOfScaleForSpinnerWidget.connect(onMVHighEndOfScale)
    alarmParamCreationMVHigh.onSetArrowUpBgColor.connect(onSetMVHighUpArrowBgColor)
    alarmParamCreationMVHigh.onSetArrowDownBgColor.connect(onSetMVHighDownArrowBgColor)

    alarmParamCreationO2Low.checkEndOfScaleForSpinnerWidget.connect(onO2LowEndOfScale)
    alarmParamCreationO2Low.onSetArrowUpBgColor.connect(onSetO2LowUpArrowBgColor)
    alarmParamCreationO2Low.onSetArrowDownBgColor.connect(onSetO2LowDownArrowBgColor)

    alarmParamCreationO2High.checkEndOfScaleForSpinnerWidget.connect(onO2HighEndOfScale)
    alarmParamCreationO2High.onSetArrowUpBgColor.connect(onSetO2HighUpArrowBgColor)
    alarmParamCreationO2High.onSetArrowDownBgColor.connect(onSetO2HighDownArrowBgColor)

    alarmParamCreationTVexpLow.checkEndOfScaleForSpinnerWidget.connect(onTVexpLowEndOfScale)
    alarmParamCreationTVexpLow.onSetArrowUpBgColor.connect(onSetTVexpLowUpArrowBgColor)
    alarmParamCreationTVexpLow.onSetArrowDownBgColor.connect(onSetTVexpLowDownArrowBgColor)

    alarmParamCreationTVexpHigh.checkEndOfScaleForSpinnerWidget.connect(onTVexpHighEndOfScale)
    alarmParamCreationTVexpHigh.onSetArrowUpBgColor.connect(onSetTVexpHighUpArrowBgColor)
    alarmParamCreationTVexpHigh.onSetArrowDownBgColor.connect(onSetTVexpHighDownArrowBgColor)

    alarmParamCreationApneaDelay.checkEndOfScaleForSpinnerWidget.connect(onApneaDelayEndOfScale)
    alarmParamCreationApneaDelay.onSetArrowUpBgColor.connect(onSetApneaDelayUpArrowBgColor)
    alarmParamCreationApneaDelay.onSetArrowDownBgColor.connect(onSetApneaDelayDownArrowBgColor)

    alarmParamCreationAlarmVolume.checkEndOfScaleForSpinnerWidget.connect(onAlarmVolumeEndOfScale)
    alarmParamCreationAlarmVolume.onSetArrowUpBgColor.connect(onSetAlarmVolumeUpArrowBgColor)
    alarmParamCreationAlarmVolume.onSetArrowDownBgColor.connect(onSetAlarmVolumeDownArrowBgColor)

}
var firstButtonIndex = 16
var lastButtonIndex = 25

//Function for handling touch state of alarmSetup btns
// param<objectName>-Name of AlarmSetup Button touched
function onSecParamAlarmMenuTouched(objectName) {

    console.log("onSecParamAlarmMenuTouched..............")
    var currentSecParam = getCompRef(defaultAlarmSetupMenu, objectName)
    for(var i=firstButtonIndex;i<=lastButtonIndex;i++) {
        var currentComp = getCompRefByIndex(defaultAlarmSetupMenu, i)

        if ((currentComp.state === "Selected" ||
             currentComp.state === "End of Scale") &&
                ((defaultAlarmSetupMenu.children[i].objectName) !== (currentSecParam.objectName))) {
            currentComp.strValue = currentComp.prevValue;
            //            console.log("currentComp:"+currentComp);
            onSelectOtherAlarmSetupBtn(currentComp,currentSecParam)
        }
    }

    if(currentSecParam.state!=="Selected" && currentSecParam.state!=="End of Scale") {
        console.log("currentScreen:"+currentSecParam);
        currentSecParam.focus =true
        changeStateInAlarmBtn(currentSecParam,"Touched")
    }
    //    var timeObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    //    timeObj.stopTimer()
    //    console.log("timer from touched comp")
}

//Function for handling Select state of alarmSetup btns
// param<objectName>-Name of AlarmSetup Button selected
function onSecParamAlarmMenuSelected(objectName) {

    console.log("onSecParamAlarmMenuSelected............")
    var currentSecParam = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("Selected State !!!" + currentSecParam.state)
    currentSecParam.prevValue = currentSecParam.strValue
    if(currentSecParam.state === "Touched") {
        changeStateInAlarmBtn(currentSecParam,"Selected")
    } else if(currentSecParam.state === "Selected" || currentSecParam.state === "End of Scale") {
        var newValue = currentSecParam.strValue;

        if(currentSecParam.objectName === "PpeakHighObj"){
            settingReader.writeDataInFile("Pmax_sKey", newValue)
            backupData.storeCriticalParams("Pmax_sKey", newValue)
            objCSBValue.writeToCSB("Pmax_sKey", newValue, "Secondary")
            settingReader.writeDataInFile("Ppeak_High", newValue)
            objCSBValue.writeToCSB("Ppeak_High", newValue, "Alarm")
            console.log("writing to Pmax")

            changeStateInAlarmBtn(currentSecParam,"Scroll")

        } else if(currentSecParam.objectName === "PpeakLowObj") {
            writeToFiles("Ppeak_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "MVLowObj") {
            writeToFiles("MV_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "MVHighObj") {
            writeToFiles("MV_High",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "O2LowObj") {
            writeToFiles("O2_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "O2HighObj") {
            writeToFiles("O2_High",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "TVexpLowObj") {
            writeToFiles("TVexp_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "TVexpHighObj") {
            writeToFiles("TVexp_High",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "ApneaDelayObj") {
            writeToFiles("Apnea_Delay",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "AlarmVolumeObj") {
            writeToFiles("Alarm_Volume",newValue,currentSecParam)
        } else {
            console.log(" ObjectName not found.......... ")
        }
        console.log("the state now is++++++++++++++++++++++++++++++++" + currentSecParam.state)
        valueBox.state="ScrollWithMouse"
    }
    //    var timeObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    //    timeObj.restartTimer()
    console.log("timer from param selected")
}

// Function for defining the state change on Touch
function changeStateInAlarmBtnOnTouch(currentSecParam,stateName){
    console.log("ChangeState.......................")
    console.log("state changed to !!!!", stateName)
    var alarmMenuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    currentSecParam.state = stateName
    //    console.log("currentSecparam:"+currentSecParam.state)
    for(var i=firstButtonIndex;i<=lastButtonIndex;i++) {
        var siblingObj = getCompRef(alarmMenuObj, alarmMenuObj.children[i].children[0].objectName)
        //        console.log("siblingObj:"+siblingObj.state)
        if(siblingObj.state !== "Disabled"){
            if(siblingObj.state !== "Active" && alarmMenuObj.children[i].objectName !== currentSecParam.objectName){
                console.log("changing state of object"+siblingObj+ " " + siblingObj.state)
                siblingObj.state = "Active"
            }
        }
    }
}

function changeStateInAlarmBtn(currentSecParam,stateName) {

    console.log("ChangeState.......................")
    console.log("state changed to !!!!", stateName)
    var alarmMenuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    currentSecParam.state = stateName
    //    console.log("currentSecparam:"+currentSecParam.state)
    for(var i=firstButtonIndex;i<=lastButtonIndex;i++) {
        var siblingObj =  getCompRef(alarmMenuObj, alarmMenuObj.children[i].objectName);
        //        console.log("objectname:"+defaultAlarmSetupMenu.children[i].objectName);
        //        console.log("siblingObj:"+siblingObj.state)
        if(siblingObj.state !== "Disabled"){
            if(siblingObj.state !== "" && siblingObj.state !== "Active" && alarmMenuObj.children[i].objectName !== currentSecParam.objectName){
                console.log("changing state of object"+siblingObj+ " " + siblingObj.state)
                alarmMenuObj.children[i].state = "Active"
            }
        }
    }
}

function resetStateInAlarmBtn() {

    var alarmMenuObj = getCompRef(mainitem,"AlarmSetupMenuObj")
    for(var i=firstButtonIndex;i<=lastButtonIndex;i++){
        var siblingObj = getCompRef(alarmMenuObj, alarmMenuObj.children[i].objectName)

        if(siblingObj.state === "Selected" || siblingObj.state === "End of Scale") {
            console.log("restore old values")
            siblingObj.strValue = siblingObj.prevValue;
        }

        if(siblingObj.state !== "Disabled"){
            if(siblingObj.state !== "Active"){
                console.log("changing state of object"+siblingObj+ " " + siblingObj.state)
                siblingObj.state = "Active"
            }
        }
    }
}

function moveClockwiseAlarmSetup(currentObjectName) {

    console.log("Move Clockwise.....")
    var alarmMenuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    var length = alarmMenuObj.children.length ;
    var currentIndex = firstButtonIndex;
    var currentComp = getCompRef(alarmMenuObj, currentObjectName)
    for(var i =firstButtonIndex; i< lastButtonIndex;i++) {
        if(alarmMenuObj.children[i].objectName === currentObjectName) {
            //            console.log("break condition");
            break;
        } else {
            currentIndex++;
            console.log("currentIndex:"+currentIndex);
        }
    }
    if (currentIndex === length - 1 ) {
        console.log("length is length " + length + " "+currentIndex)
        //        onNavigateRightInAlarmMenu()
        alarmMenuObj.children[(currentIndex + 1)%length+16].focus = true

    }  else {
        alarmMenuObj.children[(currentIndex + 1)%length].focus = true
    }
    var menuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    //    menuObj.restartTimer()
}

function moveAntiClockwiseAlarmSetup(currentObjectName) {

    var alarmMenuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    var length = alarmMenuObj.children.length;
    var currentIndex = firstButtonIndex;
    var currentComp = getCompRef(alarmMenuObj, currentObjectName)
    console.log("Moving  Anti Clockwise")
    for(var i =firstButtonIndex; i< lastButtonIndex ;i++) {
        if(alarmMenuObj.children[i].objectName === currentObjectName){
            break;
        } else {
            currentIndex++;
        }
    }
    if (currentIndex === firstButtonIndex ) {
        console.log("length is length " + length + " "+currentIndex)
        alarmMenuObj.children[(currentIndex - 1)+10].focus = true
        //        onNavigateLeftInAlarmMenu()
    } else {
        alarmMenuObj.children[(currentIndex - 1)].focus = true
    }
    //    alarmMenuObj.restartTimer()
}

function destroyAlarmSetupMenu() {

    var menuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    if (menuObj !== null && typeof(menuObj) !== "undefined"){
        console.log("destroy alarm set up menu")
        setIsAlarmSMOpen(false)
        menuObj.destroy()
        menuObj.stopTimer()
        menuObj = null;
        var ventGrpReference=getGroupRef("VentGroup")
        var ventModeObject = getCompRef(ventGrpReference.ventParentId, "ventModeBtn")
        ventModeObject.focus = true
    } else {
        console.log("alarmSetupMenuObj === null")
    }
}

function destroyedAlarmSetupMenu() {

    var menuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    if (menuObj !== null && typeof(menuObj) !== "undefined"){
        console.log("destroy alarm set up menu")
        setIsAlarmSMOpen(false)
        menuObj.destroy()
        menuObj.stopTimer()
        menuObj = null;
    } else {
        console.log("alarmSetupMenuObj === null")
    }
}

// Function to write values in Settings file and CSB
function writeToFiles(lableid,newValue,currentSecParam) {

    settingReader.writeDataInFile(lableid, newValue)
    objCSBValue.writeToCSB(lableid,newValue,"Alarm")
    changeStateInAlarmBtn(currentSecParam,"Scroll")
}

//Function for handling EnterPressed(Comwheel) state of alarmSetup btns
function onSecParamEnterPressed(objectName) {

    console.log("enter pressed!!!")
    var alarmSetupMenuObj = getCompRef(rightrect, "AlarmSetupMenuObj")
    var currentSecParam = getCompRef(alarmSetupMenuObj, objectName)
    currentSecParam.prevValue = currentSecParam.strValue
    if(currentSecParam.state === "Active" || currentSecParam.state === "" || currentSecParam.state === "Scroll"){
        changeStateInAlarmBtn(currentSecParam,"Selected")
    } else {
        var newValue = currentSecParam.strValue;
        if(currentSecParam.objectName === "PpeakHighObj"){
            settingReader.writeDataInFile("Pmax_sKey", newValue)
            backupData.storeCriticalParams("Pmax_sKey", newValue)
            objCSBValue.writeToCSB("Pmax_sKey", newValue, "Secondary")

            settingReader.writeDataInFile("Ppeak_High", newValue)
            objCSBValue.writeToCSB("Ppeak_High", newValue, "Alarm")

            console.log("writing to Pmax")
            changeStateInAlarmBtn(currentSecParam,"Scroll")
        } else if(currentSecParam.objectName === "PpeakLowObj") {
            writeToFiles("Ppeak_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "MVLowObj") {
            writeToFiles("MV_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "MVHighObj") {
            writeToFiles("MV_High",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "O2LowObj") {
            writeToFiles("O2_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "O2HighObj") {
            writeToFiles("O2_High",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "TVexpLowObj") {
            writeToFiles("TVexp_Low",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "TVexpHighObj") {
            writeToFiles("TVexp_High",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "ApneaDelayObj") {
            writeToFiles("Apnea_Delay",newValue,currentSecParam)
        } else if(currentSecParam.objectName === "AlarmVolumeObj") {
            writeToFiles("Alarm_Volume",newValue,currentSecParam)
        } else {
            console.log(" ObjectName not found.......... ")
        }
        console.log("the state now is" + currentSecParam.state)
    }
    //    alarmSetupMenuObj.restartTimer()
    console.log("timer from enter pressed")
}

function onSelectOtherAlarmSetupBtn(currentComp,SelectedComp) {

    console.log("inside other key selected............")
    SelectedComp.focus = true
    SelectedComp.forceActiveFocus()
    changeStateInAlarmBtn(SelectedComp,"Touched")
}

function hideComponent(){
    alarmSetupMenuObj.destroy();
}









