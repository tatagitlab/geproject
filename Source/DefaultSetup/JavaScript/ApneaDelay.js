var apneaDelayRangeList;
var apneaDelayMin;
var apneaDelayMax;
var apneaDelayStepSize;
apneaDelayRangeList = physicalparam.getRangeData("Apnea_Delay", "s")
apneaDelayMin = apneaDelayRangeList[0]
apneaDelayMax = apneaDelayRangeList[1]
apneaDelayStepSize = apneaDelayRangeList[2]

/*Increment step Logic for Apnea Delay */
function clkHandleApneaDelay(m_apneaDelay) {
    m_apneaDelay += parseInt(apneaDelayStepSize) ;
    return m_apneaDelay;

}

/*Decrement step Logic for Apnea Delay */
function antClkHandleApneaDelay(m_apneaDelay){
    m_apneaDelay -= parseInt(apneaDelayStepSize) ;
    return m_apneaDelay;
}

function onApneaDelayIncrementValue(objectName) {
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "ApneaDelayObj"){
    if(parseInt(currentAlarmSetupComp.strValue) < parseInt(apneaDelayMax)){
        currentAlarmSetupComp.strValue = parseInt(clkHandleApneaDelay(parseInt(currentAlarmSetupComp.strValue)))
        changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
    } else {
        changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
    }
}
}


function onApneaDelayDecrementValue(objectName) {
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "ApneaDelayObj"){
    if(parseInt(currentAlarmSetupComp.strValue) > parseInt(apneaDelayMin)){
        currentAlarmSetupComp.strValue = parseInt(antClkHandleApneaDelay(parseInt(currentAlarmSetupComp.strValue)))
        changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
    } else {
        changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
    }
}
}

function onApneaDelayEndOfScale(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "ApneaDelayObj" ))
    {
        if(currentVentComp.strValue === apneaDelayMin) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(currentVentComp.strValue === apneaDelayMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !== apneaDelayMin && currentVentComp.strValue !== apneaDelayMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetApneaDelayUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "ApneaDelayObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(apneaDelayMax) ){
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

function onSetApneaDelayDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "ApneaDelayObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(apneaDelayMin) ){
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
