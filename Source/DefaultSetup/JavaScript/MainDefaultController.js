var defltComponent;
var dfltSetup;

var mainDefaultAreaWidth = parseInt(component.getComponentXmlDataByTag("Component","DefaultSetup","DefaultWidth"))
var mainDefaultAreaHeight = parseInt(component.getComponentXmlDataByTag("Component","DefaultSetup","DefaultHeight"))
var mainDefaultAreaColor = component.getComponentXmlDataByTag("Component","DefaultSetup","DefaultLayoutColor")
var titleAreaWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","TitleRectWidth")
var titleAreaHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","TitleRectHeight")
var titleText = component.getComponentXmlDataByTag("Component","DefaultSetup","TitleRectText")
var titleTextLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","TitleRectTextLeftMargin")

var titleTextFontSize = component.getComponentXmlDataByTag("Component","DefaultSetup","TitleRectTextFont")
var leftColumnWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftRectWidth")
var leftColumnTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftRectTopMargin")
var layoutPreferredHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","LayoutPreferredHeight")
var defaultMenuLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftColumnLeftMargin")
var leftColumnFirstText = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftColumnFirstText")
var leftColumnSecondText = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftColumnSecondText")
var leftColumnThirdText = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftColumnThirdText")
var leftColumnFourthText = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftColumnFourthText")
var defaultDropdownvalue1 = component.getComponentResourceString("Component","DefaultSetup","Label","Left_Default_Area")
var defaultDropdownvalue2 = component.getComponentResourceString("Component","DefaultSetup","Label","Left_Default_Area1")
var customBorderZero = component.getComponentXmlDataByTag("Component","DefaultSetup","CustomBorderZero")
var customBorderOne = component.getComponentXmlDataByTag("Component","DefaultSetup","CustomBorderOne")
var customBorderTwo = component.getComponentXmlDataByTag("Component","DefaultSetup","CustomBorderTwo")
//var commonBorder = component.getComponentXmlDataByTag("Component","DefaultSetup","CommonBorder")
var customBorderColor = component.getComponentXmlDataByTag("Component","DefaultSetup","CustomBorderColor")
var focusColour = component.getComponentXmlDataByTag("Component","DefaultSetup","FocusColor")
var nonFocusColor = component.getComponentXmlDataByTag("Component","DefaultSetup","NonFocusColor")
var selectedColor = component.getComponentXmlDataByTag("Component","DefaultSetup","SelectedColor")
var columnSpacing = component.getComponentXmlDataByTag("Component","DefaultSetup","LeftRectColumnSpacing")
var currentScreen = component.getComponentXmlDataByTag("Component","DefaultSetup","CurrentScreen")
var dropDownTopMargin= component.getComponentXmlDataByTag("Component","DefaultSetup","DropDownTopMargin")
var dropDownLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropDownLeftMargin")


var ldrpArea = [defaultDropdownvalue1, defaultDropdownvalue2]
/*Function for initiating the creation of Default Setup*/
function createDefaultSetupComponent()
{
    console.log("calling the function default setup");
    defltComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/MainDefaultSetup.qml");
    if (defltComponent.status === Component.Ready)
        finishCreation();
    else
        defltComponent.statusChanged.connect(finishCreation);
}

// Function for creating Default Setup
function finishCreation() {
    if (defltComponent.status === Component.Ready) {
        dfltSetup = defltComponent.createObject(mainitem, {"objectName":"MainDefaultSetupScreen",
                                                    "iDefaultRectHeight":mainDefaultAreaHeight,
                                                      "iDefaultRectWidth":mainDefaultAreaWidth,
//                                                      "iDefaultRectHeight":mainDefaultAreaHeight,
													  "iDefaultRectColor":mainDefaultAreaColor,
													  "iDefaultTitleRectHeight":titleAreaHeight,
													  "iTitleText":titleText,
													  "iTitleTextLeftMargin":titleTextLeftMargin,
                                                      "iDefaultTitleFontSize":titleTextFontSize,
													  "iLeftColumnWidth":leftColumnWidth,
                                                      "iLeftColumnTopMargin":leftColumnTopMargin,
													  "iLayoutPreferredHeight":layoutPreferredHeight,
													  "iDefaultMenuLeftMargin":defaultMenuLeftMargin,
													  "iLeftColumnFirstText":leftColumnFirstText,
													  "iLeftColumnSecondText":leftColumnSecondText,
													  "iLeftColumnThirdText":leftColumnThirdText,
                                                      "iLeftColumnFourthText":leftColumnFourthText,
                                                      "arrLeftDropdownValues": ldrpArea,
                                                      "iCustomBorderDefaultZero":customBorderZero,
                                                      "iCustomBorderDefaultOne":customBorderOne,
                                                      "iCustomBorderDefaultTwo":customBorderTwo,
//                                                      "bCommonBorder":commonBorder,
                                                      "strCustomBorderColor":customBorderColor,
                                                      "strRectFocusColor": focusColour,
                                                      "strRectNonFocusColor":nonFocusColor,
                                                      "strRectSelectedColor": selectedColor,
                                                      "iRectColumnSpacing":columnSpacing,
                                                      "currentScreen":currentScreen,
                                                      "iDropDownTopMargin":dropDownTopMargin,
                                                      "iDropDownLeftMargin":dropDownLeftMargin
//                                                    "arrDefault":defaultList
													  });
//        dfltSetup.focus = true;
//        dfltSetup.forceActiveFocus();

        if (dfltSetup === null) {
                    // Error Handling
                    console.log("Error creating object");
                }
    } else if (defltComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", defltComponent.errorString());
    }
}

function destroyMainDefault() {
    console.log("CAME INSIDE DESTROY MAIN DEFAULT");
    var destroyMainDefaultRef =   getCompRef(mainitem,"MainDefaultSetupScreen");
    if (destroyMainDefaultRef !== null && typeof(destroyMainDefaultRef) !== "undefined")
    {
        console.log("destroying destroyMainDefaultRef");
        destroyMainDefaultRef.destroy();
    } else {
        console.log("destroyMainDefault deletion : destroyMainDefault === null");
    }
}

//function clockwise(currentObjectName)
//{
//        var defaultPageObj = getCompRef(mainitem,"MainDefaultSetupScreen");
//    var length = defaultPageObj.leftColumnId.children.length;
//    var currentIndex = 0;
//    var currentComp = getCompRef(col, currentObjectName)
//    for(var i =0; i<length;i++){
//        if(defaultPageObj.leftColumnId.children[i].objectName === currentObjectName){
////            console.log("defaultPageObj.leftColumnId.children[i].objectName"+defaultPageObj.leftColumnId.children[i].objectName);
//            break;
//        }else{
//            currentIndex++;
//        }
//    }
//    defaultPageObj.leftColumnId.children[currentIndex+1].focus = true;
//}

//function antiClockwise(currentObjectName)
//{
//var defaultPageObj = getCompRef(mainitem,"MainDefaultSetupScreen");
//var length = defaultPageObj.leftColumnId.children.length;
//var currentIndex = 0;
//var currentComp = getCompRef(col, currentObjectName)
//    for(var i =0; i<length;i++){
//        if(defaultPageObj.leftColumnId.children[i].objectName === currentObjectName){
////            console.log("defaultPageObj.leftColumnId.children[i].objectName"+defaultPageObj.leftColumnId.children[i].objectName);
//            break;
//        }else{
//            currentIndex++;
//        }
//    }
//    defaultPageObj.leftColumnId.children[currentIndex+-1].focus = true;
//}
