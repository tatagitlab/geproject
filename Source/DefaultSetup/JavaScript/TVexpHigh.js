var tVexpHighRangeList;
var tVexpHighMin;
var tVexpHighMax;
var tVexpHighStepSize;
tVexpHighRangeList = physicalparam.getRangeData("TVexp_High", "ml")
tVexpHighMin = tVexpHighRangeList[0]
tVexpHighMax = tVexpHighRangeList[1]
tVexpHighStepSize = tVexpHighRangeList[2]

/*Increment step Logic for TVexp High*/
function clkHandleTVolumeExpHigh(m_tVolumeHigh)
{

    var tVexpLowValue = parseInt(settingReader.read("TVexp_Low"))
    if(tVexpLowValue < parseInt(tVexpHighMin)){
        tVexpLowValue = parseInt(tVexpHighMin)
    }
    if((m_tVolumeHigh>=tVexpLowValue) && (m_tVolumeHigh < parseInt(tVexpHighMin))) {
        m_tVolumeHigh = m_tVolumeHigh+5;
    }
    else if(m_tVolumeHigh < parseInt(tVexpHighMax)) {
        m_tVolumeHigh = m_tVolumeHigh+ parseInt(tVexpHighStepSize);
    }
    return m_tVolumeHigh;
}

/*Decrement step Logic for TVexp High*/
function antClkHandleTVolumeExpHigh(m_tVolumeHigh)
{

    if((parseInt(tVexpHighMin) < m_tVolumeHigh) && (m_tVolumeHigh <= parseInt(tVexpHighMax))) {
        m_tVolumeHigh = m_tVolumeHigh - parseInt(tVexpHighStepSize);
    }
    else if((5 < m_tVolumeHigh) && (m_tVolumeHigh <= parseInt(tVexpHighStepSize))) {
        m_tVolumeHigh = m_tVolumeHigh - 5 ;
    }
    return m_tVolumeHigh;
}
function onTVexpHighIncrementValue(objectName) {
    console.log("onTVexpHighIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "TVexpHighObj"){
        if(parseInt(currentAlarmSetupComp.strValue) < parseInt(tVexpHighMax)){
            currentAlarmSetupComp.strValue = parseInt(clkHandleTVolumeExpHigh(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }

}

function onTVexpHighDecrementValue(objectName) {
    console.log("onTVexpHighDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log(currentAlarmSetupComp+"................")
    if(objectName === "TVexpHighObj"){
        var tVexpLowValue = parseInt(settingReader.read("TVexp_Low"))
        if (tVexpLowValue < parseInt(tVexpHighMin)) {
            tVexpLowValue = parseInt(tVexpHighMin)
        } else {
            tVexpLowValue = parseInt(tVexpLowValue) + parseInt(tVexpHighStepSize)
        }
        if(parseInt(currentAlarmSetupComp.strValue) > tVexpLowValue){
            currentAlarmSetupComp.strValue = parseInt(antClkHandleTVolumeExpHigh(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onTVexpHighEndOfScale(objectName){
    console.log("checking end of scale TVexpHigh")
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "TVexpHighObj" ))
    { var tVexpLowValue = parseInt(settingReader.read("TVexp_Low"))
        if (tVexpLowValue < parseInt(tVexpHighMin)) {
            tVexpLowValue =parseInt(tVexpHighMin)
        } else {
            tVexpLowValue = parseInt(tVexpLowValue) + parseInt(tVexpHighStepSize)
        }
        if(currentVentComp.strValue === tVexpLowValue.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(currentVentComp.strValue === tVexpHighMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !== tVexpLowValue.toString() && currentVentComp.strValue !== tVexpHighMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetTVexpHighUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting up arrow bg TVexpHighObj"+currentVentComp)
    if((objectName === "TVexpHighObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(tVexpHighMax) ){
            console.log("changing Color  of Up arrow to select...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing Color  of Up arrow to touch....")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}


function onSetTVexpHighDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting down arrow bg TVexpHigh "+currentVentComp.strValue)
    if((objectName === "TVexpHighObj" )){
        var tVexpLowValue = parseInt(settingReader.read("TVexp_Low"))
        if (tVexpLowValue < parseInt(tVexpHighMin)) {
            tVexpLowValue = parseInt(tVexpHighMin)
        } else {
            tVexpLowValue = parseInt(tVexpLowValue) + parseInt(tVexpHighStepSize)
        }
        if(currentVentComp.strValue === tVexpLowValue.toString()){
            console.log(" changing select color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log(" changing touch color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}




