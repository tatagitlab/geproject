var dropdownComponent;
var dropdownObj;
var dropdownValues  ;
var dropdownLabel;
var parentLabel;
var dropDownObjName;
var dropdownZValue;

var screenDropdownObj;
var casePageDropdownObj;

var dropdownBoxWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownBoxWidth")
var dropdownBoxHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownBoxHeight")
var dropdownBorderColor = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownBorderColor")
var chosenItemTextMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ChosenItemTextMargin")
var chosenItemFontFamily = component.getComponentXmlDataByTag("Component","DefaultSetup","ChosenItemFontFamily")
var chosenItemFontSize = component.getComponentXmlDataByTag("Component","DefaultSetup","ChosenItemFontSize")
var imageHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","ImageHeight")
var imageWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","ImageWidth")
var imageTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ImageTopMargin")
var dropdownHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownHeight")
var dropdownMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownMargin")
var dropdownTextMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownTextMargin")
var dropdownLabelHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownLabelHeight")
var dropdownLabelTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownLabelTopMargin")
var caseDropdownBoxLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","CaseDropdownBoxLeftMargin")
var screenDropdownBoxLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenDropdownBoxLeftMargin")
var imageRightMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ImageRightMargin")
var labelFontSize = component.getComponentXmlDataByTag("Component","DefaultSetup","LabelFontSize")
var labelBoxWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","TextLabelRectWidth")
var labelBoxHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","TextLabelRectHeight")
var dropdownwithlabelwidth = component.getComponentXmlDataByTag("Component","DefaultSetup","DropDownWithLabelWidth")
var dropdownwithlabelheight = component.getComponentXmlDataByTag("Component","DefaultSetup","DropDownWithLabelHeight")
var textlabelfontsize = component.getComponentXmlDataByTag("Component","DefaultSetup","TextLabelFontSize")
var comboboxborderfocuscolor = component.getComponentXmlDataByTag("Component","DefaultSetup","ComboBoxBorderFocusColor")
var comboboxbordercolor = component.getComponentXmlDataByTag("Component","DefaultSetup","ComboBoxBorderColor")
var spinnerDownSource = component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerDownDisabledIcon")

var dropDownObjectName;
function createDropdownwithLabelComponent(dropdownList, dropdowntext, parenttocreate, objectName,zValue) {
    dropdownValues = dropdownList;
    dropdownLabel = dropdowntext;
    parentLabel = parenttocreate;
    dropDownObjectName = objectName;
    dropdownZValue = zValue;
    //    console.log("DropdownObj"+DropdownObj);
    dropdownComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/DropdownwithLabel.qml");
    if (dropdownComponent.status === Component.Ready)
        finishCreation(zValue);
    else
        dropdownComponent.statusChanged.connect(finishCreation);
}

function finishCreation(zValue) {
    if (dropdownComponent.status === Component.Ready) {
        if(parentLabel === "screenandunits"){
            dropdownObj = dropdownComponent.createObject(screenUnitsColumn, {"objectName":dropDownObjectName,
                                                             "iDropDownWithLabelWidth":dropdownwithlabelwidth,
                                                             "iDropDownWithLabelHeight":dropdownwithlabelheight,
                                                             "iTextLabelFontSize":textlabelfontsize,
                                                             "strComboBoxBorderFocusColor":comboboxborderfocuscolor,
                                                             "strComboBoxBorderColor":comboboxbordercolor,
                                                             "iDropdownBoxWidth":dropdownBoxWidth,
                                                             "iDropdownBoxHeight":dropdownBoxHeight,
                                                             "strDropdownBorderColor":dropdownBorderColor,
                                                             "iChosenItemTextmargin":chosenItemTextMargin,
                                                             "iChosenItemFontSize":chosenItemFontSize,
                                                             "strImageSource": spinnerDownSource,
                                                             "iImageHeight":imageHeight,
                                                             "iImageWidth":imageWidth,
                                                             "iImageTopMargin":imageTopMargin,
                                                             "iDropdownHeight":dropdownHeight,
                                                             "iDropdownMargin":dropdownMargin,
                                                             "iDropdownTextMargin":dropdownTextMargin,
                                                             "iDropdownLabelHeight":dropdownLabelHeight,
                                                             "iDropdownLabelTopMargin":dropdownLabelTopMargin,
                                                             "iDropdownBoxLeftMargin":screenDropdownBoxLeftMargin,
                                                             "iImageRightMargin":imageRightMargin,
                                                             "iLabelFontSize":labelFontSize,
                                                             "iLabelBoxWidth":labelBoxWidth,
                                                             "iLabelBoxHeight":labelBoxHeight,
                                                             "iZvalue":zValue
                                                         });
            dropdownObj.screensDropdownboxClicked.connect(dropdownTouched);
            dropdownObj.screenPageDropdownMouseClicked.connect(settingScreenPageFocus);
        }
        else if(parentLabel === "caseTypeAndModeRect")
        {

            dropdownObj = dropdownComponent.createObject(caseTypeColumn, {"objectName":dropDownObjectName,
                                                             "iDropDownWithLabelWidth":dropdownwithlabelwidth,
                                                             "iDropDownWithLabelHeight":dropdownwithlabelheight,
                                                             "iTextLabelFontSize":textlabelfontsize,
                                                             "strComboBoxBorderFocusColor":comboboxborderfocuscolor,
                                                             "strComboBoxBorderColor":comboboxbordercolor,
                                                             "iDropdownBoxWidth":dropdownBoxWidth,
                                                             "iDropdownBoxHeight":dropdownBoxHeight,
                                                             "strDropdownBorderColor":dropdownBorderColor,
                                                             "iChosenItemTextmargin":chosenItemTextMargin,
                                                             "iChosenItemFontSize":chosenItemFontSize,
                                                             "iImageHeight":imageHeight,
                                                             "iImageWidth":imageWidth,
                                                             "strImageSource": spinnerDownSource,
                                                             "iImageTopMargin":imageTopMargin,
                                                             "iDropdownHeight":dropdownHeight,
                                                             "iDropdownMargin":dropdownMargin,
                                                             "iDropdownTextMargin":dropdownTextMargin,
                                                             "iDropdownLabelHeight":dropdownLabelHeight,
                                                             "iDropdownLabelTopMargin":dropdownLabelTopMargin,
                                                             "iDropdownBoxLeftMargin":caseDropdownBoxLeftMargin,
                                                             "iImageRightMargin":imageRightMargin,
                                                             "iLabelFontSize":labelFontSize,
                                                             "iLabelBoxWidth":labelBoxWidth,
                                                             "iLabelBoxHeight":labelBoxHeight,
                                                             "iZvalue":zValue
                                                         });
//            dropdownObj.caseDropdownboxClicked.connect();
            dropdownObj.casePageDropdownMouseClicked.connect(settingCaseTypePageFocus);

        }
        dropdownObj.getValue(dropdownValues, dropdownLabel);
        if (dropdownObj === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (dropdownComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}
