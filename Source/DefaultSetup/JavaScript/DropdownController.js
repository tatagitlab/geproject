var dropdownComponent;
var dropdownObj;
var dropdownValues ;

//to write in xml

var dropdownBoxWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","SingleDropdownBoxWidth")
var dropdownBoxHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","SingleDropdownBoxHeight")
var dropdownBorderColor = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownBorderColor")
var chosenItemTextMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ChosenItemTextMargin")
var chosenItemFontFamily = component.getComponentXmlDataByTag("Component","DefaultSetup","ChosenItemFontFamily")
var chosenItemFontSize = component.getComponentXmlDataByTag("Component","DefaultSetup","ChosenItemFontSize")
var imageHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","ImageHeight")
var imageWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","ImageWidth")
var imageTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","SingleImageTopMargin")
var dropdownHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownHeight")
var dropdownMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownMargin")
var dropdownTextMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownTextMargin")
var dropdownImageRightMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","DropdownImageRightMargin")
var imageSource = component.getComponentXmlDataByTag("Component","AlarmSetupButton", "AlarmSetupSpinnerDownDisabledIcon")
var dropdownObjName;
function createDropdownComponent(dropdownList, dropdownobjectname ) {
    dropdownValues = dropdownList;
    dropdownObjName = dropdownobjectname;
    dropdownComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/DropdownBox.qml");
    if (dropdownComponent.status === Component.Ready)
        finishCreation();
    else
        dropdownComponent.statusChanged.connect(finishCreation);
}

function finishCreation() {
    if (dropdownComponent.status === Component.Ready) {
        dropdownObj = dropdownComponent.createObject(col, {"objectName": dropdownObjName,
                                                         "iDropdownBoxWidth":dropdownBoxWidth,
                                                            "iDropdownBoxHeight":dropdownBoxHeight,
                                                            "strDropdownBorderColor":dropdownBorderColor,
                                                            "iChosenItemTextmargin":chosenItemTextMargin,
                                                         "iChosenItemFontSize":chosenItemFontSize,
                                                         "strImageSource":imageSource,
                                                         "iImageHeight":imageHeight,
                                                         "iImageWidth":imageWidth,
                                                         "iImageTopMargin":imageTopMargin,
                                                         "iDropdownHeight":dropdownHeight,
                                                         "iDropdownMargin":dropdownMargin,
                                                         "iDropdownTextMargin":dropdownTextMargin,
                                                          "iImageRightMargin":dropdownImageRightMargin

                                                     });
        dropdownObj.getValue(dropdownValues);
        if (dropdownObj === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (dropdownComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}
