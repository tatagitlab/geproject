var O2LowRangeList;
var O2LowMin;
var O2LowMax;
var O2LowStepSize;
O2LowRangeList = physicalparam.getRangeData("O2_Low", "%")
O2LowMin = O2LowRangeList[0]
O2LowMax = O2LowRangeList[1]
O2LowStepSize = O2LowRangeList[2]


/*Increment step Logic for O2 Low*/
function clkHandleO2Low(m_o2Low) {
    var o2HighValue = parseInt(settingReader.read("O2_High"))
    var highLimit = parseInt(O2LowMax)
    if(o2HighValue <= highLimit){
        highLimit = o2HighValue -parseInt(O2LowStepSize)
    }
    if((m_o2Low>= parseInt(O2LowMin)) && (m_o2Low < highLimit)) {
        m_o2Low += parseInt(O2LowStepSize) ;
    }
    return m_o2Low;

}
/*Decrement step Logic for O2 Low*/
function antClkHandleO2Low(m_o2Low){
    m_o2Low -= parseInt(O2LowStepSize) ;
    return m_o2Low;
}

function onO2LowIncrementValue(objectName) {
    console.log("onO2LowIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "O2LowObj"){
        var o2HighValue = parseInt(settingReader.read("O2_High"))
        var highLimit = parseInt(O2LowMax)
        if (o2HighValue <= highLimit) {
            highLimit = o2HighValue - parseInt(O2LowStepSize)
        }
        if(parseInt(currentAlarmSetupComp.strValue) < highLimit){
            currentAlarmSetupComp.strValue = parseInt(clkHandleO2Low(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onO2LowDecrementValue(objectName){
    console.log("onO2LowDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if(objectName === "O2LowObj"){
        if(parseInt(currentAlarmSetupComp.strValue) > parseInt(O2LowMin) ){
            currentAlarmSetupComp.strValue = parseInt(antClkHandleO2Low(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}

function onO2LowEndOfScale(objectName){
    console.log("checking end of scale O2Low")
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "O2LowObj" ))
    { var o2HighValue = parseInt(settingReader.read("O2_High"))
        var highLimit = parseInt(O2LowMax)
        if (o2HighValue <= highLimit) {
            highLimit = o2HighValue - parseInt(O2LowStepSize)
        }

        if(currentVentComp.strValue === O2LowMin) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(currentVentComp.strValue === highLimit.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(currentVentComp.strValue !== O2LowMin && currentVentComp.strValue !== highLimit.toString()) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetO2LowUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting up arrow bg O2"+currentVentComp)
    if((objectName === "O2LowObj" )){
        var o2HighValue = parseInt(settingReader.read("O2_High"))
        var highLimitO2Low = parseInt(O2LowMax)
        if (o2HighValue <= highLimitO2Low) {
            highLimitO2Low = o2HighValue - parseInt(O2LowStepSize)
        }
        if(parseInt(currentVentComp.strValue) === parseInt(highLimitO2Low)){
            console.log("changing Color  of Up arrow...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing Color  of Up arrow...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}

function onSetO2LowDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting down arrow bg O2 Low "+currentVentComp.strValue)
    if((objectName === "O2LowObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(O2LowMin) ){
            console.log(" changing select color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log(" changing touch color for down arrow...........")

            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
