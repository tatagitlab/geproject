var screensAndUnitsComponent;
var screensAndUnitsObj;

var firstDropdownLabel = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenFirstDropdownLabel")
var secondDropdownLabel = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenSecondDropdownLabel")
var thirdDropdownLabel = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenThirdDropdownLabel")
var screenLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenLeftMargin")
var screenSubTitleText = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenSubTitleText")
var screenSubTitlePixelSize = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenSubTitlePixelSize")
var screenSubTitleTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenSubTitleTopMargin")
var screenSubTitleLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreenSubTitleLeftMargin")
var screensDropdownWidth = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreensDropdownWidth")
var screensDropdownHeight = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreensDropdownHeight")
var screenCloumnSpacing = component.getComponentXmlDataByTag("Component","DefaultSetup","ScreensColumnSpacing")
var pawUnitsRectTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","PawUnitsRectTopMargin")
var pawUnitsObjTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","PawUnitsObjTopMargin")
var pawUnitsObjLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","PawUnitsObjLeftMargin")
var volumeRectTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VolumeRectTopMargin")
var volumeObjTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VolumeObjTopMargin")
var volumeObjLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VolumeObjLeftMargin")
var spiroRectTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","SpiroRectTopMargin")
var spiroObjTopMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","SpiroObjTopMargin")
var spiroObjLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","SpiroObjLeftMargin")
var volumeRectLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","VolumeRectLeftMargin")
var pawUnitsRectLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","PawUnitsRectLeftMargin")
var spiroRectLeftMargin = component.getComponentXmlDataByTag("Component","DefaultSetup","SpiroRectLeftMargin")
var pawUnitsValue1 = component.getComponentResourceString("Component","DefaultSetup","Label","Paw_Units")
var pawUnitsValue2 = component.getComponentResourceString("Component","DefaultSetup","Label","Paw_Units1")
var volumeWaveformValue1 = component.getComponentResourceString("Component","DefaultSetup","Label","volume_waveform")
var volumeWaveformValue2 = component.getComponentResourceString("Component","DefaultSetup","Label","volume_waveform1")
var spiroLoopValue1 = component.getComponentResourceString("Component","DefaultSetup","Label","spiro_loop")
var spiroLoopValue2 = component.getComponentResourceString("Component","DefaultSetup","Label","spiro_loop2")

var arrPawUnits = [pawUnitsValue1, pawUnitsValue2]
var arrVolumeWaveform = [volumeWaveformValue1, volumeWaveformValue2]
var arrSpiroLoop = [spiroLoopValue1, spiroLoopValue1]

//Function for initiating ScreensAndUnits
//return value is used for Checking constraint on component creation
function createScreensAndUnitsComponent() {
    console.log("func entered");
    screensAndUnitsComponent = Qt.createComponent("qrc:/Source/DefaultSetup/Qml/ScreensAndUnits.qml");
    if (screensAndUnitsComponent.status === Component.Ready)
        finishCreation();
    else
        screensAndUnitsComponent.statusChanged.connect(finishCreation);
    return 2;
}

// Function for creating ScreendAndUnits
function finishCreation() {
    if (screensAndUnitsComponent.status === Component.Ready) {
        screensAndUnitsObj = screensAndUnitsComponent.createObject(rightrect, {"strFirstDropdownLabel":firstDropdownLabel,
                                                                       "objectName": "ScreensAndUnitsObj",
                                                                                "strSecondDropdownLabel":secondDropdownLabel,
                                                                                "strThirdDropdownLabel":thirdDropdownLabel,
                                                                                "iScreenLeftMargin":screenLeftMargin,
                                                                                "strScreenSubTitleText":screenSubTitleText,
                                                                                "iScreenSubTitlePixelSize":screenSubTitlePixelSize,
                                                                                "iScreenSubTitleTopMargin":screenSubTitleTopMargin,
                                                                                "iScreenSubTitleLeftMargin":screenSubTitleLeftMargin,
                                                                                "iScreensDropdownWidth":screensDropdownWidth,
                                                                                "iScreensDropdownHeight":screensDropdownHeight,
                                                                                "iPawUnitsRectTopMargin":pawUnitsRectTopMargin,
                                                                                "iPawUnitsObjTopMargin":pawUnitsObjTopMargin,
                                                                                "iPawUnitsObjLeftMargin":pawUnitsObjLeftMargin,
                                                                                "iVolumeRectTopMargin":volumeRectTopMargin,
                                                                                "iVolumeObjTopMargin":volumeObjTopMargin,
                                                                                "iVolumeObjLeftMargin":volumeObjLeftMargin,
                                                                                "iSpiroRectTopMargin": spiroRectTopMargin,
                                                                                "iSpiroObjTopMargin": spiroObjTopMargin,
                                                                                "iSpiroObjLeftMargin": spiroObjLeftMargin,
                                                                                 "iVolumeRectLeftMargin":volumeRectLeftMargin,
                                                                                "iPawUnitsRectLeftMargin":pawUnitsRectLeftMargin,
                                                                                "iSpiroRectLeftMargin":spiroRectLeftMargin,
                                                                                "iScreensColumnSpacing":screenCloumnSpacing,
                                                                                "arrPawUnits":arrPawUnits,
                                                                                "arrVolumeWaveform":arrVolumeWaveform,
                                                                                "arrSpiroType":arrSpiroLoop

                                                                   });
       // screensAndUnitsObj.getValue(dropdownValues);
        if (screensAndUnitsObj === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (screensAndUnitsComponent.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", screensAndUnitsComponent.errorString());
    }
}

// Function for destroying ScreensAndUnits
function hideComponent()
{
    screensAndUnitsObj.destroy();
}

function changeStateIndropdownBtnOnTouch(){
//    var screensAndUnitsObj = getCompRef(rightrect, "ScreensAndUnitsObj")
    var pawUnitsRef = getCompRef(screenUnitsColumn,"PawUnitsObj");
    var volumeRef = getCompRef(screenUnitsColumn,"VolumeWaveformObj");
    var spiroRef = getCompRef(screenUnitsColumn,"SpiroTypeObj");
    var PawDropdownRef = getCompRef(pawUnitsRef,"combobox");
     var VolumeDropdownRef = getCompRef(volumeRef,"combobox");
     var SpiroDropdownRef = getCompRef(spiroRef,"combobox");
//    if(screensAndUnitsObj.children[1].children[0].children[1].state==="dropDown")
//    {
//        console.log("inside 1st");
//        screensAndUnitsObj.children[1].children[1].children[1].state="";
//         screensAndUnitsObj.children[1].children[2].children[1].state="";
//    }
//    if(screensAndUnitsObj.children[1].children[1].children[1].state==="dropDown")
//    {
//        console.log("inside 2nd");
//        screensAndUnitsObj.children[1].children[0].children[1].state="";
//        screensAndUnitsObj.children[1].children[2].children[1].state="";
//    }
//    if(screensAndUnitsObj.children[1].children[2].children[1].state==="dropDown")
//    {
//        console.log("inside 3rd");
//        screensAndUnitsObj.children[1].children[1].children[1].state="";
//        screensAndUnitsObj.children[1].children[0].children[1].state="";
//    }
    if(PawDropdownRef.state === "dropDown")
    {
        VolumeDropdownRef.state = "noDropdown";
        SpiroDropdownRef.state = "noDropdown";
    }
    if(VolumeDropdownRef.state === "dropDown")
    {
        PawDropdownRef.state = "noDropdown";
        SpiroDropdownRef.state = "noDropdown";
    }
    if(SpiroDropdownRef.state === "dropDown")
    {
        VolumeDropdownRef.state = "noDropdown";
        PawDropdownRef.state = "noDropdown";
    }
}
