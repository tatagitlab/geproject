var pPeakHighRangeList;
var pPeakHighMin;
var pPeakHighMax;
var pPeakHighStepSize;
pPeakHighRangeList = physicalparam.getRangeData("Ppeak_High", "cmH2O")
pPeakHighMin = pPeakHighRangeList[0]
pPeakHighMax = pPeakHighRangeList[1]
pPeakHighStepSize = pPeakHighRangeList[2]


function onPpeakHighSelection() {
    pPeakHighMin = ObjCCValue.getFinalPmaxMIN()
    pPeakHighMax = ObjCCValue.getFinalPmaxMAX()
}

/*Increment step Logic for Ppeak High*/
function clkHandlePpeakHigh(m_ppeakHigh) {
    var pPeakLowValue = parseInt(settingReader.read("Ppeak_Low"))
    if(pPeakLowValue < parseInt(pPeakHighMin)){
        pPeakLowValue = parseInt(pPeakHighMin)
    }
    if((m_ppeakHigh >= pPeakLowValue) && (m_ppeakHigh < parseInt(pPeakHighMax))) {
        m_ppeakHigh += parseInt(pPeakHighStepSize) ;
    }
    return m_ppeakHigh;
}

/*Decrement step Logic for Ppeak High*/
function antClkHandlePpeakHigh(m_ppeakHigh){
    m_ppeakHigh -= parseInt(pPeakHighStepSize);
    return m_ppeakHigh;
}

function onPpeakHighIncrementValue(objectName) {
    console.log("onPpeakHighIncrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log(currentAlarmSetupComp+"................")
    if(objectName === "PpeakHighObj"){
        if(parseInt(currentAlarmSetupComp.strValue) < parseInt(pPeakHighMax)){
 
            currentAlarmSetupComp.strValue =parseInt(clkHandlePpeakHigh(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}


function onPpeakHighDecrementValue(objectName){
    console.log("onPpeakHighDecrementValue calling.............")
    var currentAlarmSetupComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log(currentAlarmSetupComp+"................")
    if(objectName === "PpeakHighObj"){
        var pPeakLowValue = parseInt(settingReader.read("Ppeak_Low"))
        if (pPeakLowValue < parseInt(pPeakHighMin)) {
            pPeakLowValue = parseInt(pPeakHighMin)
        } else {
            pPeakLowValue = parseInt(pPeakLowValue) +parseInt(pPeakHighStepSize)
        }
        if(parseInt(currentAlarmSetupComp.strValue) > pPeakLowValue){
          
            currentAlarmSetupComp.strValue =parseInt(antClkHandlePpeakHigh(parseInt(currentAlarmSetupComp.strValue)))
            changeStateInAlarmBtn(currentAlarmSetupComp,"Selected")
        } else {
            changeStateInAlarmBtn(currentAlarmSetupComp,"End of Scale")
        }
    }
}


function onPpeakHighEndOfScale(objectName){
    console.log("checking end of scale PpeakHigh")
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    if((objectName === "PpeakHighObj" ))
    {var pPeakLowValue = parseInt(settingReader.read("Ppeak_Low"))
        if (pPeakLowValue < parseInt(pPeakHighMin)) {
            pPeakLowValue = parseInt(pPeakHighMin)
        } else {
            pPeakLowValue = parseInt(pPeakLowValue) + parseInt(pPeakHighStepSize)

        }
        if(parseInt(currentVentComp.strValue) === pPeakLowValue) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownDisabledIcon
        } else if(parseInt(currentVentComp.strValue) === pPeakHighMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpDisabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage=currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        } else if(parseInt(currentVentComp.strValue) !== pPeakLowValue && parseInt(currentVentComp.strValue) !== pPeakHighMax) {
            currentVentComp.strAlarmKeySpinnerUpArrowImage=currentVentComp.strAlarmSetupSpinnerUpEnabledIcon
            currentVentComp.strAlarmKeySpinnerDownArrowImage = currentVentComp.strAlarmSetupSpinnerDownEnabledIcon
        }
    }
}

function onSetPpeakHighUpArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting up arrow bg PpeakLow"+currentVentComp)
    if((objectName === "PpeakHighObj" )){
        if(parseInt(currentVentComp.strValue) === parseInt(pPeakHighMax)){
            console.log("changing Color  of Up arrow to select...........")
            currentVentComp.strSelectedStateUpAlarmArrowColor = currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("changing Color  of Up arrow to touch....")
            currentVentComp.strSelectedStateUpAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}


function onSetPpeakHighDownArrowBgColor(objectName){
    var currentVentComp = getCompRef(defaultAlarmSetupMenu, objectName)
    console.log("setting down arrow bg PpeakHigh "+currentVentComp.strValue)
    if((objectName === "PpeakHighObj" )){
        var pPeakLowValue = parseInt(settingReader.read("Ppeak_Low"))
        if (pPeakLowValue < parseInt(pPeakHighMin)) {
            pPeakLowValue = parseInt(pPeakHighMin)
        } else {
            pPeakLowValue = parseInt(pPeakLowValue) + parseInt(pPeakHighStepSize)
        }
        if(parseInt(currentVentComp.strValue) === parseInt(pPeakLowValue) ){
            console.log("inside changing select color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconSelStateColor
        } else {
            console.log("inside changing touch color for down arrow...........")
            currentVentComp.strSelectedStateDownAlarmArrowColor =  currentVentComp.strSpinnerIconTchStateColor
        }
    }
}
