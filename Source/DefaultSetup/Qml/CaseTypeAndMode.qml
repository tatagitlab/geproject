import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.VirtualKeyboard 2.1
import "qrc:/../Source/DefaultSetup/JavaScript/DropdownwithLabelController.js" as LabelDropBox
import "qrc:/../Source/DefaultSetup/JavaScript/CaseTypeAndModeController.js" as CaseTypeAndMode
Rectangle{
    id: caseTypeAndModeRect

    property int iSubTitleTopMargin
    property int iSubTitleLeftMargin
    property string iSubTitleText
    property string iSubTitleFontFamily: geFont.geFontStyle()
    property int iSubTitlePixelSize
    property string iTextFieldLabel
    property string strFirstDropdownLabel
    property string strSecondDropdownLabel
    property int iLeftmargin
    property int iTextFieldLabelTopMargin
    property string strTextFieldBorderColor
    property int iTextFieldWidth
    property int iTextFieldHeight
    property int iTextLabelRectWidth
    property int iTextLabelRectHeight
    property int iTextFieldLeftMargin
    property int iDropdownWidth
    property int iDropdownHeight
    property int iDropdownRectTopMargin
    property int iDropdownObjTopMargin
    property int ipatienttypeLeftMargin
    property int iventilatortypeLeftMargin
    property int iLabelFontSize
    property string strCheckboxLabel
    property int iCheckboxLeftMargin
    property int iCheckBoxTopMargin
    property var arrPatientType: []
    property var arrVentilationType: []
    property var columnID: caseTypeColumn
    property var patientTypeRef
    property var ventilationTypeRef
    property int iColumnSpacing
    property int iColumnZvalue
    property int iTextEntryFieldWidth
    property int iTextEntryFieldHeight
    property string strComboBoxBorderFocusColor
    property int iCheckBoxTextFontSize
    property int iCheckBoxTextLeftMargin
    property int iCheckBoxZvalue
    property bool keyNavigationCheck: false

    //*******************************************
    // Purpose:To give focus to full page when dropdown list elements are selected.
    // When dropdown elements are selected the page will gets out of focus.
    // This function will set back the focus.
    // Input Parameters:
    // Description and Use of each parameter:
    // Output Parameters:
    // Description and Use of each Parameter:
    // Return:
    // Description of the return value and type:
    //***********************************************/
    function settingCaseTypePageFocus()
    {
        caseTypeAndModeRect.focus = true;
        caseTypeAndModeRect.forceActiveFocus();
    }

    width: parent.width
    height: parent.height

    MouseArea{
        anchors.fill: parent
        onClicked: {
            keyNavigationCheck = false;
            var ventilationcomboRef = getCompRef(ventilationTypeRef,"combobox");
            ventilationcomboRef.state = "noDropdown";
            caseTypeAndModeRect.focus = true;
            caseTypeAndModeRect.forceActiveFocus();
        }
    }

    Rectangle{
        id:subtitle
        objectName: "subtitle"
        width: caseTypeAndModeRectTitle.width
        height: caseTypeAndModeRectTitle.height
        anchors {
            left: parent.left
            top: parent.top
            topMargin: iSubTitleTopMargin
            leftMargin: iSubTitleLeftMargin
        }

        Text {
            id: caseTypeAndModeRectTitle
            text: qsTr(iSubTitleText)
            font {
                family: iSubTitleFontFamily
                pixelSize: iSubTitlePixelSize
            }
        }

    }

    Column {
        id: caseTypeColumn
        spacing: iColumnSpacing
        z:iColumnZvalue
        anchors {
            left: parent.left
            top: subtitle.bottom
            topMargin: iSubTitleTopMargin
            leftMargin: iLeftmargin
        }

        Rectangle {
            id: textentryfield
            width: iTextEntryFieldWidth
            height: iTextEntryFieldHeight
            Rectangle {
                id: textrect
                width: iTextLabelRectWidth
                height: iTextLabelRectHeight
                anchors.left: parent.left
                Text {
                    id: textlabel
                    text: qsTr(iTextFieldLabel)
                    font {
                        family: iSubTitleFontFamily
                        pixelSize: iLabelFontSize
                    }
                    anchors {
                        right: textrect.right
                        verticalCenter: textrect.verticalCenter
                    }
                }
            }


            Rectangle {
                id:texteditrect
                width: texteditfield.width
                height: texteditfield.height
                border.color: strTextFieldBorderColor
                anchors {
                    left: textrect.right
                    leftMargin: iTextFieldLeftMargin
                }
                TextEdit {
                    id: texteditfield
                    width: iTextFieldWidth
                    height: iTextFieldHeight
                    font {
                        family: iSubTitleFontFamily
                        pixelSize: iLabelFontSize
                    }
                    wrapMode: TextEdit.WordWrap
                    anchors.fill: texteditrect
                    inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhPreferLowercase | Qt.ImhSensitiveData | Qt.ImhNoPredictiveText


                    Keys.onReturnPressed: {
                        ventilationTypeRef.children[1].children[0].focus = true;
                        ventilationTypeRef.children[1].children[0].forceActiveFocus();

                    }
                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        var ventilationcomboRef = getCompRef(ventilationTypeRef,"combobox");
                        ventilationcomboRef.state = "noDropdown";
                        texteditfield.focus = true;
                        texteditfield.forceActiveFocus();

                    }
                }

            }
        }
    }

    Rectangle {
        id: chkboxrect
        width: 150
        height: chkbox.height
        anchors {
            left: parent.left
            top: caseTypeColumn.bottom
            topMargin: iCheckBoxTopMargin
            leftMargin: iCheckboxLeftMargin
        }

        CheckBox {
            id: chkbox
            checked: false
            z:iCheckBoxZvalue
            Text {
                id: checkboxtext
                text: qsTr(strCheckboxLabel)
                font.family: iSubTitleFontFamily
                font.pixelSize: iCheckBoxTextFontSize
                anchors {
                    left: chkbox.right
                    verticalCenter: chkbox.verticalCenter
                    horizontalCenter: chkbox.horizontalCenter
                    leftMargin: iCheckBoxTextLeftMargin
                }
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                chkbox.checked= chkbox.checked?false:true;
            }
        }
    }

    InputPanel {
        id: inputPanel
        z: 89
        width:parent.width - 20
        height:parent.height/2
        visible:texteditfield.focus?true:false
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom:parent.bottom
            margins: 10
        }

    }


    Component.onCompleted: {
        LabelDropBox.createDropdownwithLabelComponent(arrVentilationType,strSecondDropdownLabel,"caseTypeAndModeRect","VentilationDropdownObj",40);
        ventilationTypeRef = getCompRef(caseTypeAndModeRect.columnID, "VentilationDropdownObj");
        caseTypeAndModeRect.focus = true;
        caseTypeAndModeRect.forceActiveFocus();

    }
    Keys.onRightPressed: {
        if(caseTypeAndModeRect.focus === true)
        {
            texteditfield.focus = true;
            texteditfield.forceActiveFocus();
            keyNavigationCheck = true;
        }

        else if(texteditfield.focus === true)
        {
            ventilationTypeRef.children[1].children[0].focus = true;
            ventilationTypeRef.children[1].children[0].forceActiveFocus();
        }
        else if(ventilationTypeRef.children[1].children[0].focus === true)
        {
            texteditfield.focus = true;
            texteditfield.forceActiveFocus();
            keyNavigationCheck = true;

        }

    }

    Keys.onLeftPressed: {
        if(caseTypeAndModeRect.focus === true)
        {
            texteditfield.focus = true;
            texteditfield.forceActiveFocus();
            keyNavigationCheck = true;
        }

        else if(ventilationTypeRef.children[1].children[0].focus === true)
        {
            texteditfield.focus = true;
            texteditfield.forceActiveFocus();
            keyNavigationCheck = true;
        }

        else if(texteditfield.focus===true){
            ventilationTypeRef.children[1].children[0].focus = true;
            ventilationTypeRef.children[1].children[0].forceActiveFocus();
        }
    }

    states: [
        State{
            name: "Scroll"
            when: texteditfield.activeFocus && keyNavigationCheck === true
            PropertyChanges {
                target: texteditrect;
                border.color: strComboBoxBorderFocusColor ;
            }
        }
    ]

}



