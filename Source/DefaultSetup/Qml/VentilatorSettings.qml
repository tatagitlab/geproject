import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import "qrc:/../Source/DefaultSetup/JavaScript/VentilatorSettingsController.js" as VentCtrl
Rectangle{
    id:ventSettings
    property int iSubTitleHeight
    property int iSubTitleTopmargin
    property int iSubTitleLeftmargin
    property string strSubTitleText
    property string iSubTitleFontFamily: geFont.geFontStyle()
    property int iSubTitleTextSize
    property int iProgressBarBtnWidth
    property int iProgressBarBtnHeight
    property string strProgressBarPressedColor
    property string strProgressBarColor
    property string strProgressBarBorderColor
    property int iProgressbarBorderWidth
    property int iGridViewWidth
    property int iGridViewHeight
    property int iCellWidth
    property int iCellHeight
    property int iHeaderWidth
    property int iHeaderHeight
    property string strHeaderColor
    property int iLabelViewWidth
    property int iLabelViewHeight
    property int iLabelViewYvalue
    property int iLabelRectWidth
    property int ilabelRectHeight
    property int iLabelRectTopMargin
    property int iLabelRectRightMargin
    property string strBorderFocusColor
    property string strBorderColor
    property int iSpinnerWidth
    property int iSpinnerHeight
    property int iSpinnerBorderWidth

    property string strDownArrowBtnEnabled
    property string strDownArrowBtnTouched
    property string strDownArrowBtnDisabled
    property string strUpArrowBtnEnabled
    property string strUpArrowBtnTouched
    property string strUpArrowBtnDisabled

    width: parent.width
    height: parent.height
    anchors.left: parent.left
    anchors.top: parent.top

    signal closeSpinner

    MouseArea {
        anchors.fill: parent
        onClicked: {
            ventSettings.focus = true;
            ventSettings.forceActiveFocus();
            mainGrid.currentIndex = -1;
        }
    }

    Rectangle {
        id:ventSubtitle
        width: ventRect.width
        height: iSubTitleHeight
        anchors {
            left: parent.left
            top: parent.top
            topMargin: iSubTitleTopmargin
            leftMargin: iSubTitleLeftmargin
        }
        Text {
            id: ventSettingsTitle
            text: qsTr(strSubTitleText)
            font {
                family: iSubTitleFontFamily
                pixelSize: iSubTitleTextSize
            }
        }
    }

    Rectangle {
        id:ventRect
        width: parent.width - iSubTitleLeftmargin
        height: parent.height - ventSubtitle.height - iSubTitleTopmargin
        visible: true
        anchors {
            top:ventSubtitle.bottom
            left:ventSubtitle.left
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                ventSettings.focus = true;
                ventSettings.forceActiveFocus();
                mainGrid.currentIndex = -1;
            }
        }

        ListModel{
            id:vent
            ListElement{label1:"TV";label2:"ml"}
            ListElement{label1:"RR";label2:"I/min"}
            ListElement{label1:"I:E";label2:"ratio"}
            ListElement{label1:"Pinsp";label2:"cmH2O"}
            ListElement{label1:"Pmax";label2:"cmH2O"}
            ListElement{label1:"PEEP";label2:"cmH2O"}
            ListElement{label1:"Psupp";label2:"cmH2O"}
            ListElement{label1:"Tpause";label2:"%"}
            ListElement{label1:"Inspiration time";label2:"s"}
            ListElement{label1:"Trigger Window";label2:"%"}
            ListElement{label1:"Flow Trigger";label2:"I/min"}
            ListElement{label1:"End of Breath";label2:"%"}
            ListElement{label1:"Backup Time";label2:"s"}
            ListElement{label1:"Exit Backup,spont";label2:"breaths"}
        }
        ListModel{
            id:vent1
            ListElement{box1:"VCV"}
            ListElement{box1:"PCV"}
            ListElement{box1:"SIMV VCV"}
            ListElement{box1:"SIMV PCV"}
            ListElement{box1:"PSV Pro"}
        }
        ListModel{
            id:vent2
            ListElement{box2:"1";boxColor:"lightgray"}
            ListElement{box2:"2";boxColor:"lightgray"}
            ListElement{box2:"3";boxColor:"lightgray"}
            ListElement{box2:"4";boxColor:"lightgray"}
            ListElement{box2:"5";boxColor:"lightgray"}
            ListElement{box2:"6";boxColor:"white"}
            ListElement{box2:"7";boxColor:"white"}
            ListElement{box2:"8";boxColor:"white"}
            ListElement{box2:"9";boxColor:"white"}
            ListElement{box2:"10";boxColor:"white"}
            ListElement{box2:"11";boxColor:"lightgray"}
            ListElement{box2:"12";boxColor:"lightgray"}
            ListElement{box2:"13";boxColor:"lightgray"}
            ListElement{box2:"14";boxColor:"lightgray"}
            ListElement{box2:"15";boxColor:"lightgray"}
            ListElement{box2:"16";boxColor:"white"}
            ListElement{box2:"17";boxColor:"white"}
            ListElement{box2:"18";boxColor:"white"}
            ListElement{box2:"19";boxColor:"white"}
            ListElement{box2:"20";boxColor:"white"}
            ListElement{box2:"21";boxColor:"lightgray"}
            ListElement{box2:"22";boxColor:"lightgray"}
            ListElement{box2:"23";boxColor:"lightgray"}
            ListElement{box2:"24";boxColor:"lightgray"}
            ListElement{box2:"25";boxColor:"lightgray"}
            ListElement{box2:"26";boxColor:"white"}
            ListElement{box2:"27";boxColor:"white"}
            ListElement{box2:"28";boxColor:"white"}
            ListElement{box2:"29";boxColor:"white"}
            ListElement{box2:"30";boxColor:"white"}
            ListElement{box2:"31";boxColor:"lightgray"}
            ListElement{box2:"32";boxColor:"lightgray"}
            ListElement{box2:"33";boxColor:"lightgray"}
            ListElement{box2:"34";boxColor:"lightgray"}
            ListElement{box2:"35";boxColor:"lightgray"}
            ListElement{box2:"36";boxColor:"white"}
            ListElement{box2:"37";boxColor:"white"}
            ListElement{box2:"38";boxColor:"white"}
            ListElement{box2:"39";boxColor:"white"}
            ListElement{box2:"40";boxColor:"white"}
            ListElement{box2:"41";boxColor:"lightgray"}
            ListElement{box2:"42";boxColor:"lightgray"}
            ListElement{box2:"43";boxColor:"lightgray"}
            ListElement{box2:"44";boxColor:"lightgray"}
            ListElement{box2:"45";boxColor:"lightgray"}
            ListElement{box2:"46";boxColor:"white"}
            ListElement{box2:"47";boxColor:"white"}
            ListElement{box2:"48";boxColor:"white"}
            ListElement{box2:"49";boxColor:"white"}
            ListElement{box2:"50";boxColor:"white"}
            ListElement{box2:"51";boxColor:"lightgray"}
            ListElement{box2:"52";boxColor:"lightgray"}
            ListElement{box2:"53";boxColor:"lightgray"}
            ListElement{box2:"54";boxColor:"lightgray"}
            ListElement{box2:"55";boxColor:"lightgray"}
            ListElement{box2:"56";boxColor:"white"}
            ListElement{box2:"57";boxColor:"white"}
            ListElement{box2:"58";boxColor:"white"}
            ListElement{box2:"59";boxColor:"white"}
            ListElement{box2:"60";boxColor:"white"}
            ListElement{box2:"61";boxColor:"lightgray"}
            ListElement{box2:"62";boxColor:"lightgray"}
            ListElement{box2:"63";boxColor:"lightgray"}
            ListElement{box2:"64";boxColor:"lightgray"}
            ListElement{box2:"65";boxColor:"lightgray"}
            ListElement{box2:"66";boxColor:"white"}
            ListElement{box2:"67";boxColor:"white"}
            ListElement{box2:"68";boxColor:"white"}
            ListElement{box2:"69";boxColor:"white"}
            ListElement{box2:"70";boxColor:"white"}
        }

        Rectangle{
            id : idProgressBarDownBtn
            implicitWidth : iProgressBarBtnWidth
            implicitHeight: iProgressBarBtnHeight
            color: (scrollDownArr.pressed && vent2.rowCount() > 35) ? strProgressBarPressedColor : strProgressBarColor

            border.color: strProgressBarBorderColor
            border.width: iProgressbarBorderWidth
            x:550
            y:400-iProgressBarBtnHeight
            Image {
                id: idProgressBarDownImg
                source: strDownArrowBtnEnabled
                anchors.centerIn: parent
            }
            MouseArea {
                id: scrollDownArr
                anchors.fill: parent
                onPressed: {
                    if (vent2.rowCount() > 35) {
                        idProgressBarDownImg.source = strDownArrowBtnTouched ;
                    }
                    else {
                        idProgressBarDownImg.source =  strDownArrowBtnDisabled ;
                    }
                }
                onReleased: {
                    if (vent2.rowCount()   > 35) {
                        idProgressBarDownImg.source =  strDownArrowBtnEnabled ;
                        vbar.increase()
                    } else {
                        idProgressBarDownImg.source =  strDownArrowBtnDisabled ;
                    }
                }
            }
        }
        Rectangle {
            id : idProgressBarUpBtn
            x : 550
            implicitWidth : iProgressBarBtnWidth
            implicitHeight: iProgressBarBtnHeight
            color: (scrollUpArr.pressed && vent2.rowCount() > 35)? strProgressBarPressedColor : strProgressBarColor
            border {
                color: strProgressBarBorderColor
                width: iProgressbarBorderWidth
            }
            Image {
                id: idProgressBarUpImg
                source: strUpArrowBtnEnabled
                anchors.centerIn: parent

            }
            MouseArea {
                id: scrollUpArr
                anchors.fill: parent
                onPressed: {
                    if (vent2.rowCount() > 35) {
                        idProgressBarUpImg.source =  strUpArrowBtnTouched ;
                    }
                    else {
                        idProgressBarUpImg.source =  strUpArrowBtnDisabled ;

                    }
                }

                onReleased: {
                    idProgressBarUpImg.source = (vent2.rowCount() > 35) ? strUpArrowBtnEnabled : strUpArrowBtnDisabled ;
                    vbar.decrease();
                }
            }
        }

        GridView{
            id:mainGrid
            width: iGridViewWidth
            height: iGridViewHeight
            model:70
            clip: true
            anchors {
                left:lstView.right
                top:headerComp.bottom
            }
            cellWidth:iCellWidth
            cellHeight:iCellHeight
            flow:GridView.LeftToRight
            ScrollBar.vertical: vbar
            delegate: compe
            focus: true
        }
        ListView{
            id:headerComp
            width:iHeaderWidth
            height: iHeaderHeight
            spacing: -1
            model:vent1
            anchors.left:lstView.right
            orientation: ListView.Horizontal
            delegate: Box{
                objectName: box1
                txtValue: box1
                colorOfBox: strHeaderColor
            }
        }
        ListView{
            id:lstView
            width: iLabelViewWidth
            height: iLabelViewHeight
            y:iLabelViewYvalue
            clip: true
            model:vent
            delegate:label
            ScrollBar.vertical: vbar
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    ventSettings.focus = true;
                    ventSettings.forceActiveFocus();
                    mainGrid.currentIndex = -1;
                }
            }
        }

        Component{
            id:label
            Rectangle {
                id:rect
                width:iLabelRectWidth
                height: ilabelRectHeight
                Text {
                    id: heading
                    text:label1
                    anchors {
                        right: parent.right
                        top:parent.top
                        topMargin: iLabelRectTopMargin
                        rightMargin: iLabelRectRightMargin
                    }
                }
                Text {
                    id: units
                    text:label2
                    anchors {
                        top:heading.bottom
                        right: parent.right
                        rightMargin: iLabelRectRightMargin
                    }
                }
                Component.onCompleted: {
                    rect.objectName=label1
                }
            }
        }

        Component {
            id:compe
            Box {
                id:boxComp
                border.color: GridView.isCurrentItem ? strBorderFocusColor : strBorderColor
                objectName: "ventBox"
                state: "Touched"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        mainGrid.currentIndex=index;
                        boxComp.forceActiveFocus();
                        getDelegateInstanceAt(index);
                        if(index>=0 && index<5) {
                            boxComp.state="FirstRow"
                        }
                        else if(index>=65 && index<70) {
                            boxComp.state="LastRow"
                        }

                        else {
                            boxComp.state="Selected"
                        }
                    }
                }
                Keys.onReturnPressed: {
                    mainGrid.currentIndex=index
                    getDelegateInstanceAt(index);
                    if(index>=0 && index<5) {
                        boxComp.state="FirstRow"
                    }
                    else if(index>=65 && index<70) {
                        boxComp.state="LastRow"
                    }
                    else {
                        boxComp.state="Selected"
                    }
                }

                function getDelegateInstanceAt(index) {
                    for(var i = 0; i < vent2.rowCount(); i++) {
                        var item = mainGrid.contentItem.children[i];
                        item.state="Touched";
                    }
                }
                Keys.onUpPressed: {
                    console.log("do nothing")
                }
                Keys.onDownPressed: {
                    console.log("do nothing")
                }
                Keys.onLeftPressed: {
                    if(boxComp.state === "ActiveMouse")
                    {
                        mainGrid.currentIndex = index;
                        boxComp.state = "Active";
                    }

                    else if(mainGrid.currentIndex===0){
                        mainGrid.currentIndex=vent2.rowCount()-1
                    }
                    else{
                        mainGrid.currentIndex=index-1
                    }
                }
                Keys.onRightPressed: {
                    if(boxComp.state === "ActiveMouse") {
                        mainGrid.currentIndex = index;
                        boxComp.state = "Active";
                    }
                    else if(mainGrid.currentIndex===vent2.rowCount()-1) {
                        mainGrid.currentIndex=0
                    }
                    else {
                        mainGrid.currentIndex=index+1

                    }
                }

                Rectangle{
                    id:mainSpinnerRect
                    visible:false
                    objectName: "spinnerObj"
                    width:iSpinnerWidth
                    height:iSpinnerHeight
                    anchors.left: parent.left
                    clip: false
                    border {
                        color: focus?strBorderFocusColor:strBorderColor
                        width:iSpinnerBorderWidth
                    }

                    Rectangle {
                        id:left
                        width:parent.width/3 * 2
                        height:parent.height
                        border {
                            color: focus?strBorderFocusColor:strBorderColor
                            width:iSpinnerBorderWidth
                        }
                        clip: false
                        Text {
                            anchors.centerIn: parent
                            text :boxComp.txtValue
                        }

                        Keys.onReturnPressed: {
                            if(boxComp.state==="Selected" ||boxComp.state==="FirstRow" ||boxComp.state==="LastRow"  ){
                                boxComp.state="Active"
                            }
                            else{
                                boxComp.state="Selected"
                            }
                        }
                        MouseArea {
                            anchors.fill:parent
                            onClicked: {
                                if(boxComp.state==="Selected" ||boxComp.state==="FirstRow" ||boxComp.state==="LastRow"  ){
                                    boxComp.state="ActiveMouse";
                                    boxComp.forceActiveFocus();
                                }
                                else{
                                    boxComp.state="Selected";
                                }
                            }
                        }
                    }
                    Rectangle {
                        id:arrowBox
                        width:parent.width/3
                        height: parent.height
                        anchors {
                            top:parent.top
                            right:parent.right
                        }
                        Rectangle {
                            id:top
                            width:parent.width
                            height:parent.height/2
                            border {
                                color: arrowBox.focus?strBorderFocusColor:strBorderColor
                                width:iSpinnerBorderWidth
                            }
                            anchors {
                                top: parent.top
                                left: parent.left
                            }
                            clip: false
                            Image {
                                id:up
                                width:parent.width-25
                                height:parent.height-28
                                anchors.centerIn: parent
                                source: strUpArrowBtnDisabled
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    var num = parseInt(boxComp.txtValue, 10);
                                    boxComp.txtValue=num+1
                                    arrowBox.focus=true;
                                    arrowBox.forceActiveFocus();
                                    arrowBox.z=50
                                    left.z=50
                                    top.z=50
                                    bottom.z=50
                                }
                            }

                        }
                        Rectangle {
                            id:bottom
                            width:parent.width
                            height:parent.height/2
                            border {
                                color: arrowBox.focus?strBorderFocusColor:strBorderColor
                                width:iSpinnerBorderWidth
                            }
                            anchors {
                                bottom:parent.bottom
                                left:parent.left
                            }
                            clip: false
                            Image {
                                id:down
                                width:parent.width-25
                                height:parent.height-28
                                anchors.centerIn: parent
                                source: strDownArrowBtnDisabled
                            }
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    var num = parseInt(boxComp.txtValue, 10);
                                    boxComp.txtValue=num-1
                                    arrowBox.focus=true;
                                    arrowBox.forceActiveFocus();
                                    arrowBox.z=50
                                    left.z=50
                                    top.z=50
                                    bottom.z=50
                                }
                            }
                        }
                        Keys.onReturnPressed: {
                            if(boxComp.state==="Selected" ||boxComp.state==="FirstRow" ||boxComp.state==="LastRow"  ){
                                boxComp.state="Active"
                            }
                            else{
                                boxComp.state="Selected"
                            }
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                boxComp.state="Active"
                            }
                        }
                    }
                    Keys.onRightPressed: {
                        var num = parseInt(boxComp.txtValue, 10);
                        boxComp.txtValue=num+1;
                        arrowBox.focus=true;
                        arrowBox.forceActiveFocus();
                        arrowBox.z=50;
                        left.z=50;
                        top.z=50;
                        bottom.z=50;
                    }
                    Keys.onLeftPressed: {
                        var num = parseInt(boxComp.txtValue, 10);
                        boxComp.txtValue=num-1
                        arrowBox.focus=true;
                        arrowBox.forceActiveFocus();
                        arrowBox.z=50
                        left.z=50
                        top.z=50
                        bottom.z=50
                    }
                    Keys.onReturnPressed: {
                        boxComp.state="Active"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            boxComp.state="Active"
                        }
                    }
                }
                states:[
                    State {
                        name: "Active"
                        PropertyChanges {
                            target: mainSpinnerRect
                            visible:false
                            z:20
                        }
                        PropertyChanges {
                            target: boxComp
                            focus:true
                            z:20
                        }
                        PropertyChanges {
                            target: left
                            visible:true
                            z:20
                        }
                        PropertyChanges {
                            target: top
                            visible:true
                            z:20
                        }
                        PropertyChanges {
                            target: bottom
                            visible:true
                            z:20
                        }
                        PropertyChanges{
                            target:headerComp
                            z:20
                        }
                        PropertyChanges{
                            target:idProgressBarDownBtn
                            z:20
                        }
                        PropertyChanges{
                            target: idProgressBarUpBtn
                            z:20
                        }
                        PropertyChanges{
                            target:mainGrid
                            z:20
                        }
                        PropertyChanges {
                            target: arrowBox
                            visible:true
                            z:20
                        }
                    },
                    State {
                        name: "ActiveMouse"
                        PropertyChanges{
                            target: mainSpinnerRect
                            visible:false
                            z:20
                        }
                        PropertyChanges{
                            target: boxComp
                            border.color:"black"
                            focus:true
                            z:20
                        }
                        PropertyChanges{
                            target: left
                            visible:true
                            z:20
                        }
                        PropertyChanges{
                            target: top
                            visible:true
                            z:20
                        }
                        PropertyChanges{
                            target: bottom
                            visible:true
                            z:20
                        }
                        PropertyChanges{
                            target:headerComp
                            z:20
                        }
                        PropertyChanges{
                            target:idProgressBarDownBtn
                            z:20
                        }
                        PropertyChanges{
                            target: idProgressBarUpBtn
                            z:20
                        }
                        PropertyChanges{
                            target:mainGrid
                            z:20
                        }
                        PropertyChanges{
                            target: arrowBox
                            visible:true
                            z:20
                        }
                    },
                    State {
                        name: "Selected"
                        PropertyChanges {
                            target: mainSpinnerRect
                            visible:true
                            focus:true
                            anchors.verticalCenter: boxComp.verticalCenter
                            z:50
                        }
                        PropertyChanges {
                            target: boxComp
                            z:40
                        }
                        PropertyChanges {
                            target: left
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: arrowBox
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: top
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: bottom
                            visible:true
                            z:50
                        }
                        PropertyChanges{
                            target:headerComp
                            z:30
                        }
                        PropertyChanges{
                            target:idProgressBarDownBtn
                            z:30
                        }
                        PropertyChanges{
                            target: idProgressBarUpBtn
                            z:30
                        }
                        PropertyChanges{
                            target:mainGrid
                            z:30
                        }
                    },
                    State {
                        name: "FirstRow"
                        PropertyChanges {
                            target: boxComp
                            z:40
                        }
                        PropertyChanges {
                            target: mainSpinnerRect
                            visible:true
                            anchors.top:boxComp.top
                            focus:true
                            z:50
                        }
                        PropertyChanges {
                            target: left
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: arrowBox
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: top
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: bottom
                            visible:true
                            z:50
                        }
                        PropertyChanges{
                            target:headerComp
                            z:30
                        }
                        PropertyChanges{
                            target:idProgressBarDownBtn
                            z:30
                        }
                        PropertyChanges{
                            target: idProgressBarUpBtn
                            z:30
                        }
                        PropertyChanges{
                            target:mainGrid
                            z:30
                        }
                    },
                    State {
                        name: "LastRow"
                        PropertyChanges {
                            target: boxComp
                            z:40
                        }
                        PropertyChanges {
                            target: mainSpinnerRect
                            visible:true
                            focus:true
                            anchors.bottom:boxComp.bottom
                            z:50
                        }

                        PropertyChanges {
                            target: left
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: arrowBox
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: top
                            visible:true
                            z:50
                        }
                        PropertyChanges {
                            target: bottom
                            visible:true
                            z:50
                        }
                        PropertyChanges{
                            target:headerComp
                            z:30
                        }
                        PropertyChanges{
                            target:idProgressBarDownBtn
                            z:30
                        }
                        PropertyChanges{
                            target: idProgressBarUpBtn
                            z:30
                        }
                        PropertyChanges{
                            target:mainGrid
                            z:30
                        }
                    },
                    State {
                        name: "Touched"
                        PropertyChanges {
                            target: boxComp
                            z:30
                        }
                        PropertyChanges {
                            target: mainSpinnerRect
                            visible:false
                            z:20
                        }
                    }
                ]
                Component.onCompleted: {
                    if(index>=0 && index<5){
                        for(var i=0;i<5;i++) {
                            if(i===index) {
                                boxComp.objectName=lstView.contentItem.children[0].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="SIMV PCV" || headerComp.contentItem.children[i].objectName==="PSV Pro") {
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[0].objectName,"defaultValue");
                                }
                            }
                        }
                        boxComp.colorOfBox="lightgray"
                    }

                    if(index>=5 && index<10) {
                        for(i=0;i<5;i++){
                            if((i+5)===index) {
                                boxComp.objectName=lstView.contentItem.children[1].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                if(index>6 && index<10) {
                                    boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[1].objectName,"defaultValue")+"(spont)";
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[1].objectName,"defaultValue");
                                }

                            }
                        }
                        boxComp.colorOfBox="white"
                    }

                    if(index>=10 && index<15) {
                        for(i=0;i<5;i++){
                            if(i+10===index) {
                                boxComp.objectName=lstView.contentItem.children[2].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="SIMV VCV" || headerComp.contentItem.children[i].objectName==="SIMV PCV" || headerComp.contentItem.children[i].objectName==="PSV Pro"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[2].objectName,"defaultValue");
                                }
                            }
                        }
                        boxComp.colorOfBox="lightgray"
                    }

                    if(index>=15 && index<20) {
                        for(i=0;i<5;i++) {
                            if((i+15)===index) {
                                boxComp.objectName=lstView.contentItem.children[3].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[3].objectName,"defaultValue");                            }
                        }
                        boxComp.colorOfBox="white"
                    }

                    if(index>=20 && index<25) {
                        for(i=0;i<5;i++) {
                            if((i+20)===index) {
                                boxComp.objectName=lstView.contentItem.children[4].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[4].objectName,"defaultValue");                            }
                        }
                        boxComp.colorOfBox="lightgray"
                    }

                    if(index>=25 && index<30) {
                        for(i=0;i<5;i++){
                            if((i+25)===index) {
                                boxComp.objectName=lstView.contentItem.children[5].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[5].objectName,"defaultValue");                            }
                        }
                        boxComp.colorOfBox="white"
                    }

                    if(index>=30 && index<35) {
                        for(i=0;i<5;i++) {
                            if((i+30)===index){
                                boxComp.objectName=lstView.contentItem.children[6].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                boxComp.txtValue=physicalparam.getParamData(lstView.contentItem.children[6].objectName+"ort","defaultValue");                            }
                        }
                        boxComp.colorOfBox="lightgray"
                    }

                    if(index>=35 && index<40) {
                        for(i=0;i<5;i++) {
                            if((i+35)===index) {
                                boxComp.objectName=lstView.contentItem.children[7].objectName+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="SIMV PCV" || headerComp.contentItem.children[i].objectName==="PSV Pro"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData("TPause","defaultValue");
                                }
                            }

                        }
                        boxComp.colorOfBox="white"
                    }

                    if(index>=40 && index<45) {
                        for(i=0;i<5;i++) {
                            if((i+40)===index) {
                                boxComp.objectName="Tinsp"+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="VCV"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData("Tinsp","defaultValue");;
                                }
                            }

                        }
                        boxComp.colorOfBox="lightgray"
                    }

                    if(index>=45 && index<50) {
                        for(i=0;i<5;i++) {
                            if((i+45)===index) {
                                boxComp.objectName="TrigWindow"+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="VCV"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData("TrigWindow","defaultValue");;
                                }
                            }

                        }
                        boxComp.colorOfBox="white"
                    }

                    if(index>=50 && index<55) {
                        for(i=0;i<5;i++) {
                            if((i+50)===index) {
                                boxComp.objectName="FlowTrigger"+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="VCV"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=parseInt(physicalparam.getParamData("FlowTrigger","defaultValue"));
                                }
                            }

                        }
                        boxComp.colorOfBox="lightgray"
                    }

                    if(index>=55 && index<60) {
                        for(i=0;i<5;i++) {
                            if((i+55)===index) {
                                boxComp.objectName="EndOfBreath"+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="VCV"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData("EndOfBreath","defaultValue");
                                }
                            }

                        }
                        boxComp.colorOfBox="white"
                    }

                    if(index>=60 && index<65) {
                        for(i=0;i<5;i++) {
                            if((i+60)===index) {
                                boxComp.objectName="BackupTime"+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="VCV"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=1;
                                }
                            }

                        }
                        boxComp.colorOfBox="lightgray"
                    }

                    if(index>=65 && index<70) {
                        for(i=0;i<5;i++) {
                            if((i+65)===index) {
                                boxComp.objectName="ExitBackup"+"-"+headerComp.contentItem.children[i].objectName;
                                if(headerComp.contentItem.children[i].objectName==="PCV" || headerComp.contentItem.children[i].objectName==="VCV"){
                                    boxComp.txtValue="-"
                                }
                                else {
                                    boxComp.txtValue=physicalparam.getParamData("ExitBackup","defaultValue");
                                }
                            }

                        }
                        boxComp.colorOfBox="white"
                    }
                }
            }
        }

        ScrollBar {
            id: vbar
            active: true
            orientation: Qt.Vertical
            height:400-80
            interactive: true
            size:0.1
            policy: ScrollBar.AlwaysOn
            anchors.top: parent.top
            anchors.topMargin: 40
            x:548
            contentItem: Rectangle {
                id: idHandlebutton
                implicitWidth: 30
                implicitHeight: parent.height/* - (2*10)*/
                color: "gray"
                border.color: "gray"
                border.width: 1
            }
        }

        Component.onCompleted: {
            mainGrid.z=10
        }
    }
    Component.onCompleted: {
        ventSettings.focus = true;
        ventSettings.forceActiveFocus();
        mainGrid.currentIndex = -1
    }

    Keys.onRightPressed: {
        if(ventSettings.focus === true && mainGrid.currentIndex === -1)
        {
            mainGrid.currentIndex = 0;
            mainGrid.focus = true;
            mainGrid.forceActiveFocus();
        }
    }
    Keys.onLeftPressed: {
        if(ventSettings.focus === true && mainGrid.currentIndex === -1)
        {
            mainGrid.currentIndex = 0;
            mainGrid.focus = true;
            mainGrid.forceActiveFocus();
        }
    }

}
