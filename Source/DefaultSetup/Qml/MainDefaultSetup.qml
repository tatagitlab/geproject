import QtQuick 2.0
import QtQuick.Layouts 1.3

import "qrc:/../Source/DefaultSetup/JavaScript/DropdownController.js" as DropdownCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/CaseTypeAndModeController.js" as CaseTypeAndModeCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/ScreensAndUnitsController.js" as ScreensAndUnitsCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/VentilatorSettingsController.js" as VentSettingCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/DefaultAlarmSetupController.js" as AlarmCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/LogoutScreenController.js" as LogoutCtrl
import "qrc:/../Source/DefaultSetup/JavaScript/MainDefaultController.js" as MainDfltCtrl

Rectangle {
    focus:true
    id: mainrect
    z: 10
    //main rect
    property int iDefaultRectHeight
    property int iDefaultRectWidth
    property string iDefaultRectColor
    //title rect area
    property int iDefaultTitleRectHeight
    property string iTitleText
    property int iTitleTextLeftMargin
    property int iDefaultTitleFontSize
    // left rectangle area
    property int iLeftColumnWidth
    property int iLeftColumnTopMargin
    property int iLayoutPreferredHeight
    property int iDefaultMenuLeftMargin
    property string iLeftColumnFirstText
    property string iLeftColumnSecondText
    property string iLeftColumnThirdText
    property string iLeftColumnFourthText

    property var arrLeftDropdownValues: []

    property string strFontFamily: geFont.geFontStyle()

    property var dropdownRef

    property int iRightRectTopMargin
    property int iRightRectLeftMargin
    property int currentScreen
    property int iDropDownTopMargin
    property int iDropDownLeftMargin

    //Custom border
    property int iCustomBorderDefaultZero
    property int iCustomBorderDefaultOne
    property int iCustomBorderDefaultTwo
    //    property bool bCommonBorder
    property string strCustomBorderColor

    property string strRectFocusColor
    property string strRectNonFocusColor
    property string strRectSelectedColor
    property int iRectColumnSpacing
    property var leftColumnId: col

    //*******************************************
    // Purpose: Opens only one screen and closes the other screens when tabs are selected
    // Description: hides(destroys) the other screens when one screen is clicked
    //***********************************************/
    function hideScreens() {
        if(currentScreen === 1){
            CaseTypeAndModeCtrl.hideComponent();
            caseTypeAndModeColElement.color = "white"

        } else if(currentScreen === 2) {
            ScreensAndUnitsCtrl.hideComponent();
            screenAndUnitsColElement.color = "white"
        }else if(currentScreen === 3){
            VentSettingCtrl.hideVent();
            ventilatorSettingsColElement.color="white";
        }

        else if(currentScreen === 4) {
            AlarmCtrl.hideComponent();
            alarmsColElement.color = "white"
        }
    }

    width:  iDefaultRectWidth
    height: iDefaultRectHeight
    color: iDefaultRectColor

    MouseArea {
        anchors.fill: parent
        onClicked: {
            dropdownRef.state = "noDropdown";
        }
    }

    Rectangle {
        id: upperRect
        objectName: "defaultUpperRectObj"
        width: mainrect.width
        height: iDefaultTitleRectHeight
        anchors.top: mainrect.top
        Text {
            id: caseDefaultSetup
            text: qsTr(iTitleText)
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: iTitleTextLeftMargin
            font.family: strFontFamily
            font.pixelSize:iDefaultTitleFontSize
        }

        CustomBorder
        {
            commonBorder: false
            lBorderwidth: iCustomBorderDefaultZero
            rBorderwidth: iCustomBorderDefaultZero
            tBorderwidth: iCustomBorderDefaultZero
            bBorderwidth: iCustomBorderDefaultTwo
            borderColor: strCustomBorderColor
        }
        Rectangle{
            id:logoutButton
            anchors.verticalCenter: upperRect.verticalCenter
            anchors.right:upperRect.right
            anchors.rightMargin: 10
            height: 50
            width:100
            border.width:1
            Text{
                text:qsTr("Logout")
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
            }
            MouseArea{
                anchors.fill:logoutButton
                onClicked: {
                    LogoutCtrl.createLogoutObjects();
                }
            }

        }
    }

    Rectangle {
        id: leftrect
        objectName: "defaultLeftRectObj"
        width: iLeftColumnWidth
        height: mainrect.height - upperRect.height - iCustomBorderDefaultTwo
        anchors.top: upperRect.bottom
        anchors.topMargin: iLeftColumnTopMargin
        anchors.left: mainrect.left

        CustomBorder
        {
            commonBorder: false
            lBorderwidth: iCustomBorderDefaultZero
            rBorderwidth: iCustomBorderDefaultTwo
            tBorderwidth: iCustomBorderDefaultZero
            bBorderwidth: iCustomBorderDefaultTwo
            borderColor: strCustomBorderColor
        }

        ColumnLayout{
            id:col
            objectName: "defaultLeftRectColumnObj"
            spacing: iRectColumnSpacing
            Rectangle{
                id:dropdownColElement
                Layout.preferredWidth: leftrect.width
                Layout.preferredHeight: iLayoutPreferredHeight
                color: dropdownRef.children[0].focus?strRectFocusColor:strRectNonFocusColor
                border.color: strCustomBorderColor
            }


            Rectangle {
                id:caseTypeAndModeColElement
                objectName: "caseTypeColElementObj"
                Layout.preferredWidth: leftrect.width
                Layout.preferredHeight: iLayoutPreferredHeight
                border.color: strCustomBorderColor
                Text{
                    id:caseTypeAndModetxt
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: iDefaultMenuLeftMargin
                    text: qsTr(iLeftColumnFirstText)
                    font.family: strFontFamily
                    font.pixelSize:iDefaultTitleFontSize

                }
                color: focus?strRectFocusColor:strRectNonFocusColor
                MouseArea{

                    anchors.fill:parent
                    onClicked: {
                        leftrect.focus = true;
                        leftrect.forceActiveFocus();
                        if(currentScreen!==1){
                            hideScreens();
                            currentScreen = CaseTypeAndModeCtrl.createCaseTypeAndModeComponent();
                            caseTypeAndModeColElement.color = strRectSelectedColor
                        }
                    }

                }
                Keys.onReturnPressed: {
                    if(currentScreen!==1){
                        hideScreens();
                        currentScreen = CaseTypeAndModeCtrl.createCaseTypeAndModeComponent();
                        caseTypeAndModeColElement.color = strRectSelectedColor
                    }
                }
            }

            Rectangle {
                id:screenAndUnitsColElement
                objectName: "screensUnitsColElementObj"
                Layout.preferredWidth: leftrect.width
                Layout.preferredHeight: iLayoutPreferredHeight
                border.color: strCustomBorderColor
                Text{
                    id:screenAndUnitstxt
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: iDefaultMenuLeftMargin
                    text: qsTr(iLeftColumnSecondText)
                    font.family: strFontFamily
                    font.pixelSize:iDefaultTitleFontSize
                }
                color: focus?strRectFocusColor:strRectNonFocusColor
                MouseArea{

                    anchors.fill:parent
                    onClicked: {
                        leftrect.focus = true;
                        leftrect.forceActiveFocus();
                        if(currentScreen!==2){
                            hideScreens();
                            currentScreen = ScreensAndUnitsCtrl.createScreensAndUnitsComponent();
                            screenAndUnitsColElement.color = strRectSelectedColor
                        }
                    }
                }
                Keys.onReturnPressed: {
                    if(currentScreen!==2){
                        hideScreens();
                        currentScreen = ScreensAndUnitsCtrl.createScreensAndUnitsComponent();
                        screenAndUnitsColElement.color = strRectSelectedColor
                    }
                }
            }

            Rectangle {
                id:ventilatorSettingsColElement
                objectName: "ventilatorColElementObj"
                Layout.preferredWidth: leftrect.width
                Layout.preferredHeight: iLayoutPreferredHeight
                border.color: strCustomBorderColor
                Text{
                    id:ventilatorSettingstxt
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: iDefaultMenuLeftMargin
                    text:qsTr(iLeftColumnThirdText)
                    font.family: strFontFamily
                    font.pixelSize:iDefaultTitleFontSize
                }
                color: focus?strRectFocusColor:"white"
                MouseArea{

                    anchors.fill:parent
                    onClicked: {
                        leftrect.focus = true;
                        leftrect.forceActiveFocus();
                        if(currentScreen!==3){
                            hideScreens();
                            currentScreen = VentSettingCtrl.createVentObjects();
                            ventilatorSettingsColElement.color = strRectSelectedColor
                        }
                    }
                }
                Keys.onReturnPressed: {
                    if(currentScreen!==3){
                        hideScreens();
                        currentScreen = VentSettingCtrl.createVentObjects();
                        ventilatorSettingsColElement.color = strRectSelectedColor
                    }
                }
            }
            Rectangle {
                id:alarmsColElement
                objectName: "alarmColElementObj"
                Layout.preferredWidth: leftrect.width
                Layout.preferredHeight: iLayoutPreferredHeight
                border.color: strCustomBorderColor
                Text{
                    id:alarmstxt
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    anchors.leftMargin: iDefaultMenuLeftMargin
                    text:qsTr(iLeftColumnFourthText)
                    font.family: strFontFamily
                    font.pixelSize:iDefaultTitleFontSize
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        leftrect.focus = true;
                        leftrect.forceActiveFocus();
                        if(currentScreen!=4){
                            hideScreens();
                            currentScreen =AlarmCtrl.createAlarmSetupMenu();
                            alarmsColElement.color=strRectSelectedColor
                        }
                    }
                }
                Keys.onReturnPressed: {

                    if(currentScreen!=4){
                        hideScreens();
                        currentScreen =AlarmCtrl.createAlarmSetupMenu();
                        alarmsColElement.color=strRectSelectedColor
                    }
                }
                color: focus?strRectFocusColor:strRectNonFocusColor
            }

        }
        Component.onCompleted: {
            var drlist = DropdownCtrl.createDropdownComponent(arrLeftDropdownValues,"mainDefaultdropdownObj");
            DropdownCtrl.dropdownObj.anchors.top = col.top;
            DropdownCtrl.dropdownObj.anchors.topMargin = iDropDownTopMargin;
            DropdownCtrl.dropdownObj.anchors.left = col.left;
            DropdownCtrl.dropdownObj.anchors.leftMargin = iDropDownLeftMargin
            DropdownCtrl.dropdownObj.focus = true;
            DropdownCtrl.dropdownObj.forceActiveFocus();
            dropdownRef = getCompRef(col,"mainDefaultdropdownObj");
            //            console.log("dropdownRef"+dropdownRef);
            //            console.log("leftColumnId"+leftColumnId)
            leftrect.focus = true;
            leftrect.forceActiveFocus();

        }

        Keys.onRightPressed: {
            if(leftrect.focus === true)
            {
                dropdownRef.children[0].focus = true;
                dropdownRef.children[0].forceActiveFocus();
            }

            else if(dropdownRef.children[0].focus === true)
            {
                caseTypeAndModeColElement.focus = true;
                caseTypeAndModeColElement.forceActiveFocus();
            }
            else if(caseTypeAndModeColElement.focus === true)
            {
                screenAndUnitsColElement.focus = true;
                screenAndUnitsColElement.forceActiveFocus();
            }
            else if(screenAndUnitsColElement.focus === true)
            {
                ventilatorSettingsColElement.focus = true;
                ventilatorSettingsColElement.forceActiveFocus();
            }
            else if(ventilatorSettingsColElement.focus === true)
            {
                alarmsColElement.focus = true;
                alarmsColElement.forceActiveFocus();
            }
            else if(alarmsColElement.focus === true)
            {
                dropdownRef.children[0].focus = true;
                dropdownRef.children[0].forceActiveFocus();
            }
        }

        Keys.onLeftPressed: {
            if(leftrect.focus === true)
            {
                dropdownRef.children[0].focus = true;
                dropdownRef.children[0].forceActiveFocus();
            }
            else if(dropdownRef.children[0].focus === true)
            {
                alarmsColElement.focus = true;
                alarmsColElement.forceActiveFocus();
            }
            else if(alarmsColElement.focus === true){
                ventilatorSettingsColElement.focus = true;
                ventilatorSettingsColElement.forceActiveFocus();
            }
            else if(ventilatorSettingsColElement.focus === true)
            {
                screenAndUnitsColElement.focus = true;
                screenAndUnitsColElement.forceActiveFocus();
            }
            else if(screenAndUnitsColElement.focus === true)
            {
                caseTypeAndModeColElement.focus = true;
                caseTypeAndModeColElement.forceActiveFocus();
            }
            else if(caseTypeAndModeColElement.focus === true)
            {
                dropdownRef.children[0].focus = true;
                dropdownRef.children[0].forceActiveFocus();
            }
        }

    }

    Rectangle {
        id:rightrect
        width: mainrect.width - leftrect.width - iCustomBorderDefaultTwo
        height: mainrect.height - upperRect.height - iCustomBorderDefaultTwo
        anchors.top:upperRect.bottom
        anchors.left:leftrect.right
        anchors.topMargin: iCustomBorderDefaultTwo
        anchors.leftMargin: iCustomBorderDefaultTwo
    }



    Keys.onRightPressed: {
        caseTypeAndModeColElement.focus = true;
        caseTypeAndModeColElement.forceActiveFocus();
    }
    Keys.onLeftPressed: {
        caseTypeAndModeColElement.focus = true;
        caseTypeAndModeColElement.forceActiveFocus();
    }

}
