import QtQuick 2.0
import QtQuick.Controls 2.2
import QtMultimedia 5.8
import QtQuick.Layouts 1.3
import "qrc:/Source/DefaultSetup/JavaScript/DefaultAlarmSetupController.js" as AlarmSetupController

Rectangle {
    id: valueBox
    //Value Box Property
    property int iBtnValueFontSize
    property int iValueBoxBorderwidth
    property int iValueBoxHeight
    property int iValueBoxRadius
    property int iValueBoxWidth
    property string strBgColor
    property string strBtnValueFontColor
    property string strParamName
    property string strValue
    property string strValueBoxBorderColor
    property var prevValue
    //Spinner Property
    property bool bIsSpinnerTouchedDown
    property bool bIsSpinnerTouchedUp
    property int iAlarmParamScrollStateBorderWidth
    property int iAlarmParamSelectedStateBorderWidth
    property int iSelectedStateAlarmArrowHeight
    property int iSelectedStateAlarmArrowWidth
    property string strAlarmParamEndOfScaleStateColor
    property string strAlarmParamScrollStateBorderColor
    property string strAlarmParamSelectedStateColor
    property string strAlarmParamSelectedStateFontColor
    property string strAlarmParamTouchedStateColor
    property string strAlarmSetupSpinnerDownDisabledIcon
    property string strAlarmSetupSpinnerDownEnabledIcon
    property string strAlarmSetupSpinnerUpEnabledIcon
    property string strAlarmSetupSpinnerUpDisabledIcon
    property string strSpinnerIconSelStateColor
    property string strSelectedStateAlarmArrowColor
    property string strSelectedStateUpAlarmArrowColor
    property string strSelectedStateDownAlarmArrowColor
    property string uniqueObjectID
    property int iSpinnerRadius
    property int iSpinnerWidth
    property int iArrowWidth
    property int iArrowZvalue

    //Spinner Image Property
    property string strAlarmKeySpinnerUpArrowImage: strAlarmSetupSpinnerUpEnabledIcon
    property string strAlarmKeySpinnerDownArrowImage: strAlarmSetupSpinnerDownEnabledIcon

    //signals
    signal incrementValue(string objectName)
    signal decrementValue(string objectName)
    signal checkEndOfScaleForSpinnerWidget(string objectName)
    signal onSetArrowUpBgColor(string objectName)
    signal onSetArrowDownBgColor(string objectName)
    signal selection()

    function setUniqueID(uniqueName) {
        valueBox.uniqueObjectID = uniqueName
    }

    function stopAccelarationTimers(){
        bIsSpinnerTouchedUp = false ;
        bIsSpinnerTouchedDown = false ;
        valueBox.strSelectedStateUpAlarmArrowColor =strSelectedStateAlarmArrowColor
        valueBox.strSelectedStateDownAlarmArrowColor =strSelectedStateAlarmArrowColor
    }
    //*******************************************
    // Purpose: Changes color of spinner box
    // Description: When spinner box is selected the box color will change.
    // This function will set back the focus.
    //***********************************************/
    function stopTouch(){

        valueBox.color = strBgColor

    }

    //AlarmSetupBtn properties
    width: iValueBoxWidth
    height: iValueBoxHeight
    color: strBgColor
    radius: iValueBoxRadius
    border.width: iValueBoxBorderwidth
    border.color: strValueBoxBorderColor

    Text {
        id: btnValue
        text: qsTr(strValue)
        anchors.centerIn: parent
        font.pointSize: iBtnValueFontSize
        color: strBtnValueFontColor
    }

    Rectangle {
        id: spinnerRectDown
        width: iSelectedStateAlarmArrowWidth
        height: iSelectedStateAlarmArrowHeight
        color: strSelectedStateDownAlarmArrowColor
        visible: false
        anchors.right: valueBox.left
        Rectangle{
            id: radiusDownArr
            width: iArrowWidth
            height: spinnerRectDown.height
            anchors.right: spinnerRectDown.right
            color: spinnerRectDown.color
            visible: spinnerRectDown.visible
            z: iArrowZvalue
        }

        Image {
            id: spinnerRectDownIcon
            anchors.centerIn: parent
            source: strAlarmKeySpinnerDownArrowImage
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                decrementValue(valueBox.objectName)
            }
            onPressed: {
                bIsSpinnerTouchedDown= true ;
                onSetArrowDownBgColor(valueBox.objectName) ;
            }
            onReleased: {
                bIsSpinnerTouchedDown= false  ;
                valueBox.strSelectedStateDownAlarmArrowColor = strSpinnerIconSelStateColor;
            }
            onExited: {
                bIsSpinnerTouchedDown= false  ;
                valueBox.strSelectedStateDownAlarmArrowColor = strSpinnerIconSelStateColor;
            }
        }
    }

    Rectangle {
        id: spinnerRectUp
        width: iSelectedStateAlarmArrowWidth
        height: iSelectedStateAlarmArrowHeight
        color: strSelectedStateUpAlarmArrowColor
        visible: false
        anchors.left: valueBox.right
        Rectangle{
            id: radiusUpArr
            width: iArrowWidth
            height: spinnerRectUp.height
            anchors.left: spinnerRectUp.left
            color: spinnerRectUp.color
            visible: spinnerRectUp.visible
            z: iArrowZvalue
        }
        Image {
            id: spinnerRectUpIcon
            anchors.centerIn: parent
            source: strAlarmKeySpinnerUpArrowImage
        }

        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                incrementValue(valueBox.objectName)
            }

            onPressed: {
                bIsSpinnerTouchedUp = true ;
                onSetArrowUpBgColor(valueBox.objectName);
            }
            onReleased: {
                valueBox.strSelectedStateUpAlarmArrowColor = strSpinnerIconSelStateColor
                bIsSpinnerTouchedUp = false ;
//                alarmParIncrementTimer.stop() ;
            }
            onExited: {
                valueBox.strSelectedStateUpAlarmArrowColor = strSpinnerIconSelStateColor
                bIsSpinnerTouchedUp = false ;
//                alarmParIncrementTimer.stop() ;
            }
        }
    }

    Keys.onRightPressed: {
//        stopAccelarationTimers()
        if(valueBox.state === "Selected" || valueBox.state === "End of Scale"){
            incrementValue(valueBox.objectName);
        }
        else if(valueBox.state === "Touched"){
            stopTouch()
            valueBox.state = "Scroll"
            AlarmSetupController.moveClockwiseAlarmSetup(valueBox.objectName)
        }
        else {
            AlarmSetupController.moveClockwiseAlarmSetup(valueBox.objectName)
        }
    }

    Keys.onLeftPressed: {
//        stopAccelarationTimers()
        if(valueBox.state === "Selected" || valueBox.state === "End of Scale"){
            decrementValue(valueBox.objectName);
        }
        else if(valueBox.state === "Touched"){
            valueBox.state = "Scroll"
            stopTouch()
            AlarmSetupController.moveAntiClockwiseAlarmSetup(valueBox.objectName)

        }
        else {
            AlarmSetupController.moveAntiClockwiseAlarmSetup(valueBox.objectName)
        }
    }

    Keys.onReturnPressed: {
//        stopAccelarationTimers()
        if(valueBox.state === "Touched"){
            valueBox.state = "Selected"
            AlarmSetupController.onSecParamEnterPressed(valueBox.objectName)

        }

        else{
            AlarmSetupController.onSecParamEnterPressed(valueBox.objectName)
        }
    }

    MouseArea{
        anchors.fill: parent
        onPressed: {
            AlarmSetupController.onSecParamAlarmMenuTouched(valueBox.objectName)
        }
        onReleased: {

            AlarmSetupController.onSecParamAlarmMenuSelected(valueBox.objectName)


        }
    }

    states: [State{
            name: "Scroll"
            when: valueBox.activeFocus
            PropertyChanges {
                target: valueBox;
                border.color: strAlarmParamScrollStateBorderColor ;
                border.width: iAlarmParamScrollStateBorderWidth ;
                radius:iSpinnerRadius
            }
        },
        State {
            name: "ScrollWithMouse"

            PropertyChanges {
                target: valueBox;
                radius:iSpinnerRadius
            }
        },
        State{
            name: "Selected"
            PropertyChanges {
                target: valueBox;
                color: strAlarmParamSelectedStateColor ;
                border.width: iAlarmParamSelectedStateBorderWidth;
                radius:iSpinnerWidth
            }
            PropertyChanges {
                target: btnValue; color: strAlarmParamSelectedStateFontColor

            }
            PropertyChanges {
                target: spinnerRectUp
                visible: true
                border.width: iSpinnerWidth
                radius:iSpinnerRadius
            }
            PropertyChanges {
                target: spinnerRectDown
                visible: true
                border.width: iSpinnerWidth
                radius:iSpinnerRadius

            }
            StateChangeScript {
                name: "example"
                script: {selection(); checkEndOfScaleForSpinnerWidget(valueBox.objectName)}
            }
        },
        State{
            name: "Active"
            PropertyChanges {
                target: valueBox; color: "white"; radius:iSpinnerRadius
            }
        },
        State{
            name: "Touched"
            PropertyChanges {
                target: valueBox;
                color: strAlarmParamTouchedStateColor;
                radius:iSpinnerRadius
                border.width: iAlarmParamSelectedStateBorderWidth;
            }

        },
        State{
            name: "End of Scale"
            PropertyChanges {
                target: valueBox; color: strAlarmParamEndOfScaleStateColor;border.width: iSpinnerWidth; radius:iSpinnerWidth
            }
            PropertyChanges {
                target: btnValue; color: strAlarmParamSelectedStateFontColor

            }
            PropertyChanges {
                target: spinnerRectUp
                visible: true
                border.width: iSpinnerWidth
                radius: iSpinnerRadius
            }
            PropertyChanges {
                target: spinnerRectDown
                visible: true
                border.width: iSpinnerWidth
                radius: iSpinnerRadius

            }}
    ]

    onStateChanged: {
        if(state === "Selected"){
            checkEndOfScaleForSpinnerWidget(valueBox.objectName)
        }
    }

    onStrValueChanged: {
        checkEndOfScaleForSpinnerWidget(valueBox.objectName)
    }

    onBIsSpinnerTouchedUpChanged:{
        if (bIsSpinnerTouchedUp === false){
            alarmParIncrementTimer.stop() ;
        }
    }

    onBIsSpinnerTouchedDownChanged: {
        if(bIsSpinnerTouchedDown === false){
            alarmParDecrementTimer.stop() ;
        }
    }
}





