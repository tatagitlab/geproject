import QtQuick 2.0
import QtQuick.Controls 2.2

Rectangle{
    id:dropdownwithlabel

    property var dropdownValues;
    property string strDropDownLabel;
    property int iDropdownLabelHeight
    property int iDropdownLabelTopMargin
    property int iDropdownBoxWidth
    property int iDropdownBoxHeight
    property int iDropdownBoxLeftMargin
    property string strDropdownBorderColor
    property int iChosenItemTextmargin
    property string strChosenItemFontFamily: geFont.geFontStyle()
    property int iLabelFontSize
    property int iChosenItemFontSize
    property int iImageHeight
    property int iImageWidth
    property int iImageTopMargin
    property int iImageRightMargin
    property int iDropdownHeight
    property int iDropdownMargin
    property int iDropdownTextMargin
    property int iLabelBoxWidth
    property int iLabelBoxHeight
    property int iZvalue
    property int iDropDownWithLabelWidth
    property int iDropDownWithLabelHeight
    property int iTextLabelFontSize
    property string strComboBoxBorderFocusColor
    property string strComboBoxBorderColor
    property string strImageSource

    signal screensDropdownboxClicked
    signal screenPageDropdownMouseClicked
    signal casePageDropdownMouseClicked


    //*******************************************
    // Purpose: getting the dropdown values from main default page
    // InputParameter: dropdownvalues
    // Description: gets the dropdown values passed from the case type and mode or screns and units page
    // InputParameter: label
    // Description: gets the dropdown label passed from the case type and mode or screns and units page
    //***********************************************/
    function getValue(dropdownvalues, label)
    {
        dropdownValues = dropdownvalues;
        strDropDownLabel = label;
    }

    width: iDropDownWithLabelWidth
    height: iDropDownWithLabelHeight
    z: iZvalue;

    Rectangle {
        id:textlabelrect
        width: iLabelBoxWidth
        height: iLabelBoxHeight
        Text {
            id: dropdownlabel
            text: qsTr(strDropDownLabel)
            font.family: strChosenItemFontFamily
            font.pixelSize: iTextLabelFontSize
            anchors.right: textlabelrect.right
            anchors.verticalCenter: textlabelrect.verticalCenter
        }
        anchors.verticalCenter: comboBox.verticalCenter
    }

    Rectangle {
        id:comboBox
        objectName: "combobox"
        border.color: comboBox.focus?strComboBoxBorderFocusColor:strComboBoxBorderColor
        property alias selectedItem: chosenItemText.text;
        property alias selectedIndex: listView.currentIndex;

        signal comboClicked;
        width: iDropdownBoxWidth ;
        height: iDropdownBoxHeight;
        smooth:true;
        anchors.left: textlabelrect.right
        anchors.leftMargin: iDropdownBoxLeftMargin

        Rectangle {
            id:chosenItem
            width:parent.width;
            height:comboBox.height;
            border.color: focus? strComboBoxBorderFocusColor:strComboBoxBorderColor
            smooth:true;
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                id:chosenItemText
                text:qsTr(dropdownwithlabel.dropdownValues[0]);
                font.family: strChosenItemFontFamily
                font.pointSize: iChosenItemFontSize;
                smooth:true
            }

            Image {
                id: dropdownSymbol
                source: strImageSource
                width: iImageWidth
                height: iImageHeight
                anchors.right: chosenItem.right
                anchors.rightMargin: iImageRightMargin
                anchors.verticalCenter: chosenItem.verticalCenter
            }

            MouseArea {
                anchors.fill: parent;
                onClicked: {
                    comboBox.state = comboBox.state==="dropDown"?"noDropdown":"dropDown"

                    screensDropdownboxClicked();
                    listView.focus = true;
                    listView.forceActiveFocus();

                    if(comboBox.state === "noDropdown")
                    {
                        casePageDropdownMouseClicked();
                        screenPageDropdownMouseClicked();
                    }
                }
            }
            Keys.onReturnPressed: {
                comboBox.state = comboBox.state==="dropDown"?"noDropdown":"dropDown"
                screensDropdownboxClicked();
                listView.focus = true;
                listView.forceActiveFocus();
            }
        }

        Rectangle {
            id:dropDown
            width:comboBox.width;
            height:iDropdownHeight;
            clip:true;
            anchors.top: chosenItem.bottom;
            anchors.margins: iDropdownMargin;
            border.color: strDropdownBorderColor
            ListView {
                id:listView
                height: iDropdownBoxHeight*dropdownwithlabel.dropdownValues.length;
                model: dropdownwithlabel.dropdownValues
                currentIndex: 0
                delegate: dropDownDelegate
                highlight: highlight
                focus:true
                interactive: false
            }
            Component{
                id:dropDownDelegate
                Item{
                    width:comboBox.width;
                    height: comboBox.height;
                    signal itemChanged(var item)
                    Text {
                        text: qsTr(modelData)
                        font.family: strChosenItemFontFamily
                        font.pixelSize: iChosenItemFontSize
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter

                    }
                    MouseArea {
                        anchors.fill: parent;
                        onClicked: {
                            comboBox.state = "noDropdown"
                            var prevSelection = chosenItemText.text
                            chosenItemText.text = modelData
                            if(chosenItemText.text !== prevSelection){
                                comboBox.comboClicked();
                            }
                            screenPageDropdownMouseClicked();
                            casePageDropdownMouseClicked();
                        }
                    }

                    Keys.onPressed: {
                        event.accepted = true;
                        if(event.key===Qt.Key_Left){
                            if(listView.currentIndex>0){
                                listView.currentIndex=listView.currentIndex-1
                            }
                            else if(listView.currentIndex===0){
                                listView.currentIndex=dropdownwithlabel.dropdownValues.length-1
                            }
                        }
                        if(event.key===Qt.Key_Right){
                            if(dropdownwithlabel.dropdownValues.length-1>listView.currentIndex){
                                listView.currentIndex=listView.currentIndex+1
                            }
                            else if(dropdownwithlabel.dropdownValues.length-1===listView.currentIndex){
                                listView.currentIndex=0
                            }
                        }
                        if(event.key===Qt.Key_Return){
                            console.log("ënter pressed");
                            comboBox.state = "noDropdown"
                            var prevSelection = chosenItemText.text
                            chosenItemText.text = modelData
                            if(chosenItemText.text !== prevSelection){
                                comboBox.comboClicked();
                            }
                            listView.currentIndex = 0
                            chosenItem.focus = true;
                            chosenItem.forceActiveFocus();
                        }
                    }
                }
            }
        }

        Component {
            id: highlight
            Rectangle {
                width:comboBox.width
                color: strComboBoxBorderColor
            }
        }

        states:[ State {
                name: "dropDown";
                PropertyChanges { target: dropDown; height:50*dropdownwithlabel.dropdownValues.length }
            } ,
            State {
                name: "noDropdown"
                PropertyChanges {
                    target: dropDown; height: 0 }
            }]

        transitions: Transition {
            NumberAnimation { target: dropDown; properties: "height"; easing.type: Easing.OutExpo; duration: 1000 }
        }
    }
}
