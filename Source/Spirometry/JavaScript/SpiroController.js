
var spiroComp;
var spiroObj = null;

var spiroWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroWidth"))
var spiroHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroHeight"))
var spiroZvalue = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroZvalue"))
var spiroColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroColor")
var spiroBorderColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroBorderColor")
var spiroTitleText = component.getComponentXmlDataByTag("Component","Spirometry","SpiroTitleText")
var spiroTitlePixelSize = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroTitlePixelSize"))
var spiroTitleColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroTitleColor")
var spiroTitleLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroTitleLeftMargin"))
var spiroTitleTopMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroTitleTopMargin"))
var spiroCloseBtnWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseBtnWidth"))

var spiroCloseBtnHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseBtnHeight"))
var spiroCloseBtnBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseBtnBorderWidth"))
var spiroCloseRightMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseRightMargin"))
var spiroCloseTopMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseTopMargin"))
var spiroTchColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroTchColor")
var spiroCloseBtnBgColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseBtnBgColor")
var spiroStateBorderColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroStateBorderColor")
var spiroCloseBtnBorderColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseBtnBorderColor")
var spiroCloseBtnSource = component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseBtnSource")
var spiroCloseBtnTchSource = component.getComponentXmlDataByTag("Component","Spirometry","SpiroCloseBtnTchSource")
var spiroValueLabel = component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueLabel")
var spiroValueLabelPixelSize = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueLabelPixelSize"))
var spiroValueLabelColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueLabelColor")
var spiroValueLabelBottomMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueLabelBottomMargin"))
var spiroValueLabelLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueLabelLeftMargin"))
var spiroValue = component.getComponentXmlDataByTag("Component","Spirometry","SpiroValue")
var spiroValuePixelSize = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValuePixelSize"))
var spiroValueColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueColor")
var spiroValueBottomMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueBottomMargin"))
var spiroValueLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueLeftMargin"))
var spiroValueRightLabel = component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueRightLabel")
var spiroValueRightLabelPixelSize = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueRightLabelPixelSize"))
var spiroValueRightLabelColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueRightLabelColor")
var spiroValueRightLabelBottomMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueRightLabelBottomMargin"))
var spiroValueRightLabelLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroValueRightLabelLeftMargin"))
var spiroDropdownLabel = component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownLabel")
var spiroDropdowntLabelPixelSize = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdowntLabelPixelSize"))
var spiroDropdownLabelColor = component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownLabelColor")
var spiroDropdownLabelBottomMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownLabelBottomMargin"))
var spiroDropdownLabelLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroDropdownLabelLeftMargin"))
var spiroSaveBtnWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroSaveBtnWidth"))
var spiroSaveBtnHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroSaveBtnHeight"))
var saveBtnBgColor = component.getComponentXmlDataByTag("Component","Spirometry","SaveBtnBgColor")
var saveBtnBorderWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SaveBtnBorderWidth"))
var saveBtnBorderColor = component.getComponentXmlDataByTag("Component","Spirometry","SaveBtnBorderColor")
var spiroSavebtnBottomMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroSavebtnBottomMargin"))
var spiroSavebtnRightMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","SpiroSavebtnRightMargin"))
var spiroSavebtnImage = component.getComponentXmlDataByTag("Component","Spirometry","SpiroSavebtnImage")
var topDropdownLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownLeftMargin"))
var topDropdownTopMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownTopMargin"))
var bottomDropdownBottomMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownBottomMargin"))
var bottomDropdownLeftMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownLeftMargin"))
var topDropdownHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownHeight"))
var topDropdownWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownWidth"))
var bottomDropdownHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownHeight"))
var bottomDropdownWidth = component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownWidth")

var topDropdownColor = component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownColor")
var topDropdownFontSize = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownFontSize"))
var topDropdownImageWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownImageWidth"))
var topDropdownImageHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownImageHeight"))
var topDropdownImageTopMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownImageTopMargin"))
var topDropdownImageRightMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownImageRightMargin"))
var bottomDropdownColor = component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownColor")
var bottomropdownFontSize = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownFontSize"))
var bottomDropdownImageWidth = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownImageWidth"))
var bottomDropdownImageHeight = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownImageHeight"))
var bottomDropdownImageTopMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownImageTopMargin"))
var bottomDropdownImageRightMargin = parseInt(component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownImageRightMargin"))
var topDropdownBorderColor = component.getComponentXmlDataByTag("Component","Spirometry","TopDropdownBorderColor")
var bottomDropdownBorderColor = component.getComponentXmlDataByTag("Component","Spirometry","BottomDropdownBorderColor")



function createSpiroComponent() {
    console.log("Creation of spiro object")
    spiroComp = Qt.createComponent("qrc:/Source/Spirometry/Qml/SpirometryMainPage.qml");
    if (spiroComp.status === Component.Ready)
        finishCreation();
    else
        spiroComp.statusChanged.connect(finishCreation);
}

function finishCreation() {
    if (spiroComp.status === Component.Ready) {
        spiroObj = spiroComp.createObject(mainitem,{"iSpiroWidth":spiroWidth,
                                              "iSpiroHeight":spiroHeight,
                                              "iSpiroZvalue":spiroZvalue,
                                              "strSpiroColor":spiroColor,
                                              "strSpiroBorderColor":spiroBorderColor,
                                              "strSpiroTitleText":spiroTitleText,
                                              "iSpiroTitlePixelSize":spiroTitlePixelSize,
                                              "strSpiroTitleColor":spiroTitleColor,
                                              "iSpiroTitleLeftMargin":spiroTitleLeftMargin,
                                              "iSpiroTitleTopMargin":spiroTitleTopMargin,
                                              "iSpiroCloseBtnWidth":spiroCloseBtnWidth,
                                              "iSpiroCloseBtnHeight":spiroCloseBtnHeight,
                                              "iSpiroCloseBtnBorderWidth":spiroCloseBtnBorderWidth,
                                              "iSpiroCloseRightMargin":spiroCloseRightMargin,
                                              "iSpiroCloseTopMargin":spiroCloseTopMargin,
                                              "strSpiroTchColor":spiroTchColor,
                                              "strSpiroCloseBtnBgColor":spiroCloseBtnBgColor,
                                              "strSpiroStateBorderColor":spiroStateBorderColor,
                                              "strSpiroCloseBtnBorderColor":spiroCloseBtnBorderColor,
                                              "strSpiroCloseBtnSource":spiroCloseBtnSource,
                                              "strSpiroCloseBtnTchSource":spiroCloseBtnTchSource,
                                              "strSpiroValueLabel":spiroValueLabel,
                                              "iSpiroValueLabelPixelSize":spiroValueLabelPixelSize,
                                              "strSpiroValueLabelColor":spiroValueLabelColor,
                                              "iSpiroValueLabelBottomMargin":spiroValueLabelBottomMargin,
                                              "iSpiroValueLabelLeftMargin":spiroValueLabelLeftMargin,
                                              "strSpiroValue":spiroValue,
                                              "iSpiroValuePixelSize":spiroValuePixelSize,
                                              "strSpiroValueColor":spiroValueColor,
                                              "iSpiroValueBottomMargin":spiroValueBottomMargin,
                                              "iSpiroValueLeftMargin":spiroValueLeftMargin,
                                              "strSpiroValueRightLabel":spiroValueRightLabel,
                                              "iSpiroValueRightLabelPixelSize":spiroValueRightLabelPixelSize,
                                              "strSpiroValueRightLabelColor":spiroValueRightLabelColor,
                                              "iSpiroValueRightLabelBottomMargin":spiroValueRightLabelBottomMargin,
                                              "iSpiroValueRightLabelLeftMargin":spiroValueRightLabelLeftMargin,
                                              "strSpiroDropdownLabel":spiroDropdownLabel,
                                              "iSpiroDropdowntLabelPixelSize":spiroDropdowntLabelPixelSize,
                                              "strSpiroDropdownLabelColor":spiroDropdownLabelColor,
                                              "iSpiroDropdownLabelBottomMargin":spiroDropdownLabelBottomMargin,
                                              "iSpiroDropdownLabelLeftMargin":spiroDropdownLabelLeftMargin,
                                              "iSpiroSaveBtnWidth":spiroSaveBtnWidth,
                                              "iSpiroSaveBtnHeight":spiroSaveBtnHeight,
                                              "strSaveBtnBgColor":saveBtnBgColor,
                                              "iSaveBtnBorderWidth":saveBtnBorderWidth,
                                              "strSaveBtnBorderColor":saveBtnBorderColor,
                                              "iSpiroSavebtnBottomMargin":spiroSavebtnBottomMargin,
                                              "iSpiroSavebtnRightMargin":spiroSavebtnRightMargin,
                                              "strSpiroSavebtnImage":spiroSavebtnImage,
                                              "iTopDropdownLeftMargin":topDropdownLeftMargin,
                                              "iTopDropdownTopMargin":topDropdownTopMargin,
                                              "iBottomDropdownBottomMargin":bottomDropdownBottomMargin,
                                              "iBottomDropdownLeftMargin":bottomDropdownLeftMargin,
                                              "iTopDropdownHeight":topDropdownHeight,
                                              "iTopDropdownWidth":topDropdownWidth,
                                              "iBottomDropdownHeight":bottomDropdownHeight,
                                              "iBottomDropdownWidth":bottomDropdownWidth,
                                              "strTopDropdownColor":topDropdownColor,
                                              "iTopDropdownFontSize":topDropdownFontSize,
                                              "iTopDropdownImageWidth":topDropdownImageWidth,
                                              "iTopDropdownImageHeight":topDropdownImageHeight,
                                              "iTopDropdownImageTopMargin":topDropdownImageTopMargin,
                                              "iTopDropdownImageRightMargin":topDropdownImageRightMargin,
                                              "strBottomDropdownColor":bottomDropdownColor,
                                              "iBottomdropdownFontSize":bottomropdownFontSize,
                                              "iBottomDropdownImageWidth":bottomDropdownImageWidth,
                                              "iBottomDropdownImageHeight":bottomDropdownImageHeight,
                                              "iBottomDropdownImageTopMargin":bottomDropdownImageTopMargin,
                                              "iBottomDropdownImageRightMargin":bottomDropdownImageRightMargin,
                                              "strTopDropdownBorderColor":topDropdownBorderColor,
                                              "strBottomDropdownBorderColor":bottomDropdownBorderColor,

                                          });

        var alarmGrpRef = getCompRef(mainitem,"AlarmGroup");
        spiroObj.anchors.top = alarmGrpRef.bottom;
//        spiroObj.anchors.topMargin = 5;
        var btnbarObjRef = getGroupRef("ButtonGroup");
        spiroObj.spiroCloseClicked.connect(btnbarObjRef.spiroClosebtnState);
        if (spiroObj === null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (spiroComp.status === Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}
function resizeWaveformArea()
{
    var WaveformRef = getCompRef(mainitem,"waveformGroup");
    WaveformRef.width = 585;
    WaveformRef.anchors.left = spiroObj.right;
}

function destroySplitComponent() {
    var SpiroObjRef = /*getGroupRef("SpirometryGrp");*/getCompRef(mainitem,"SpirometryGrp");
    if (SpiroObjRef !== null && typeof(SpiroObjRef) !== "undefined"){
        SpiroObjRef.destroy();
        var WaveformRef = getCompRef(mainitem,"waveformGroup");
        WaveformRef.width = 885;
        WaveformRef.anchors.left = mainitem.left;
    } else {
        console.log("No object")
    }
}

function changeStateIndropdownTouched()
{
    var spiroTopdropdownRef = getCompRef(spiromainpage,"topDropdown");
    var spiroBottomdropdownRef = getCompRef(spiromainpage,"bottomDropdown");

    if(spiroBottomdropdownRef.state === "dropDown")
    {
//        console.log("spiroBottomdropdownRef.state"+spiroBottomdropdownRef.state);
        console.log("2nd dropdown");
        spiroTopdropdownRef.state = "noDropdown";
    }
    else if(spiroTopdropdownRef.state === "dropDown")
    {
        console.log("1st dropdown");
        spiroBottomdropdownRef.state = "noDropdown";
    }

}
