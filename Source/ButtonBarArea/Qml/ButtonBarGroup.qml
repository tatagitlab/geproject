import QtQuick 2.0
import QtQuick.Layouts 1.3
import "../../Checkout/JavaScript/CheckoutController.js" as CheckoutComp
import "../../Standby/JavaScript/StandbyController.js" as StandbyComp
import "../JavaScript/ButtonBarGroupController.js" as ButtonGrpCtrl
import "../../DefaultSetup/JavaScript/MainDefaultController.js" as DefaultCtrl
import "../../DefaultSetup/JavaScript/PasswordPageController.js" as PwdCtrl
import "../../Spirometry/JavaScript/SpiroController.js" as SpiroCtrl

Rectangle {
    id: buttonBarGrpId
    objectName: "ButtonGroup"

    property int iIconGrpHeight: 672
    property int iIconGrpWidth: 100
    property string strIconGrpColor: "Gray"
    property string strIconPreviousState: "Active"
    property var buttongrpnam: maincolumn

    property int iconWidth: 78
    property int iconHeight: 71
    property int iconRadius: 3
    property string iconColor: "#2e2e2e"
    property string iconBorderColorOnActive:  "#edbf00"
    property string iconBorderColorOnInactive: "#444444"
    property int iconBorderWidthOnActive: 2
    property int iconBorderWidthOnInactive: 0

    property int standbyIconWidth: 50
    property int standbyIconHeight: 50
    property string standbyIconSource: "qrc:/Config/Assets/icon_systemsetup.png"

    signal iconGrpClicked();
    signal setCheckoutAsObjectName(string objectName);
    signal spiroBtnClicked();
    width:iIconGrpWidth
    height:iIconGrpHeight
    color:strIconGrpColor
    border.color:"black"
    border.width: 0
    anchors.right:parent.right

    //*******************************************
    // Purpose: Handles navigation change across groups
    // Input Parameters: gets whether Right/Left navigation
    // Description: Gets the navigation type(right/left) and based on that calls the
    // corresponding handler
    //***********************************************/
    function groupNavigationChange(key) {
        if(key === "Left") {
            ButtonGrpCtrl.groupChangeOnLeft();
        } else if(key === "Right") {
            ButtonGrpCtrl.groupChangeOnRight();
        }
    }

    // spiro close button state function
    function spiroClosebtnState()
    {
        console.log("spiro state defining");
        spirobtn.state = 'original';
    }

    Rectangle {
        id: buttonbarleftborder
        width: 1
        height: buttonBarGrpId.height
        color: "black"
        anchors.left: parent.left

    }

    ColumnLayout {
        id: maincolumn
        anchors.fill: parent/*
        Rectangle {
            id: iconbtn
            width: 78
            height: 71
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            radius: 3
            color: "#2e2e2e"
            border.width: activeFocus ? 2 : 0
            border.color: activeFocus ? "#edbf00" :"#444444"
            Image {
                id: settingicon
                width:50
                height:50
                source: "qrc:/Config/Assets/icon_systemsetup.png"
                anchors.centerIn: parent
            }
            MouseArea{
                anchors.fill: iconbtn
                onClicked: {
                    console.log("creating checkout components from icon...")
                    if (buttonBarGrpId.strIconPreviousState !== "Scroll") {
                        buttonBarGrpId.strIconPreviousState = "Touched"
                    }
                    setCheckoutAsObjectName("checkout")
                    iconGrpClicked();
                }
            }
            Keys.onReturnPressed: {
                console.log("enter pressed....")
                setCheckoutAsObjectName("checkout")
                iconGrpClicked();
            }
            Keys.onRightPressed: {
                console.log("ButtonArea->>Settings right pressed")
                ButtonGrpCtrl.onNavigateRightInButtonGroup()
            }
            Keys.onLeftPressed: {
                console.log("ButtonArea->>Settings left pressed")
                ButtonGrpCtrl.onNavigateLeftInButtonGroup()
            }
        }*/

        Rectangle{
            id: standbyIcon
            width: iconWidth
            height: iconHeight
            //anchors.top: iconbtn.top
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10

            radius: iconRadius
            color: iconColor
            border.width: activeFocus ? iconBorderWidthOnActive : iconBorderWidthOnInactive
            border.color: activeFocus ? iconBorderColorOnActive : iconBorderColorOnInactive
            Image {
                anchors.centerIn: parent
                //                width:standbyIconWidth
                //                height:standbyIconHeight
                source: standbyIconSource
            }
            MouseArea{
                anchors.fill: standbyIcon
                onClicked: {
                    setCheckoutAsObjectName("standby")
                    iconGrpClicked();
                }
            }
            Keys.onReturnPressed: {
                console.log("standby enter pressed....")
                setCheckoutAsObjectName("standby")
                iconGrpClicked();
            }
        }

        // Button for rapid Screen

//        Rectangle{
//            id: defaultsetup
//            width: iconWidth
//            height: iconHeight
//            //anchors.top: iconbtn.top
//            anchors.top: standbyIcon.bottom
//            anchors.topMargin: 10
//            anchors.right: parent.right
//            anchors.rightMargin: 10

//            radius: iconRadius
//            color: iconColor
//            border.width: activeFocus ? iconBorderWidthOnActive : iconBorderWidthOnInactive
//            border.color: activeFocus ? iconBorderColorOnActive : iconBorderColorOnInactive
//            Image {
//                anchors.centerIn: parent
//                width:standbyIconWidth
//                height:standbyIconHeight
//                source: "qrc:/Config/Assets/icon_pass.png"
//            }
//            MouseArea{
//                anchors.fill: defaultsetup
//                onClicked: {

//                    PwdCtrl.createPasswordPageComponent();
//                    DefaultCtrl.hideComponent();
//                }
//            }
//            //            Keys.onReturnPressed: {
//            //                console.log("standby enter pressed....")
//            //                setCheckoutAsObjectName("standby")
//            //                iconGrpClicked();
//            //            }
//        }

        //Button for spirometry

        Rectangle{
            id: spirobtn
            width: iconWidth
            height: iconHeight
            //anchors.top: iconbtn.top
            anchors.top: standbyIcon.bottom
            anchors.topMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10

            radius: iconRadius
            color: iconColor
            border.width: activeFocus ? iconBorderWidthOnActive : iconBorderWidthOnInactive
            border.color: activeFocus ? iconBorderColorOnActive : iconBorderColorOnInactive
            Image {
                anchors.centerIn: parent
                //                width:standbyIconWidth
                //                height:standbyIconHeight
                source: "qrc:/Config/Assets/icon_alarmstep_gray.png"
            }
            MouseArea{
                anchors.fill: spirobtn
                onClicked: {
                    spirobtn.state==='default'?spirobtn.state='spirometrychange':spirobtn.state === 'original'? spirobtn.state = 'spirometrychange': spirobtn.state = 'original'
                    //                    spiroBtnClicked();
                }
            }
            Keys.onReturnPressed: {
                spirobtn.state==='default'?spirobtn.state='spirometrychange':spirobtn.state === 'original'? spirobtn.state = 'spirometrychange': spirobtn.state = 'original'
            }

            state: 'default'
            states: [
                State {
                    name: "default"
                },
                State {
                    name: "original"
                    StateChangeScript {
                        name: "original"
                        script: {SpiroCtrl.destroySplitComponent()}
                    }

                },
                State {
                    name: "spirometrychange"
                    StateChangeScript {
                        name: "change"
                        script: {spiroBtnClicked();}
                    }
                }
            ]

        }

        Keys.onRightPressed: {
            buttonBarGrpId.strIconPreviousState = "Scroll"
            buttongrpnam.children[1].focus = true
            if(buttongrpnam.children[1].focus === true)
            {
                buttongrpnam.children[0].focus = true
            }

        }

        Keys.onLeftPressed: {
            buttonBarGrpId.strIconPreviousState = "Scroll"
            buttongrpnam.children[0].focus = true
            if(buttongrpnam.children[0].focus === true)
            {
                buttongrpnam.children[1].focus = true
            }
        }
    }
}
