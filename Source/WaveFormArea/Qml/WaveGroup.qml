import QtQuick 2.0

Rectangle {
    id: waveformGrpId
    objectName: "waveformGroup"

    property int iWaveGrpHeight: 885
    property int iWaveGrpWidth: 612
    property string strWaveGrpColor: "Gray"
    property var waveformGrp: maincolumn

    signal waveGrpClicked();
    signal setWaveformAsObjectName(string objectName);

    width:iWaveGrpWidth
    height:iWaveGrpHeight
    color:strWaveGrpColor
    border.color:"black"
    border.width: 0

    Column {
        id: maincolumn
        anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            console.log("waveform grp clicked!!!!")
            waveGrpClicked()
            setWaveformAsObjectName("waveform")
        }
    }
}
