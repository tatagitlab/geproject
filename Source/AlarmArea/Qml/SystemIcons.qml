import QtQuick 2.9
import QtQuick.Controls 2.2

Rectangle {
    id: sysIcons

    property int iBatteryIconHeight
    property int iBatteryIconWidth
    property int iSysIconAreaHeight
    property int iSysIconAreaWidth
    property int iSysTimePixelSize
    property int iSysTimeTopMargin
    property string strAlarmColor
    property string strBatteryIconSource
    property string strSysTimeFontColor

    width: iSysIconAreaWidth
    height: iSysIconAreaHeight
    color: strAlarmColor
    anchors.right: parent.right

    Text {
        id: system_time
        text: Qt.formatTime(new Date(), "hh:mm")
        color: strSysTimeFontColor
        font.pixelSize: iSysTimePixelSize
        anchors.top: parent.top
        anchors.topMargin: iSysTimeTopMargin
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Timer {
        id: timer
        interval: 60000
        repeat: true
        running: true
        onTriggered: {
            system_time.text = Qt.formatTime(new Date(),
                                             "hh:mm")
        }
    }

    Image {
        id: batteryIcon
        source: strBatteryIconSource
        width: iBatteryIconWidth
        height: iBatteryIconHeight
        anchors.horizontalCenter: system_time.horizontalCenter
        anchors.bottom: parent.bottom
    }
}
