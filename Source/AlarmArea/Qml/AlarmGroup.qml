import QtQuick 2.9
import "../JavaScript/AlarmGroupController.js" as Alarmcontrol

Rectangle {
    id: alarmGrpId
    objectName: "AlarmGroup"

    property int iAlarmGrpHeight: 60
    property int iAlarmGrpWidth: 1180
    property string strAlarmGrpColor: "grey"
    property var alarmRow: mainrow

    signal alarmGrpClicked()
    signal setAlarmAsObjectName(string objectName);

    width: iAlarmGrpWidth
    height: iAlarmGrpHeight
    color: strAlarmGrpColor
    border.color: "black"
    anchors.top: parent.top

    Connections {
        target: alarm
        onAlarmMasterListUpdated:{
            var alarmsList = alarm.getMasterList();
            Alarmcontrol.alarmProcessing(alarmsList);
        }
    }

    Row {
        id: mainrow
        spacing: 3
    }
    
//    MouseArea {
//        anchors.fill: parent
//        onClicked: {
//            console.log("alarm grp clicked!!!!")
//            alarmGrpClicked()
//            setAlarmAsObjectName("alarm");
//        }
//    }
    Component.onCompleted: {
        Alarmcontrol.createComponent();
    }
}
