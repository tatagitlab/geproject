#include "../Source/AlarmArea/Include/alarmreader.h"

/**
 * @brief AlarmReader::AlarmReader - Constructor
 * @param errorStatus - initializing variables
 */
AlarmReader::AlarmReader(bool errorStatus):m_errorStat(errorStatus)
{

}

/**
 * @brief AlarmReader::getMasterList
 * @return returns the Master list
 */
QList<QString> AlarmReader::getMasterList()
{
    return m_masterList ;
}

/**
 * @brief AlarmReader::getList - get the Alarm log data model
 * @return - returns Alarm log data model
 */
AlarmLogDataModel* AlarmReader::getList()
{
    return &m_AlarmLogDataModel;

}

/**
 * @brief AlarmReader::updateLowPriorityList  - Updating Lower Priority list
 * @param iAlarmId - Lower priority alarm id to add it to Low priority list
 */
void AlarmReader::updateLowPriorityList(int iAlarmId)
{
    QString strAlarmId = "LA_"+QString::number(iAlarmId) ;
    //qDebug()<<"m_lowPriorityList.length: "<<m_lowPriorityList.length();
    if(m_lowPriorityList.isEmpty()){
        //qDebug()<<"First element in m_lowPriorityList AlarmId:"<<strAlarmId;
        m_lowPriorityList.append(strAlarmId);

    }else{

        for (int i = 0; i < m_lowPriorityList.size(); ++i) {
            if (m_lowPriorityList.at(i) == strAlarmId){
               // //qDebug()<<"Alarm id is already present in the m_lowPriorityList";
                return;
            }

        }
        //qDebug()<<"UPDATING AlarmId: "<<strAlarmId;
        m_lowPriorityList.prepend(strAlarmId);

    }
    updateMasterList();
}

/**
 * @brief AlarmReader::updateMediumPriorityList - Updating Medium Priority list
 * @param iAlarmId - Medium priority alarm id to add it to Medium priority list
 * @param strTimeStamp - Timestamp of the alarm
 */
void AlarmReader::updateMediumPriorityList(int iAlarmId, QString strTimeStamp)
{
    QString strAlarmId = "MA_"+QString::number(iAlarmId) ;
    //qDebug()<<"m_mediumPriorityList.length: "<<m_mediumPriorityList.length();
    if(m_mediumPriorityList.isEmpty()){
        //qDebug()<<"First element in m_mediumPriorityList AlarmId:"<<strAlarmId;
        m_mediumPriorityList.append(strAlarmId);


    }else{

        for (int i = 0; i < m_mediumPriorityList.size(); ++i) {
            if (m_mediumPriorityList.at(i) == strAlarmId){
               // //qDebug()<<"Alarm id is already present in the m_mediumPriorityList";
                return;
            }
        }
        //qDebug()<<"UPDATING AlarmId: "<<strAlarmId;
        m_mediumPriorityList.prepend(strAlarmId);
    }

    //Alarm log menu
    //qDebug() << "Adding data"<<endl;
    m_AlarmLogDataModel.addAlarmLogData(strAlarmId,QT_TRID_NOOP("Alarm_Priority_Medium"),strTimeStamp);
    updateMasterList();
}

/**
 * @brief AlarmReader::updateHighPriorityList - - Updating High Priority list
 * @param iAlarmId - High priority alarm id to add it to High priority list
 * @param strTimeStamp - Timestamp of the alarm
 */
void AlarmReader::updateHighPriorityList(int iAlarmId, QString strTimeStamp)
{
    QString strAlarmId = "HA_"+QString::number(iAlarmId) ;
    //qDebug()<<"m_highPriorityList.length: "<<m_highPriorityList.length();
    if(m_highPriorityList.isEmpty()){
        //qDebug()<<"First element in m_highPriorityList iAlarmId: "<<strAlarmId;
        m_highPriorityList.append(strAlarmId);

    }else{

        for (int i = 0; i < m_highPriorityList.size(); ++i) {
            if (m_highPriorityList.at(i) == strAlarmId){
               // //qDebug()<<"Alarm id is already present in the m_highPriorityList";
                return;
            }
        }
        //qDebug()<<"UPDATING AlarmId: "<<strAlarmId;
        m_highPriorityList.prepend(strAlarmId);

    }

    //Alarm log menu
    //qDebug() << "Adding data"<<endl;
    m_AlarmLogDataModel.addAlarmLogData(strAlarmId,QT_TRID_NOOP("Alarm_Priority_High"),strTimeStamp);
    updateMasterList();
}

/**
 * @brief AlarmReader::updateMasterList - Updating the Masterlist(order - High, Medium, Low)
 */
void AlarmReader::updateMasterList()
{
    //qDebug()<<"Updating Masterlist";
    m_masterList.clear();
    if(!m_highPriorityList.isEmpty()){

        m_masterList.append(m_highPriorityList);
    }

    if(!m_mediumPriorityList.isEmpty()){

        m_masterList.append(m_mediumPriorityList);

    }

    if(!m_lowPriorityList.isEmpty()){
        m_masterList.append(m_lowPriorityList);

    }

    //qDebug()<<"Masterlist size: "<< m_masterList.size();

    for(int i = 0; i<m_masterList.size(); i++){
        //qDebug()<<"Masterlist contents: masterList["<<i<<"]: "<< m_masterList.at(i);
    }

    emit alarmMasterListUpdated();
}

/**
 * @brief AlarmReader::deleteFromLowPriorityList - Delete the particular alarm from Low Priority list
 * @param iAlarmId - Alarm id that needs to delete from Low Priority list
 */
void AlarmReader::deleteFromLowPriorityList(int iAlarmId)
{
    QString strAlarmId = "LA_"+QString::number(iAlarmId) ;
    for (int i = 0; i < m_lowPriorityList.size(); ++i) {
        if (m_lowPriorityList.at(i) == strAlarmId){
            //qDebug()<<"Deleting AlarmId: "<<strAlarmId;
            m_lowPriorityList.removeAt(i);
            updateMasterList();
            return;
        }
    }
}

/**
 * @brief AlarmReader::deleteFromMediumPriorityList - Delete the particular alarm from Medium Priority list
 * @param iAlarmId - Alarm id that needs to delete from Medium Priority list
 */
void AlarmReader::deleteFromMediumPriorityList(int iAlarmId)
{
    QString strAlarmId = "MA_"+QString::number(iAlarmId) ;
    for (int i = 0; i < m_mediumPriorityList.size(); ++i) {
        if (m_mediumPriorityList.at(i) == strAlarmId){
            //qDebug()<<"Deleting AlarmId: "<<strAlarmId;
            m_mediumPriorityList.removeAt(i);
            updateMasterList();
            return;
        }
    }
}

/**
 * @brief AlarmReader::deleteFromHighPriorityList - Delete the particular alarm from High Priority list
 * @param iAlarmId - Alarm id that needs to delete from Medium Priority list
 */
void AlarmReader::deleteFromHighPriorityList(int iAlarmId)
{
    QString strAlarmId = "HA_"+QString::number(iAlarmId) ;

    for (int i = 0; i < m_highPriorityList.size(); ++i) {
        if (m_highPriorityList.at(i) == strAlarmId){
            //qDebug()<<"Deleting index: "<<i<<" AlarmId: "<<strAlarmId;
            m_highPriorityList.removeAt(i);
            updateMasterList();
            return;
        }
    }
}
