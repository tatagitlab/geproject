#include "../Source/AlarmArea/Include/alarmdatamodel.h"
#include <QDebug>
#define ALARMLIST_ITEMS_MAX_COUNT 50

/**
 * @brief AlarmLogData::AlarmLogData - Constructor to intialize member variables
 * @param timestamp - It holds time stamp of generated alarm message
 * @param alarmName - It holds name of generated alarm message
 * @param priority - It holds priority of generated alarm message
 */
AlarmLogData::AlarmLogData(const QString &timestamp, const QString &alarmName, const QString &priority)
    : m_timestamp(timestamp), m_alarmName(alarmName),m_priority(priority)
{
}

/**
 * @brief AlarmLogData::priority - This method returns priority of alarm message
 */
QString AlarmLogData::priority() const
{
    return m_priority;
}

/**
 * @brief AlarmLogData::alarmName - This method returns alarmName of alarm message
 */
QString AlarmLogData::alarmName() const
{
    return m_alarmName;
}

/**
 * @brief AlarmLogData::timestamp - This method returns timestamp of alarm message
 */
QString AlarmLogData::timestamp() const
{
    return m_timestamp;
}

/**
 * @brief AlarmLogDataModel::AlarmLogDataModel - Constructor to intialize member variables
 * @param parent - It holds pointer to QObject class
 */
AlarmLogDataModel::AlarmLogDataModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

/**
 * @brief AlarmLogDataModel::addAlarmLogData - This method is used to update alarm list with generated alarms.
 * @param strAlarmId - It holds id of generated alarm message
 * @param strPriority - It holds priority of generated alarm message
 * @param strTimestamp - It holds time stamp of generated alarm message
 */
void AlarmLogDataModel::addAlarmLogData(QString strAlarmId, QString strPriority, QString strTimestamp)
{

    qDebug()<<"strAlarmId : "<<strAlarmId<<endl;
    qDebug()<<"m_listAlarmLogData.length() "<<m_listAlarmLogData.length()<<endl;

    if (m_listAlarmLogData.length() == ALARMLIST_ITEMS_MAX_COUNT )
    {
        beginResetModel();
        qDebug()<<"remove last element of list"<<endl;
        m_listAlarmLogData.removeAt(ALARMLIST_ITEMS_MAX_COUNT - 1);
        endResetModel();
    }

    beginResetModel();
    qDebug()<<"Add alarm data"<<endl;
    m_listAlarmLogData.prepend(AlarmLogData(strTimestamp,strAlarmId,strPriority));
    endResetModel();
}

/**
 * @brief AlarmLogDataModel::clearAlarmLogData - This method is used to clear alarm list with generated alarms.
 */
void AlarmLogDataModel::clearAlarmLogData()
{
    qDebug()<<"clear list "<<endl;
    beginResetModel();
    m_listAlarmLogData.clear();
    endResetModel();
}

/**
 * @brief AlarmLogDataModel::rowCount - This method is used to count the length of alarm list.
 * @param parent - It holds pointer to QModelIndex.
 */
int AlarmLogDataModel::rowCount(const QModelIndex & parent) const
{
    Q_UNUSED(parent);
    return m_listAlarmLogData.count();
}

/**
 * @brief AlarmLogDataModel::data - This method returns the data stored under the given role for the item referred to by the index.
 * @param index - It holds reference to QModelIndex.
 * @param role - It holds role for the item.
 */
QVariant AlarmLogDataModel::data(const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.row() >= m_listAlarmLogData.count())
        return QVariant();

    const AlarmLogData &AlarmLogData = m_listAlarmLogData[index.row()];
    if (role == TimeStampRole)
        return AlarmLogData.timestamp();
    else if (role == AlarmNameRole)
        return AlarmLogData.alarmName();
    else if (role == PriorityRole)
        return AlarmLogData.priority();
    return QVariant();
}

/**
 * @brief AlarmLogDataModel::roleNames - This method returns models role names.
 */
QHash<int, QByteArray> AlarmLogDataModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TimeStampRole] = "timestamp";
    roles[AlarmNameRole] = "alarmName";
    roles[PriorityRole] = "priority";
    return roles;
}
