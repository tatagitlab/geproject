#ifndef ALARMDATAMODEL_H
#define ALARMDATAMODEL_H

#include <QAbstractListModel>
#include <QStringList>

class AlarmLogData
{
public:
    AlarmLogData(const QString &timestamp, const QString &alarmName, const QString &priority);
    QString timestamp() const;
    QString alarmName() const;
    QString priority() const;
private:
    QString m_timestamp;
    QString m_alarmName;
    QString m_priority;
};

class AlarmLogDataModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum AlarmLogDataRoles {
        TimeStampRole = Qt::UserRole + 1,
        AlarmNameRole,
        PriorityRole
    };

    AlarmLogDataModel(QObject *parent = 0);

    void addAlarmLogData(QString strAlarmId, QString strPriority, QString strTimestamp);

    Q_INVOKABLE void clearAlarmLogData();

    Q_INVOKABLE int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<AlarmLogData> m_listAlarmLogData;
};

#endif // ALARMDATAMODEL_H
