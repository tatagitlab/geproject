#ifndef GROUPDOMREADER_H
#define GROUPDOMREADER_H
#include <QObject>
#include <QString>
#include <QDomDocument>

class GroupDomReader : public QObject
{
    Q_OBJECT

public:
    GroupDomReader(bool errorStatus,QString errorString);
    Q_INVOKABLE void init(QString xmlfilepath, QString schemapath);
    Q_INVOKABLE QString getMainGroupDataByTag(QString groupname, QString groupstyle, QDomElement mainelements);
    Q_INVOKABLE QString getMainGroupDataByTag(QString groupname, QString groupstyle);
    Q_INVOKABLE QString getComponentGroupDataByTag(QString groupname, QString componenttype, QString componentgroupname, QString componentgroupstyle);
    Q_INVOKABLE QString getSecondaryComponentGroupDataByTag(QString groupname, QString componenttype, QString componentgroupname, QString subgroupname, QString secondarycomponenttype, QString secondarycomponentgroup, QString secondarygroupstyle);
    Q_INVOKABLE int getMainComponentLength(QString groupname);
    Q_INVOKABLE int getSubGroupLength(QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QDomElement mainelement);
    Q_INVOKABLE int getSubGroupLength(QString groupname, QString componenttype, QString componentgroupname, QString subgroup);
    Q_INVOKABLE int getSecondarySubGroupLength(QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString secondarycomponenttype, QString secondarygroupname);
    Q_INVOKABLE QString getComponentSubGroupDataByTag(int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString componentsubgroupstyle, QDomElement mainelement);
    Q_INVOKABLE QString getComponentSubGroupDataByTag(int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString componentsubgroupstyle);
    Q_INVOKABLE QString getSecondaryComponentSubGroupDataByTag(int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString secondarycomponenttype, QString secondarygroupname, QString secondarycomponentsubgroupstyle);
    Q_INVOKABLE QString getMainComponentType(int index, QString groupname);
    Q_INVOKABLE QString getComponentType(int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QDomElement mainelement);
    Q_INVOKABLE QString getComponentType(int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup);
    Q_INVOKABLE QString getErrorString();
    QDomDocument groupdocument;
    QDomElement groupelement;
    bool m_errorStat ;
    QString m_errorStr ;
};

#endif // BACKEND_DOM_H
