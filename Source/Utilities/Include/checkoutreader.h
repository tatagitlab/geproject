#ifndef CHECKOUTREADER_H
#define CHECKOUTREADER_H

#include <QObject>
#include <QString>
#include <QDomDocument>
#include <QtCore>
#include <QtXml/QDomDocument>
#include <QDebug>
#include <QXmlSchema>
#include <QXmlSchemaValidator>
#include<QString>
#include<QDateTime>

#define MAX_CHECKOUT_HOURS 24
class Checkoutreader : public QObject
{
    Q_OBJECT
public:
    explicit Checkoutreader(QObject *parent = nullptr);
    Q_INVOKABLE void init(QString xmlfilepath, QString schemapath);
    Q_INVOKABLE QString getErrorString() const;
    Q_INVOKABLE bool getErrorStat();
    Q_INVOKABLE QString ReadXMLData(QString rootItem, QString testType, QString attributeName);
    Q_INVOKABLE bool isCheckoutDoneBeforeTwentyFourHours();
    Q_INVOKABLE QStringList getListOfString() const;
    Q_INVOKABLE void readXMLContents();
signals:

public slots:

private:
    QDomDocument checkoutDocument;
    bool m_errorStat;
    QString m_errorStr;
    QStringList m_listOfString;
};

#endif // CHECKOUTREADER_H
