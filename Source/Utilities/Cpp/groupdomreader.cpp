#include "../Source/Utilities/Include/groupdomreader.h"
#include <QtCore>
#include <QtXml/QDomDocument>
#include <QDebug>
#include <QXmlSchema>
#include <QXmlSchemaValidator>

GroupDomReader::GroupDomReader(bool errorStatus= false , QString errorString = "no error"):m_errorStat(errorStatus),m_errorStr(errorString)
{
}

/*
********************************************
@brief Method to load the xml file and schema validation
@param[in] QString xmlfilepath, QString schemapath
********************************************
*/

void GroupDomReader::init(QString xmlfilepath, QString schemapath)
{

    try{
        //xml schema validation for the group
        m_errorStat = false;
        m_errorStr = "no error";
        QFileInfo sf(schemapath);
        QString schemaext = sf.suffix();
        QFileInfo xf(xmlfilepath);
        QString xmlext = xf.suffix();
        if (schemaext != "xsd")
        {
            m_errorStr = "not a valid xsd file";
            throw m_errorStr;
        }
        if (xmlext != "xml")
        {
            m_errorStr = "not a valid xml file";
            throw m_errorStr;
        }
        QFile schemafile(schemapath);
        QFile file(xmlfilepath);
        if (!schemafile.exists())
        {
            m_errorStr = "schema file does not exist";
            throw m_errorStr;
        }
        if (!file.exists())
        {
            m_errorStr = "file does not exist";
            throw m_errorStr;
        }
        if (!schemafile.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read schema file in parser";
            throw m_errorStr;

        }
        if (!file.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read xml file in parser";
            throw m_errorStr;

        }
        QXmlSchema schema;
        schema.load(&schemafile, QUrl::fromLocalFile(schemafile.fileName()));
        if (schema.isValid())
        {
            QXmlSchemaValidator validator (schema);
            if (validator.validate (&file, QUrl::fromLocalFile(file.fileName())))
            {
                file.close();
            }
            else
            {
                m_errorStr = "xml does not confirm with the schema valueGroup";
                throw m_errorStr;
            }
        }
        else
        {
            m_errorStr = "schema is invalid valueGroup";
            throw m_errorStr;
        }
        file.open(QIODevice::ReadOnly);
        if (!groupdocument.setContent(&file))
        {
            m_errorStr = "could not load component xml file in parser";
            file.close();
            throw m_errorStr;
        }
        groupelement = groupdocument.documentElement();
        file.close();
        schemafile.close();
    }
    catch (QString errorStr)
    {
        qDebug()<<errorStr;
        m_errorStat = true;
    }
}

/*
********************************************
@brief Method to read styling for the main group by its tag. Returns the styling value of the specified tag.
@param[in]  QString groupname: ex. "VentGroup", QString groupstyle, QDomElement mainelement: element loaded with the xml
@param[out] QString groupattributevalue: value of the tag
********************************************
*/

QString GroupDomReader::getMainGroupDataByTag (QString groupname, QString groupstyle, QDomElement mainelement)
{
    QString groupattributevalue = "";
    try{
        if (!m_errorStat)
        {
            QDomNode group =  mainelement.firstChild();
            while(!group.isNull()){
                if(group.toElement().attribute("type") == groupname)
                {
                    groupattributevalue = group.firstChildElement(groupstyle).text();
                    break;

                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return groupattributevalue;

}

/*
********************************************
@brief Overloaded method to pass default Domdocument element to  getMainGroupDataByTag() method
@param[in]  QString groupname: ex. "VentGroup", QString groupstyle: required tag
@param[out] QString groupattributevalue: value of the tag
********************************************
*/

QString GroupDomReader::getMainGroupDataByTag (QString groupname, QString groupstyle)
{
    return getMainGroupDataByTag(groupname, groupstyle, groupelement);
}

/*
********************************************
@brief Method to read styling for the Secondary group by its tag. Returns the styling value of the specified tag.
@param[in]  QString groupname: ex. "VentGroup", QString componenttype: main component type, QString componentgroupname: secondary group, QString componentgroupstyle: required tag
@param[out] QString secondarystylingvalue: value of the tag
********************************************
*/

QString GroupDomReader::getComponentGroupDataByTag (QString groupname, QString componenttype, QString componentgroupname, QString componentgroupstyle)
{
    bool continueloop = true;
    QString secondarystylingvalue = "";
    QDomNode group =  groupelement.firstChild();
    QDomElement compelement;
    try{
        if (!m_errorStat)
        {
            if(!group.isNull())
            {
                while(!group.isNull() && continueloop)
                {
                    if(group.toElement().attribute("type") == groupname)
                    {
                        compelement = group.firstChildElement("Component");
                        while (!compelement.isNull() && continueloop)
                        {                            
                            if(compelement.attribute("type") == componenttype)
                            {
                                secondarystylingvalue = getMainGroupDataByTag(componentgroupname, componentgroupstyle, compelement);
                                continueloop = false;
                            }
                            compelement = compelement.nextSiblingElement();
                        }
                    }
                    group = group.nextSibling();
                }
            }
            else
            {
                qDebug()<<"No elements inside the main group";
            }
        }
        else
        {
            throw m_errorStr;
        }
        return secondarystylingvalue;
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }
return secondarystylingvalue;
}


/*
********************************************
@brief Method to read styling for the secondary component group by its tag. Returns the styling value of the specified tag.
@param[in]  QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroupname: ex."VCV_mode", QString secondarycomponenttype : ex."QuickKey" , QString secondarycomponentgroup: ex."MoreMenuGroup", QString secondarygroupstyle:ex."MoreSettingLabel"
@param[out] QString groupattributevalue: value of the tag
********************************************
*/

QString GroupDomReader::getSecondaryComponentGroupDataByTag (QString groupname, QString componenttype, QString componentgroupname, QString subgroupname, QString secondarycomponenttype, QString secondarycomponentgroup, QString secondarygroupstyle)
{
    bool continueloop = true;
    QDomNode group =  groupelement.firstChild();
    QDomElement compelement;
    QDomElement componentgroup;
    QDomElement componentsubgroup;
    QDomElement secondarycomponent;
    QString secondarycomponentgroupvalue;
    try{
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    compelement = group.firstChildElement("Component");
                    while (!compelement.isNull() && continueloop)
                    {
                        if (compelement.attribute("type") == componenttype)
                        {
                            componentgroup = compelement.firstChildElement("Group");
                            while (!componentgroup.isNull() && continueloop)
                            {
                                if (componentgroup.toElement().attribute("type") == componentgroupname)
                                {
                                    componentsubgroup = componentgroup.firstChildElement("SubGroup");
                                    while (!componentsubgroup.isNull() && continueloop)
                                    {
                                        if (componentsubgroup.toElement().attribute("type") == subgroupname)
                                        {
                                            secondarycomponent = componentsubgroup.firstChildElement("Component");
                                            while (!secondarycomponent.isNull() && continueloop)
                                            {
                                                if (secondarycomponent.toElement().attribute("type") == secondarycomponenttype)
                                                {
                                                    secondarycomponentgroupvalue = getMainGroupDataByTag(secondarycomponentgroup, secondarygroupstyle, secondarycomponent);
                                                    continueloop = false;
                                                }
                                                secondarycomponent = secondarycomponent.nextSiblingElement();
                                            }
                                        }
                                        componentsubgroup = componentsubgroup.nextSiblingElement();
                                    }

                                }
                                componentgroup = componentgroup.nextSiblingElement();
                            }

                        }
                        compelement = compelement.nextSiblingElement();
                    }
                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }
return secondarycomponentgroupvalue;
}

/*
********************************************
@brief Method to read length of the main components within a group.
@param[in]  QString groupname: ex. "VentGroup"
@param[out] int maincomponentlength: length of the components
********************************************
*/

int GroupDomReader::getMainComponentLength (QString groupname)
{
    bool continueloop = true;
    int maincomponentlength = 0;
    QDomElement maincomponent;
    try{
        QDomNode group =  groupelement.firstChild();
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    maincomponent = group.firstChildElement("Component");
                    while (!maincomponent.isNull() && continueloop)
                    {                        
                        maincomponentlength = maincomponentlength+1;
                        maincomponent = maincomponent.nextSiblingElement("Component");
                        continueloop = false;
                    }
                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }


    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return maincomponentlength;
}

/*
********************************************
@brief Method to read length of components in subgroup within a group.
@param[in]  QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroupname: ex."VCV_mode", QDomElement mainelement: ex."groupelement"
@param[out] int componentlength: length of the components inside the sub group
********************************************
*/


int GroupDomReader::getSubGroupLength (QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QDomElement mainelement)
{
    bool continueloop = true;
    QDomNode group =  mainelement.firstChild();
    QDomElement compelement;
    QDomElement componentgroup;
    QDomElement componentsubgroup;
    QDomNodeList componentlist;
    int componentlength = 0;
    try{
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    compelement = group.firstChildElement("Component");
                    while (!compelement.isNull() && continueloop)
                    {
                        if (compelement.attribute("type") == componenttype)
                        {
                            componentgroup = compelement.firstChildElement("Group");
                            while (!componentgroup.isNull() && continueloop)
                            {
                                if (componentgroup.toElement().attribute("type") == componentgroupname)
                                {
                                    componentsubgroup = componentgroup.firstChildElement("SubGroup");
                                    while (!componentsubgroup.isNull() && continueloop)
                                    {
                                        if (componentsubgroup.attribute("type") == subgroup || componentsubgroup.attribute("type") == "")
                                        {
                                            componentlist = componentsubgroup.childNodes();
                                            componentlength = componentlist.length();
                                            continueloop = false;
                                        }
                                        componentsubgroup = componentsubgroup.nextSiblingElement("SubGroup");
                                    }
                                }
                                componentgroup = componentgroup.nextSiblingElement("Group");
                            }
                        }
                        compelement = compelement.nextSiblingElement("Component");
                    }
                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }


    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return componentlength;
}


/*
********************************************
@brief Overloaded method to pass default Domdocument element to  getSubGroupLength() method
@param[in]  QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroupname: ex."VCV_mode", QDomElement mainelement: ex."groupelement"
@param[out] int componentlength: length of the components inside the sub group
********************************************
*/

int GroupDomReader::getSubGroupLength (QString groupname, QString componenttype, QString componentgroupname, QString subgroup)
{
    return getSubGroupLength(groupname, componenttype, componentgroupname, subgroup, groupelement);
}

/*
********************************************
@brief Method to read length of secondary sub group components.
@param[in]  QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroupname: ex."VCV_mode", QString secondarycomponenttype : ex."MoreSettings" , QString secondarygroupname: ex."MoreMenuGroup"
@param[out] QString secondarycomponentlength:length of secondary sub group components.
********************************************
*/

int GroupDomReader::getSecondarySubGroupLength (QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString secondarycomponenttype, QString secondarygroupname)
{
    bool continueloop = true;
    QDomNode group =  groupelement.firstChild();
    QDomElement compelement;
    QDomElement componentgroup;
    QDomElement componentsubgroup;
    int secondarycomponentlength = 0;
    try{
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    compelement = group.firstChildElement("Component");
                    while (!compelement.isNull() && continueloop)
                    {
                        if (compelement.attribute("type") == componenttype)
                        {
                            componentgroup = compelement.firstChildElement("Group");
                            while (!componentgroup.isNull() && continueloop)
                            {
                                if(componentgroup.attribute("type") == componentgroupname)
                                {
                                    secondarycomponentlength = getSubGroupLength(subgroup, secondarycomponenttype, secondarygroupname,"", componentgroup);
                                    continueloop = false;
                                }
                                componentgroup = componentgroup.nextSiblingElement("Group");
                            }
                        }
                        compelement = compelement.nextSiblingElement("Component");
                    }
                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return secondarycomponentlength;

}

/*
********************************************
@brief Method to read styling for main component sub group.
@param[in]  int index: ex."1", QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroupname: ex."VCV_mode", QString secondarycomponenttype : ex."MoreSettings" , QString secondarygroupname: ex."MoreMenuGroup"
@param[out] QString secondarycomponentlength:length of secondary sub group components.
********************************************
*/

QString GroupDomReader::getComponentSubGroupDataByTag (int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString subgroupstyle, QDomElement mainelement)
{
    bool continueloop = true;
    QDomNode group =  mainelement.firstChild();
    QDomElement compelement;
    QDomElement componentgroup;
    QDomElement componentsubgroup;
    QDomNodeList componentlist;
    QDomNode component;
    QString subgroupstylevalue;
    try{
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    compelement = group.firstChildElement("Component");
                    while (!compelement.isNull() && continueloop)
                    {
                        if (compelement.attribute("type") == componenttype)
                        {
                            componentgroup = compelement.firstChildElement("Group");
                            while (!componentgroup.isNull() && continueloop)
                            {
                                if (componentgroup.toElement().attribute("type") == componentgroupname)
                                {
                                    componentsubgroup = componentgroup.firstChildElement("SubGroup");
                                    while (!componentsubgroup.isNull() && continueloop)
                                    {
                                        if (componentsubgroup.attribute("type") == subgroup || componentsubgroup.attribute("type") == "")
                                        {
                                            componentlist = componentsubgroup.childNodes();
                                            int compLen = componentlist.length();
                                            component = componentlist.at(index);
                                            if(index >= 0 && index <= compLen-1)
                                                            {
                                                                qDebug()<<"The index is valid";
                                                            }
                                                            else{
                                                                m_errorStr = "The index is invalid";
                                                                throw m_errorStr;
                                                            }
                                            subgroupstylevalue = component.firstChildElement(subgroupstyle).text();
                                            continueloop = false;
                                        }
                                        componentsubgroup = componentsubgroup.nextSiblingElement("SubGroup");
                                    }
                                }
                                componentgroup = componentgroup.nextSiblingElement("Group");
                            }
                        }
                        compelement = compelement.nextSiblingElement("Component");
                    }
                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return subgroupstylevalue;

}

/*
********************************************
@brief Overloaded method to pass default Domdocument element to  getSubGroupLength() method
@param[in]  int index: ex."1", QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroup: ex."VCV_mode", QString secondarycomponenttype : ex."QuickKey" , QString secondarygroupname: ex."MoreMenuGroup", QString secondarygroupstyle:ex."MoreSettingLabel"
@param[out] QString groupattributevalue: value of the tag
********************************************
*/

QString GroupDomReader::getComponentSubGroupDataByTag (int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString subgroupstyle)
{
    return getComponentSubGroupDataByTag(index, groupname, componenttype, componentgroupname, subgroup, subgroupstyle, groupelement);
}

/*
********************************************
@brief Method to read styling for the secondary component group by its tag. Returns the styling value of the specified tag.
@param[in]  int index: ex."1", QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroup: ex."VCV_mode", QString secondarycomponenttype : ex."QuickKey" , QString secondarygroupname: ex."MoreMenuGroup", QString secondarygroupstyle:ex."MoreSettingLabel"
@param[out] QString secondarysubgroupstylevalue: value of the tag
********************************************
*/

QString GroupDomReader::getSecondaryComponentSubGroupDataByTag (int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QString secondarycomponenttype, QString secondarygroupname, QString secondarysubgroupstyle)
{
    bool continueloop = true;
    QDomNode group =  groupelement.firstChild();
    QDomElement compelement;
    QDomElement componentgroup;
    QString secondarysubgroupstylevalue;
    try{
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    compelement = group.firstChildElement("Component");
                    while (!compelement.isNull() && continueloop)
                    {
                        if (compelement.attribute("type") == componenttype)
                        {
                            componentgroup = compelement.firstChildElement("Group");
                            while (!componentgroup.isNull() && continueloop)
                            {
                                if (componentgroup.attribute("type") == componentgroupname)
                                {
                                    secondarysubgroupstylevalue = getComponentSubGroupDataByTag(index, subgroup, secondarycomponenttype, secondarygroupname,"", secondarysubgroupstyle, componentgroup);
                                    continueloop = false;
                                }
                                componentgroup = componentgroup.nextSiblingElement("Group");
                            }
                        }
                        compelement = compelement.nextSiblingElement("Component");
                    }
                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }

    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return secondarysubgroupstylevalue;

}

/*
********************************************
@brief Method to read component type
@param[in]  int index: ex."1", QString groupname: ex. "VentGroup"
@param[out] QString maincomponenttype: value of the component type attribute
********************************************
*/

QString GroupDomReader::getMainComponentType (int index, QString groupname)
{
    bool continueloop = true;
    QString maincomponenttype;
    QDomNode group =  groupelement.firstChild();
    QDomElement maincomponent;
    int maincomponentindex = 0;
    try{
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    maincomponent = group.firstChildElement("Component");
                    while (!maincomponent.isNull() && continueloop)
                    {
                        qDebug()<<maincomponent.attribute("type");
                        if (maincomponentindex == index)
                        {
                            maincomponenttype = maincomponent.attribute("type");
                            continueloop = false;
                        }
                        maincomponent = maincomponent.nextSiblingElement("Component");
                        maincomponentindex = maincomponentindex+1;
                    }

                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }

    return maincomponenttype;
}

/*
********************************************
@brief Method to read component type of the subgroup
@param[in]  int index: ex."1", QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroup: ex."VCV_mode"
@param[out] QString subgroupcomponenttype: value of the component type attribute
********************************************
*/

QString GroupDomReader::getComponentType (int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup, QDomElement mainelement)
{
    bool continueloop = true;
    QDomNode group =  mainelement.firstChild();
    QDomElement compelement;
    QDomElement componentgroup;
    QDomElement componentsubgroup;
    QDomNodeList componentlist;
    QDomNode component;
    QString subgroupcomponenttype;
    try{
        if (!m_errorStat)
        {
            while (!group.isNull() && continueloop)
            {
                if (group.toElement().attribute("type") == groupname)
                {
                    compelement = group.firstChildElement("Component");
                    while (!compelement.isNull() && continueloop)
                    {
                        if (compelement.attribute("type") == componenttype)
                        {
                            componentgroup = compelement.firstChildElement("Group");
                            while (!componentgroup.isNull() && continueloop)
                            {
                                if (componentgroup.toElement().attribute("type") == componentgroupname)
                                {
                                    componentsubgroup = componentgroup.firstChildElement("SubGroup");
                                    while (!componentsubgroup.isNull() && continueloop) {
                                        if (componentsubgroup.attribute("type") == subgroup || componentsubgroup.attribute("type") == "")
                                        {
                                            componentlist = componentsubgroup.childNodes();
                                            component = componentlist.at(index);
                                            subgroupcomponenttype = component.toElement().attribute("type");
                                            continueloop = false;
                                        }
                                        componentsubgroup = componentsubgroup.nextSiblingElement("SubGroup");
                                    }
                                }
                                componentgroup = componentgroup.nextSiblingElement("Group");
                            }
                        }
                        compelement = compelement.nextSiblingElement("Component");
                    }
                }
                group = group.nextSibling();
            }
        }
        else
        {
            throw m_errorStr;
        }
    }
    catch (QString errorStr)
    {
        qDebug() << errorStr;
    }


    return subgroupcomponenttype;

}

/*
********************************************
@brief Method to read styling for the secondary component group by its tag. Returns the styling value of the specified tag.
@param[in]  QString groupname: ex. "VentGroup", QString componenttype: ex. "VentMode", QString componentgroupname: ex."VentSetting", QString subgroupname: ex."VCV_mode", QString secondarycomponenttype : ex."QuickKey" , QString secondarycomponentgroup: ex."MoreMenuGroup", QString secondarygroupstyle:ex."MoreSettingLabel"
@param[out] QString groupattributevalue: value of the component type attribute
********************************************
*/

QString GroupDomReader::getComponentType (int index, QString groupname, QString componenttype, QString componentgroupname, QString subgroup)
{
    return getComponentType(index, groupname, componenttype, componentgroupname, subgroup, groupelement);
}

QString GroupDomReader::getErrorString()
{
    return m_errorStr;
}














