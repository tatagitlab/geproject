#include "../Source/Utilities/Include/physicalparamdomreader.h"
#include <QtCore>
#include <QtXml/QDomDocument>
#include <QDebug>
#include <QXmlSchema>
#include <QXmlSchemaValidator>


PhysicalParamDomReader::PhysicalParamDomReader(bool errorStatus, QString errorString):m_errorStat(errorStatus),m_errorStr(errorString)
{
}

QString PhysicalParamDomReader::getParamData(QString physicalparam, QString param)
{
   QString paramvalue;
   QDomElement paramblock = physicalparamelement.firstChildElement("ParamBlock");
   while(!paramblock.isNull()){
       if(paramblock.attribute("type") == physicalparam){
           paramvalue = paramblock.attribute(param);
           break;
       }
       paramblock = paramblock.nextSiblingElement("ParamBlock");
   }
   return paramvalue;
}

/**
 * @brief PhysicalParamDomReader::getErrorString - Method to get the error string in case of error in schema validation or file corruption.
 * @return returns the error string in case of error in schema validation or file corruption.
 */

QString PhysicalParamDomReader::getErrorString()
{
    return m_errorStr;
}

/**
 * @brief PhysicalParamDomReader::getErrorStat - Method to get the error status in case of error in schema validation or file corruption.
 * @return returns the error status in case of error in schema validation or file corruption.
 */

bool PhysicalParamDomReader::getErrorStat()
{
    return m_errorStat;
}

/**
 * @brief PhysicalParamDomReader::getRangeData - Method to get the min, max and the intermediate range along with the step size.
 * @param physicalparam - parameter to get the range for
 * @param pawunit - unit of the parameter
 * @return returns a list containing all the range and the step size data for the given param.
 */

QStringList PhysicalParamDomReader::getRangeData(QString physicalparam, QString pawunit)
{
    QStringList rangeList;
    QDomElement paramblock = physicalparamelement.firstChildElement("ParamBlock");
    while(!paramblock.isNull()){
        if(paramblock.attribute("type") == physicalparam && paramblock.attribute("unit") == pawunit){
            QDomElement range = paramblock.firstChildElement("Range");
            while(!range.isNull()){
                rangeList.push_back(range.attribute("min"));
                rangeList.push_back(range.attribute("max"));
                rangeList.push_back(range.attribute("step"));
                range = range.nextSiblingElement("Range");

            }
            break;
        }
        paramblock = paramblock.nextSiblingElement("ParamBlock");
    }
    return rangeList;
}

/**
 * @brief PhysicalParamDomReader::getAlarmData - Method to get the alarm string id corresponding to the code
 * @param physicalparam - alarm block
 * @param alarmcode - tag of the required alarm
 * @return
 */

QString PhysicalParamDomReader::getAlarmData(QString physicalparam, QString alarmcode)
{
    QString alarmMsg;
    QDomElement paramblock = physicalparamelement.firstChildElement("ParamBlock");
    while(!paramblock.isNull()){
        if(paramblock.attribute("type") == physicalparam){
            alarmMsg = paramblock.firstChildElement(alarmcode).text();
            break;
        }
        paramblock = paramblock.nextSiblingElement("ParamBlock");

    }
    return alarmMsg;
}

QStringList PhysicalParamDomReader::getAlarmInstructionData(QString physicalparam, QString alarmcode)
{
    int instructionCount;
    QStringList instructionList;
    QDomElement paramblock = physicalparamelement.firstChildElement("ParamBlock");
    while(!paramblock.isNull()){
        if(paramblock.attribute("type") == physicalparam){
           QDomElement alarmId = paramblock.firstChildElement(alarmcode);
           QDomNodeList instructionNodes = alarmId.elementsByTagName("Instruction");
           for(instructionCount = 0; instructionCount < instructionNodes.count();instructionCount++)
           {
               QDomNode eachInstructionNode = instructionNodes.at(instructionCount);
               if(eachInstructionNode.isElement())
               {
                   QDomElement instructionElement = eachInstructionNode.toElement();
                   instructionList.push_back(instructionElement.attribute("text"));
               }
           // break;
           }
        }
        paramblock = paramblock.nextSiblingElement("ParamBlock");

    }
    return instructionList;
}


/**
 * @brief PhysicalParamDomReader::init - Method to load the xml file and schema validation
 * @param xmlfilepath
 * @param schemapath
 */

void PhysicalParamDomReader::init(QString xmlfilepath, QString schemapath)
{

    try{
        //xml schema validation for the group
        m_errorStat = false;
        m_errorStr = "no error";
        QFileInfo sf(schemapath);
        QString schemaext = sf.suffix();
        QFileInfo xf(xmlfilepath);
        QString xmlext = xf.suffix();
        if (schemaext != "xsd")
        {
            m_errorStr = "not a valid xsd file";
            throw m_errorStr;
        }
        if (xmlext != "xml")
        {
            m_errorStr = "not a valid xml file";
            throw m_errorStr;
        }
        QFile schemafile(schemapath);
        QFile file(xmlfilepath);
        if (!schemafile.exists())
        {
            m_errorStr = "schema file does not exist";
            throw m_errorStr;
        }
        if (!file.exists())
        {
            m_errorStr = "file does not exist";
            throw m_errorStr;
        }
        if (!schemafile.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read schema file in parser";
            throw m_errorStr;

        }
        if (!file.open(QIODevice::ReadOnly))
        {
            m_errorStr = "could not read xml file in parser";
            throw m_errorStr;

        }
        QXmlSchema schema;
        schema.load(&schemafile, QUrl::fromLocalFile(schemafile.fileName()));
        if (schema.isValid())
        {
            QXmlSchemaValidator validator (schema);
            if (validator.validate (&file, QUrl::fromLocalFile(file.fileName())))
            {
                file.close();
            }
            else
            {
                m_errorStr = "xml does not confirm with the schema valueGroup";
                throw m_errorStr;
            }
        }
        else
        {
            m_errorStr = "schema is invalid valueGroup";
            throw m_errorStr;
        }
        file.close();
        file.open(QIODevice::ReadOnly);
        if (!physicalparamdocument.setContent(&file))
        {
            m_errorStr = "could not load component xml file in parser";
            file.close();
            throw m_errorStr;
        }
        physicalparamelement = physicalparamdocument.documentElement();
        file.close();
        schemafile.close();
    }
    catch (QString errorStr)
    {
        qDebug()<<errorStr;
        m_errorStat = true;
    }
}
