<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
	<name>VentModeBtn</name>
    <message id = "BtnMode_VCV">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>VCV</translation>
    </message>
	<message id = "BtnMode_PCV">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>PCV</translation>
    </message>
	<message id = "BtnMode_SIMVVCV">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>SIMV VCV</translation>
    </message>
	<message id = "BtnMode_SIMVPCV">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>SIMV PCV</translation>
    </message>
	<message id = "BtnMode_PSVPro">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>PSVPro</translation>
    </message>
	<message id = "VentStatus_On">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>Ventilator On</translation>
    </message>
	<message id = "VentStatus_Off">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>Ventilator Off</translation>
    </message>
	<message id = "Btn_Label">
        <location filename="Source/VentSettingArea/Qml/VentModeBtn.qml"/>
        <translation>Mode</translation>
    </message>
	<name>VentMenu</name>
    <message id = "VentMode_Label">
        <location filename="Source/VentSettingArea/Qml/VentMenu.qml"/>
        <translation>Ventilation Mode</translation>
    </message>
	<name>VentMoreSettingBtn</name>
    <message id = "More_Label">
        <location filename="Source/VentSettingArea/Qml/VentMoreSettingBtn.qml"/>
        <translation>More</translation>
    </message>
	<name>MoreSettingsParam</name>
    <message id = "Pmax_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Pmax</translation>
    </message>
	<message id = "Tpause_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Tpause</translation>
    </message>
	<message id = "Pmax_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>cmH2O</translation>
    </message>
	<message id = "Tpause_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>%</translation>
    </message>
	<message id = "FlowTrigger_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Flow Trigger</translation>
    </message>
		<message id = "FlowTrigger_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>l/min</translation>
    </message>
		<message id = "Tinsp_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Tinsp</translation>
    </message>
		<message id = "Tinsp_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>S</translation>
    </message>
		<message id = "Trig_Window_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Trig Window</translation>
    </message>
		<message id = "Trig_Window_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>%</translation>
    </message>
		<message id = "EndofBreath_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>End of Breath</translation>
    </message>
    <message id = "EndofBreath_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>%</translation>
    </message>
	 <message id = "ExitBackup_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Exit Backup</translation>
    </message>
	 <message id = "ExitBackup_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Breaths</translation>
    </message>
	 <message id = "BackupTime_sKey">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>Backup Time</translation>
    </message>
	 <message id = "BackupTime_sUnit">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsParam.qml"/>
        <translation>s</translation>
    </message>
	<name>MoreSettingsMenu</name>
    <message id = "MoreSetting_Label">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsMenu.qml"/>
        <translation>More Settings:</translation>
    </message>
	<message id = "VCV_mode">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsMenu.qml"/>
        <translation>VCV</translation>
    </message>
	<message id = "SIMV VCV_mode">
        <location filename="Source/VentSettingArea/Qml/MoreSettingsMenu.qml"/>
        <translation>SIMV VCV</translation>
    </message>
    <name>VentBtn</name>
    <message id = "TV_qKey">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>TV</translation>
    </message>
	<message id = "RR_qKey">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>RR</translation>
    </message>
	<message id = "RRMech_qKey">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>RR</translation>
    </message>
	<message id = "IE_qKey">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>I:E</translation>
    </message>
	<message id = "PEEP_qKey">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>PEEP</translation>
    </message>
	<message id = "Pinsp_qKey">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>Pinsp</translation>
    </message>
	<message id = "Psupport_qKey">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>Psupport</translation>
    </message>
	<message id = "ml_unit">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>ml</translation>
    </message>
	<message id = "min_unit">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>/min</translation>
    </message>
	<message id = "NA_unit">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation> </translation>
    </message>
	<message id = "cmH2O_unit">
        <location filename="Source/VentSettingArea/Qml/VentBtn.qml"/>
        <translation>cmH2O</translation>
    </message>
	<name>VentMode</name>
    <message id = "BtnMode_VCV">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>VCV</translation>
    </message>
	<message id = "BtnMode_PCV">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>PCV</translation>
    </message>
	<message id = "BtnMode_SIMVVCV">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>SIMV VCV</translation>
    </message>
	<message id = "BtnMode_SIMVPCV">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>SIMV PCV</translation>
    </message>
	<message id = "BtnMode_PSVPro">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>PSVPro</translation>
    </message>
	<message id = "BtnMode_Bypass">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>         Start
Cardiac Bypass</translation>
    </message>
	<message id = "VCV_Exp">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>Volume Control</translation>
    </message>
	<message id = "PCV_Exp">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>Pressure Control</translation>
    </message>
	<message id = "SIMVVCV_Exp">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>Synchronized Volume Control</translation>
    </message>
	<message id = "SIMVPCV_Exp">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>Synchronized Pressure Control</translation>
    </message>
	<message id = "PSVPro_Exp">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation>Pressure Support with apnea backup</translation>
    </message>
	<message id = "Bypass_Exp">
        <location filename="Source/VentSettingArea/Qml/VentMode.qml"/>
        <translation> </translation>
    </message>
	<name>VentModeConfirmPopup</name>
    <message id = "cancel_qKey">
        <location filename="Source/VentSettingArea/Qml/VentModeConfirmPopup.qml"/>
        <translation>Cancel</translation>
    </message>
	<message id = "confirmMode_VCV">
        <location filename="Source/VentSettingArea/Qml/VentModeConfirmPopup.qml"/>
        <translation>Confirm setting to start VCV mode.</translation>
    </message>
	<message id = "confirmMode_PCV">
        <location filename="Source/VentSettingArea/Qml/VentModeConfirmPopup.qml"/>
        <translation>Confirm setting to start PCV mode.</translation>
    </message>
	<message id = "confirmMode_SIMVVCV">
        <location filename="Source/VentSettingArea/Qml/VentModeConfirmPopup.qml"/>
        <translation>Confirm setting to start SIMV VCV mode.</translation>
    </message>
	<message id = "confirmMode_SIMVPCV">
        <location filename="Source/VentSettingArea/Qml/VentModeConfirmPopup.qml"/>
        <translation>Confirm setting to start SIMV PCV mode.</translation>
    </message>
	<message id = "confirmMode_PSVPro">
        <location filename="Source/VentSettingArea/Qml/VentModeConfirmPopup.qml"/>
        <translation>Confirm setting to start PSVPro mode.</translation>
    </message>
	<name>SettingConfirmPopUp</name>
    <message id = "cancel_qKey">
        <location filename="Source/VentSettingArea/Qml/SettingConfirmPopUp.qml"/>
        <translation>Cancel</translation>
    </message>
	 <message id = "confirm_qKey">
        <location filename="Source/VentSettingArea/Qml/SettingConfirmPopUp.qml"/>
        <translation>Confirm</translation>
    </message>
	<name>AlarmSetupMenu</name>
    <message id = "Alarm_Setup_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>Alarm Setup</translation>
    </message>
	<message id = "LowLimit_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>Low Limit</translation>
    </message>
	<message id = "HighLimit_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>High Limit</translation>
    </message>
	<message id = "AlarmLog_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>Alarm Log</translation>
    </message>
	<message id = "AlarmPP_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>Ppeak</translation>
    </message>
	<message id = "AlarmPP_Unit">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>cmH2O</translation>
    </message>
	<message id = "AlarmMV_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>MV</translation>
    </message>
	<message id = "AlarmMV_Unit">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>l/min</translation>
    </message>
	<message id = "AlarmTVexp_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>TVexp</translation>
    </message>
	<message id = "AlarmTVexp_Unit">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>ml</translation>
    </message>
	<message id = "AlarmO2_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>O2</translation>
    </message>
	<message id = "AlarmO2_Unit">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>%</translation>
    </message>
	<message id = "Apnea_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>Apnea Delay</translation>
    </message>
	<message id = "Apnea_Unit">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>s</translation>
    </message>
	<message id = "Volume_Label">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>Alarm</translation>
    </message>
	<message id = "Volume_Unit">
        <location filename="Source/Generic/Qml/AlarmSetupMenu.qml"/>
        <translation>Volume</translation>
    </message>
	<name>AlarmMessage</name>
	 <message id = "Apnea_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Apnea</translation>
    </message>
	<message id = "Apnea120_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Apnea >120 seconds</translation>
    </message>
	<message id = "BckModActive_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Backup mode active</translation>
    </message>
	<message id = "ChkFloSensors_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Check flow sensors</translation>
    </message>
	<message id = "CnctO2Cell_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Connect O2 cell</translation>
    </message>
	<message id = "MVHigh_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>MVexp high</translation>
    </message>
	<message id = "MVLow_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>MVexp low</translation>
    </message>
	<message id = "NegAirPressure_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Negative airway pressure</translation>
    </message>
	<message id = "NoBreatCircuit_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>No breathing circuit</translation>
    </message>
	<message id = "NoCO2Absorp_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>No CO2 absorption</translation>
    </message>
	<message id = "NoDriveGas_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>No drive gas</translation>
    </message>
	<message id = "NoExpFloSensor_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>No exp flow sensor</translation>
    </message>
	<message id = "NoInspFloSensor_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>No insp flow sensor</translation>
    </message>
	<message id = "NoO2Pres_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>No O2 pressure</translation>
    </message>
	<message id = "ChkPower_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>On battery, power ok?</translation>
    </message>
	<message id = "O2Flush_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>O2 flush stuck on?</translation>
    </message>
	<message id = "O2High_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>O2% high</translation>
    </message>
	<message id = "O2Low_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>O2% low</translation>
    </message>
	<message id = "PatCirLeak_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Patient circuit leak</translation>
    </message>
	<message id = "PEEPHigh_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>PEEP high</translation>
    </message>
	<message id = "Checkout_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Please do checkout</translation>
    </message>
	<message id = "PpeakHigh_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Ppeak high</translation>
    </message>
	<message id = "PpeakLow_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Ppeak low</translation>
    </message>
	<message id = "ReplaceExpSensor_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Replace exp flow sensor</translation>
    </message>
	<message id = "ReplaceInspSensor_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Replace insp flow sensor</translation>
    </message>
	<message id = "RevExpFlow_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Reverse exp flow</translation>
    </message>
	<message id = "RevInspFlow_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Reverse insp flow</translation>
    </message>
	<message id = "ServiceCalib_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Service calibration advised</translation>
    </message>
	<message id = "SysLeak_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>System leak?</translation>
    </message>
	<message id = "SysShut30_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>System shutdown in &lt; 30 min</translation>
    </message>
	<message id = "SysShut1_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>System shutdown in &lt; 1 min</translation>
    </message>
	<message id = "TVExpHigh_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>TVexp high</translation>
    </message>
	<message id = "TVExpLow_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>TVexp low</translation>
    </message>
	<message id = "TVNotAch_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>TV not achieved</translation>
    </message>
	<message id = "VolSensDisagree_aMsg">
        <location filename="Source/AlarmArea/Qml/AlarmMessage.qml"/>
        <translation>Volume sensors disagree</translation>
    </message>
	<name>AlarmLogMenu</name>
	<message id = "Alarm_Log_Label">
        <location filename="Source/Generic/Qml/AlarmLogMenu.qml"/>
        <translation>Alarm Log</translation>
    </message>
	<message id = "Time_Label">
        <location filename="Source/Generic/Qml/AlarmLogMenu.qml"/>
        <translation>Time</translation>
    </message>
	<message id = "Alarm_Label">
        <location filename="Source/Generic/Qml/AlarmLogMenu.qml"/>
        <translation>Alarm</translation>
    </message>
	<message id = "Priority_Label">
        <location filename="Source/Generic/Qml/AlarmLogMenu.qml"/>
        <translation>Priority</translation>
    </message>
	<name>alarmreader</name>
	<message id = "Alarm_Priority_High">
        <location filename="Source/AlarmArea/Cpp/alarmreader.cpp"/>
        <translation>High</translation>
    </message>
	<message id = "Alarm_Priority_Medium">
        <location filename="Source/AlarmArea/Cpp/alarmreader.cpp"/>
        <translation>Medium</translation>
    </message>	
	<name>PriorToTest</name>
    <message id = "Prior_test_title">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>Checkout Full test: Prior to test checks</translation>
    </message>
	 <message id = "Prior_test_subtitle">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>Perform and confirm the checks.</translation>
    </message>
	 <message id = "Prior_test_inst1">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>1. Install the patient circuit and the manual bag.</translation>
    </message>
 <message id = "Prior_test_inst2">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>2. Check the absorber and the absorbent.</translation>
    </message>
	 <message id = "Prior_test_inst3">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>3. Check the gas supplies and connections.</translation>
    </message>
	 <message id = "Prior_test_inst4">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>4. Check the scavenging flow.</translation>
    </message>
	 <message id = "Prior_test_inst5">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>5. Check that vaporizers are off and filled with agent.</translation>
    </message>
	 <message id = "Prior_test_inst6">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>6. Check with Air flow that the O2 shows 21 %.</translation>
    </message>
	 <message id = "Btn_Confirm">
        <location filename="Source/Checkout/Qml/PriorToTest.qml"/>
        <translation>Confirm</translation>
	</message>
	<name>ByPassArea</name>
	 <message id = "ByPass_Title">
        <location filename="Source/Checkout/Qml/ByPassCheckout.qml"/>
        <translation>Bypass checkout</translation>
	</message>
	 <message id = "ByPass_WarningOne">
        <location filename="Source/Checkout/Qml/ByPassCheckout.qml"/>
        <translation>Checkout has not passed.</translation>
	</message>
	<message id = "ByPass_WarningTwo">
        <location filename="Source/Checkout/Qml/ByPassCheckout.qml"/>
        <translation>Checkout bypass will be recorded to the system log.</translation>
	</message>
	<name>OtherEquipTest</name>
	 <message id = "Btn_Confirm">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>Confirm</translation>
	</message>	
		<message id = "EqpChkList_Title">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>Checkout Full test: Other equipment checklist</translation>
    </message>
	<message id = "EqpChkList_SubTitle">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>Complete and confirm the checks.</translation>
    </message>
	<message id = "EqpChkList_inst1">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>1. Check the pressure of the backup O2 supply.</translation>
    </message>
	<message id = "EqpChkList_inst2">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>2. Check that backup ventilation is operational e.g. AMBU bag.</translation>
    </message>
	<message id = "EqpChkList_inst3">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>3. Check that patient suction is set up correctly.</translation>
    </message>
	<message id = "EqpChkList_inst4">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>4. Check external patient monitoring equipment.</translation>
    </message>
	<message id = "Btn_Skip">
        <location filename="Source/Checkout/Qml/OtherEquipTest.qml"/>
        <translation>Skip</translation>
	</message>
	<name>CheckoutResults</name>
    <message id = "CheckOut_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Checkout Full Test: Results</translation>
    </message>
	<message id = "VentLeak_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Ventilator Leak: OK</translation>
    </message>
	<message id = "Compilance_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Compliance:</translation>
    </message>
	<message id = "LeftVepo_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Left Vaporizer Leak: OK</translation>
    </message>	
	<message id = "RightVepo_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Right Vaporizer Leak: OK</translation>
    </message>
	<message id = "GasControls_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Gas Controls</translation>
    </message>
	<message id = "CircuitLeak_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Circuit Leak: OK</translation>
    </message>
	<message id = "EquipCheck_testRes_title">
        <location filename="Source/Checkout/Qml/CheckoutResults.qml"/>
        <translation>Equipment Checklists</translation>
    </message>	
	<message id = "Full_Test">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Full Test</translation>
    </message>	
	<message id = "Circuit_Leak_Test">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Circuit Leak Test</translation>
    </message>	
	<message id = "Calibrations">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Calibrations</translation>
    </message>	
	<message id = "Pass">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Pass</translation>
    </message>	
	<message id = "Done">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Done</translation>
    </message>
    <message id = "h_ago">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>h ago</translation>
    </message>	
	<message id = "days_ago">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>days ago</translation>
    </message>	
	<message id = "Battery">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Battery</translation>
    </message>	
	<message id = "Charging">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>charging</translation>
    </message>	
	<message id = "Log">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Log</translation>
    </message>
    <message id = "Wall_Gas_supplies">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Wall Gas supplies</translation>
    </message>	
	<message id = "O2">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>O2</translation>
    </message>	
	<message id = "Air">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Air</translation>
    </message>	
	<message id = "N2O">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>N2O</translation>
    </message>	
	<message id = "psi">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>psi</translation>
    </message>
    <message id = "Lock_Screen">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Lock Screen</translation>
    </message>	
	<message id = "System">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>System</translation>
    </message>	
	<message id = "Case_Defaults">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Case Defaults</translation>
    </message>	
	<message id = "Default">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Default</translation>
    </message>	
	<message id = "Custom">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Custom</translation>
    </message>
	<message id = "Defaults_Setup">
        <location filename="Source/Standby/Qml/StandbyComponent.qml"/>
        <translation>Defaults Setup</translation>
    </message>	
</context>
</TS>
	